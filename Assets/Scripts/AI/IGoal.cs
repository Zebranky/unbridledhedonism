﻿namespace Assets.Scripts.AI
{
    interface IGoal
    {
        GoalReturn ExecuteStep();

        string ReportGoal();
    }
}
