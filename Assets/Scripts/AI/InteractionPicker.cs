﻿using System;
using System.Collections.Generic;
using System.Linq;
using static HelperFunctions;

static class InteractionPicker
{
    internal static InteractionType PreyForceVoreOtherPrey(Person self, Person prey)
    {
        WeightedList<InteractionType> list = new WeightedList<InteractionType>();

        CheckAdd(
            InteractionType.PreyEatOtherPrey,
            (int)(self.AI.Desires.InterestInOralVore(prey) * 1000)
        );
        CheckAdd(
            InteractionType.PreyUnbirthOtherPrey,
            (int)(self.AI.Desires.InterestInUnbirth(prey) * 1000)
        );
        CheckAdd(
            InteractionType.PreyCockVoreOtherPrey,
            (int)(self.AI.Desires.InterestInCockVore(prey) * 1000)
        );
        CheckAdd(
            InteractionType.PreyAnalVoreOtherPrey,
            (int)(self.AI.Desires.InterestInAnalVore(prey) * 1000)
        );

        return list.GetResult();

        void CheckAdd(InteractionType type, int weight)
        {
            if (InteractionList.List[type].AppearConditional(self, prey))
            {
                list.Add(type, weight);
            }
        }
    }

    internal static InteractionType PreyAskVoreOtherPrey(Person self, Person prey)
    {
        WeightedList<InteractionType> list = new WeightedList<InteractionType>();

        if (
            Rand.NextFloat(0, self.AI.Desires.DesireToDigestTarget(prey))
            > Rand.NextFloat(0, self.AI.Desires.DesireToEndoTarget(prey))
        )
        {
            CheckAdd(
                InteractionType.PreyAskToOralVoreDigest,
                (int)(self.AI.Desires.InterestInOralVore(prey) * 1000)
            );
            CheckAdd(
                InteractionType.PreyAskToUnbirthDigest,
                (int)(self.AI.Desires.InterestInUnbirth(prey) * 1000)
            );
            CheckAdd(
                InteractionType.PreyAskToCockVoreDigest,
                (int)(self.AI.Desires.InterestInCockVore(prey) * 1000)
            );
            CheckAdd(
                InteractionType.PreyAskToAnalVoreDigest,
                (int)(self.AI.Desires.InterestInAnalVore(prey) * 1000)
            );
        }
        else
        {
            CheckAdd(
                InteractionType.PreyAskToOralVore,
                (int)(self.AI.Desires.InterestInOralVore(prey) * 1000)
            );
            CheckAdd(
                InteractionType.PreyAskToUnbirth,
                (int)(self.AI.Desires.InterestInUnbirth(prey) * 1000)
            );
            CheckAdd(
                InteractionType.PreyAskToCockVore,
                (int)(self.AI.Desires.InterestInCockVore(prey) * 1000)
            );
            CheckAdd(
                InteractionType.PreyAskToAnalVore,
                (int)(self.AI.Desires.InterestInAnalVore(prey) * 1000)
            );
        }

        return list.GetResult();

        void CheckAdd(InteractionType type, int weight)
        {
            if (InteractionList.List[type].AppearConditional(self, prey))
            {
                list.Add(type, weight);
            }
        }
    }

    internal static SexInteractionType SexForceEat(Person self, Person prey)
    {
        WeightedList<SexInteractionType> list = new WeightedList<SexInteractionType>();

        CheckAdd(
            SexInteractionType.KissVore,
            (int)(self.AI.Desires.InterestInOralVore(prey) * 1000)
        );
        CheckAdd(
            SexInteractionType.SexUnbirth,
            (int)(self.AI.Desires.InterestInUnbirth(prey) * 1000)
        );
        CheckAdd(
            SexInteractionType.SexCockVore,
            (int)(self.AI.Desires.InterestInCockVore(prey) * 1000)
        );
        CheckAdd(
            SexInteractionType.SexAnalVore,
            (int)(self.AI.Desires.InterestInAnalVore(prey) * 1000)
        );

        return list.GetResult();

        void CheckAdd(SexInteractionType type, int weight)
        {
            if (SexInteractionList.List[type].AppearConditional(self, prey))
            {
                list.Add(type, weight);
            }
        }
    }

    internal static SexInteractionType SexAskEatDigest(Person self, Person prey)
    {
        WeightedList<SexInteractionType> list = new WeightedList<SexInteractionType>();

        CheckAdd(
            SexInteractionType.SexAskToOralVoreDigest,
            (int)(self.AI.Desires.InterestInOralVore(prey) * 1000)
        );
        CheckAdd(
            SexInteractionType.SexAskToUnbirthDigest,
            (int)(self.AI.Desires.InterestInUnbirth(prey) * 1000)
        );
        CheckAdd(
            SexInteractionType.SexAskToCockVoreDigest,
            (int)(self.AI.Desires.InterestInCockVore(prey) * 1000)
        );
        CheckAdd(
            SexInteractionType.SexAskToAnalVoreDigest,
            (int)(self.AI.Desires.InterestInAnalVore(prey) * 1000)
        );

        return list.GetResult();

        void CheckAdd(SexInteractionType type, int weight)
        {
            if (SexInteractionList.List[type].AppearConditional(self, prey))
            {
                list.Add(type, weight);
            }
        }
    }

    internal static SexInteractionType SexAskEatEndo(Person self, Person prey)
    {
        WeightedList<SexInteractionType> list = new WeightedList<SexInteractionType>();

        CheckAdd(
            SexInteractionType.SexAskToOralVore,
            (int)(self.AI.Desires.InterestInOralVore(prey) * 1000)
        );
        CheckAdd(
            SexInteractionType.SexAskToUnbirth,
            (int)(self.AI.Desires.InterestInUnbirth(prey) * 1000)
        );
        CheckAdd(
            SexInteractionType.SexAskToCockVore,
            (int)(self.AI.Desires.InterestInCockVore(prey) * 1000)
        );
        CheckAdd(
            SexInteractionType.SexAskToAnalVore,
            (int)(self.AI.Desires.InterestInAnalVore(prey) * 1000)
        );

        return list.GetResult();

        void CheckAdd(SexInteractionType type, int weight)
        {
            if (SexInteractionList.List[type].AppearConditional(self, prey))
            {
                list.Add(type, weight);
            }
        }
    }

    /// <summary>
    /// Friendly, unfriendly and romantic all wrapped up together
    /// </summary>
    internal static InteractionType GeneralInteraction(Person self, Person target)
    {
        WeightedList<InteractionType> list = new WeightedList<InteractionType>();
        var rel = self.GetRelationshipWith(target);
        int meanWeight = (int)(self.AI.Desires.InterestInUnfriendly(target) * 5000);
        list.Add(InteractionType.Push, meanWeight);
        list.Add(InteractionType.Insult, meanWeight * 2);
        var living = target.VoreController.GetLiving(VoreLocation.Stomach);
        bool CanOralEndo = self.VoreController.CouldVoreTarget(
            target,
            VoreType.Oral,
            DigestionAlias.CanEndo
        );
        bool CanAnalEndo = self.VoreController.CouldVoreTarget(
            target,
            VoreType.Anal,
            DigestionAlias.CanEndo
        );
        bool CanUnbirthEndo = self.VoreController.CouldVoreTarget(
            target,
            VoreType.Unbirth,
            DigestionAlias.CanEndo
        );
        bool CanCockEndo = self.VoreController.CouldVoreTarget(
            target,
            VoreType.Cock,
            DigestionAlias.CanEndo
        );
        int canEndoCount =
            (CanOralEndo ? 1 : 0)
            + (CanAnalEndo ? 1 : 0)
            + (CanUnbirthEndo ? 1 : 0)
            + (CanCockEndo ? 1 : 0);
        float voreMult = self.AI.Desires.DesireToEndoTarget(target);
        bool CanOral = self.VoreController.CouldVoreTarget(
            target,
            VoreType.Oral,
            DigestionAlias.CanVore
        );
        bool CanAnal = self.VoreController.CouldVoreTarget(
            target,
            VoreType.Anal,
            DigestionAlias.CanVore
        );
        bool CanUnbirth = self.VoreController.CouldVoreTarget(
            target,
            VoreType.Unbirth,
            DigestionAlias.CanVore
        );
        bool CanCock = self.VoreController.CouldVoreTarget(
            target,
            VoreType.Cock,
            DigestionAlias.CanVore
        );
        int canVoreCount =
            (CanOral ? 1 : 0) + (CanAnal ? 1 : 0) + (CanUnbirth ? 1 : 0) + (CanCock ? 1 : 0);
        float voreDigestMult = self.AI.Desires.DesireToVoreAndDigestTarget(target);
        if (self.Needs.Hunger > 1.5f)
            voreDigestMult *= self.Needs.Hunger * 5;

        float hugMult = (self.HasTrait(Traits.LovesHugs) ? 2 : 1);
        float kissMult = (self.HasTrait(Traits.LovesKissing) ? 2 : 1);
        float kindnessMult = (self.Personality.Kindness / 2) + 0.75f;
        float domMult = (self.Personality.Dominance / 2) + 0.75f;
        float flirtMult = (self.HasTrait(Traits.Flirtatious) ? 2 : 1);
        float sadistMult = (self.HasTrait(Traits.Sadistic) ? 3 : 1);

        if (State.World.Settings.SecretiveVore)
        {
            if (InPrivateArea(self, target) == false)
            {
                voreMult = 0;
                voreDigestMult = 0;
            }
        }

        // if vendetta with target, add hostile vore with huge change to pick
        if (
            rel.Vendetta
            && self.HasTrait(Traits.Vengeful)
            && canVoreCount > 0
            && self.VoreController.TargetVoreImmune(target, DigestionAlias.CanVore) == false
        )
        {
            if (CanAnal && self.Personality.AnalVoreInterest > Rand.NextFloat(0, 1))
                list.Add(InteractionType.AnalVore, 200000);
            else if (CanUnbirth && self.Personality.UnbirthInterest > Rand.NextFloat(0, 1))
                list.Add(InteractionType.Unbirth, 200000);
            else if (CanCock && self.Personality.CockVoreInterest > Rand.NextFloat(0, 1))
                list.Add(InteractionType.CockVore, 200000);
            else
                list.Add(InteractionType.OralVore, 200000);
        }

        if (self.Romance.CanSafelyRomance(target))
        {
            if (
                rel.RomanticLevel > .6f
                && self.Romance.IsDating == false
                && target.Romance.IsDating == false
                && rel.LastAskedAboutDating > 40
            )
                list.Add(
                    InteractionType.AskOut,
                    (int)(
                        120000
                        * rel.RomanticLevel
                        * self.AI.Desires.InterestInRomanticRelations(target)
                    )
                );

            float romanticMult = self.AI.Desires.InterestInRomanceNow(target);

            if (rel.RomanticLevel < .2f)
                romanticMult /= 1.5f;
            if (rel.RomanticLevel < -0.05f)
                romanticMult /= 8;
            if (romanticMult > 1)
                romanticMult = 1;

            if (
                self.VoreController.CapableOfVore()
                && self.VoreController.TargetVoreImmune(target, DigestionAlias.CanEndo) == false
                && rel.RomanticLevel > .2f
                && self.Personality.VorePreference != VorePreference.Digestion
                && State.World.Settings.CheckDigestion(self, DigestionAlias.CanEndo)
            )
            {
                if (canEndoCount > 0)
                {
                    list.Add(InteractionType.AskToOralVore, (int)(200 * voreMult));
                }
            }

            if (living != null)
            {
                float rubWeight = self.Personality.Voraphilia + rel.RomanticLevel;
                if (self.HasTrait(Quirks.PredWorship))
                    rubWeight *= 1.5f;
                if (target.HasTrait(Quirks.Motherly))
                    rubWeight *= 1.2f;
                if (self.HasTrait(Traits.BellyFixation))
                    rubWeight *= 1.3f;
                if (living.Location != VoreLocation.Balls)
                    list.Add(InteractionType.RubBelly, (int)(1250 * rubWeight));
                else
                    list.Add(InteractionType.RubBalls, (int)(1250 * rubWeight));
            }

            if (self.VoreController.BellySize() > 0)
            {
                float askRubWeight = self.Personality.Voraphilia + rel.RomanticLevel;
                if (self.HasTrait(Traits.DemandsWorship))
                    askRubWeight *= 2f;
                if (self.HasTrait(Quirks.Motherly))
                    askRubWeight *= 1.3f;
                if (self.HasTrait(Traits.BellyFixation))
                    askRubWeight *= 1.2f;
                list.Add(InteractionType.AskForBellyRub, (int)(1250 * askRubWeight * domMult));
            }
            if (self.VoreController.BallsSize() > 0)
            {
                float askRubWeight = self.Personality.Voraphilia + rel.RomanticLevel;
                if (self.HasTrait(Traits.DemandsWorship))
                    askRubWeight *= 2f;
                list.Add(InteractionType.AskForBallsRub, (int)(1250 * askRubWeight * domMult));
            }

            list.Add(InteractionType.Flirt, (int)(10000 * romanticMult * flirtMult));

            list.Add(InteractionType.Hug, (int)(10000 * romanticMult * hugMult));
            list.Add(
                InteractionType.Taste,
                (int)(2500 * romanticMult * domMult * Math.Max(voreMult, voreDigestMult))
            );
            
            list.Add(InteractionType.Kiss, (int)(18000 * (romanticMult * kissMult - .25f)));
            list.Add(
                InteractionType.ComplimentAppearance,
                (int)(4000 * romanticMult * kindnessMult * flirtMult)
            );

            list.Add(
                InteractionType.MakeOut,
                (int)(4500 * (domMult * romanticMult * kissMult - .35f))
            );

            if (HelperFunctions.InPrivateRoom(self) && target.ClothingStatus != ClothingStatus.Nude)
            {
                list.Add(InteractionType.AskThemToStrip, (int)(5000 * romanticMult * domMult));
            }
        }

        if (State.World.Settings.EndoisOnlyRomantic == false)
        {
            if (
                self.VoreController.CapableOfVore()
                && self.VoreController.TargetVoreImmune(target, DigestionAlias.CanEndo) == false
                && rel.RomanticLevel > .2f
                && self.Personality.VorePreference != VorePreference.Digestion
                && State.World.Settings.CheckDigestion(self, DigestionAlias.CanEndo)
            )
            {
                if (canEndoCount > 0)
                {
                    list.Add(InteractionType.AskToOralVore, (int)(150 * voreMult)); //Shortcut to the other vore options
                }
            }
        }

        if (
            self.VoreController.CapableOfVore()
            && self.VoreController.TargetVoreImmune(target, DigestionAlias.CanVore) == false
            && self.Personality.VorePreference != VorePreference.Endosoma
            && State.World.Settings.CheckDigestion(self, DigestionAlias.CanVore)
        )
        {
            if (canVoreCount > 0)
                list.Add(InteractionType.AskToOralVoreDigest, (int)(80 * voreDigestMult)); //Shortcut to the other vore options
        }

        if (living != null)
        {
            if (living.Willing && self.HasRelationshipWith(living.Target))
            {
                list.Add(
                    InteractionType.TalkWithPrey,
                    (int)(
                        1000
                        * self.GetRelationshipWith(living.Target).FriendshipLevel
                        * (self.HasTrait(Traits.Talkative) ? 1.2f : 1)
                    )
                );
                list.Add(
                    InteractionType.PlayfulTeasePrey,
                    (int)(
                        1000
                        * self.GetRelationshipWith(living.Target).FriendshipLevel
                        * (self.HasTrait(Quirks.PredWorship) ? 1.2f : 1)
                    )
                );
            }

            if (living.Willing == false)
            {
                list.Add(InteractionType.TauntPrey, (int)(meanWeight * sadistMult));
            }

            if (
                living.Willing == false
                && living.IsSwallowing()
                && self.HasTrait(Quirks.PredWorship)
            )
            {
                list.Add(InteractionType.ShoveIn, meanWeight * 4);
            }

            float rubWeight = self.Personality.Voraphilia + rel.FriendshipLevel;
            if (self.HasTrait(Quirks.PredWorship))
                rubWeight *= 4;
            if (target.HasTrait(Quirks.Motherly))
                rubWeight *= 1.5f;
            if (living.Location != VoreLocation.Balls)
                list.Add(InteractionType.RubBelly, (int)(300 * rubWeight));
        }

        list.Add(InteractionType.Talk, 12000 * (self.HasTrait(Traits.Talkative) ? 1.2f : 1));

        if (self.Romance.IsDating == false)
            list.Add(InteractionType.AskIfSingle, 50);
        list.Add(InteractionType.Compliment, 4500 * kindnessMult * flirtMult);
        if (rel.FriendshipLevel > .25f)
            list.Add(InteractionType.FriendlyHug, 5000 * hugMult * kindnessMult);

        if (
            target.VoreController.Swallowing(VoreLocation.Any)
            && self.GetRelationshipWith(target).FriendshipLevel > .5f
        )
        {
            if (self.Personality.Voraphilia > .5f || self.HasTrait(Quirks.PredWorship))
                list.Add(InteractionType.ShoveIn, 25000);
            else
                list.Add(InteractionType.ShoveIn, 2000);
        }

        var result = list.GetResult();

        if (result == InteractionType.AskToOralVore)
        {
            WeightedList<InteractionType> newList = new WeightedList<InteractionType>();

            newList.Add(
                InteractionType.AskToOralVore,
                (int)(1000 * self.AI.Desires.InterestInOralVore(target) * (CanOralEndo ? 1 : 0))
            );
            newList.Add(
                InteractionType.AskToAnalVore,
                (int)(1000 * self.AI.Desires.InterestInAnalVore(target) * (CanAnalEndo ? 1 : 0))
            );
            newList.Add(
                InteractionType.AskToUnbirth,
                (int)(1000 * self.AI.Desires.InterestInUnbirth(target) * (CanUnbirthEndo ? 1 : 0))
            );
            newList.Add(
                InteractionType.AskToCockVore,
                (int)(1000 * self.AI.Desires.InterestInCockVore(target) * (CanCockEndo ? 1 : 0))
            );
            result = newList.GetResult();
        }
        if (result == InteractionType.AskToOralVoreDigest)
        {
            WeightedList<InteractionType> newList = new WeightedList<InteractionType>();

            newList.Add(
                InteractionType.AskToOralVoreDigest,
                (int)(1000 * self.AI.Desires.InterestInOralVore(target) * (CanOral ? 1 : 0))
            );
            newList.Add(
                InteractionType.AskToAnalVoreDigest,
                (int)(1000 * self.AI.Desires.InterestInAnalVore(target) * (CanAnal ? 1 : 0))
            );
            newList.Add(
                InteractionType.AskToUnbirthDigest,
                (int)(1000 * self.AI.Desires.InterestInUnbirth(target) * (CanUnbirth ? 1 : 0))
            );
            newList.Add(
                InteractionType.AskToCockVoreDigest,
                (int)(1000 * self.AI.Desires.InterestInCockVore(target) * (CanCock ? 1 : 0))
            );
            result = newList.GetResult();
        }
        if (result == InteractionType.OralVore)
        {
            WeightedList<InteractionType> newList = new WeightedList<InteractionType>();

            newList.Add(
                InteractionType.OralVore,
                (int)(1000 * self.AI.Desires.InterestInOralVore(target) * (CanOral ? 1 : 0))
            );
            newList.Add(
                InteractionType.AnalVore,
                (int)(1000 * self.AI.Desires.InterestInAnalVore(target) * (CanAnal ? 1 : 0))
            );
            newList.Add(
                InteractionType.Unbirth,
                (int)(1000 * self.AI.Desires.InterestInUnbirth(target) * (CanUnbirth ? 1 : 0))
            );
            newList.Add(
                InteractionType.CockVore,
                (int)(1000 * self.AI.Desires.InterestInCockVore(target) * (CanCock ? 1 : 0))
            );
            result = newList.GetResult();
        }
        return result;
    }

    internal static void InPredWillingActions(Person self, Person pred)
    {
        List<VoreProgress> sisterPrey;
        var progress = pred.VoreController.GetProgressOf(self);

        WeightedList<Action> list = new WeightedList<Action>();

        sisterPrey = self.FindMyPredator().VoreController.GetSisterPrey(self);

        if (
            self.VoreController.HasPrey(VoreLocation.Stomach)
            && Rand.Next(self.HasTrait(Traits.Gassy) ? 10 : 40) == 0
        )
            list.Add(new Action(() => SelfActionList.List[SelfActionType.Burp].OnDo(self)), 50000);

        if (sisterPrey.Any())
        {
            VoreProgress sisterProgress;

            var prey = sisterPrey[Rand.Next(sisterPrey.Count)].Target;

            var type = PreyForceVoreOtherPrey(self, prey);
            if (type != InteractionType.None)
                list.Add(
                    new Action(() => InteractionList.List[type].RunCheck(self, prey)),
                    self.AI.Desires.DesireToForciblyEatAndDigestTarget(prey) * 10
                );

            if (self.Personality.EndoDominator)
            {
                var type3 = PreyForceVoreOtherPrey(self, prey);
                if (type3 != InteractionType.None)
                    list.Add(
                        new Action(() => InteractionList.List[type3].RunCheck(self, prey)),
                        self.AI.Desires.DesireToForciblyEndoTarget(prey) * 10
                    );
            }

            var type2 = PreyAskVoreOtherPrey(self, prey);
            if (type2 != InteractionType.None)
                list.Add(
                    new Action(() => InteractionList.List[type2].RunCheck(self, prey)),
                    self.AI.Desires.DesireToVoreTarget(prey) * 20
                );

            if (
                progress.ExpectingEndosoma == false
                && Rand.Next(20) == 0
                && State.World.Settings.PreventAIAskToBeDigested == false
            )
            {
                foreach (var sister in sisterPrey)
                {
                    if (sister.ExpectingEndosoma)
                    {
                        list.Add(
                            new Action(
                                () =>
                                    InteractionList.List[
                                        InteractionType.PreyConvinceOtherPreyToBeDigested
                                    ].RunCheck(self, prey)
                            ),
                            100
                        );
                    }
                }
            }

            sisterProgress = sisterPrey.FirstOrDefault(s => s.Target.Romance.Dating == self);
            if (
                sisterProgress == null
                && (
                    self.Romance.IsDating == false
                    || self.Personality.CheatOnPartner != ThreePointScale.Never
                )
            )
                sisterProgress = sisterPrey
                    .Where(s => self.GetRelationshipWith(s.Target).RomanticLevel > .2f)
                    .OrderByDescending(s => self.GetRelationshipWith(s.Target).RomanticLevel)
                    .FirstOrDefault();
            if (sisterProgress != null)
            {
                list.Add(
                    new Action(
                        () =>
                            InteractionList.List[InteractionType.PreyFondleOtherPrey].RunCheck(
                                self,
                                sisterProgress.Target
                            )
                    ),
                    (int)(1000 * self.AI.Desires.InterestInRomanceNow(sisterProgress.Target))
                );
                list.Add(
                    new Action(
                        () =>
                            InteractionList.List[InteractionType.PreyKissOtherPrey].RunCheck(
                                self,
                                sisterProgress.Target
                            )
                    ),
                    (int)(1000 * self.AI.Desires.InterestInRomanceNow(sisterProgress.Target))
                );
            }
            else
                list.Add(
                    new Action(
                        () =>
                            InteractionList.List[InteractionType.PreyTalkToAnotherPrey].RunCheck(
                                self,
                                prey
                            )
                    ),
                    (int)(
                        400
                        * self.AI.Desires.InterestInFriendly(prey)
                        * (self.HasTrait(Traits.Talkative) ? 1.2f : 1)
                    )
                );
        }
        if (self.Needs.Horniness > .7f)
            list.Add(
                new Action(
                    () =>
                        InteractionList.List[InteractionType.PreyWillingMasturbate].RunCheck(
                            self,
                            pred
                        )
                ),
                (int)(4000 + 30000 * (self.Needs.Horniness - 0.7f))
            );

        if (self.BeingSwallowed == false)
            list.Add(
                new Action(
                    () => InteractionList.List[InteractionType.PreyWillingNap].RunCheck(self, pred)
                ),
                (int)(600 * self.Needs.Energy)
            );
        list.Add(
            new Action(
                () => InteractionList.List[InteractionType.PreyWillingYell].RunCheck(self, pred)
            ),
            15 + 30 * self.Personality.Extroversion + 15 * self.Personality.Voraphilia
        );
        if (
            self.AI.Desires.DesireToBeDigested(pred) > .6f
            && pred.VoreController.TargetIsBeingDigested(self) == false
            && State.World.Settings.CheckDigestion(pred, DigestionAlias.CanVore)
            && State.World.Settings.PreventAIAskToBeDigested == false
            && pred.VoreController.GetProgressOf(self).TurnsSinceLastAsk > 100
        )
        {
            if (
                progress.Location == VoreLocation.Stomach
                || progress.Location == VoreLocation.Bowels
            )
                list.Add(
                    new Action(
                        () =>
                            InteractionList.List[InteractionType.PreyAskToBeDigested].RunCheck(
                                self,
                                pred
                            )
                    ),
                    20 * self.AI.Desires.DesireToBeDigested(pred)
                );
            else
                list.Add(
                    new Action(
                        () =>
                            InteractionList.List[InteractionType.PreyAskToBeMelted].RunCheck(
                                self,
                                pred
                            )
                    ),
                    20 * self.AI.Desires.DesireToBeDigested(pred)
                );
        }

        if (
            pred.VoreController.TargetIsBeingDigested(self) == false
            && progress.Stage > 30
            && pred.VoreController.GetProgressOf(self).TurnsSinceLastAsk > 100
        )
            list.Add(
                new Action(
                    () =>
                        InteractionList.List[InteractionType.PreyWillingAskToBeLetOut].RunCheck(
                            self,
                            pred
                        )
                ),
                15 * (1 - self.AI.Desires.DesireToBeVored(pred))
            );

        if (progress.Location == VoreLocation.Stomach)
            list.Add(
                new Action(
                    () =>
                        InteractionList.List[InteractionType.PreyWillingBellyRub].RunCheck(
                            self,
                            pred
                        )
                ),
                30 * self.AI.Desires.InterestInOralVore(pred)
            );
        else if (progress.Location == VoreLocation.Womb)
            list.Add(
                new Action(
                    () =>
                        InteractionList.List[InteractionType.PreyWillingWombRub].RunCheck(
                            self,
                            pred
                        )
                ),
                30 * self.AI.Desires.InterestInUnbirth(pred)
            );
        else if (progress.Location == VoreLocation.Balls)
            list.Add(
                new Action(
                    () =>
                        InteractionList.List[InteractionType.PreyWillingBallsRub].RunCheck(
                            self,
                            pred
                        )
                ),
                30 * self.AI.Desires.InterestInCockVore(pred)
            );
        else //if (progress.Location == VoreLocation.Bowels)
            list.Add(
                new Action(
                    () =>
                        InteractionList.List[InteractionType.PreyWillingAnusRub].RunCheck(
                            self,
                            pred
                        )
                ),
                30 * self.AI.Desires.InterestInAnalVore(pred)
            );

        list.Add(
            new Action(
                () => InteractionList.List[InteractionType.PreyWillingSquirm].RunCheck(self, pred)
            ),
            250
        );

        list.GetResult().Invoke();
    }

    internal static void InPredUnwillingActions(Person self, Person pred)
    {
        if (self.Needs.Horniness > .9f)
        {
            InteractionList.List[InteractionType.PreyMasturbate].RunCheck(self, pred);
            return;
        }

        if (
            self.Health < Constants.HealthMax / 3
            && CanUseMagic(self)
            && self.Magic.Mana >= 1f
            && Rand.Next(3) == 0
        )
        {
            SelfActionList.List[SelfActionType.CastHealSelf].OnDo(self);
            return;
        }

        WeightedList<Action> list = new WeightedList<Action>();
        List<VoreProgress> sisterPrey;
        var progress = pred.VoreController.GetProgressOf(self);

        if (
            self.VoreController.HasPrey(VoreLocation.Stomach)
            && Rand.Next(self.HasTrait(Traits.Gassy) ? 10 : 40) == 0
        )
            list.Add(new Action(() => SelfActionList.List[SelfActionType.Burp].OnDo(self)), 5000);

        sisterPrey = self.FindMyPredator().VoreController.GetSisterPrey(self);
        if (sisterPrey.Any())
        {
            var prey = sisterPrey[Rand.Next(sisterPrey.Count)].Target;

            if (
                self.GetRelationshipWith(prey).FriendshipLevel >= 1 - self.Personality.Kindness
                && prey.Health < Constants.HealthMax / 3
                && CanUseMagic(self)
                && self.Magic.Mana >= 1f
            )
                list.Add(
                    new Action(
                        () => InteractionList.List[InteractionType.CastHeal].RunCheck(self, prey)
                    ),
                    self.GetRelationshipWith(prey).FriendshipLevel * 30
                );

            var type = PreyForceVoreOtherPrey(self, prey);
            if (type != InteractionType.None)
                list.Add(
                    new Action(() => InteractionList.List[type].RunCheck(self, prey)),
                    self.AI.Desires.DesireToForciblyEatAndDigestTarget(prey) * 15
                );

            if (self.Personality.EndoDominator)
            {
                var type3 = PreyForceVoreOtherPrey(self, prey);
                if (type3 != InteractionType.None)
                    list.Add(
                        new Action(() => InteractionList.List[type3].RunCheck(self, prey)),
                        self.AI.Desires.DesireToForciblyEndoTarget(prey) * 15
                    );
            }

            var type2 = PreyAskVoreOtherPrey(self, prey);
            if (type2 != InteractionType.None)
                list.Add(
                    new Action(() => InteractionList.List[type2].RunCheck(self, prey)),
                    self.AI.Desires.DesireToVoreTarget(prey) * 10
                );
        }

        if (
            progress.Stage > 4
            && (pred == State.World.ControlledPerson == false || progress.TimesBegged == 0)
            && InteractionList.List[InteractionType.PreyConvinceToSpare].AppearConditional(
                self,
                pred
            )
        )
        {
            list.Add(
                new Action(
                    () =>
                        InteractionList.List[InteractionType.PreyConvinceToSpare].RunCheck(
                            self,
                            pred
                        )
                ),
                220 / (1 + 3 * progress.TimesBegged * progress.TimesBegged)
            );
        }

        if (self.HasTrait(Traits.PersistentBeggar))
        {
            list.Add(
                new Action(
                    () => InteractionList.List[InteractionType.PreyBeg].RunCheck(self, pred)
                ),
                70
            );
        }

        if (pred == State.World.ControlledPerson == false || progress.TimesBegged == 0)
        {
            list.Add(
                new Action(
                    () => InteractionList.List[InteractionType.PreyBeg].RunCheck(self, pred)
                ),
                220 / (1 + 3 * progress.TimesBegged * progress.TimesBegged)
            );
        }

        list.Add(
            new Action(() => InteractionList.List[InteractionType.PreyWait].RunCheck(self, pred)),
            50
        );
        list.Add(
            new Action(
                () => InteractionList.List[InteractionType.PreyRecover].RunCheck(self, pred)
            ),
            70
        );

        list.Add(
            new Action(() => InteractionList.List[InteractionType.PreyScream].RunCheck(self, pred)),
            10 + 30 * self.Personality.Extroversion
        );
        list.Add(
            new Action(
                () => InteractionList.List[InteractionType.PreyStruggle].RunCheck(self, pred)
            ),
            (int)(300 * (.9f - self.Needs.Energy))
        );
        list.Add(
            new Action(
                () => InteractionList.List[InteractionType.PreyViolentStruggle].RunCheck(self, pred)
            ),
            (int)(200 * (.9f - self.Needs.Energy))
        );

        list.GetResult().Invoke();
    }

    internal static InteractionType AskToBeVored(Person self, Person target)
    {
        WeightedList<InteractionType> list = new WeightedList<InteractionType>();
        if (target.VoreController.CouldVoreTarget(self, VoreType.Oral, DigestionAlias.CanVore))
            list.Add(
                InteractionType.AskToBeOralVored,
                (int)(
                    1000
                    * self.AI.Desires.DesireToBeVored(target)
                    * self.AI.Desires.InterestInOralVore(target)
                )
            );
        if (target.VoreController.CouldVoreTarget(self, VoreType.Anal, DigestionAlias.CanVore))
            list.Add(
                InteractionType.AskToBeAnalVored,
                (int)(
                    1000
                    * self.AI.Desires.DesireToBeVored(target)
                    * self.AI.Desires.InterestInAnalVore(target)
                )
            );
        if (target.VoreController.CouldVoreTarget(self, VoreType.Unbirth, DigestionAlias.CanVore))
            list.Add(
                InteractionType.AskToBeUnbirthed,
                (int)(
                    1000
                    * self.AI.Desires.DesireToBeVored(target)
                    * self.AI.Desires.InterestInUnbirth(target)
                )
            );
        if (target.VoreController.CouldVoreTarget(self, VoreType.Cock, DigestionAlias.CanVore))
            list.Add(
                InteractionType.AskToBeCockVored,
                (int)(
                    1000
                    * self.AI.Desires.DesireToBeVored(target)
                    * self.AI.Desires.InterestInCockVore(target)
                )
            );
        if (target.VoreController.CouldVoreTarget(self, VoreType.Oral, DigestionAlias.CanEndo))
            list.Add(
                InteractionType.AskToBeOralVoredEndo,
                (int)(
                    1000
                    * self.AI.Desires.DesireToBeVoredEndo(target)
                    * self.AI.Desires.InterestInOralVore(target)
                )
            );
        if (target.VoreController.CouldVoreTarget(self, VoreType.Anal, DigestionAlias.CanEndo))
            list.Add(
                InteractionType.AskToBeAnalVoredEndo,
                (int)(
                    1000
                    * self.AI.Desires.DesireToBeVoredEndo(target)
                    * self.AI.Desires.InterestInAnalVore(target)
                )
            );
        if (target.VoreController.CouldVoreTarget(self, VoreType.Unbirth, DigestionAlias.CanEndo))
            list.Add(
                InteractionType.AskToBeUnbirthedEndo,
                (int)(
                    1000
                    * self.AI.Desires.DesireToBeVoredEndo(target)
                    * self.AI.Desires.InterestInUnbirth(target)
                )
            );
        if (target.VoreController.CouldVoreTarget(self, VoreType.Cock, DigestionAlias.CanEndo))
            list.Add(
                InteractionType.AskToBeCockVoredEndo,
                (int)(
                    1000
                    * self.AI.Desires.DesireToBeVoredEndo(target)
                    * self.AI.Desires.InterestInCockVore(target)
                )
            );
        return list.GetResult();
    }
}
