﻿using OdinSerializer;
using System.Collections.Generic;
using System.Linq;
using static HelperFunctions;

namespace Assets.Scripts.AI
{
    class HuntForPrey : IGoal
    {
        [OdinSerialize]
        Person Target;

        [OdinSerialize]
        readonly Person Self;

        [OdinSerialize]
        Vec2 Destination;

        [OdinSerialize]
        int TurnsFollowed;

        [OdinSerialize]
        readonly Dictionary<Person, bool> Evaluated;

        [OdinSerialize]
        readonly bool Endo;

        [OdinSerialize]
        int TimesChanged = 0;

        public HuntForPrey(Person self, bool endo)
        {
            Self = self;
            Evaluated = new Dictionary<Person, bool>();
            Endo = endo;
            DebugManager.Log("HuntForPrey : " + (endo ? "Endo" : "Digest"));
        }

        public GoalReturn ExecuteStep()
        {
            if (Target != null)
            {
                if (Target.BeingEaten || Target.Dead)
                    return GoalReturn.AbortGoal;
                if (Self.VoreController.CapableOfVore() == false)
                    return GoalReturn.AbortGoal;
                if (TurnsFollowed > 7)
                    return GoalReturn.AbortGoal;
                Destination = Target.Position;
                if (Self.Position == Destination)
                {
                    DebugManager.Log("HuntForPrey Engage");
                    Self.AI.EngageVoreWithTarget(Target, Endo);
                    return GoalReturn.GoalAlreadyDone;
                }
                TurnsFollowed++;
                if (Self.AI.TryMove(Destination))
                    return GoalReturn.DidStep;

                // if trymove did not work, 50% to cast passdoor and try again
                // chance is 100% if char has room invader
                if (
                    CanUseMagic(Self)
                    && Self.Magic.Mana >= 1
                    && Self.Magic.Duration_Passdoor == 0
                    && Rand.Next(1) + (Self.HasTrait(Traits.RoomInvader) ? 1 : 0) >= 1
                )
                {
                    SelfActionList.List[SelfActionType.CastPassdoor].OnDo(Self);
                    return GoalReturn.DidStep;
                }
            }
            else
            {
                var closePeople = HelperFunctions.GetPeopleWithinXSquares(Self, 3);
                closePeople = closePeople.Where(s => Evaluated.ContainsKey(s) == false).ToList();
                if (CanUseMagic(Self) && Self.Magic.Mana >= 1 && Self.HasTrait(Traits.RoomInvader)) // search additionally for people in their own bedrooms with RoomInvader
                    closePeople = closePeople
                        .Where(s => State.World.Zones[s.Position.x, s.Position.y].Accepts(s))
                        .ToList();
                else // search only areas where Self can reach
                    closePeople = closePeople
                        .Where(s => State.World.Zones[s.Position.x, s.Position.y].Accepts(Self))
                        .ToList();

                if (closePeople.Any())
                {
                    foreach (Person target in closePeople)
                    {
                        float weight = 1;
                        // Adjust chance of choosing this target for vore attempt based on...

                        // PredHunter and PredRespect traits - raise/lower if target is a predator
                        if (target.VoreController.CapableOfVore())
                            {
                            float mod = 1 + (target.MiscStats.TimesSwallowedOther / 5);
                            if (mod > 2)
                                mod = 2;

                            if (Self.HasTrait(Traits.PredRespect))
                                weight /= mod;
                            if (Self.HasTrait(Traits.PredHunter))
                                weight *= mod;
                            }

                        if (target.VoreController.HasAnyPrey())
                        {
                            if (Self.HasTrait(Traits.Jealous) || Self.HasTrait(Quirks.BiggerFish) || Self.HasTrait(Traits.PredHunter))
                                weight *= 2f;
                        }

                        // Sadistic trait - raise chance based on how unwilling target is
                        if (Self.HasTrait(Traits.Sadistic))
                            weight *= (0.5f / target.Personality.PreyWillingness);

                        // Bully trait - raise chance if char has previously eaten target and are unfriendlly
                        if (Self.HasTrait(Traits.Bully) && Self.GetRelationshipWith(target).HasEaten && target.GetRelationshipWith(Self).FriendshipLevel < 0)
                            weight *= 3;

                        // Target is weakened or indesposed - having sex, exhausted, or low health
                        // Chance is further raised with Opportunist trait
                        if (
                            target.Health < Constants.HealthMax
                            || target.Needs.Energy >= 1
                            || target.StreamingSelfAction == SelfActionType.Rest
                            || target.StreamingSelfAction == SelfActionType.Masturbate
                            || target.ActiveSex != null
                        )
                        {
                            if (Self.HasTrait(Traits.Opportunist))
                                weight *= 3f;
                            else
                                weight *= 1.5f;
                        }

                        // LustDevourer trait - raise chance if char is uncontrollably horny, masturbating, or having sex
                        if (
                            Self.HasTrait(Traits.LustDevourer) &&
                            (target.Needs.Horniness > 0.8
                            || target.StreamingSelfAction == SelfActionType.Masturbate
                            || target.ActiveSex != null)
                        )
                            weight *= 10f;

                        // Widowmaker trait - raise chance if char is in a relationship with someone else
                        if (Self.HasTrait(Traits.Widowmaker) && target.Romance.Dating != null && target.Romance.Dating != Self)
                            weight *= 2f;

                        // Clothing status - raise chances if char is exposed
                        if (target.ClothingStatus == ClothingStatus.Nude)
                            weight *= 1.5f;
                        if (target.ClothingStatus == ClothingStatus.Underwear)
                            weight *= 1.2f;

                        // Vendetta - massively raise chance if char has a vendetta with target
                        if (Self.GetRelationshipWith(target).Vendetta)
                            weight *= 10;
                        
                        // Evaluate
                        if (Endo)
                        {
                            if (
                                Self.VoreController.TargetVoreImmune(target, DigestionAlias.CanEndo)
                                    == false
                                && Rand.NextFloat(0.10f, 1)
                                    < weight * Self.AI.Desires.DesireToForciblyEndoTarget(target)
                            )
                            {
                                Target = target;
                                return ExecuteStep();
                            }
                        }
                        else
                        {
                            if (
                                Self.HasTrait(Traits.DoesNotForce)
                                || State.World.Settings.ForcedPredWillingness
                                    < Rand.NextFloat(0, 0.2f)
                            )
                            {
                                if (
                                    Self.VoreController.TargetVoreImmune(
                                        target,
                                        DigestionAlias.CanVore
                                    ) == false
                                    && Rand.NextFloat(0.15f, .9f)
                                        < weight
                                            * Self.AI.Desires.DesireToVoreAndDigestTarget(target)
                                )
                                {
                                    Target = target;
                                    return ExecuteStep();
                                }
                            }
                            else
                            {
                                if (
                                    Self.VoreController.TargetVoreImmune(
                                        target,
                                        DigestionAlias.CanVore
                                    ) == false
                                    && Rand.NextFloat(0.10f, .9f)
                                        < weight
                                            * Self.AI.Desires.DesireToForciblyEatAndDigestTarget(
                                                target
                                            )
                                )
                                {
                                    Target = target;
                                    return ExecuteStep();
                                }
                            }
                        }

                        Evaluated.Add(target, true);
                    }
                }

                if (Self.Position == Destination)
                {
                    if (2 + Rand.Next(4) <= TimesChanged)
                        return GoalReturn.AbortGoal;
                    if (
                        Self.Needs.Hunger > .95f
                        && Rand.Next(3) == 0
                        && Self.HasTrait(Traits.PrefersLivingPrey) == false
                    )
                    {
                        return GoalReturn.AbortGoal;
                    }
                    TimesChanged++;
                    Destination = Self.AI.RandomSquare();
                }
                if (Self.AI.TryMove(Destination))
                    return GoalReturn.DidStep;
                int rand = Rand.Next(10);
                if (rand < 2)
                    Destination = Self.AI.RandomAccessibleSquareOfType(ObjectType.Shower);
                else if (rand < 5)
                    Destination = Self.AI.RandomAccessibleSquareOfType(ObjectType.Food);
                else
                    Destination = Self.AI.RandomSquare();
                if (Self.AI.TryMove(Destination))
                    return GoalReturn.DidStep;
            }
            return GoalReturn.AbortGoal;
        }

        public string ReportGoal()
        {
            if (Target == null)
                return "Looking for prey.";
            return $"Advancing on {Target.FirstName} with the intent to eat.";
        }
    }
}
