﻿using Assets.Scripts.AI;
using OdinSerializer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using static HelperFunctions;

class AI
{
    [OdinSerialize]
    readonly Person Self;

    [OdinSerialize]
    int WaitTurns;

    [OdinSerialize]
    internal int StripTurns;

    [OdinSerialize]
    readonly List<IGoal> Tasks;

    [OdinSerialize]
    internal Person LastInteractedWith;

    List<Vec2> Path;
    Vec2 LastDestination;

    public AI(Person person)
    {
        Self = person;
        Tasks = new List<IGoal>();
    }

    private Desires _desires;

    public Desires Desires
    {
        get
        {
            if (_desires == null)
                _desires = new Desires(Self);
            return _desires;
        }
    }

    internal void SetWait(int turns)
    {
        WaitTurns = turns;
    }

    internal void GetGoals(ref StringBuilder sb)
    {
        if (Tasks.Any())
            sb.AppendLine("Goal: " + Tasks[0].ReportGoal());
    }

    internal void DiscoveredVoreAttempt(Person screamer)
    {
        if (screamer == Self)
            return;
        Person predator = screamer.FindMyPredator();
        if (predator == null)
        {
            UnityEngine.Debug.Log("Discovered a vore attempt that didn't currently exist.");
            return;
        }
        if (Self == predator)
            return;

        if (screamer.Dead)
            return;
        bool refusedHelp = false;

        if (
            Self.GetRelationshipWith(predator)
                .KnowledgeAbout.SeenVore.Contains(predator.VoreController.GetProgressOf(screamer))
                == false
            && Self.VoreController.ContainsPerson(screamer, VoreLocation.Any) == false
        )
        {
            var rel = Self.GetRelationshipWith(screamer);
            if (
                Self.HasTrait(Traits.Vengeful)
                && Self.VoreController.CapableOfVore()
                && (rel.FriendshipLevel > .5f || rel.RomanticLevel > .5f)
            )
            {
                Self.GetRelationshipWith(predator).Vendetta = true;
            }
            if (
                Self.HasTrait(Traits.Jealous)
                && Self.VoreController.CapableOfVore()
                && Rand.Next(4) == 0
            )
            {
                Self.GetRelationshipWith(predator).Vendetta = true;
            }
            Self.GetRelationshipWith(predator)
                .KnowledgeAbout.SeenVore.Add(predator.VoreController.GetProgressOf(screamer));
            if (rel.FriendshipLevel < -.4f || Self.HasTrait(Quirks.PredWorship))
            {
                var predRel = Self.GetRelationshipWith(predator);
                predRel.FriendshipLevel = Utility.PushTowardOne(
                    Self.GetRelationshipWith(predator).FriendshipLevel,
                    .3f
                );
                refusedHelp = true;
                switch (predator.VoreController.GetProgressOf(screamer).OriginalType)
                {
                    case VoreType.Oral:
                        InteractionList.List[InteractionType.PleasedAtOralVore].OnSucceed(
                            Self,
                            screamer
                        );
                        break;
                    case VoreType.Anal:
                        InteractionList.List[InteractionType.PleasedAtAnalVore].OnSucceed(
                            Self,
                            screamer
                        );
                        break;
                    case VoreType.Unbirth:
                        InteractionList.List[InteractionType.PleasedAtUnbirth].OnSucceed(
                            Self,
                            screamer
                        );
                        break;
                    case VoreType.Cock:
                        InteractionList.List[InteractionType.PleasedAtCockVore].OnSucceed(
                            Self,
                            screamer
                        );
                        break;
                }
            }
            else if (rel.FriendshipLevel > .2f && Self.HasTrait(Traits.Uncaring) == false)
            {
                float decrease = .3f;
                decrease = Math.Max(decrease, .3f);
                decrease *= (1 + rel.FriendshipLevel) * (1 + rel.RomanticLevel);
                if (decrease > 1)
                    decrease = 1;
                if (Self.Romance.Dating == screamer)
                    decrease = 1;
                decrease *= State.World.Settings.VoreAnger;
                HelperFunctions.DecreaseFriendship(Self, predator, decrease);
                if (decrease > .05f)
                {
                    switch (predator.VoreController.GetProgressOf(screamer).OriginalType)
                    {
                        case VoreType.Oral:
                            InteractionList.List[InteractionType.OutragedAtOralVore].OnSucceed(
                                Self,
                                screamer
                            );
                            break;
                        case VoreType.Anal:
                            InteractionList.List[InteractionType.OutragedAtAnalVore].OnSucceed(
                                Self,
                                screamer
                            );
                            break;
                        case VoreType.Unbirth:
                            InteractionList.List[InteractionType.OutragedAtUnbirth].OnSucceed(
                                Self,
                                screamer
                            );
                            break;
                        case VoreType.Cock:
                            InteractionList.List[InteractionType.OutragedAtCockVore].OnSucceed(
                                Self,
                                screamer
                            );
                            break;
                    }
                }
            }
            else if (State.World.Settings.VoreAnger > .5f)
                HelperFunctions.DecreaseFriendship(
                    Self,
                    predator,
                    .25f * (-.5f + State.World.Settings.VoreAnger)
                );
        }

        if (Self.BeingEaten)
            return;

        if (Self == State.World.ControlledPerson)
            return;

        if (Self.VoreController.GetLiving(VoreLocation.Any) != null)
            return;

        if (refusedHelp)
            return;

        float friendValue =
            Self.GetRelationshipWith(screamer).FriendshipLevel
            + Self.GetRelationshipWith(screamer).RomanticLevel;
        if (friendValue > 0)
            friendValue *= 0.5f + Self.Personality.PredLoyalty;

        float helpValue =
            Self.Personality.Kindness
            + friendValue
            - (Self.GetRelationshipWith(predator).FriendshipLevel / 2)
            - 1
            + State.World.Settings.HelpPreyBias
            - (screamer.Personality.PreyWillingness / 4)
            - 0.5f
            + (2 / (3 + predator.VoreController.TotalDigestions))
            - .125f;
        if (predator.HasTrait(Traits.Intimidating))
            helpValue /= 3;
        if (Self.HasTrait(Quirks.Rescuer))
            helpValue *= 2;
        if (State.World.Settings.HelpPreyBias < 1)
            helpValue *= State.World.Settings.HelpPreyBias;
        if (helpValue < Rand.NextFloat(0, .5f))
            return;

        if (Self.HasTrait(Traits.Uncaring))
            return;

        var path = PathFinder.GetPath(Self.Position, predator.Position, Self);

        if (path != null && path.Count > 7) //Don't bother chasing down someone who's too far
            return;

        if (path != null || Self.Position == predator.Position)
        {
            Self.EndStreamingActions();
            Tasks.Clear();
            switch (predator.VoreController.GetProgressOf(screamer).OriginalType)
            {
                case VoreType.Oral:
                    Tasks.Add(new InteractWithPerson(GoalType.FreeOralVoredTarget, predator, Self));
                    break;
                case VoreType.Unbirth:
                    Tasks.Add(new InteractWithPerson(GoalType.FreeUnbirthedTarget, predator, Self));
                    break;
                case VoreType.Cock:
                    Tasks.Add(new InteractWithPerson(GoalType.FreeCockVoredTarget, predator, Self));
                    break;
                case VoreType.Anal:
                    Tasks.Add(new InteractWithPerson(GoalType.FreeAnalVoredTarget, predator, Self));
                    break;
            }
        }
    }

    internal void FollowPerson(Person target)
    {
        Tasks.Clear();
        if (target != null)
        {
            Tasks.Add(new FollowPerson(target, Self));
        }
    }

    internal bool IsFollowing(Person target) =>
        Tasks.Any() && Tasks[0] is FollowPerson follow && follow.GetTarget() == target;

    internal void SpitUpPreyEquivalent(VoreProgress progress)
    {
        InteractionType type = InteractionType.SpitUpPrey;
        //if (progress.Location == VoreLocation.Stomach) type = InteractionType.SpitUpPrey;
        if (progress.OriginalType == VoreType.Cock)
            type = InteractionType.ReleaseCockPrey;
        if (progress.OriginalType == VoreType.Unbirth)
            type = InteractionType.BirthPrey;
        if (progress.OriginalType == VoreType.Anal)
            type = InteractionType.ReleaseAnalPrey;
        InteractionList.List[type].RunCheck(Self, progress.Target);
    }

    internal void Execute(bool secondChance = false, bool digestionTickHappened = false)
    {
        if (Self.LastName == "----") //Observer shouldn't act
            return;
        if (Self.VoreController.HasPrey(VoreLocation.Any) && digestionTickHappened == false)
        {
            Self.VoreController.CheckIfShouldDigest();
            bool isSwallowing = false;
            foreach (var progress in Self.VoreController.GetAllProgress(VoreLocation.Any))
            {
                if (progress.IsSwallowing())
                    isSwallowing = true;
            }
            Self.VoreController.AdvanceStages();

            if (
                isSwallowing == false
                && Self.StreamingAction != InteractionType.None
                && InteractionList.List[Self.StreamingAction].Class == ClassType.VoreConsuming
            )
                Self.EndStreamingActions();
            if (isSwallowing)
            {
                return;
            }

            foreach (var progress in Self.VoreController.GetAllProgress(VoreLocation.Any))
            {
                if (Self.VoreController.GetProgressOf(progress.Target) == null) //Done to prevent in case the list was modified while iterating through it.
                    continue;
                if (
                    progress.Target.Dead == false
                    && (
                        Self.VoreController.TargetIsBeingDigested(progress.Target) == false
                        && Desires.DesiresToReleaseEndoPrey(progress.Target)
                    )
                )
                    SpitUpPreyEquivalent(progress);
                else if (
                    progress.Target.Dead == false
                    && Self.VoreController.TargetIsBeingDigested(progress.Target)
                    && Desires.DesireToDigestTarget(progress.Target) < .04f
                )
                {
                    if (
                        State.World.Settings.CheckDigestion(false, DigestionAlias.CanEndo)
                        && Self.Personality.VorePreference != VorePreference.Digestion
                    )
                        Self.VoreController.SetPartDigest(progress.Location, false);
                    else
                        SpitUpPreyEquivalent(progress);
                }
            }
        }
        else if (
            Self.StreamingAction != InteractionType.None
            && InteractionList.List[Self.StreamingAction].Class == ClassType.VoreConsuming
        )
            Self.EndStreamingActions();

        if (WaitTurns > 0)
        {
            WaitTurns--;
            return;
        }

        if (Self.BeingEaten)
        {
            BeingEatenTasks();
            return;
        }

        if (
            Self.VoreController.HasPrey(VoreLocation.Any)
            && State.World.Settings.CheckDigestion(Self, DigestionAlias.CanSwitch)
        )
        {
            foreach (var progress in Self.VoreController.GetAllProgress(VoreLocation.Any))
            {
                if (Self.VoreController.TargetIsBeingDigested(progress.Target) == false)
                {
                    if (
                        Self.VoreController
                            .GetSisterPrey(progress.Target, includeBeingSwallowed: true)
                            .Any() == false
                    )
                    {
                        var starveMod = 0f;
                        if (Self.Needs.Hunger > 1)
                            starveMod = (Self.Needs.Hunger - 1f) * 2;

                        if (
                            Desires.DesireToDigestTarget(progress.Target)
                            > (
                                (Self.HasTrait(Traits.Traitor) ? .4f : .8f)
                                + (progress.TimesBegged > 0 ? .5f : 0)
                                - starveMod
                            )
                        )
                        {
                            // begin digesting target if within threshold set above
                            if (State.World.Settings.SneakyDigestion == false)
                            {
                                // send interaction with popup message allowing player to stay willing or become unwilling
                                // only do this if SneakyDigestion is false
                                if (progress.Location == VoreLocation.Stomach
                                   || progress.Location == VoreLocation.Bowels)
                                    InteractionList.List[InteractionType.UnexpectedDigestion].RunCheck(Self, progress.Target);
                                else
                                    InteractionList.List[InteractionType.UnexpectedHeat].RunCheck(Self, progress.Target);
                             }

                            // sets part to digest regardless
                            Self.VoreController.SetPartDigest(progress.Location, true);
                        }
                        else if (
                            Desires.DesireToDigestTarget(progress.Target) > (.55f - starveMod)
                            && progress.TriedConverting == false
                            && State.World.Settings.PreventAIAskToDigest == false
                        )
                        {
                            if (
                                progress.Location == VoreLocation.Stomach
                                || progress.Location == VoreLocation.Bowels
                            )
                                InteractionList.List[InteractionType.AskIfCanDigest].RunCheck(
                                    Self,
                                    progress.Target
                                );
                            else
                                InteractionList.List[InteractionType.AskIfCanMelt].RunCheck(
                                    Self,
                                    progress.Target
                                );
                            return;
                        }
                    }
                }
            }
        }

        if (
            Self.StreamingAction != InteractionType.None
            && Self.StreamingAction != InteractionType.StartSex
        )
        {
            if (Rand.Next(Math.Max(100 / (10 + Self.StreamedTurns), 1)) != 0)
            {
                var action = InteractionList.List[Self.StreamingAction];
                if (
                    action.Range
                    < Self.Position.GetNumberOfMovesDistance(Self.StreamingTarget.Position)
                )
                {
                    Self.EndStreamingActions();
                }
                else
                {
                    action.OnSucceed(Self, Self.StreamingTarget, true);
                    return;
                }
            }
            else
            {
                Self.EndStreamingActions();
            }
        }
        if (Self.StreamingSelfAction != SelfActionType.None)
        {
            switch (Self.StreamingSelfAction)
            {
                case SelfActionType.None:
                    break;
                case SelfActionType.EatFood:
                    if (Self.Needs.Hunger > .1)
                    {
                        ContinueSelfStreamingAction();
                        return;
                    }
                    break;
                case SelfActionType.Rest:
                    if (Self.Needs.Energy > .1)
                    {
                        ContinueSelfStreamingAction();
                        return;

                    }
                    else if (Self.HasTrait(Traits.HeavySleeper) && Self.StreamedTurns < 30 + Rand.Next(10))
                    {
                        ContinueSelfStreamingAction();
                        return;
                    }
                    break;
                case SelfActionType.Shower:
                    if (Self.Needs.Cleanliness > .1)
                    {
                        ContinueSelfStreamingAction();
                        return;
                    }
                    Self.EndStreamingActions();
                    break;
                case SelfActionType.TreatWounds:
                    if (Self.Health < Constants.HealthMax)
                    {
                        ContinueSelfStreamingAction();
                        return;
                    }
                    break;
                case SelfActionType.Masturbate:
                    if (Self.TurnsSinceOrgasm > 2)
                    {
                        ContinueSelfStreamingAction();
                        return;
                    }
                    break;
                case SelfActionType.Exercise:
                    if (Self.Needs.Energy < .8f && Self.StreamedTurns < 12)
                    {
                        ContinueSelfStreamingAction();
                        return;
                    }
                    break;
                case SelfActionType.StudyArcane:
                case SelfActionType.Meditate:
                case SelfActionType.BrowseWeb:
                case SelfActionType.ResearchCommunication:
                    if (Self.StreamedTurns < 8 + Rand.Next(10))
                    {
                        ContinueSelfStreamingAction();
                        return;
                    }
                    break;
                case SelfActionType.ScatDisposalBathroom: //Do these until done
                case SelfActionType.ScatDisposalFloor:
                case SelfActionType.CockDisposalBathroom:
                case SelfActionType.CockDisposalFloor:
                case SelfActionType.UnbirthDisposalBathroom:
                case SelfActionType.UnbirthDisposalFloor:
                    ContinueSelfStreamingAction();
                    return;
                default:
                    UnityEngine.Debug.Log("No handler for AI streaming a self action");
                    break;
            }
        }

        FindNewTask();

        if (Tasks.Any())
        {
            var ret = Tasks[0].ExecuteStep();
            switch (ret)
            {
                case GoalReturn.DidStep:
                    return;
                case GoalReturn.CompletedGoal:
                    if (Tasks.Any())
                        Tasks.RemoveAt(0);
                    return;
                case GoalReturn.AbortGoal:
                    Tasks.Clear();
                    break;
                case GoalReturn.GoalAlreadyDone:
                    if (Tasks.Any())
                        Tasks.RemoveAt(0);
                    if (Tasks.Any())
                    {
                        Execute(secondChance, true);
                        return;
                    }
                    break;
            }
            if (secondChance == false)
                Execute(true, true);
            //else
            //    UnityEngine.Debug.Log("Failed to assign a backup task");
            return;
        }
        else
            UnityEngine.Debug.Log("This shouldn't have happened");
    }

    private void FindNewTask()
    {
        // HIGH PRIORITY TASKS
        if (Self.ActiveSex != null && (Tasks.Any() == false || Tasks[0] is HaveSex == false))
            Tasks.Insert(0, new HaveSex(Self));

        if (
            Tasks.Any() == false
            && Self.VoreController.HasPrey(VoreLocation.Stomach)
            && Rand.Next(Self.HasTrait(Traits.Gassy) ? 10 : 40) == 0
        )
            Tasks.Add(new DoSelfAction(Self, SelfActionType.Burp));

        if (
            Tasks.Any() == false
            && Self.VoreController.GetLiving(VoreLocation.Stomach) != null
            && State.World.Settings.AnalVoreGoesDirectlyToStomach == false
            && Self.HasTrait(Traits.FullTour)
            && Rand.Next(3) == 0
            && (
                Self.VoreController.HasPrey(VoreLocation.Bowels) == false
                || Self.VoreController.PartCurrentlyDigests(VoreLocation.Stomach)
                    == Self.VoreController.PartCurrentlyDigests(VoreLocation.Bowels)
            )
        )
        {
            Self.VoreController.SetPartDigest(
                VoreLocation.Bowels,
                Self.VoreController.PartCurrentlyDigests(VoreLocation.Stomach)
            );
            Tasks.Add(
                new InteractWithPerson(
                    GoalType.Social,
                    Self.VoreController.GetLiving(VoreLocation.Stomach).Target,
                    Self,
                    InteractionType.TransferPreyToBowels
                )
            );
        }

        // Heal self if very injured and not currently eaten (unwilling vore check is in InPredUnwillingActions)
        if (
            CanUseMagic(Self)
            && Self.Magic.Mana >= 1
            && Self.Health < Constants.HealthMax / 3
            && Self.BeingEaten == false
        )
        {
            Tasks.Add(new DoSelfAction(Self, SelfActionType.CastHealSelf));
        }

        if (
            Tasks.Any() == false
            && Self.HasTrait(Traits.BlackoutPred)
            && Self.VoreController.IsDigesting()
        )
        {
            if (Self.ZoneContainsBed())
                Tasks.Add(new DoSelfAction(Self, SelfActionType.Rest));
            else if (Self.Position != Self.MyRoom)
                Tasks.Add(new NavigateTo(Self, Self.MyRoom));
        }

        if (
            Tasks.Any() == false
            && Self.HasTrait(Traits.AbductorPred)
            && Self.VoreController.IsDigesting()
        )
        {
            if (Self.Position != Self.MyRoom)
                Tasks.Add(new NavigateTo(Self, Self.MyRoom));
            else if (Self.Needs.Horniness > .5f)
                Tasks.Add(new DoSelfAction(Self, SelfActionType.Masturbate));
            else if (Self.Needs.Energy > .2f)
                Tasks.Add(new DoSelfAction(Self, SelfActionType.Rest));
            else
                Tasks.Add(new Wait());
        }

        ClothingTaskCheck();

        if (
            Tasks.Any() == false
            && Self.VoreController.CapableOfVore()
            && Self.VoreController.FreeSpace(VoreLocation.Any) >= 1
            && Self.HasTrait(Traits.RomanticPred) == false
        )
        {
            if (
                (State.World.Settings.SecretiveVore || Self.HasTrait(Traits.ShyPred)) == false
                || Rand.Next(30) == 0
                || Self.Magic.Duration_Hunger > 0
            )
            {
                if (
                    Self.Personality.EndoDominator
                    && State.World.Settings.CheckDigestion(false, DigestionAlias.CanEndo)
                    && Self.Needs.Hunger < .8f
                    && Self.Personality.VorePreference != VorePreference.Digestion
                )
                {
                    if (
                        State.World.Settings.VoreHuntingBias
                            * 3
                            * (
                                Self.Personality.PredWillingness
                                + Self.Personality.Voraphilia
                                + Self.Personality.Voracity
                            )
                        > Rand.NextFloat(0, 60)
                    )
                    {
                        Tasks.Add(new HuntForPrey(Self, true));
                    }
                }
                if (
                    State.World.Settings.CheckDigestion(false, DigestionAlias.CanVore)
                    && Self.Needs.Hunger > .52f
                    && (
                        Self.VoreController.StomachDigestsPrey
                        || Self.VoreController.GetLiving(VoreLocation.Stomach) == null
                    )
                    && Self.Personality.VorePreference != VorePreference.Endosoma
                )
                {
                    if (
                        State.World.Settings.VoreHuntingBias
                            * Math.Min(
                                Self.Personality.PredWillingness
                                    * 2
                                    * Math.Max(0.5f, Self.Needs.Hunger),
                                5f
                            )
                        > Rand.NextFloat(0, 6)
                    )
                    {
                        Tasks.Add(new HuntForPrey(Self, false));
                    }
                }
                if (
                    Self.HasTrait(Traits.BingeEater)
                    && Self.VoreController.HasPrey(VoreLocation.Any)
                    && (
                        State.World.Settings.VoreHuntingBias
                            * 3
                            * (
                                Self.Personality.PredWillingness
                                + Self.Personality.Voraphilia
                                + Self.Personality.Voracity
                            )
                        > Rand.NextFloat(0, 30)
                    )
                )
                {
                    Tasks.Add(new HuntForPrey(Self, true));
                }
            }
        }

        // ELSE...
        // CREATE WEIGHTED LIST OF POSSIBLE TASKS
        if (Tasks.Any() == false || (Tasks.Count == 1 && Tasks[0] is NavigateTo))
        {
            var list = new WeightedList<Action>();

            // EAT AT CAFETERIA
            if (
                State.World.HasCafeteria
                && (
                    Self.VoreController.HasPrey(VoreLocation.Stomach)
                    && Self.VoreController.StomachDigestsPrey
                ) == false
                && (Self.Magic.Duration_Hunger == 0 || Self.VoreController.CapableOfVore() == false)
            )
            {
                if (Self.HasTrait(Traits.PrefersLivingPrey))
                    list.Add(
                        new Action(() => SetGoalToObject(ObjectType.Food)),
                        (int)(400 * (Self.Needs.Hunger - 0.9) * (Self.Needs.Hunger > 1 ? 2 : 1) * State.World.Settings.CafeteriaUsageRate)
                    );
                else
                    list.Add(
                        new Action(() => SetGoalToObject(ObjectType.Food)),
                        (int)(400 * (Self.Needs.Hunger - 0.5) * (Self.Needs.Hunger > 1 ? 8 : 1) * State.World.Settings.CafeteriaUsageRate)
                    );

                // if extremely starving, don't continue searching for things to do
                if (Self.Needs.Hunger > 1.5)
                    return;
            }

            // SLEEP
            list.Add(
                new Action(() => SetGoalToObject(ObjectType.Bed)),
                (int)(300 * (Self.Needs.Energy - 0.7) * State.World.Settings.BedUsageRate)
            );

            // if extremely exhausted, pass out on the floor
            if (Self.Needs.Energy > 1.5)
                list.Add(
                    new Action(() => Tasks.Add(new DoSelfAction(Self, SelfActionType.Rest))),
                    1000
                );

            // SHOWER
            if (Self.HasTrait(Traits.RarelyShowers))
                list.Add(
                    new Action(() => SetGoalToObject(ObjectType.Shower)),
                    (int)(100 * (Self.Needs.Cleanliness - 0.85) * State.World.Settings.ShowerUsageRate)
                );
            else
                list.Add(
                    new Action(() => SetGoalToObject(ObjectType.Shower)),
                    (int)(100 * (Self.Needs.Cleanliness - 0.45) * State.World.Settings.ShowerUsageRate)
                );

            // HEAL AT NURSE
            if (State.World.HasNurse && Self.Needs.Hunger < 1)
                list.Add(
                    new Action(() => SetGoalToObject(ObjectType.NurseOffice)),
                    (250 * (Constants.HealthMax / 2 - Self.Health))
                );

            // READ AT LIBRARY
            if (State.World.HasLibrary)
            {
                if (Self.HasTrait(Traits.Bookworm))
                    list.Add(
                        new Action(() => SetGoalToObject(ObjectType.Library)),
                        (30 * State.World.Settings.LibraryUsageRate) - Self.LibrarySatisfaction
                    );
                else
                    list.Add(
                        new Action(() => SetGoalToObject(ObjectType.Library)),
                        (15 * State.World.Settings.LibraryUsageRate) - Self.LibrarySatisfaction
                    );
            }

            // WORKOUT AT GYM
            if (State.World.HasGym)
            {
                if (Self.HasTrait(Traits.GymRat))
                    list.Add(
                        new Action(() => SetGoalToObject(ObjectType.Gym)),
                        (30 * State.World.Settings.GymUsageRate) - Self.GymSatisfaction
                    );
                else
                    list.Add(
                        new Action(() => SetGoalToObject(ObjectType.Gym)),
                        (15 * State.World.Settings.GymUsageRate) - Self.GymSatisfaction
                    );
            }

            // GO SOMEWHERE RANDOM
            list.Add(
                new Action(() => Tasks.Add(new NavigateTo(Self, RandomSquare()))),
                2 + (int)(8 * Self.Personality.Extroversion)
            );

            // GO TO THEIR ROOM
            list.Add(
                new Action(() => Tasks.Add(new NavigateTo(Self, Self.MyRoom))),
                10 - (int)(8 * Self.Personality.Extroversion)
            );

            // GO TO PARTNER'S ROOM
            if (Self.Romance.IsDating)
                list.Add(
                    new Action(() => Tasks.Add(new NavigateTo(Self, Self.Romance.Dating.MyRoom))),
                    8 + (int)(8 * Self.Personality.Extroversion)
                );

            // DISPOSAL
            list.Add(
                new Action(() => SetGoalToObject(ObjectType.Bathroom)),
                Self.Disposals.Any() ? 50 - State.World.Settings.PublicDisposalRate * 50 : 0
            );

            list.Add(
                new Action(() => SetGoalAsPublicScat()),
                Self.Disposals.Any() ? State.World.Settings.PublicDisposalRate * 50 : 0
            );

            // SEEK ORGASM (Masturbate or Ask for Sex)
            if (Self.Needs.Horniness > .75f && Self.HasTrait(Traits.SexAddict) == false)
                list.Add(SeekOrgasm, (int)(2000 * (Self.Needs.Horniness - 0.7) * State.World.Settings.SeekOrgasmRate));
            if (Self.Needs.Horniness > .5f && Self.HasTrait(Traits.SexAddict))
                list.Add(SeekOrgasm, (int)(2000 * (Self.Needs.Horniness - 0.5) * State.World.Settings.SeekOrgasmRate));
            if (
                Self.HasTrait(Traits.HornyWhenFull)
                && (Self.VoreController.HasPrey(VoreLocation.Any) || Self.Needs.Hunger < 0.1)
            )
                list.Add(SeekOrgasm, (int)(2000 * ((Self.Needs.Hunger / 0.1) - 0.5)) * State.World.Settings.SeekOrgasmRate);

            if (
                LastInteractedWith != null
                && LastInteractedWith.Position == Self.Position
                && Self.GetRelationshipWith(LastInteractedWith).FriendshipLevel > .1f
            )
                list.Add(
                    new Action(
                        () =>
                            Tasks.Add(
                                new InteractWithPerson(GoalType.Social, LastInteractedWith, Self)
                            )
                    ),
                    300
                );
            else
                LastInteractedWith = null;

            list.Add(PickPerson, 50 + 200 * Self.Personality.Extroversion);

            // INTERACT WITH OWN PREY
            var living = Self.VoreController.GetLiving(VoreLocation.Any);
            if (living != null && Self.HasRelationshipWith(living.Target))
            {
                if (
                    Self.HasTrait(Traits.Sadistic)
                    && Self.VoreController.TargetIsBeingDigested(living.Target)
                    && living.Target.Health < Constants.HealthMax / 6
                )
                {
                    if (CanUseMagic(Self) && Self.Magic.Mana >= 1)
                        list.Add(
                            new Action(
                                () =>
                                    Tasks.Add(
                                        new InteractWithPerson(
                                            GoalType.Social,
                                            living.Target,
                                            Self,
                                            InteractionType.CastHeal
                                        )
                                    )
                            ),
                            30
                        );
                    else
                        list.Add(
                            new Action(
                                () =>
                                    Tasks.Add(
                                        new InteractWithPerson(
                                            GoalType.Social,
                                            living.Target,
                                            Self,
                                            InteractionType.StopDigesting
                                        )
                                    )
                            ),
                            5 + 10 * Self.Personality.Voraphilia
                        );
                }

                if (living.Willing)
                {
                    list.Add(
                        new Action(
                            () =>
                                Tasks.Add(
                                    new InteractWithPerson(
                                        GoalType.Social,
                                        living.Target,
                                        Self,
                                        InteractionType.TalkWithPrey
                                    )
                                )
                        ),
                        5 + 10 * Self.Personality.Extroversion
                    );
                    list.Add(
                        new Action(
                            () =>
                                Tasks.Add(
                                    new InteractWithPerson(
                                        GoalType.Social,
                                        living.Target,
                                        Self,
                                        InteractionType.PlayfulTeasePrey
                                    )
                                )
                        ),
                        5
                            + 10
                                * Self.Personality.Extroversion
                                * (Self.HasTrait(Traits.Sadistic) ? 2 : 1)
                                * (Self.HasTrait(Traits.Bully) ? 2 : 1)
                    );
                    if (CanUseMagic(Self) && Self.Magic.Mana >= 1)
                        list.Add(
                            new Action(
                                () =>
                                    Tasks.Add(
                                        new InteractWithPerson(
                                            GoalType.Social,
                                            living.Target,
                                            Self,
                                            InteractionType.CastHeal
                                        )
                                    )
                            ),
                            10 * Self.Personality.Voraphilia
                        );
                }
                else
                {
                    // taunt prey if unfriendly, increase if bully or sadistic
                    list.Add(
                        new Action(
                            () =>
                                Tasks.Add(
                                    new InteractWithPerson(
                                        GoalType.Social,
                                        living.Target,
                                        Self,
                                        InteractionType.TauntPrey
                                    )
                                )
                        ),
                        ((Self.HasTrait(Traits.Sadistic) ? 10 : 0) + (Self.HasTrait(Traits.Bully) ? 5 : 0)
                            + 20) * Self.AI.Desires.InterestInUnfriendly(living.Target)
                    );

                    // soothe pred is friendly and kind, increase if motherly or endo dom, do not do if sadistic
                    list.Add(
                        new Action(
                            () =>
                                Tasks.Add(
                                    new InteractWithPerson(
                                        GoalType.Social,
                                        living.Target,
                                        Self,
                                        InteractionType.SoothePrey
                                    )
                                )
                        ),
                        (Self.HasTrait(Traits.Sadistic) ? 0 : 1) * ((Self.HasTrait(Quirks.Motherly) ? 20 : 1)
                            + (Self.Personality.EndoDominator ? 5 : 1) + 5) * Self.AI.Desires.InterestInFriendly(living.Target) * Self.Personality.Kindness
                    );
                }
                    
                list.Add(
                    new Action(() => Tasks.Add(new DoSelfAction(Self, SelfActionType.RubOwnBelly))),
                    (int)(40 * Self.Personality.Voraphilia)
                );
            }

            if (list.HasAction()) //Only add wait if there are other actions, prevents it from being the default.
                list.Add(new Action(() => Tasks.Add(new Wait())), 5);

            // DO THE SELECTED ACTION
            list.GetResult().Invoke();
        }

        //if (Self.Needs.Horniness > .8f && Tasks.Any() == false)
        //    PickSexTarget();

        if (Tasks.Any() == false)
        {
            if (Rand.Next(2) == 0)
                Tasks.Add(new Wait());
            else
                Tasks.Add(new NavigateTo(Self, RandomSquare()));
        }
    }

    private void ClothingTaskCheck()
    {
        if (Tasks.Any())
            return;
        if (StripTurns > 0)
            return;
        if (StepsFromPreferred() < 0)
        {
            Tasks.Add(new DoSelfAction(Self, SelfActionType.Strip));
        }
        else if (StepsFromPreferred() > 0 && StepsInTile() > 0)
        {
            Tasks.Add(new DoSelfAction(Self, SelfActionType.Reclothe));
            if (StepsFromPreferred() > 1 && StepsInTile() > 1)
                Tasks.Add(new DoSelfAction(Self, SelfActionType.Reclothe));
        }
        else if (StepsFromPreferred() == 2)
        {
            Tasks.Add(new NavigateTo(Self, Self.MyRoom));
            Tasks.Add(new DoSelfAction(Self, SelfActionType.Reclothe));
            Tasks.Add(new DoSelfAction(Self, SelfActionType.Reclothe));
        }
        else if (StepsFromPreferred() == 1)
        {
            Tasks.Add(new NavigateTo(Self, Self.MyRoom));
            Tasks.Add(new DoSelfAction(Self, SelfActionType.Reclothe));
        }

        int StepsFromPreferred() => Self.ClothingStatus - Self.Personality.PreferredClothing;
        int StepsInTile()
        {
            if (Self.ZoneContainsObject(ObjectType.Clothes) && Self.MyRoom == Self.Position)
                return 2 - (int)Self.ClothingStatus;
            return Self.ClothingStatus - Self.ClothesInTile;
        }
    }

    private void BeingEatenTasks()
    {
        var pred = Self.FindMyPredator();
        if (pred == null)
            return;
        if (Self.Dead)
        {
            InteractionList.List[InteractionType.PreyWait].RunCheck(Self, pred);
            return;
        }

        var progress = pred.VoreController.GetProgressOf(Self);
        if (progress.Willing)
        {
            InteractionPicker.InPredWillingActions(Self, pred);
        }
        else
        {
            InteractionPicker.InPredUnwillingActions(Self, pred);
        }
    }

    internal void ClearTasks()
    {
        Tasks.Clear();
    }

    internal Person FollowingPersonExternal()
    {
        if (Tasks.Any() && Tasks[0] is FollowPerson follow)
            return follow.GetTarget();
        return null;
    }

    internal bool TryFollowExternal()
    {
        if (Tasks.Any() && Tasks[0] is FollowPerson follow)
        {
            if (follow.GetTarget().BeingEaten || follow.GetTarget().Dead)
            {
                ClearTasks();
                return false;
            }
            var path = PathFinder.GetPath(
                Self.Position,
                follow.GetTarget().Position,
                follow.GetTarget()
            ); //Act as if goalperson, to get places only they can enter
            if (path != null && path.Count != 0)
            {
                if (
                    State.World.Move(
                        Self,
                        path[0].x - Self.Position.x,
                        path[0].y - Self.Position.y,
                        true
                    )
                )
                {
                    return true;
                }
            }
        }
        else
            ClearTasks();
        return false;
    }

    internal void ClearPath()
    {
        Path = null;
        LastDestination.x = -15;
    }

    internal bool TryMove(Vec2 pos)
    {
        LastInteractedWith = null;
        if (pos.x != -1)
        {
            List<Vec2> path;
            if (Path != null && Path.Any())
                Path.RemoveAt(0);
            if (Path != null && Path.Any() && Path[0].GetNumberOfMovesDistance(Self.Position) > 1) //If invalid, clear, to prevent a half player half ai from teleporting around
                Path = null;
            if (pos == LastDestination && (Path?.Any() ?? false))
                path = Path;
            else
                path = PathFinder.GetPath(Self.Position, pos, Self);
            Path = path;
            LastDestination = pos;
            if (path != null && path.Count != 0)
            {
                return State.World.Move(
                    Self,
                    path[0].x - Self.Position.x,
                    path[0].y - Self.Position.y
                );
            }
        }
        return false;
    }

    private void PickPerson()
    {
        var closePeople = HelperFunctions.GetPeopleWithinXSquares(Self, 3);
        closePeople = closePeople.Where(s => s.IsBusy() == false).ToList();

        if (CanUseMagic(Self) && Self.Magic.Mana >= 1 && Self.HasTrait(Traits.RoomInvader)) // search additionally for people in their own bedrooms with RoomInvader
            closePeople = closePeople
                .Where(s => State.World.Zones[s.Position.x, s.Position.y].Accepts(s))
                .ToList();
        else // search only areas where Self can reach
            closePeople = closePeople
                .Where(s => State.World.Zones[s.Position.x, s.Position.y].Accepts(Self))
                .ToList();

        var unMet = closePeople.Where(s => s.HasRelationshipWith(Self) == false).FirstOrDefault();
        if (unMet != null)
        {
            Tasks.Add(new InteractWithPerson(GoalType.Social, unMet, Self));
        }
        else if (closePeople.Any())
        {
            if (
                Self.IsWillingPreyToPublic()
                && State.World.Settings.WillingOfferOdds > 0
                && Rand.Next((int)(400 / State.World.Settings.WillingOfferOdds)) == 0
                && State.World.Settings.SecretiveVore == false
            )
            {
                Tasks.Add(new DoSelfAction(Self, SelfActionType.AreaVoreTease));
                return;
            }
            var list = new WeightedList<Person>();
            foreach (Person target in closePeople)
            {
                if (
                    Self.Romance.Dating == target
                    && Self.GetRelationshipWith(target).RomanticLevel < -.2f
                )
                {
                    Tasks.Add(
                        new InteractWithPerson(
                            GoalType.Social,
                            target,
                            Self,
                            InteractionType.BreakUp
                        )
                    );
                    return;
                }
                int weight =
                    (int)(
                        80
                        + 250 * Self.GetRelationshipWith(target).RomanticLevel
                        + 200 * Self.GetRelationshipWith(target).FriendshipLevel
                    )
                    - 30 * Self.Position.GetNumberOfMovesDistance(target.Position)
                    - target.Needs.CleanlinessScore(.4f, 60);
                if (Self.HasTrait(Quirks.Olfactophilic))
                    weight += target.Needs.CleanlinessScore(.4f, 60) * 2;
                if (
                    target.HasTrait(Traits.Unapproachable)
                    && Self.GetRelationshipWith(target).FriendshipLevel < .5f
                )
                    weight /= 3;
                list.Add(target, Math.Max(weight, 1)); //A tiny chance to pick someone regardless
            }
            list.Add(null, 50);

            var random = list.GetResult();
            if (random != null)
            {
                Tasks.Clear();
                LastInteractedWith = random;
                Tasks.Add(new InteractWithPerson(GoalType.Social, random, Self));
            }
        }
        else if (Tasks.Any() == false) //Don't double up wander orders
        {
            Tasks.Add(new NavigateTo(Self, RandomSquare()));
        }
    }

    void PickSexTarget()
    {
        var closePeople = HelperFunctions.GetPeopleWithinXSquares(Self, 5);
        closePeople = closePeople.Where(s => s.IsBusy() == false).ToList();

        if (CanUseMagic(Self) && Self.Magic.Mana >= 1 && Self.HasTrait(Traits.RoomInvader)) // search additionally for people in their own bedrooms with RoomInvader
            closePeople = closePeople
                .Where(s => State.World.Zones[s.Position.x, s.Position.y].Accepts(s))
                .ToList();
        else // search only areas where Self can reach
            closePeople = closePeople
                .Where(s => State.World.Zones[s.Position.x, s.Position.y].Accepts(Self))
                .ToList();

        closePeople = closePeople.Where(s => s.HasRelationshipWith(Self)).ToList();

        if (closePeople.Any())
        {
            var list = new WeightedList<Person>();
            foreach (Person target in closePeople)
            {
                if (Self.Romance.CanSafelyRomance(target) == false)
                    return;

                // high chance to seek sex with their prey's partner, if possible
                if (Self.VoreController.HasLovedOne(target) && Self.HasTrait(Traits.Sadistic))
                    list.Add(
                        target,
                        (int)(10000 * Self.GetRelationshipWith(target).RomanticLevel)
                    );


                // if slutty and has never had sex with target, big buff to odds
                else if (Self.HasTrait(Traits.Slutty) && Self.GetRelationshipWith(target).HaveHadSex == false)
                    list.Add(
                        target,
                        (int)(1000 * Self.GetRelationshipWith(target).RomanticLevel)
                    );

                // if olfactophilic, cleanliness penalty is reversed
                else if (Self.HasTrait(Quirks.Olfactophilic))
                    list.Add(
                        target,
                        (int)(250 * Self.GetRelationshipWith(target).RomanticLevel)
                            + target.Needs.CleanlinessScore(.5f, 40)
                    );

                // else, add char to the list normally
                else
                    list.Add(
                        target,
                        (int)(250 * Self.GetRelationshipWith(target).RomanticLevel)
                            - target.Needs.CleanlinessScore(.5f, 40)
                    );
            }
            list.Add(null, 50);

            var random = list.GetResult();
            if (random != null)
            {
                // MAGIC CHECK for charm or arouse before asking for sex
                if (
                    CanUseMagic(Self)
                    && Self.Magic.Mana >= 1
                    && InteractionList.List[InteractionType.StartSex].SuccessOdds(Self, random)
                        < 0.7f
                )
                {
                    if (Rand.Next(9) >= 8)
                        Tasks.Add(
                            new InteractWithPerson(
                                GoalType.Social,
                                random,
                                Self,
                                InteractionType.CastArouse
                            )
                        );
                    else if (Rand.Next(9) + (Self.HasTrait(Quirks.Charmer) ? 5 : 0) >= 7)
                        Tasks.Add(
                            new InteractWithPerson(
                                GoalType.Social,
                                random,
                                Self,
                                InteractionType.CastCharm
                            )
                        );
                }

                // if both chars are promiscuous, ask for sex
                if (Self.Personality.Promiscuity > .6f && random.Personality.Promiscuity > .6f)
                {
                    // if transmuter and dom or sub, can grow or shrink self
                    if (
                        CanUseMagic(Self)
                        && Self.Magic.Mana >= 1
                        && Rand.Next(9) + (Self.HasTrait(Quirks.Transmuter) ? 4 : 0) >= 9
                    )
                    {
                        if (Self.Personality.Dominance < 0.4)
                            Tasks.Add(new DoSelfAction(Self, SelfActionType.CastShrinkSelf));
                        else if (Self.Personality.Dominance >= 0.6)
                            Tasks.Add(new DoSelfAction(Self, SelfActionType.CastGrowSelf));
                    }
                    // ask for sex
                    Tasks.Add(
                        new InteractWithPerson(
                            GoalType.Social,
                            random,
                            Self,
                            InteractionType.StartSex
                        )
                    );
                }
                // if asking char is promiscuous and target is a player char, chance to ask regardless. also occurrs if has sexaddict trait
                else if (
                    Self.Personality.Promiscuity > .6f
                    && (
                        random == State.World.ControlledPerson && Rand.Next(4) == 0
                        || Self.HasTrait(Traits.SexAddict)
                    )
                )
                {
                    // if transmuter and dom or sub, can grow or shrink self
                    if (
                        CanUseMagic(Self)
                        && Self.Magic.Mana >= 1
                        && Rand.Next(9) + (Self.HasTrait(Quirks.Transmuter) ? 4 : 0) >= 9
                    )
                    {
                        if (Self.Personality.Dominance < 0.4)
                            Tasks.Add(new DoSelfAction(Self, SelfActionType.CastShrinkSelf));
                        else if (Self.Personality.Dominance >= 0.6)
                            Tasks.Add(new DoSelfAction(Self, SelfActionType.CastGrowSelf));
                    }
                    // ask for sex
                    Tasks.Add(
                        new InteractWithPerson(
                            GoalType.Social,
                            random,
                            Self,
                            InteractionType.StartSex
                        )
                    );
                }
                // else if loves private sex, ask them to follow you home for sex
                else if (
                    (Self.Personality.Promiscuity > .6f && Self.HasTrait(Traits.LovesPrivateSex))
                    || Self.HasTrait(Traits.SexAddict) || Self.HasTrait(Traits.DownToFuck)
                )
                    Tasks.Add(new Lure(Self, random, Self.MyRoom, InteractionType.StartSex));
                // else if odds are decent, ask them to follow you home for sex
                else if (
                    InteractionList.List[InteractionType.AskToFollow].SuccessOdds(Self, random)
                    > .35f
                )
                    Tasks.Add(new Lure(Self, random, Self.MyRoom, InteractionType.StartSex));
            }
        }
    }

    internal void HuntDownWillingPrey(Person target, bool endo)
    {
        Self.EndStreamingActions();
        Tasks.Clear();
        Tasks.Add(new ChasingDownWillingMeal(Self, target, endo));
    }

    internal void HuntDownPrey()
    {
        Self.EndStreamingActions();
        Tasks.Clear();

        if (
            State.World.Settings.CheckDigestion(false, DigestionAlias.CanVore)
            && (
                Self.VoreController.StomachDigestsPrey
                || Self.VoreController.GetLiving(VoreLocation.Stomach) == null
            )
            && Self.Personality.VorePreference != VorePreference.Endosoma
        )
        {
            if (
                State.World.Settings.VoreHuntingBias
                    * Math.Min(
                        Self.Personality.PredWillingness * 2 * Math.Max(0.5f, Self.Needs.Hunger),
                        5f
                    )
                > Rand.NextFloat(0, 4)
            )
            {
                Tasks.Add(new HuntForPrey(Self, false));
            }
        }
        else
            Tasks.Add(new HuntForPrey(Self, true));
    }

    internal void EngageVoreWithTarget(Person target, bool endo)
    {
        WeightedList<InteractionType> list = new WeightedList<InteractionType>();
        DigestionAlias alias = DigestionAlias.CanVore;
        if (endo)
            alias = DigestionAlias.CanEndo;
        if (Self.HasTrait(Traits.DoesNotForce))
        {
            if (endo)
            {
                if (IsPartOkay(VoreLocation.Stomach))
                    list.Add(
                        InteractionType.AskToOralVore,
                        (int)(1000 * Self.AI.Desires.InterestInOralVore(target))
                            * (
                                Self.VoreController.CouldVoreTarget(target, VoreType.Oral, alias)
                                    ? 1
                                    : 0
                            )
                    );
                if (IsPartOkay(VoreLocation.Womb))
                    list.Add(
                        InteractionType.AskToUnbirth,
                        (int)(1000 * Self.AI.Desires.InterestInUnbirth(target))
                            * (
                                Self.VoreController.CouldVoreTarget(target, VoreType.Unbirth, alias)
                                    ? 1
                                    : 0
                            )
                    );
                if (IsPartOkay(VoreLocation.Balls))
                    list.Add(
                        InteractionType.AskToCockVore,
                        (int)(1000 * Self.AI.Desires.InterestInCockVore(target))
                            * (
                                Self.VoreController.CouldVoreTarget(target, VoreType.Cock, alias)
                                    ? 1
                                    : 0
                            )
                    );
                if (
                    (
                        State.World.Settings.AnalVoreGoesDirectlyToStomach
                        && IsPartOkay(VoreLocation.Stomach)
                    )
                    || (
                        State.World.Settings.AnalVoreGoesDirectlyToStomach == false
                        && IsPartOkay(VoreLocation.Bowels)
                    )
                )
                    list.Add(
                        InteractionType.AskToAnalVore,
                        (int)(1000 * Self.AI.Desires.InterestInAnalVore(target))
                            * (
                                Self.VoreController.CouldVoreTarget(target, VoreType.Anal, alias)
                                    ? 1
                                    : 0
                            )
                    );
            }
            else
            {
                if (IsPartOkay(VoreLocation.Stomach))
                    list.Add(
                        InteractionType.AskToOralVoreDigest,
                        (int)(1000 * Self.AI.Desires.InterestInOralVore(target))
                            * (
                                Self.VoreController.CouldVoreTarget(target, VoreType.Oral, alias)
                                    ? 1
                                    : 0
                            )
                    );
                if (IsPartOkay(VoreLocation.Womb))
                    list.Add(
                        InteractionType.AskToUnbirthDigest,
                        (int)(1000 * Self.AI.Desires.InterestInUnbirth(target))
                            * (
                                Self.VoreController.CouldVoreTarget(target, VoreType.Unbirth, alias)
                                    ? 1
                                    : 0
                            )
                    );
                if (IsPartOkay(VoreLocation.Balls))
                    list.Add(
                        InteractionType.AskToCockVoreDigest,
                        (int)(1000 * Self.AI.Desires.InterestInCockVore(target))
                            * (
                                Self.VoreController.CouldVoreTarget(target, VoreType.Cock, alias)
                                    ? 1
                                    : 0
                            )
                    );
                if (
                    (
                        State.World.Settings.AnalVoreGoesDirectlyToStomach
                        && IsPartOkay(VoreLocation.Stomach)
                    )
                    || (
                        State.World.Settings.AnalVoreGoesDirectlyToStomach == false
                        && IsPartOkay(VoreLocation.Bowels)
                    )
                )
                    list.Add(
                        InteractionType.AskToAnalVoreDigest,
                        (int)(1000 * Self.AI.Desires.InterestInAnalVore(target))
                            * (
                                Self.VoreController.CouldVoreTarget(target, VoreType.Anal, alias)
                                    ? 1
                                    : 0
                            )
                    );
            }
        }
        else
        {
            if (IsPartOkay(VoreLocation.Stomach))
                list.Add(
                    InteractionType.OralVore,
                    (int)(1000 * Self.AI.Desires.InterestInOralVore(target))
                        * (
                            Self.VoreController.CouldVoreTarget(target, VoreType.Oral, alias)
                                ? 1
                                : 0
                        )
                );
            if (IsPartOkay(VoreLocation.Womb))
                list.Add(
                    InteractionType.Unbirth,
                    (int)(1000 * Self.AI.Desires.InterestInUnbirth(target))
                        * (
                            Self.VoreController.CouldVoreTarget(target, VoreType.Unbirth, alias)
                                ? 1
                                : 0
                        )
                );
            if (IsPartOkay(VoreLocation.Balls))
                list.Add(
                    InteractionType.CockVore,
                    (int)(1000 * Self.AI.Desires.InterestInCockVore(target))
                        * (
                            Self.VoreController.CouldVoreTarget(target, VoreType.Cock, alias)
                                ? 1
                                : 0
                        )
                );
            if (
                (
                    State.World.Settings.AnalVoreGoesDirectlyToStomach
                    && IsPartOkay(VoreLocation.Stomach)
                )
                || (
                    State.World.Settings.AnalVoreGoesDirectlyToStomach == false
                    && IsPartOkay(VoreLocation.Bowels)
                )
            )
                list.Add(
                    InteractionType.AnalVore,
                    (int)(1000 * Self.AI.Desires.InterestInAnalVore(target))
                        * (
                            Self.VoreController.CouldVoreTarget(target, VoreType.Anal, alias)
                                ? 1
                                : 0
                        )
                );
        }

        var result = list.GetResult();
        if (result != InteractionType.None)
        {
            VoreLocation part = VoreLocation.Stomach;
            switch (result)
            {
                case InteractionType.OralVore:
                    part = VoreLocation.Stomach;
                    break;
                case InteractionType.AnalVore:
                    if (State.World.Settings.AnalVoreGoesDirectlyToStomach)
                        part = VoreLocation.Stomach;
                    else
                        part = VoreLocation.Bowels;
                    break;
                case InteractionType.CockVore:
                    part = VoreLocation.Balls;
                    break;
                case InteractionType.Unbirth:
                    part = VoreLocation.Womb;
                    break;
            }

            if (
                Self.VoreController.GetLiving(part) == null
                && endo == false
                && Self.Personality.VorePreference != VorePreference.Endosoma
                && State.World.Settings.CheckDigestion(Self, DigestionAlias.CanVore)
            )
                Self.VoreController.SetPartDigest(part, true);
            
            if (
                (endo || Self.Personality.VorePreference == VorePreference.Endosoma)
                && State.World.Settings.CheckDigestion(Self, DigestionAlias.CanEndo)
            )
                Self.VoreController.SetPartDigest(part, false);

            bool targetWilling = target.IsWillingPreyToTarget(Self, Self.VoreController.PartCurrentlyDigests(part));

            // if theyre willing & vore not secret, high chance to eat them w/o magic spells
            if (
                targetWilling
                && Rand.Next(4) == 0
                && State.World.Settings.SecretiveVore == false
            )
            {
                // if dominance is high, chance to ask them to strip first
                if (
                    target.ClothingStatus != ClothingStatus.Nude
                    && (Rand.NextFloat(0.0, 1.0) + Self.Personality.Dominance) >= 1.2
                )
                    Tasks.Add(
                        new InteractWithPerson(
                            GoalType.Social,
                            target,
                            Self,
                            InteractionType.AskThemToStrip
                        )
                    );
                
                // if dominance is high and romance reasonably high, ask to taste first
                else if ( Self.GetRelationshipWith(target).GetRomanticLevelToTarget() > 0.2 && (Rand.NextFloat(0.2, 1.0) + Self.Personality.Dominance) >= 1.2 )
                    Tasks.Add(
                        new InteractWithPerson(
                            GoalType.Social,
                            target,
                            Self,
                            InteractionType.Taste
                        )
                    );

                // use the vore type previously set
                Tasks.Add(new InteractWithPerson(GoalType.Social, target, Self, result));
            }
            // else, if vore not secret, chance to eat
            else if (Rand.Next(9) == 0 && State.World.Settings.SecretiveVore == false)
            {
                // if can cast magic and has magic, chance to use an offensive spell
                if (CanUseMagic(Self) && Self.Magic.Mana >= 1)
                {
                    // chance to cast shrink on prey or grow on self, scales with transmuter quirk
                    if ((Rand.Next(9) + (Self.HasTrait(Quirks.Transmuter) ? 2 : 0)) >= 8)
                        if (Rand.Next(1) == 0)
                            Tasks.Add(
                                new InteractWithPerson(
                                    GoalType.Social,
                                    target,
                                    Self,
                                    InteractionType.CastShrink
                                ));
                        else
                            Tasks.Add(new DoSelfAction(Self, SelfActionType.CastGrowSelf));

                    // chance to cast charm, scales with charmer quirk and high charisma
                    else if ((Rand.Next(9) + (Self.HasTrait(Quirks.Charmer) ? 3 : 0) + (Self.Personality.Charisma > 0.6 ? 1 : 0)) >= 8)
                        Tasks.Add(
                            new InteractWithPerson(
                                GoalType.Social,
                                target,
                                Self,
                                InteractionType.CastCharm
                            )
                        );

                    // chance to cast disrobe on prey
                    else if (Rand.Next(9) >= 7 && target.ClothingStatus != ClothingStatus.Nude)
                                Tasks.Add(
                                    new InteractWithPerson(
                                        GoalType.Social,
                                        target,
                                        Self,
                                        InteractionType.CastDisrobe
                                    )
                                );

                    // chance to cast freeze on prey
                    else if (Rand.Next(9) >= 6 && target.Magic.Duration_Freeze < 2)
                        Tasks.Add(
                            new InteractWithPerson(
                                GoalType.Social,
                                target,
                                Self,
                                InteractionType.CastFreeze
                            )
                        );
                }
                // use the vore type previously set
                Tasks.Add(new InteractWithPerson(GoalType.Social, target, Self, result));
            }
            // else, follow normal behavior
            else
            {
                // start by checking if pred should try to lure the prey back to a bedroom
                // set the minimum odds for the "ask to follow" event required to attempt it
                float minOdds = .45f;
                if (Self.HasTrait(Traits.SirenPred))
                    minOdds = .2f;
                if (Self.HasTrait(Traits.NoVoreZone))
                    minOdds = 5555;
                if (State.World.Settings.SecretiveVore)
                    minOdds = -99990;

                // attempt lure if ask to follow success odds are high
                if (
                    InteractionList.List[InteractionType.AskToFollow].SuccessOdds(Self, target)
                    > minOdds
                )
                {
                    // use the vore type previously set
                    Tasks.Add(new Lure(Self, target, Self.MyRoom, result));
                }

                // if no lure, just try and eat them
                else
                {
                    // if can cast magic and has magic, chance to use an offensive spell
                    if (CanUseMagic(Self) && Self.Magic.Mana >= 1)
                    {
                        // chance to cast shrink on prey or grow on self, scales with transmuter quirk
                        if ((Rand.Next(9) + (Self.HasTrait(Quirks.Transmuter) ? 2 : 0)) >= 8)
                            if (Rand.Next(1) == 0)
                                Tasks.Add(
                                    new InteractWithPerson(
                                        GoalType.Social,
                                        target,
                                        Self,
                                        InteractionType.CastShrink
                                    ));
                            else
                                Tasks.Add(new DoSelfAction(Self, SelfActionType.CastGrowSelf));

                        // chance to cast charm, scales with charmer quirk and high charisma
                        else if ((Rand.Next(9) + (Self.HasTrait(Quirks.Charmer) ? 3 : 0) + (Self.Personality.Charisma > 0.6 ? 1 : 0)) >= 8)
                            Tasks.Add(
                                new InteractWithPerson(
                                    GoalType.Social,
                                    target,
                                    Self,
                                    InteractionType.CastCharm
                                )
                            );

                        // chance to cast disrobe on prey
                        else if (Rand.Next(9) >= 7 && target.ClothingStatus != ClothingStatus.Nude)
                            Tasks.Add(
                                new InteractWithPerson(
                                    GoalType.Social,
                                    target,
                                    Self,
                                    InteractionType.CastDisrobe
                                )
                            );

                        // chance to cast freeze on prey
                        else if (Rand.Next(9) >= 6 && target.Magic.Duration_Freeze < 2)
                            Tasks.Add(
                                new InteractWithPerson(
                                    GoalType.Social,
                                    target,
                                    Self,
                                    InteractionType.CastFreeze
                                )
                            );
                    }
                    // use the vore type previously set
                    Tasks.Add(new InteractWithPerson(GoalType.Social, target, Self, result));
                }
            }
        }

        bool IsPartOkay(VoreLocation part)
        {
            return Self.VoreController.GetAllProgress(part).Any() == false
                || Self.VoreController.PartCurrentlyDigests(part) != endo;
        }
    }

    internal void PickAndDoInteraction(Person target)
    {
        InteractionType type;
        if (Self.HasRelationshipWith(target) == false)
        {
            type = InteractionType.Meet;
        }
        else
        {
            if (
                Self.IsWillingPreyToTarget(target)
                && State.World.Settings.WillingOfferOdds > 0.01f
                && Rand.Next((int)(4000 / State.World.Settings.WillingOfferOdds))
                    < (Self.Personality.PreyWillingness * 100 * (target.HasTrait(Quirks.PredPheromones) ? 3 : 0))
                        - (100 * State.World.Settings.WillingThreshold)
                && target.VoreController.CapableOfVore()
                && (State.World.Settings.SecretiveVore == false || InPrivateArea(Self, target))
            )
            {
                type = InteractionPicker.AskToBeVored(Self, target);
                if (type == InteractionType.None)
                {
                    PickAndDoInteraction(target);
                    return;
                }
            }
            else
            {
                type = InteractionPicker.GeneralInteraction(Self, target);
                if (
                    type == InteractionType.TalkWithPrey
                    || type == InteractionType.PlayfulTeasePrey
                    || type == InteractionType.TauntPrey
                    || type == InteractionType.SoothePrey
                )
                    target = target.VoreController.GetLiving(VoreLocation.Any).Target;
            }
        }
        if (type == InteractionType.None)
            UnityEngine.Debug.Log("Assigned type of none!");
        UseInteractionOnTarget(target, type);
    }

    /// <summary>
    /// Returns false if using is impossible
    /// </summary>
    internal bool UseInteractionOnTarget(Person target, InteractionType type)
    {
        if (type == InteractionType.None)
            return false;
        var interaction = InteractionList.List[type];
        if (interaction.Range >= Self.Position.GetNumberOfMovesDistance(target.Position))
        {
            // check every interaction that is set to ask player
            if (interaction.AsksPlayer && State.World.ControlledPerson == target)
            {
                //if player is charmed and the interaction % chance is higher than the charm threshold, auto succeed
                if (interaction.SuccessOdds(Self, target) >= State.World.Settings.PlayerCharmThreshold && target.Magic.IsCharmedBy(Self))
                    interaction.OnSucceed(Self, target);

                // if an interaction targeting player has a higher calculated % chance than the threshold set in settings, auto succeed
                else if (interaction.SuccessOdds(Self, target) >= State.World.Settings.PlayerAutoSucceedThreshold
                    && (State.World.Settings.DecideType == AutoDecide.Enabled || State.World.Settings.DecideType == AutoDecide.SucceedOnly))
                    interaction.OnSucceed(Self, target);

                // if an interaction targeting player has a lower calculated % chance than the threshold set in settings, auto fail
                else if (interaction.SuccessOdds(Self, target) <= State.World.Settings.PlayerAutoFailThreshold
                    && (State.World.Settings.DecideType == AutoDecide.Enabled || State.World.Settings.DecideType == AutoDecide.FailOnly))
                    interaction.OnFail(Self, target);

                // else, show the popup window and ask the player
                else
                    State.World.AskPlayer(Self, interaction);
            }
            
            // also check for spell casts vs the player
            else if (
                interaction.Class == ClassType.CastTarget
                && State.World.ControlledPerson == target
                && State.World.TestingMode == false
            )
            {
                // if auto success and character is guinea pig, magic will auto-succeed vs player
                if (target.HasTrait(Traits.GuineaPig)
                    && (State.World.Settings.DecideType == AutoDecide.Enabled || State.World.Settings.DecideType == AutoDecide.SucceedOnly))
                    interaction.OnSucceed(Self, target);

                // if not guinea pig and immersive mode enabled, do not prompt dialogue when having spells cast on you
                else if (target.HasTrait(Traits.GuineaPig) == false
                    && (State.World.Settings.DecideType == AutoDecide.Immersive))
                    interaction.RunCheck(Self, target, cleared: true);

                // else, show the popup window and ask the player
                else
                   State.World.AskPlayerWillingMagic(Self, interaction);
            }
            
            // also check hostile vore attacks vs the player
            else if (
                interaction.Class == ClassType.VoreConsuming
                && State.World.ControlledPerson == target
                && State.World.TestingMode == false
            )
            {
                //if player is charmed and prey willingness is higher than the charm threshold, auto succeed
                if (target.Personality.PreyWillingness >= State.World.Settings.PlayerCharmThreshold && target.Magic.IsCharmedBy(Self))
                    interaction.OnSucceed(Self, target);

                // if player prey willingness higher than the threshold set in settings, auto succeed
                else if (target.Personality.PreyWillingness >= State.World.Settings.WillingThreshold
                    && (State.World.Settings.DecideType == AutoDecide.Enabled || State.World.Settings.DecideType == AutoDecide.SucceedOnly))
                    interaction.OnSucceed(Self, target);

                // if player prey willingness lower than the threshold set in settings, auto decide
                else if (target.Personality.PreyWillingness <= State.World.Settings.WillingThreshold
                    && (State.World.Settings.DecideType == AutoDecide.Enabled || State.World.Settings.DecideType == AutoDecide.FailOnly || State.World.Settings.DecideType == AutoDecide.Immersive))
                    interaction.RunCheck(Self, target, cleared: true);

                // else, show the popup window and ask the player
                else
                    State.World.AskPlayerWillingPrey(Self, interaction);
            }

            else
                interaction.RunCheck(Self, target);
            return true;
        }
        return false;
    }

    private void ContinueSelfStreamingAction()
    {
        var action = SelfActionList.List[Self.StreamingSelfAction];
        if (Self.StreamedTurns >= action.MaxStreamLength)
        {
            Self.EndStreamingActions();
        }
        else
        {
            action.OnDo(Self, true);
        }
    }

    private void SeekOrgasm()
    {
        Tasks.Clear();
        PickSexTarget();
        if (Tasks.Any() == false && Self.HasTrait(Traits.SexOnly) == false)
        {
            if (
                Self.Personality.Promiscuity + (Self.Magic.Duration_Aroused > 0 ? .4f : 0 ) > Rand.NextFloat(.6, 1.2f)
                || Self.HasTrait(Traits.Exhibitionist)
            )
            {
                Tasks.Add(new DoSelfAction(Self, SelfActionType.Masturbate));
            }
            else
            {
                Tasks.Add(new NavigateTo(Self, Self.MyRoom));
                Tasks.Add(new DoSelfAction(Self, SelfActionType.Masturbate));
            }
        }
    }

    private void SetGoalToObject(ObjectType obj)
    {
        Tasks.Clear();
        Vec2 destination;
        if (obj == ObjectType.Bed)
            destination = Self.MyRoom;
        else
            destination = RandomAccessibleSquareOfType(obj);
        Tasks.Add(new InteractWithObject(obj, Self, destination));
    }

    private void SetGoalAsPublicScat()
    {
        Tasks.Clear();
        Vec2 destination = RandomSquare();
        Tasks.Add(new InteractWithObject(ObjectType.Bathroom, Self, destination));
    }

    internal Vec2 RandomSquare()
    {
        List<Vec2> tiles = new List<Vec2>();
        for (int x = 0; x <= State.World.Zones.GetUpperBound(0); x++)
        {
            for (int y = 0; y <= State.World.Zones.GetUpperBound(1); y++)
            {
                if (State.World.Zones[x, y] != null)
                {
                    if (State.World.Zones[x, y].Accepts(Self))
                        tiles.Add(new Vec2(x, y));
                }
            }
        }
        if (tiles.Any() == false)
        {
            return new Vec2(-1, -1);
        }
        return tiles[Rand.Next(tiles.Count)];
    }

    internal Vec2 RandomAccessibleSquareOfType(ObjectType type)
    {
        WeightedList<Vec2> tiles = new WeightedList<Vec2>();
        for (int x = 0; x <= State.World.Zones.GetUpperBound(0); x++)
        {
            for (int y = 0; y <= State.World.Zones.GetUpperBound(1); y++)
            {
                if (State.World.Zones[x, y] != null)
                {
                    if (State.World.Zones[x, y].Objects.Contains(type))
                    {
                        if (State.World.Zones[x, y].Accepts(Self))
                            tiles.Add(
                                new Vec2(x, y),
                                100000
                                    / (
                                        1
                                        + (
                                            Self.Position.GetNumberOfMovesDistance(new Vec2(x, y))
                                            * Self.Position.GetNumberOfMovesDistance(new Vec2(x, y))
                                        )
                                    )
                            );
                    }
                }
            }
        }
        if (tiles.HasAction() == false)
        {
            return new Vec2(-1, -1);
        }
        return tiles.GetResult();
    }
}
