﻿using OdinSerializer;
using System.Linq;

namespace Assets.Scripts.AI
{
    class InteractWithObject : IGoal
    {
        [OdinSerialize]
        ObjectType GoalObject;

        [OdinSerialize]
        Person Self;

        [OdinSerialize]
        Vec2 Destination;

        public InteractWithObject(ObjectType goalObject, Person self, Vec2 destination)
        {
            GoalObject = goalObject;
            Self = self;
            Destination = destination;
        }

        public GoalReturn ExecuteStep()
        {
            if (Destination == Self.Position)
            {
                //if (Goal == GoalType.Orgasm && GoalPerson == null)
                //{
                //    DoSelfAction(SelfActionType.Masturbate);
                //}
                if (GoalObject == ObjectType.Food)
                {
                    DoSelfAction(SelfActionType.EatFood);
                }
                else if (GoalObject == ObjectType.Bed)
                {
                    DoSelfAction(SelfActionType.Rest);
                }
                else if (GoalObject == ObjectType.Shower)
                {
                    if (Self.ClothingStatus != ClothingStatus.Nude)
                    {
                        DoSelfAction(SelfActionType.Strip);
                        return GoalReturn.DidStep;
                    }
                    DoSelfAction(SelfActionType.Shower);
                }
                else if (GoalObject == ObjectType.NurseOffice)
                {
                    DoSelfAction(SelfActionType.TreatWounds);
                }
                else if (GoalObject == ObjectType.Gym)
                {
                    if (
                        Self.Needs.Energy < Rand.NextFloat(0, 1)
                        || (Self.CanUseMagic() && Self.Magic.Mana < Self.Magic.MaxMana)
                    )
                        DoSelfAction(SelfActionType.Meditate);
                    else
                        DoSelfAction(SelfActionType.Exercise);
                }
                else if (GoalObject == ObjectType.Library)
                {
                    if (
                        Self.CanUseMagic()
                        && (Rand.Next(2) == 0 || Self.Magic.Mana < Self.Magic.MaxMana)
                    )
                        DoSelfAction(SelfActionType.StudyArcane);
                    else if (Rand.Next(2) == 0)
                        DoSelfAction(SelfActionType.ResearchCommunication);
                    else
                        DoSelfAction(SelfActionType.BrowseWeb);
                }
                else if (GoalObject == ObjectType.Bathroom)
                {
                    if (State.World.GetZone(Self.Position).Objects.Contains(ObjectType.Bathroom))
                    {
                        if (Self.Disposals.Any() == false)
                            return GoalReturn.AbortGoal;
                        else if (Self.Disposals[0].Location == VoreLocation.Balls)
                            DoSelfAction(SelfActionType.CockDisposalBathroom);
                        else if (Self.Disposals[0].Location == VoreLocation.Womb)
                            DoSelfAction(SelfActionType.UnbirthDisposalBathroom);
                        else
                            DoSelfAction(SelfActionType.ScatDisposalBathroom);
                    }
                    else
                    {
                        if (Self.Disposals.Any() == false)
                            return GoalReturn.AbortGoal;
                        else if (Self.Disposals[0].Location == VoreLocation.Balls)
                            DoSelfAction(SelfActionType.CockDisposalFloor);
                        else if (Self.Disposals[0].Location == VoreLocation.Womb)
                            DoSelfAction(SelfActionType.UnbirthDisposalFloor);
                        else
                            DoSelfAction(SelfActionType.ScatDisposalFloor);
                    }
                }
                return GoalReturn.CompletedGoal;
            }
            if (Self.AI.TryMove(Destination))
            {
                return GoalReturn.DidStep;
            }
            return GoalReturn.AbortGoal;

            void DoSelfAction(SelfActionType type)
            {
                var selfAction = SelfActionList.List[type];
                selfAction.OnDo(Self);
            }
        }

        public string ReportGoal()
        {
            return $"Heading for the {GoalObject} at {Destination.x}, {Destination.y}";
        }
    }
}
