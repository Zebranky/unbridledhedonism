﻿using GeneratedSelfInteractions;
using UnityEngine;

class Desires
{
    private Person Self;

    public Desires(Person self)
    {
        Self = self;
    }

    float CleanlinessScore(Person target)
    {
        if (target.Needs.Cleanliness < .3f)
            return 1;
        return 2 / (2 + (target.Needs.Cleanliness - .3f));
    }

    internal float InterestInOralVore(Person target) => Self.Personality.OralVoreInterest;

    internal float InterestInUnbirth(Person target) => Self.Personality.UnbirthInterest;

    internal float InterestInCockVore(Person target) => Self.Personality.CockVoreInterest;

    internal float InterestInAnalVore(Person target) => Self.Personality.AnalVoreInterest;

    internal float VornyModifier(Person target) =>
        0.9f + (Self.Personality.Voraphilia * (Mathf.Max(Self.Needs.Horniness, Mathf.Pow(Self.Personality.Promiscuity, 2)) / 2));

    internal float InterestInUnfriendly(Person target)
    {
        var rel = Self.GetRelationshipWith(target);
        if (Self.HasTrait(Traits.Jerk))
            return 1.5f * (.4f - rel.FriendshipLevel);
        return -rel.FriendshipLevel;
    }

    internal float InterestInFriendly(Person target) =>
        .25f
        + ((Self.Personality.Extroversion + Self.GetRelationshipWith(target).FriendshipLevel) / 2)
        + (Self.Magic.IsCharmedBy(target) ? .25f : 0);

    internal float InterestInRomanticRelations(Person target) =>
        Self.GetRelationshipWith(target).RomanticLevel
        + (Self.Magic.IsCharmedBy(target) ? .25f : 0);

    internal float InterestInRomanceNow(Person target)
    {
        var rel = Self.GetRelationshipWith(target);

        float val = (((rel.FriendshipLevel * 0.3f) + (rel.RomanticLevel * 1.7f)) / 2)
        + (Self.Personality.Promiscuity / 3)
        + (Self.Needs.Horniness / 2)
        + (Self.Magic.IsCharmedBy(target) ? .2f : 0);

        if (Self.Magic.Duration_Aroused > 0)
            val *= 1.5f;

        return val;
    }

    internal float MiddleOverallAttachment(Person target)
    {
        var rel = Self.GetRelationshipWith(target);
        float val = 1f
            + ((rel.FriendshipLevel + rel.RomanticLevel) / 2)
            + ((target.Personality.Charisma - .5f) / 3)
            + (Self.Magic.IsCharmedBy(target) ? .5f : 0);

        return val;
    }

    /// <summary>
    /// Higher friendship/relationship will drive this value lower
    /// </summary>
    /// <param name="target"></param>
    /// <returns></returns>
    float MiddleCloseAttachment(Person target)
    {
        var rel = Self.GetRelationshipWith(target);
        var val = (1 - rel.FriendshipLevel * .99f) * (1 - rel.RomanticLevel * .99f);
        val = Mathf.Lerp(val, 1, 1 - Self.Personality.PredLoyalty);
        val = Mathf.Clamp(val, 0.025f, 1.5f);
        return val;
    }

    float MiddleInterestInForcingVore(Person target) =>
        MiddleCloseAttachment(target)
        * State.World.Settings.ForcedPredWillingness
        * MiddleInterestInVore(target)
        * (Self.HasTrait(Traits.DoesNotForce) ? 0 : 1)
        * (target.Magic.Duration_PreyCurse > 0 ? 2 : 1);

    float MiddleInterestInVore(Person target)
    {
        if (
            Self.HasTrait(Traits.RomanticPred)
            && Self.Romance.Dating != target
            && Self.GetRelationshipWith(target).RomanticLevel < .6f
        )
            return 0;
        if (
            Self.HasTrait(Traits.OrientationFeeding)
            && Self.Romance.DesiresGender(target.GenderType) == false
        )
            return 0;

        // value for vore interest
        float val = Self.Personality.PredWillingness // main governing stat
        * VornyModifier(target) // modifier based on horniness and voraphilia
        * (0.9f + (Self.Personality.Voracity / 4)) // smaller modifier based on voracity
        * target.Boosts.AttractsPreds; // target's boosts which affect prey

        // apply modifiers based on traits or abilities
        if (Self.HasTrait(Quirks.Olfactophilic) == false)
            val *= CleanlinessScore(target);
        if (Self.HasTrait(Traits.ConfusedAppetites))
            val *= 1 + (Self.Needs.Horniness / 2);
        if (Self.Magic.Duration_Hunger > 0)
            val *= 3f;
        if (target.Magic.Duration_PreyCurse > 0)
            val *= 3f;
        if (target.PartList.Height < Self.PartList.Height * 2 / 3)
            val *= 1.5f;

        return val;
    }

    float MiddleInterestInDigestingPrey(Person target)
    {
        // if has Unnatural Hunger effect, force high digestion interest
        if (Self.Magic.Duration_Hunger > 0)
            return 5;

        // if has ForgetfulPred and is sleeping, check based solely on hunger
        if (Self.HasTrait(Traits.ForgetfulPred) && Self.StreamingSelfAction == SelfActionType.Rest)
            return Self.Needs.Hunger * 3;

        // else, run check as normal
        float val = Mathf.Pow(Self.Personality.PredWillingness, .75f)
        * (Self.Needs.Hunger + (1 - Self.Personality.Kindness))
        * MiddleCloseAttachment(target)
        * (State.World.Settings.NursesActive ? 1.5f : .85f)
        * (Self.Personality.EndoDominator ? 0.35f : 1)
        * State.World.Settings.DigestionBias
        * (Self.Personality.VorePreference == VorePreference.Endosoma ? 0 : 1);

        // if has digestible quirk, multiply value by 3
        if (target.HasTrait(Quirks.Digestible))
            val *= 3f;

        return val;
    }

    float MiddleInterestInBeingVored(Person target)
    {
        float val = Self.Personality.PreyWillingness // main governing stat
        * VornyModifier(target) // modifier based on horniness and voraphilia
        * MiddleOverallAttachment(target); // modifier based on friendship towards the predator

        if (Self.HasTrait(Traits.PreySlut))
            val *= 1 + (Self.Needs.Horniness / 4);

        if (target.HasTrait(Quirks.PredPheromones))
            val *= 1.5f;

        return val;
    }

    float MiddleInterestInBeingDigested(Person target)
    {
        float val = Self.Personality.PreyDigestionInterest;

        if (Self.HasTrait(Traits.PrefersAction))
            val = (1 + val) / 2;

        return val;
    }

    internal float DesireToForciblyEatAndDigestTarget(Person target) =>
        MiddleInterestInForcingVore(target) * MiddleInterestInDigestingPrey(target);

    internal float DesireToForciblyEndoTarget(Person target) => MiddleInterestInForcingVore(target);

    internal float DesireToVoreTarget(Person target)
    {
        return MiddleInterestInVore(target) * State.World.Settings.AskPredWillingness;
    }

    internal float DesireToVoreTargetWithoutPredWillingness(Person target)
    {
        return MiddleInterestInVore(target);
    }

    internal float DesireToVoreAndDigestTarget(Person target)
    {
        return MiddleInterestInVore(target)
            * MiddleInterestInDigestingPrey(target)
            * State.World.Settings.AskPredWillingness;
    }

    internal float DesireToEndoTarget(Person target)
    {
        if (Self.Personality.VorePreference == VorePreference.Digestion)
            return 0;

        float val = MiddleInterestInVore(target)
            * State.World.Settings.EndoBias
            * State.World.Settings.AskPredWillingness;

        if (State.World.Settings.EndoisOnlyRomantic)
            val *= InterestInRomanceNow(target);
        else
            val *= MiddleOverallAttachment(target);

        return val;
    }

    internal float DesireToVoreAndDigestTargetOffer(Person target)
    {
        return DesireToVoreAndDigestTarget(target)
            * Mathf.Max(MiddleOverallAttachment(target), 1);
    }

    internal float DesireToDigestTarget(Person target)
    {
        return MiddleInterestInDigestingPrey(target);
    }

    internal float PreyTargetWeight(Person target)
    {
        if (
            Self.HasTrait(Traits.SelectivelyWilling)
            && (
                Self.GetRelationshipWith(target).FriendshipLevel > .7f
                || Self.GetRelationshipWith(target).RomanticLevel > .7f
            ) == false
        )
            return 0;
        if (Self.HasTrait(Traits.NeverWilling))
            return 0;
        return 1;
    }

    internal float DesireToBeVored(Person target)
    {

        if (Self.Magic.IsCharmedBy(target))
            return 1;

        float val = MiddleInterestInBeingVored(target)
            * MiddleInterestInBeingDigested(target)
            * PreyTargetWeight(target);

        return val;
    }

    internal float DesireToBeVoredEndo(Person target)
    {
        if (Self.Magic.IsCharmedBy(target))
            return 1;

        float val = MiddleInterestInBeingVored(target)
            * PreyTargetWeight(target);

        if (Self.HasTrait(Traits.PrefersAction))
            val *= 0.2f;

        return val;
    }

    internal float DesireToBeDigested(Person target)
    {
        if (Self.Magic.IsCharmedBy(target))
            return 1;

        float val = MiddleInterestInBeingDigested(target) 
            * PreyTargetWeight(target);

        return val;
    }

    internal float AskedDesireToReleaseEndoPrey(Person target)
    {
        if (Self.VoreController.TargetIsBeingDigested(target))
            return 0;
        if (Self.Personality.EndoDominator && State.World.Settings.EndoDominatorsHoldForever)
            return 0;
        var prog = Self.VoreController.GetProgressOf(target);
        if (prog == null)
            return 0;
        int weight = -State.World.Settings.EndoPreferredTurns / 2;
        if (Self.Personality.EndoDominator)
            weight *= 2;
        if (Self.HasTrait(Traits.Greedy))
            weight *= 5;

        weight /= 3;

        weight += prog.Stage;

        if (weight < 0)
            return 0;

        return (float)weight / State.World.Settings.EndoPreferredTurns;
    }

    internal bool DesiresToReleaseEndoPrey(Person target)
    {
        if (Self.Personality.EndoDominator && State.World.Settings.EndoDominatorsHoldForever)
            return false;
        if (Self.HasTrait(Traits.Greedy))
            return false;
        var prog = Self.VoreController.GetProgressOf(target);
        if (prog == null)
            return false;
        int weight = -State.World.Settings.EndoPreferredTurns;
        if (Self.Personality.EndoDominator)
            weight *= 2;

        weight += prog.Stage;

        weight += 3 - Rand.Next(6);

        return weight > 0;
    }
}
