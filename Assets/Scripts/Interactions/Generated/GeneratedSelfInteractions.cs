using static InteractionFunctions;
using static HelperFunctions;
using static InteractionOddsFunctions;
using System.Linq;

namespace GeneratedSelfInteractions
{
    class EatFood : SelfActionBase
    {
        public EatFood()
        {
            Name = "EatFood";
            Description = "Eat food from the cafeteria.";
            Class = ClassType.Self;
            Type = SelfActionType.EatFood;
            SoundRange = -1;
            Streaming = true;
            StreamingDescription = "Eating Food";
            Interrupts = true;
            Silent = false;
            MaxStreamLength = 3;
            AppearConditional = (a) =>
                a.ZoneContainsObject(ObjectType.Food) && a.BeingEaten == false;
            Effect = (a) => EatFood(a);
            SuccessOdds = (a) => 1f;
        }
    }

    class Rest : SelfActionBase
    {
        public Rest()
        {
            Name = "Rest";
            Description = "Lie down and go to sleep.";
            Class = ClassType.Self;
            Type = SelfActionType.Rest;
            SoundRange = -1;
            Streaming = true;
            StreamingDescription = "Sleeping";
            Interrupts = true;
            Silent = false;
            MaxStreamLength = 999;
            AppearConditional = (a) =>
                (a.ZoneContainsObject(ObjectType.Bed) || a.Needs.Energy >= 0.9f || a.StreamingSelfAction == SelfActionType.Rest)
                && a.BeingEaten == false;
            Effect = (a) => a.Needs.ChangeEnergy(-.05f);
            SuccessOdds = (a) => 1f;
        }
    }

    class Shower : SelfActionBase
    {
        public Shower()
        {
            Name = "Shower";
            Description = "Take a shower (requires nudity).";
            Class = ClassType.Self;
            Type = SelfActionType.Shower;
            SoundRange = -1;
            Streaming = true;
            StreamingDescription = "Showering";
            Interrupts = true;
            Silent = false;
            MaxStreamLength = 8;
            AppearConditional = (a) =>
                a.ZoneContainsObject(ObjectType.Shower)
                && a.ClothingStatus == ClothingStatus.Nude
                && a.BeingEaten == false;
            Effect = (a) => a.Needs.Cleanliness = a.Needs.Cleanliness * .9f - .16f;
            SuccessOdds = (a) => 1f;
        }
    }

    class Strip : SelfActionBase
    {
        public Strip()
        {
            Name = "Strip";
            Description = "Take off a layer of clothes";
            Class = ClassType.Self;
            Type = SelfActionType.Strip;
            SoundRange = -1;
            Streaming = false;
            StreamingDescription = "";
            Interrupts = false;
            Silent = false;
            MaxStreamLength = 999;
            AppearConditional = (a) => a.ClothingStatus != ClothingStatus.Nude;
            Effect = (a) => Strip(a);
            SuccessOdds = (a) => 1f;
        }
    }

    class Reclothe : SelfActionBase
    {
        public Reclothe()
        {
            Name = "Reclothe";
            Description = "Put back on a layer of clothes.";
            Class = ClassType.Self;
            Type = SelfActionType.Reclothe;
            SoundRange = -1;
            Streaming = false;
            StreamingDescription = "";
            Interrupts = false;
            Silent = false;
            MaxStreamLength = 999;
            AppearConditional = (a) =>
                a.ClothingStatus != ClothingStatus.Normal
                && (
                    (
                        a.ZoneContainsObject(ObjectType.Clothes)
                        && a.MyRoom == a.Position
                        && a.BeingEaten == false
                    )
                    || a.ClothesInTile < a.ClothingStatus
                );
            Effect = (a) => Clothe(a);
            SuccessOdds = (a) => 1f;
        }
    }

    class Masturbate : SelfActionBase
    {
        public Masturbate()
        {
            Name = "Masturbate";
            Description = "Play with yourself to build, or release horniness.";
            Class = ClassType.Self;
            Type = SelfActionType.Masturbate;
            SoundRange = -1;
            Streaming = true;
            StreamingDescription = "Masturbating";
            Interrupts = true;
            Silent = false;
            MaxStreamLength = 999;
            AppearConditional = (a) => a.BeingEaten == false && a.ActiveSex == null;
            Effect = (a) => Masturbate(a);
            SuccessOdds = (a) => 1f;
        }
    }

    class RubOwnBelly : SelfActionBase
    {
        public RubOwnBelly()
        {
            Name = "RubOwnBelly";
            Description = "Rub your own belly to feel the prey within.";
            Class = ClassType.Self;
            Type = SelfActionType.RubOwnBelly;
            SoundRange = -1;
            Streaming = false;
            StreamingDescription = "";
            Interrupts = false;
            Silent = false;
            MaxStreamLength = 999;
            AppearConditional = (a) => a.VoreController.HasBellyPreySwallowed();
            Effect = (a) => { };
            SuccessOdds = (a) => 1f;
        }
    }

    class TreatWounds : SelfActionBase
    {
        public TreatWounds()
        {
            Name = "TreatWounds";
            Description = "Treat any wounds that you have to make them heal faster.";
            Class = ClassType.Self;
            Type = SelfActionType.TreatWounds;
            SoundRange = -1;
            Streaming = true;
            StreamingDescription = "Treating wounds";
            Interrupts = true;
            Silent = false;
            MaxStreamLength = 999;
            AppearConditional = (a) =>
                a.ZoneContainsObject(ObjectType.NurseOffice)
                && a.BeingEaten == false
                && a.Needs.Hunger < 1;
            Effect = (a) => TreatWounds(a);
            SuccessOdds = (a) => 1f;
        }
    }

    class Exercise : SelfActionBase
    {
        public Exercise()
        {
            Name = "Exercise";
            Description =
                "Exercise, releasing stress (and increasing strength if flexible stats are on)";
            Class = ClassType.Self;
            Type = SelfActionType.Exercise;
            SoundRange = -1;
            Streaming = true;
            StreamingDescription = "Exercising";
            Interrupts = true;
            Silent = false;
            MaxStreamLength = 999;
            AppearConditional = (a) =>
                a.ZoneContainsObject(ObjectType.Gym) && a.BeingEaten == false && a.Needs.Energy < 1;
            Effect = (a) => Exercise(a);
            SuccessOdds = (a) => 1f;
        }
    }

    class BrowseWeb : SelfActionBase
    {
        public BrowseWeb()
        {
            Name = "BrowseWeb";
            Description = "Browse the internet, just to pass time.  ";
            Class = ClassType.Self;
            Type = SelfActionType.BrowseWeb;
            SoundRange = -1;
            Streaming = true;
            StreamingDescription = "Browsing the web";
            Interrupts = true;
            Silent = false;
            MaxStreamLength = 999;
            AppearConditional = (a) =>
                a.ZoneContainsObject(ObjectType.Library)
                && a.BeingEaten == false
                && a.Needs.Energy < 1;
            Effect = (a) => BrowseWeb(a);
            SuccessOdds = (a) => 1f;
        }
    }

    class ResearchCommunication : SelfActionBase
    {
        public ResearchCommunication()
        {
            Name = "ResearchCommunication";
            Description =
                "Research better ways to communicate, raising your charisma if flexible skills is on";
            Class = ClassType.Self;
            Type = SelfActionType.ResearchCommunication;
            SoundRange = -1;
            Streaming = true;
            StreamingDescription = "Researching about communication";
            Interrupts = true;
            Silent = false;
            MaxStreamLength = 999;
            AppearConditional = (a) =>
                a.ZoneContainsObject(ObjectType.Library)
                && a.BeingEaten == false
                && a.Needs.Energy < 1;
            Effect = (a) => ResearchCommunication(a);
            SuccessOdds = (a) => 1f;
        }
    }

    class StudyArcane : SelfActionBase
    {
        public StudyArcane()
        {
            Name = "StudyArcane";
            Description =
                "Study magical tomes and texts. Raises spell success chance somewhat, and generates a small amount of mana.";
            Class = ClassType.Self;
            Type = SelfActionType.StudyArcane;
            SoundRange = -1;
            Streaming = true;
            StreamingDescription = "Studying the arcane";
            Interrupts = true;
            Silent = false;
            MaxStreamLength = 999;
            AppearConditional = (a) =>
                a.ZoneContainsObject(ObjectType.Library)
                && a.BeingEaten == false
                && CanUseMagic(a)
                && a.Needs.Energy < 1;
            Effect = (a) => StudyArcane(a);
            SuccessOdds = (a) => 1f;
        }
    }

    class Meditate : SelfActionBase
    {
        public Meditate()
        {
            Name = "Meditate";
            Description =
                "Stretch and meditate. Raises magic resistance somewhat, and generates a small amount of mana.";
            Class = ClassType.Self;
            Type = SelfActionType.Meditate;
            SoundRange = -1;
            Streaming = true;
            StreamingDescription = "Meditating";
            Interrupts = true;
            Silent = false;
            MaxStreamLength = 999;
            AppearConditional = (a) =>
                a.ZoneContainsObject(ObjectType.Gym) && a.BeingEaten == false;
            Effect = (a) => Meditate(a);
            SuccessOdds = (a) => 1f;
        }
    }

    class AreaVoreTease : SelfActionBase
    {
        public AreaVoreTease()
        {
            Name = "AreaVoreTease";
            Description =
                "Tell everyone nearby that you'd like to digest inside one of them.  Note that if they can't get to you, and you weren't eaten, they'll be frustrated at you for messing with them.";
            Class = ClassType.Self;
            Type = SelfActionType.AreaVoreTease;
            SoundRange = 3;
            Streaming = false;
            StreamingDescription = "";
            Interrupts = false;
            Silent = false;
            MaxStreamLength = 999;
            AppearConditional = (a) => a.VoreImmune == false && a.BeingEaten == false && (a.Personality.PreyWillingness * a.Personality.PreyDigestionInterest) >= State.World.Settings.WillingThreshold;
            Effect = (a) => AreaVoreTease(a, false);
            SuccessOdds = (a) => 1f;
        }
    }

    class AreaVoreTeaseEndo : SelfActionBase
    {
        public AreaVoreTeaseEndo()
        {
            Name = "AreaVoreTeaseEndo";
            Description =
                "Tell everyone nearby that you'd like to spend time inside one of them.  Note that if they can't get to you, and you weren't eaten, they'll be frustrated at you for messing with them.";
            Class = ClassType.Self;
            Type = SelfActionType.AreaVoreTeaseEndo;
            SoundRange = 3;
            Streaming = false;
            StreamingDescription = "";
            Interrupts = false;
            Silent = false;
            MaxStreamLength = 999;
            AppearConditional = (a) => a.VoreImmune == false && a.BeingEaten == false && a.Personality.PreyWillingness >= State.World.Settings.WillingThreshold;
            Effect = (a) => AreaVoreTease(a, true);
            SuccessOdds = (a) => 1f;
        }
    }

    class Burp : SelfActionBase
    {
        public Burp()
        {
            Name = "Burp";
            Description = "Burp";
            Class = ClassType.Self;
            Type = SelfActionType.Burp;
            SoundRange = 2;
            Streaming = false;
            StreamingDescription = "";
            Interrupts = false;
            Silent = false;
            MaxStreamLength = 999;
            AppearConditional = (a) => a.VoreController.GetAllProgress(VoreLocation.Stomach).Any();
            Effect = (a) => { };
            SuccessOdds = (a) => 1f;
        }
    }

    class Scream : SelfActionBase
    {
        public Scream()
        {
            Name = "Scream";
            Description = "";
            Class = ClassType.Event;
            Type = SelfActionType.Scream;
            SoundRange = 3;
            Streaming = false;
            StreamingDescription = "";
            Interrupts = false;
            Silent = false;
            MaxStreamLength = 999;
            AppearConditional = (a) => false;
            Effect = (a) => { };
            SuccessOdds = (a) => 1f;
        }
    }

    class WillingScream : SelfActionBase
    {
        public WillingScream()
        {
            Name = "WillingScream";
            Description = "";
            Class = ClassType.Event;
            Type = SelfActionType.WillingScream;
            SoundRange = 3;
            Streaming = false;
            StreamingDescription = "";
            Interrupts = false;
            Silent = false;
            MaxStreamLength = 999;
            AppearConditional = (a) => false;
            Effect = (a) => { };
            SuccessOdds = (a) => 1f;
        }
    }

    class Orgasm : SelfActionBase
    {
        public Orgasm()
        {
            Name = "Orgasm";
            Description = "";
            Class = ClassType.Event;
            Type = SelfActionType.Orgasm;
            SoundRange = 2;
            Streaming = false;
            StreamingDescription = "";
            Interrupts = false;
            Silent = false;
            MaxStreamLength = 999;
            AppearConditional = (a) => false;
            Effect = (a) => Orgasm(a);
            SuccessOdds = (a) => 1f;
        }
    }

    class FinishDigestion : SelfActionBase
    {
        public FinishDigestion()
        {
            Name = "FinishDigestion";
            Description = "";
            Class = ClassType.Event;
            Type = SelfActionType.FinishDigestion;
            SoundRange = 0;
            Streaming = false;
            StreamingDescription = "";
            Interrupts = false;
            Silent = false;
            MaxStreamLength = 999;
            AppearConditional = (a) => false;
            Effect = (a) => { };
            SuccessOdds = (a) => 1f;
        }
    }

    class FinishUnbirth : SelfActionBase
    {
        public FinishUnbirth()
        {
            Name = "FinishUnbirth";
            Description = "";
            Class = ClassType.Event;
            Type = SelfActionType.FinishUnbirth;
            SoundRange = 0;
            Streaming = false;
            StreamingDescription = "";
            Interrupts = false;
            Silent = false;
            MaxStreamLength = 999;
            AppearConditional = (a) => false;
            Effect = (a) => { };
            SuccessOdds = (a) => 1f;
        }
    }

    class FinishCockVore : SelfActionBase
    {
        public FinishCockVore()
        {
            Name = "FinishCockVore";
            Description = "";
            Class = ClassType.Event;
            Type = SelfActionType.FinishCockVore;
            SoundRange = 0;
            Streaming = false;
            StreamingDescription = "";
            Interrupts = false;
            Silent = false;
            MaxStreamLength = 999;
            AppearConditional = (a) => false;
            Effect = (a) => { };
            SuccessOdds = (a) => 1f;
        }
    }

    class FinishAnalVore : SelfActionBase
    {
        public FinishAnalVore()
        {
            Name = "FinishAnalVore";
            Description = "";
            Class = ClassType.Event;
            Type = SelfActionType.FinishAnalVore;
            SoundRange = 0;
            Streaming = false;
            StreamingDescription = "";
            Interrupts = false;
            Silent = false;
            MaxStreamLength = 999;
            AppearConditional = (a) => false;
            Effect = (a) => { };
            SuccessOdds = (a) => 1f;
        }
    }

    class TurnedOnByNudity : SelfActionBase
    {
        public TurnedOnByNudity()
        {
            Name = "TurnedOnByNudity";
            Description = "";
            Class = ClassType.Event;
            Type = SelfActionType.TurnedOnByNudity;
            SoundRange = 0;
            Streaming = false;
            StreamingDescription = "";
            Interrupts = false;
            Silent = false;
            MaxStreamLength = 999;
            AppearConditional = (a) => false;
            Effect = (a) => { };
            SuccessOdds = (a) => 1f;
        }
    }

    class TurnedOnBySex : SelfActionBase
    {
        public TurnedOnBySex()
        {
            Name = "TurnedOnBySex";
            Description = "";
            Class = ClassType.Event;
            Type = SelfActionType.TurnedOnBySex;
            SoundRange = 0;
            Streaming = false;
            StreamingDescription = "";
            Interrupts = false;
            Silent = false;
            MaxStreamLength = 999;
            AppearConditional = (a) => false;
            Effect = (a) => { };
            SuccessOdds = (a) => 1f;
        }
    }

    class TurnedOnByVore : SelfActionBase
    {
        public TurnedOnByVore()
        {
            Name = "TurnedOnByVore";
            Description = "";
            Class = ClassType.Event;
            Type = SelfActionType.TurnedOnByVore;
            SoundRange = 0;
            Streaming = false;
            StreamingDescription = "";
            Interrupts = false;
            Silent = false;
            MaxStreamLength = 999;
            AppearConditional = (a) => false;
            Effect = (a) => { };
            SuccessOdds = (a) => 1f;
        }
    }

    class TurnedOnByOwnVore : SelfActionBase
    {
        public TurnedOnByOwnVore()
        {
            Name = "TurnedOnByOwnVore";
            Description = "";
            Class = ClassType.Event;
            Type = SelfActionType.TurnedOnByOwnVore;
            SoundRange = 0;
            Streaming = false;
            StreamingDescription = "";
            Interrupts = false;
            Silent = false;
            MaxStreamLength = 999;
            AppearConditional = (a) => false;
            Effect = (a) => { };
            SuccessOdds = (a) => 1f;
        }
    }

    class TurnedOnByDisposal : SelfActionBase
    {
        public TurnedOnByDisposal()
        {
            Name = "TurnedOnByDisposal";
            Description = "";
            Class = ClassType.Event;
            Type = SelfActionType.TurnedOnByDisposal;
            SoundRange = 0;
            Streaming = false;
            StreamingDescription = "";
            Interrupts = false;
            Silent = false;
            MaxStreamLength = 999;
            AppearConditional = (a) => false;
            Effect = (a) => { };
            SuccessOdds = (a) => 1f;
        }
    }

    class SawCheating : SelfActionBase
    {
        public SawCheating()
        {
            Name = "SawCheating";
            Description = "";
            Class = ClassType.Event;
            Type = SelfActionType.SawCheating;
            SoundRange = 0;
            Streaming = false;
            StreamingDescription = "";
            Interrupts = false;
            Silent = false;
            MaxStreamLength = 999;
            AppearConditional = (a) => false;
            Effect = (a) => { };
            SuccessOdds = (a) => 1f;
        }
    }

    class SawAttemptedCheating : SelfActionBase
    {
        public SawAttemptedCheating()
        {
            Name = "SawAttemptedCheating";
            Description = "";
            Class = ClassType.Event;
            Type = SelfActionType.SawAttemptedCheating;
            SoundRange = 0;
            Streaming = false;
            StreamingDescription = "";
            Interrupts = false;
            Silent = false;
            MaxStreamLength = 999;
            AppearConditional = (a) => false;
            Effect = (a) => { };
            SuccessOdds = (a) => 1f;
        }
    }

    class SawSORejectCheating : SelfActionBase
    {
        public SawSORejectCheating()
        {
            Name = "SawSORejectCheating";
            Description = "";
            Class = ClassType.Event;
            Type = SelfActionType.SawSORejectCheating;
            SoundRange = 0;
            Streaming = false;
            StreamingDescription = "";
            Interrupts = false;
            Silent = false;
            MaxStreamLength = 999;
            AppearConditional = (a) => false;
            Effect = (a) => { };
            SuccessOdds = (a) => 1f;
        }
    }

    class GrewBigger : SelfActionBase
    {
        public GrewBigger()
        {
            Name = "GrewBigger";
            Description = "";
            Class = ClassType.Event;
            Type = SelfActionType.GrewBigger;
            SoundRange = 0;
            Streaming = false;
            StreamingDescription = "";
            Interrupts = false;
            Silent = false;
            MaxStreamLength = 999;
            AppearConditional = (a) => false;
            Effect = (a) => { };
            SuccessOdds = (a) => 1f;
        }
    }

    class StomachGrowl : SelfActionBase
    {
        public StomachGrowl()
        {
            Name = "StomachGrowl";
            Description = "";
            Class = ClassType.Event;
            Type = SelfActionType.StomachGrowl;
            SoundRange = 1;
            Streaming = false;
            StreamingDescription = "";
            Interrupts = false;
            Silent = false;
            MaxStreamLength = 999;
            AppearConditional = (a) => false;
            Effect = (a) => { };
            SuccessOdds = (a) => 1f;
        }
    }

    class Stunned : SelfActionBase
    {
        public Stunned()
        {
            Name = "Stunned";
            Description = "";
            Class = ClassType.Event;
            Type = SelfActionType.Stunned;
            SoundRange = 2;
            Streaming = false;
            StreamingDescription = "";
            Interrupts = false;
            Silent = false;
            MaxStreamLength = 999;
            AppearConditional = (a) => false;
            Effect = (a) => { };
            SuccessOdds = (a) => 1f;
        }
    }

    class ScatDisposalBathroom : SelfActionBase
    {
        public ScatDisposalBathroom()
        {
            Name = "ScatDisposalBathroom";
            Description = "Dispose your stomach's contents into a toilet";
            Class = ClassType.Disposal;
            Type = SelfActionType.ScatDisposalBathroom;
            SoundRange = 0;
            Streaming = true;
            StreamingDescription = "Pooping in the toilet";
            Interrupts = true;
            Silent = false;
            MaxStreamLength = 4;
            AppearConditional = (a) =>
                a.BeingEaten == false
                && a.Disposals
                    .Where(
                        s => s.Location == VoreLocation.Stomach || s.Location == VoreLocation.Bowels
                    )
                    .Any()
                && a.ZoneContainsObject(ObjectType.Bathroom);
            Effect = (a) => Dispose(a, VoreLocation.Stomach);
            SuccessOdds = (a) => 1f;
        }
    }

    class ScatDisposalFloor : SelfActionBase
    {
        public ScatDisposalFloor()
        {
            Name = "ScatDisposalFloor";
            Description = "Dispose your stomach's contents onto the floor";
            Class = ClassType.Disposal;
            Type = SelfActionType.ScatDisposalFloor;
            SoundRange = 0;
            Streaming = true;
            StreamingDescription = "Pooping on the floor";
            Interrupts = true;
            Silent = false;
            MaxStreamLength = 4;
            AppearConditional = (a) =>
                a.BeingEaten == false
                && a.Disposals
                    .Where(
                        s => s.Location == VoreLocation.Stomach || s.Location == VoreLocation.Bowels
                    )
                    .Any();
            Effect = (a) => Dispose(a, VoreLocation.Stomach);
            SuccessOdds = (a) => 1f;
        }
    }

    class CockDisposalBathroom : SelfActionBase
    {
        public CockDisposalBathroom()
        {
            Name = "CockDisposalBathroom";
            Description = "Dispose your balls contents into a toilet";
            Class = ClassType.Disposal;
            Type = SelfActionType.CockDisposalBathroom;
            SoundRange = 0;
            Streaming = true;
            StreamingDescription = "Emptying their balls in the Bathroom";
            Interrupts = true;
            Silent = false;
            MaxStreamLength = 4;
            AppearConditional = (a) =>
                a.BeingEaten == false
                && a.Disposals.Where(s => s.Location == VoreLocation.Balls).Any()
                && a.ZoneContainsObject(ObjectType.Bathroom);
            Effect = (a) => Dispose(a, VoreLocation.Balls);
            SuccessOdds = (a) => 1f;
        }
    }

    class CockDisposalFloor : SelfActionBase
    {
        public CockDisposalFloor()
        {
            Name = "CockDisposalFloor";
            Description = "Dispose your balls contents onto the floor";
            Class = ClassType.Disposal;
            Type = SelfActionType.CockDisposalFloor;
            SoundRange = 0;
            Streaming = true;
            StreamingDescription = "Emptying their balls on the floor";
            Interrupts = true;
            Silent = false;
            MaxStreamLength = 4;
            AppearConditional = (a) =>
                a.BeingEaten == false
                && a.Disposals.Where(s => s.Location == VoreLocation.Balls).Any();
            Effect = (a) => Dispose(a, VoreLocation.Balls);
            SuccessOdds = (a) => 1f;
        }
    }

    class UnbirthDisposalBathroom : SelfActionBase
    {
        public UnbirthDisposalBathroom()
        {
            Name = "UnbirthDisposalBathroom";
            Description = "Dispose your womb's contents into a toilet";
            Class = ClassType.Disposal;
            Type = SelfActionType.UnbirthDisposalBathroom;
            SoundRange = 0;
            Streaming = true;
            StreamingDescription = "Emptying their womb in the Bathroom";
            Interrupts = true;
            Silent = false;
            MaxStreamLength = 4;
            AppearConditional = (a) =>
                a.BeingEaten == false
                && a.Disposals.Where(s => s.Location == VoreLocation.Womb).Any()
                && a.ZoneContainsObject(ObjectType.Bathroom);
            Effect = (a) => Dispose(a, VoreLocation.Womb);
            SuccessOdds = (a) => 1f;
        }
    }

    class UnbirthDisposalFloor : SelfActionBase
    {
        public UnbirthDisposalFloor()
        {
            Name = "UnbirthDisposalFloor";
            Description = "Dispose your womb's contents onto the floor";
            Class = ClassType.Disposal;
            Type = SelfActionType.UnbirthDisposalFloor;
            SoundRange = 0;
            Streaming = true;
            StreamingDescription = "Emptying their womb on the floor";
            Interrupts = true;
            Silent = false;
            MaxStreamLength = 4;
            AppearConditional = (a) =>
                a.BeingEaten == false
                && a.Disposals.Where(s => s.Location == VoreLocation.Womb).Any();
            Effect = (a) => Dispose(a, VoreLocation.Womb);
            SuccessOdds = (a) => 1f;
        }
    }

    // SELF ACTION SPELLS

    class CastPassdoor : SelfActionBase
    {
        public CastPassdoor()
        {
            Name = "CastPassdoor";
            Description = "Use magic to phase through doors for a while";
            Class = ClassType.CastSelf;
            Type = SelfActionType.CastPassdoor;
            SoundRange = -1;
            Streaming = false;
            StreamingDescription = "";
            Interrupts = false;
            Silent = false;
            MaxStreamLength = 999;
            AppearConditional = (a) => State.World.Settings.SpellDurationMod != 0 && CanUseMagic(a);
            SuccessOdds = (a) => SelfSpellcastOdds(this, a, 0.1f);
            Effect = (a) => CastPassdoor(a);
        }
    }

    class CastHealSelf : SelfActionBase
    {
        public CastHealSelf()
        {
            Name = "CastHealSelf";
            Description = "Use magic to heal your wounds";
            Class = ClassType.CastSelf;
            Type = SelfActionType.CastHealSelf;
            SoundRange = -1;
            Streaming = false;
            StreamingDescription = "";
            Interrupts = false;
            Silent = false;
            MaxStreamLength = 999;
            AppearConditional = (a) => a.Health < (Constants.HealthMax / 1.5) && CanUseMagic(a);
            SuccessOdds = (a) => SelfSpellcastOdds(this, a, 0.0f);
            Effect = (a) => CastHeal(a);
        }
    }

    class CastGrowSelf : SelfActionBase
    {
        public CastGrowSelf()
        {
            Name = "CastGrowSelf";
            Description = "Use magic to enlarge yourself for a while";
            Class = ClassType.CastSelf;
            Type = SelfActionType.CastGrowSelf;
            SoundRange = -1;
            Streaming = false;
            StreamingDescription = "";
            Interrupts = false;
            Silent = false;
            MaxStreamLength = 999;
            AppearConditional = (a) =>
                State.World.Settings.SizeChangeMod != 1
                && State.World.Settings.SpellDurationMod != 0
                && CanUseMagic(a);
            SuccessOdds = (a) => SelfSpellcastOdds(this, a, 0.4f);
            Effect = (a) => CastGrow(a);
        }
    }

    class CastShrinkSelf : SelfActionBase
    {
        public CastShrinkSelf()
        {
            Name = "CastShrinkSelf";
            Description = "Use magic to shrink yourself for a while";
            Class = ClassType.CastSelf;
            Type = SelfActionType.CastShrinkSelf;
            SoundRange = -1;
            Streaming = false;
            StreamingDescription = "";
            Interrupts = false;
            Silent = false;
            MaxStreamLength = 999;
            AppearConditional = (a) =>
                State.World.Settings.SizeChangeMod != 1
                && State.World.Settings.SpellDurationMod != 0
                && CanUseMagic(a);
            SuccessOdds = (a) => SelfSpellcastOdds(this, a, -0.1f);
            Effect = (a) => CastShrink(a);
        }
    }

    class CastPreyCurseSelf : SelfActionBase
    {
        public CastPreyCurseSelf()
        {
            Name = "CastPreyCurseSelf";
            Description = "Use magic to make yourself appear more appetizing to potential predators for a while";
            Class = ClassType.CastSelf;
            Type = SelfActionType.CastPreyCurseSelf;
            SoundRange = -1;
            Streaming = false;
            StreamingDescription = "";
            Interrupts = false;
            Silent = false;
            MaxStreamLength = 999;
            AppearConditional = (a) => a.Personality.PreyWillingness >= State.World.Settings.WillingThreshold
                && State.World.Settings.SpellDurationMod != 0
                && CanUseMagic(a);
            SuccessOdds = (a) => SelfSpellcastOdds(this, a, -0.2f);
            Effect = (a) => CastPreyCurse(a);
        }
    }

    // MAGIC EVENTS
    class ResetShrink : SelfActionBase
    {
        public ResetShrink()
        {
            Name = "ResetShrink";
            Description = "";
            Class = ClassType.Event;
            Type = SelfActionType.ResetShrink;
            SoundRange = 1;
            Streaming = false;
            StreamingDescription = "";
            Interrupts = false;
            Silent = false;
            MaxStreamLength = 999;
            AppearConditional = (a) => false;
            Effect = (a) => { };
            SuccessOdds = (a) => 1f;
        }
    }

    class ResetGrow : SelfActionBase
    {
        public ResetGrow()
        {
            Name = "ResetGrow";
            Description = "";
            Class = ClassType.Event;
            Type = SelfActionType.ResetGrow;
            SoundRange = 1;
            Streaming = false;
            StreamingDescription = "";
            Interrupts = false;
            Silent = false;
            MaxStreamLength = 999;
            AppearConditional = (a) => false;
            Effect = (a) => { };
            SuccessOdds = (a) => 1f;
        }
    }

    class JustShrunk : SelfActionBase
    {
        public JustShrunk()
        {
            Name = "JustShrunk";
            Description = "";
            Class = ClassType.Event;
            Type = SelfActionType.JustShrunk;
            SoundRange = 1;
            Streaming = false;
            StreamingDescription = "";
            Interrupts = false;
            Silent = false;
            MaxStreamLength = 999;
            AppearConditional = (a) => false;
            Effect = (a) => { };
            SuccessOdds = (a) => 1f;
        }
    }

    class JustGrew : SelfActionBase
    {
        public JustGrew()
        {
            Name = "JustGrew";
            Description = "";
            Class = ClassType.Event;
            Type = SelfActionType.JustGrew;
            SoundRange = 1;
            Streaming = false;
            StreamingDescription = "";
            Interrupts = false;
            Silent = false;
            MaxStreamLength = 999;
            AppearConditional = (a) => false;
            Effect = (a) => { };
            SuccessOdds = (a) => 1f;
        }
    }


    class CharmEnded : SelfActionBase
    {
        public CharmEnded()
        {
            Name = "CharmEnded";
            Description = "";
            Class = ClassType.Event;
            Type = SelfActionType.CharmEnded;
            SoundRange = 1;
            Streaming = false;
            StreamingDescription = "";
            Interrupts = false;
            Silent = false;
            MaxStreamLength = 999;
            AppearConditional = (a) => false;
            Effect = (a) => CharmEnded(a);
            SuccessOdds = (a) => 1f;
        }
    }
}
