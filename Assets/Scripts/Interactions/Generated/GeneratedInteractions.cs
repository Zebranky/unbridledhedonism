using static InteractionFunctions;
using static HelperFunctions;
using static InteractionOddsFunctions;
using static UnityEngine.GraphicsBuffer;

namespace GeneratedInteractions
{
    class Meet : InteractionBase
    {
        public Meet()
        {
            Name = "Meet";
            Description = "Introduce yourself to them.";
            Class = ClassType.Friendly;
            Type = InteractionType.Meet;
            Range = 0;
            SoundRange = 2;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "";
            AsksPlayer = false;
            UsedOnPrey = false;
            Hostile = false;
            AppearConditional = (a, t) => a.HasRelationshipWith(t) == false;
            SuccessOdds = (a, t) => 1;
            SuccessEffect = (a, t) => Meet(a, t);
        }
    }

    class HostileMeet : InteractionBase
    {
        public HostileMeet()
        {
            Name = "HostileMeet";
            Description = "";
            Class = ClassType.Friendly;
            Type = InteractionType.HostileMeet;
            Range = 0;
            SoundRange = 2;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "";
            AsksPlayer = false;
            UsedOnPrey = false;
            Hostile = false;
            AppearConditional = (a, t) => false;
            SuccessOdds = (a, t) => 1;
            SuccessEffect = (a, t) => Meet(a, t);
        }
    }

    class Talk : InteractionBase
    {
        public Talk()
        {
            Name = "Talk";
            Description = "Talk with them about a variety of topics.";
            Class = ClassType.Friendly;
            Type = InteractionType.Talk;
            Range = 0;
            SoundRange = 2;
            Streaming = true;
            StreamingDescription = "Talking to";
            AsksPlayerDescription = "wants to talk to you";
            AsksPlayer = true;
            UsedOnPrey = false;
            Hostile = false;
            AppearConditional = (a, t) => true;
            SuccessOdds = (a, t) => FriendshipCheckOdds(this, a, t, -.15f);
            SuccessEffect = (a, t) => IncreaseFriendshipSymetric(a, t, .015f, .015f);
        }
    }

    class Compliment : InteractionBase
    {
        public Compliment()
        {
            Name = "Compliment";
            Description = "Compliment them.";
            Class = ClassType.Friendly;
            Type = InteractionType.Compliment;
            Range = 0;
            SoundRange = 2;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "";
            AsksPlayer = false;
            UsedOnPrey = false;
            Hostile = false;
            AppearConditional = (a, t) => true;
            SuccessOdds = (a, t) => FriendshipCheckOdds(this, a, t, -.3f);
            SuccessEffect = (a, t) => Compliment(a, t);
        }
    }

    class AskWait : InteractionBase
    {
        public AskWait()
        {
            Name = "AskWait";
            Description =
                "Call out to them to ask them to wait at their current location for a few turns.";
            Class = ClassType.Friendly;
            Type = InteractionType.AskWait;
            Range = 3;
            SoundRange = 3;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "";
            AsksPlayer = false;
            UsedOnPrey = false;
            Hostile = false;
            AppearConditional = (a, t) => true;
            SuccessOdds = (a, t) => FriendshipCheckOdds(this, a, t, 0.4f);
            SuccessEffect = (a, t) => t.AI.SetWait(3);
        }
    }

    class AskIfSingle : InteractionBase
    {
        public AskIfSingle()
        {
            Name = "AskIfSingle";
            Description = "Ask them if they are dating anyone.";
            Class = ClassType.Friendly;
            Type = InteractionType.AskIfSingle;
            Range = 0;
            SoundRange = 1;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "asks if you'll tell them whether you're single";
            AsksPlayer = true;
            UsedOnPrey = false;
            Hostile = false;
            AppearConditional = (a, t) => a.Romance.Dating != t;
            SuccessOdds = (a, t) => FriendshipCheckOdds(this, a, t, 0);
            SuccessEffect = (a, t) => AskIfSingle(a, t);
        }
    }

    class AskToFollow : InteractionBase
    {
        public AskToFollow()
        {
            Name = "AskToFollow";
            Description = "Ask them to follow you for a while.";
            Class = ClassType.Friendly;
            Type = InteractionType.AskToFollow;
            Range = 1;
            SoundRange = 2;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "asks you to follow them";
            AsksPlayer = true;
            UsedOnPrey = false;
            Hostile = false;
            AppearConditional = (a, t) => true;
            SuccessOdds = (a, t) => FriendshipCheckOdds(this, a, t, 0.4f);
            SuccessEffect = (a, t) => t.AI.FollowPerson(a);
        }
    }

    class AskToEnter : InteractionBase
    {
        public AskToEnter()
        {
            Name = "AskToEnter";
            Description = "Ask if you can enter their room.";
            Class = ClassType.Friendly;
            Type = InteractionType.AskToEnter;
            Range = 1;
            SoundRange = 2;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "asks to come inside your room";
            AsksPlayer = true;
            UsedOnPrey = false;
            Hostile = false;
            AppearConditional = (a, t) =>
                t.ZoneContainsObject(ObjectType.Bed)
                && t.BeingEaten == false
                && a.ZoneContainsObject(ObjectType.Bed) == false
                && (a.Romance.Dating != t || State.World.Settings.DatingEntry == false);
            SuccessOdds = (a, t) => FriendshipCheckOdds(this, a, t, 0.5f);
            SuccessEffect = (a, t) => a.AI.FollowPerson(t);
        }
    }

    class StopFollowingMe : InteractionBase
    {
        public StopFollowingMe()
        {
            Name = "StopFollowingMe";
            Description = "Tell them to stop following you.";
            Class = ClassType.Friendly;
            Type = InteractionType.StopFollowingMe;
            Range = 1;
            SoundRange = 1;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "";
            AsksPlayer = false;
            UsedOnPrey = false;
            Hostile = true;
            AppearConditional = (a, t) => t.AI.IsFollowing(a);
            SuccessOdds = (a, t) => 1;
            SuccessEffect = (a, t) => t.AI.FollowPerson(null);
        }
    }

    class DoneFollowingYou : InteractionBase
    {
        public DoneFollowingYou()
        {
            Name = "DoneFollowingYou";
            Description = "They tell you they're done following you.";
            Class = ClassType.Friendly;
            Type = InteractionType.DoneFollowingYou;
            Range = 1;
            SoundRange = 1;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "";
            AsksPlayer = false;
            UsedOnPrey = false;
            Hostile = true;
            AppearConditional = (a, t) => false;
            SuccessOdds = (a, t) => 1;
            SuccessEffect = (a, t) => a.AI.FollowPerson(null);
        }
    }

    class FriendlyHug : InteractionBase
    {
        public FriendlyHug()
        {
            Name = "FriendlyHug";
            Description = "Give them a non-romantic hug.";
            Class = ClassType.Friendly;
            Type = InteractionType.FriendlyHug;
            Range = 0;
            SoundRange = -1;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "wants to non-romantically hug you";
            AsksPlayer = true;
            UsedOnPrey = false;
            Hostile = false;
            AppearConditional = (a, t) => true;
            SuccessOdds = (a, t) => FriendshipCheckOdds(this, a, t, 0.2f);
            SuccessEffect = (a, t) => FriendlyHug(a, t);
        }
    }

    class ShoveIn : InteractionBase
    {
        public ShoveIn()
        {
            Name = "ShoveIn";
            Description =
                "Shoves their prey in a bit, accelerating the speed of swallowing.  The pred will like this, and the prey will not like this if they are unwilling.";
            Class = ClassType.Friendly;
            Type = InteractionType.ShoveIn;
            Range = 0;
            SoundRange = -1;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "";
            AsksPlayer = false;
            UsedOnPrey = false;
            Hostile = false;
            AppearConditional = (a, t) => t.VoreController.Swallowing(VoreLocation.Any);
            SuccessOdds = (a, t) => 1;
            SuccessEffect = (a, t) => ShoveInPrey(a, t);
        }
    }

    class Push : InteractionBase
    {
        public Push()
        {
            Name = "Push";
            Description = "Aggressively shove them, stunning them for a turn.";
            Class = ClassType.Unfriendly;
            Type = InteractionType.Push;
            Range = 0;
            SoundRange = 1;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "";
            AsksPlayer = false;
            UsedOnPrey = false;
            Hostile = true;
            AppearConditional = (a, t) => true;
            SuccessOdds = (a, t) => StrengthCheckOdds(a, t, 0.1f);
            SuccessEffect = (a, t) => Push(a, t);
            FailEffect = (a, t) => Insult(a, t, 0.05f, true);
        }
    }

    class Insult : InteractionBase
    {
        public Insult()
        {
            Name = "Insult";
            Description = "Insult some element of them.";
            Class = ClassType.Unfriendly;
            Type = InteractionType.Insult;
            Range = 0;
            SoundRange = 1;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "";
            AsksPlayer = false;
            UsedOnPrey = false;
            Hostile = true;
            AppearConditional = (a, t) => true;
            SuccessOdds = (a, t) => 1;
            SuccessEffect = (a, t) => Insult(a, t, 0.05f);
        }
    }

    class Flirt : InteractionBase
    {
        public Flirt()
        {
            Name = "Flirt";
            Description = "Romantically flirt with them a little.";
            Class = ClassType.Romantic;
            Type = InteractionType.Flirt;
            Range = 0;
            SoundRange = 1;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "wants to flirt with you";
            AsksPlayer = true;
            UsedOnPrey = false;
            Hostile = false;
            AppearConditional = (a, t) => true;
            SuccessOdds = (a, t) => RomanticCheckOdds(this, a, t, 0);
            SuccessEffect = (a, t) => Flirt(a, t);
            FailEffect = (a, t) => RomanticRejection(a, t, .5f);
        }
    }

    class Hug : InteractionBase
    {
        public Hug()
        {
            Name = "Hug";
            Description = "Give them a romantic hug.";
            Class = ClassType.Romantic;
            Type = InteractionType.Hug;
            Range = 0;
            SoundRange = -1;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "wants to romantically hug you";
            AsksPlayer = true;
            UsedOnPrey = false;
            Hostile = false;
            AppearConditional = (a, t) => true;
            SuccessOdds = (a, t) => RomanticCheckOdds(this, a, t, 0);
            SuccessEffect = (a, t) => Flirt(a, t);
            FailEffect = (a, t) => RomanticRejection(a, t);
        }
    }

    class Kiss : InteractionBase
    {
        public Kiss()
        {
            Name = "Kiss";
            Description = "Give them a romantic kiss.";
            Class = ClassType.Romantic;
            Type = InteractionType.Kiss;
            Range = 0;
            SoundRange = -1;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "wants to kiss you";
            AsksPlayer = true;
            UsedOnPrey = false;
            Hostile = false;
            AppearConditional = (a, t) => true;
            SuccessOdds = (a, t) => RomanticCheckOdds(this, a, t, .2f);
            SuccessEffect = (a, t) => Kiss(a, t);
            FailEffect = (a, t) => RomanticRejection(a, t);
        }
    }

    class MakeOut : InteractionBase
    {
        public MakeOut()
        {
            Name = "MakeOut";
            Description = "Start making out with them ";
            Class = ClassType.Romantic;
            Type = InteractionType.MakeOut;
            Range = 0;
            SoundRange = -1;
            Streaming = true;
            StreamingDescription = "Making out with";
            AsksPlayerDescription = "wants to make out with you";
            AsksPlayer = true;
            UsedOnPrey = false;
            Hostile = false;
            AppearConditional = (a, t) => true;
            SuccessOdds = (a, t) => RomanticCheckOdds(this, a, t, .25f);
            SuccessEffect = (a, t) => MakeOut(a, t);
            FailEffect = (a, t) => RomanticRejection(a, t);
        }
    }

    class Taste : InteractionBase
    {
        public Taste()
        {
            Name = "Taste";
            Description =
                "Taste them (The act is considered both romantic, and a potential prelude to vore)";
            Class = ClassType.Romantic;
            Type = InteractionType.Taste;
            Range = 0;
            SoundRange = -1;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "wants to taste you \n(this act is considered romantic)";
            AsksPlayer = true;
            UsedOnPrey = false;
            Hostile = false;
            AppearConditional = (a, t) =>
                a.VoreController.CouldVoreTarget(t, VoreType.Oral, DigestionAlias.Any);
            SuccessOdds = (a, t) => TasteCheckOdds(this, a, t);
            SuccessEffect = (a, t) => Taste(a, t);
            FailEffect = (a, t) => RomanticRejection(a, t);
        }
    }

    class ComplimentAppearance : InteractionBase
    {
        public ComplimentAppearance()
        {
            Name = "ComplimentAppearance";
            Description = "Compliments their general appearance and sexual appeal.";
            Class = ClassType.Romantic;
            Type = InteractionType.ComplimentAppearance;
            Range = 0;
            SoundRange = -1;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "wants to compliment your appearance and sexual appeal";
            AsksPlayer = true;
            UsedOnPrey = false;
            Hostile = false;
            AppearConditional = (a, t) => true;
            SuccessOdds = (a, t) => RomanticCheckOdds(this, a, t, -.15f);
            SuccessEffect = (a, t) => ComplimentAppearance(a, t);
            FailEffect = (a, t) => RomanticRejection(a, t, .4f);
        }
    }

    class AskOut : InteractionBase
    {
        public AskOut()
        {
            Name = "AskOut";
            Description = "Ask them if they're willing to date you exclusively.";
            Class = ClassType.Romantic;
            Type = InteractionType.AskOut;
            Range = 0;
            SoundRange = -1;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "asks you if you'll go out with them";
            AsksPlayer = true;
            UsedOnPrey = false;
            Hostile = false;
            AppearConditional = (a, t) => a.Romance.IsDating == false;
            SuccessOdds = (a, t) => RomanticCheckOdds(this, a, t, .4f);
            SuccessEffect = (a, t) => AskOut(a, t);
            FailEffect = (a, t) => AskOutFail(a, t);
        }
    }

    class BreakUp : InteractionBase
    {
        public BreakUp()
        {
            Name = "BreakUp";
            Description = "Break up with them and end your dating relationship.";
            Class = ClassType.Unfriendly;
            Type = InteractionType.BreakUp;
            Range = 0;
            SoundRange = -1;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "";
            AsksPlayer = false;
            UsedOnPrey = false;
            Hostile = true;
            AppearConditional = (a, t) => a.Romance.Dating == t;
            SuccessOdds = (a, t) => 1;
            SuccessEffect = (a, t) => BreakUp(a, t);
            FailEffect = (a, t) => RomanticRejection(a, t);
        }
    }

    class StartSex : InteractionBase
    {
        public StartSex()
        {
            Name = "StartSex";
            Description = "Ask them to start a sexual encounter.";
            Class = ClassType.Romantic;
            Type = InteractionType.StartSex;
            Range = 0;
            SoundRange = -1;
            Streaming = true;
            StreamingDescription = "Having sex with";
            AsksPlayerDescription = "wants to have sex";
            AsksPlayer = true;
            UsedOnPrey = false;
            Hostile = false;
            AppearConditional = (a, t) => a.ActiveSex == null && t.ActiveSex == null;
            SuccessOdds = (a, t) => RomanticCheckOdds(this, a, t, .7f);
            SuccessEffect = (a, t) => StartSex(a, t);
            FailEffect = (a, t) => RomanticRejection(a, t);
        }
    }

    class OutragedAtSex : InteractionBase
    {
        public OutragedAtSex()
        {
            Name = "OutragedAtSex";
            Description = "";
            Class = ClassType.Event;
            Type = InteractionType.OutragedAtSex;
            Range = 0;
            SoundRange = 3;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "";
            AsksPlayer = false;
            UsedOnPrey = false;
            Hostile = false;
            AppearConditional = (a, t) => false;
            SuccessOdds = (a, t) => 1;
            SuccessEffect = (a, t) => OutragedAtSex(a, t);
        }
    }

    class OutragedAtDisposal : InteractionBase
    {
        public OutragedAtDisposal()
        {
            Name = "OutragedAtDisposal";
            Description = "";
            Class = ClassType.Event;
            Type = InteractionType.OutragedAtDisposal;
            Range = 0;
            SoundRange = 3;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "";
            AsksPlayer = false;
            UsedOnPrey = false;
            Hostile = false;
            AppearConditional = (a, t) => false;
            SuccessOdds = (a, t) => 1;
            SuccessEffect = (a, t) => OutragedAtDisposal(a, t);
        }
    }

    class PleasedAtDisposal : InteractionBase
    {
        public PleasedAtDisposal()
        {
            Name = "PleasedAtDisposal";
            Description = "";
            Class = ClassType.Event;
            Type = InteractionType.PleasedAtDisposal;
            Range = 0;
            SoundRange = 3;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "";
            AsksPlayer = false;
            UsedOnPrey = false;
            Hostile = false;
            AppearConditional = (a, t) => false;
            SuccessOdds = (a, t) => 1;
        }
    }

    class OutragedAtCockDisposal : InteractionBase
    {
        public OutragedAtCockDisposal()
        {
            Name = "OutragedAtCockDisposal";
            Description = "";
            Class = ClassType.Event;
            Type = InteractionType.OutragedAtCockDisposal;
            Range = 0;
            SoundRange = 3;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "";
            AsksPlayer = false;
            UsedOnPrey = false;
            Hostile = false;
            AppearConditional = (a, t) => false;
            SuccessOdds = (a, t) => 1;
            SuccessEffect = (a, t) => OutragedAtDisposal(a, t);
        }
    }

    class PleasedAtCockDisposal : InteractionBase
    {
        public PleasedAtCockDisposal()
        {
            Name = "PleasedAtCockDisposal";
            Description = "";
            Class = ClassType.Event;
            Type = InteractionType.PleasedAtCockDisposal;
            Range = 0;
            SoundRange = 3;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "";
            AsksPlayer = false;
            UsedOnPrey = false;
            Hostile = false;
            AppearConditional = (a, t) => false;
            SuccessOdds = (a, t) => 1;
        }
    }

    class OutragedAtWombDisposal : InteractionBase
    {
        public OutragedAtWombDisposal()
        {
            Name = "OutragedAtWombDisposal";
            Description = "";
            Class = ClassType.Event;
            Type = InteractionType.OutragedAtWombDisposal;
            Range = 0;
            SoundRange = 3;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "";
            AsksPlayer = false;
            UsedOnPrey = false;
            Hostile = false;
            AppearConditional = (a, t) => false;
            SuccessOdds = (a, t) => 1;
            SuccessEffect = (a, t) => OutragedAtDisposal(a, t);
        }
    }

    class PleasedAtWombDisposal : InteractionBase
    {
        public PleasedAtWombDisposal()
        {
            Name = "PleasedAtWombDisposal";
            Description = "";
            Class = ClassType.Event;
            Type = InteractionType.PleasedAtWombDisposal;
            Range = 0;
            SoundRange = 3;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "";
            AsksPlayer = false;
            UsedOnPrey = false;
            Hostile = false;
            AppearConditional = (a, t) => false;
            SuccessOdds = (a, t) => 1;
        }
    }

    class SexPartnerEaten : InteractionBase
    {
        public SexPartnerEaten()
        {
            Name = "SexPartnerEaten";
            Description = "";
            Class = ClassType.Event;
            Type = InteractionType.SexPartnerEaten;
            Range = 0;
            SoundRange = 3;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "";
            AsksPlayer = false;
            UsedOnPrey = false;
            Hostile = false;
            AppearConditional = (a, t) => false;
            SuccessOdds = (a, t) => 1;
        }
    }

    class OralVore : InteractionBase
    {
        public OralVore()
        {
            Name = "OralVore";
            Description = "Try to eat them without their consent.";
            Class = ClassType.VoreConsuming;
            Type = InteractionType.OralVore;
            Range = 0;
            SoundRange = -1;
            Streaming = true;
            StreamingDescription = "Eating";
            AsksPlayerDescription = "";
            AsksPlayer = false;
            UsedOnPrey = false;
            Hostile = true;
            AppearConditional = (a, t) =>
                a.VoreController.CouldVoreTarget(t, VoreType.Oral, DigestionAlias.Any);
            SuccessOdds = (a, t) => VoreCheckOdds(a, t, VoreType.Oral);
            SuccessEffect = (a, t) => Vore(a, t, VoreType.Oral);
            FailEffect = (a, t) => VoreRefused(a, t, a.VoreController.PartCurrentlyDigests(VoreLocation.Stomach));
        }
    }

    class Unbirth : InteractionBase
    {
        public Unbirth()
        {
            Name = "Unbirth";
            Description = "Try to unbirth them without their consent.";
            Class = ClassType.VoreConsuming;
            Type = InteractionType.Unbirth;
            Range = 0;
            SoundRange = -1;
            Streaming = true;
            StreamingDescription = "Unbirthing";
            AsksPlayerDescription = "";
            AsksPlayer = false;
            UsedOnPrey = false;
            Hostile = true;
            AppearConditional = (a, t) =>
                a.VoreController.CouldVoreTarget(t, VoreType.Unbirth, DigestionAlias.Any);
            SuccessOdds = (a, t) => VoreCheckOdds(a, t, VoreType.Unbirth);
            SuccessEffect = (a, t) => Vore(a, t, VoreType.Unbirth);
            FailEffect = (a, t) => VoreRefused(a, t, a.VoreController.PartCurrentlyDigests(VoreLocation.Womb));
        }
    }

    class CockVore : InteractionBase
    {
        public CockVore()
        {
            Name = "CockVore";
            Description = "Try to cock vore them without their consent.";
            Class = ClassType.VoreConsuming;
            Type = InteractionType.CockVore;
            Range = 0;
            SoundRange = -1;
            Streaming = true;
            StreamingDescription = "Cock Voring";
            AsksPlayerDescription = "";
            AsksPlayer = false;
            UsedOnPrey = false;
            Hostile = true;
            AppearConditional = (a, t) =>
                a.VoreController.CouldVoreTarget(t, VoreType.Cock, DigestionAlias.Any);
            SuccessOdds = (a, t) => VoreCheckOdds(a, t, VoreType.Cock);
            SuccessEffect = (a, t) => Vore(a, t, VoreType.Cock);
            FailEffect = (a, t) => VoreRefused(a, t, a.VoreController.PartCurrentlyDigests(VoreLocation.Balls));
        }
    }

    class AnalVore : InteractionBase
    {
        public AnalVore()
        {
            Name = "AnalVore";
            Description = "Try to anal vore them without their consent.";
            Class = ClassType.VoreConsuming;
            Type = InteractionType.AnalVore;
            Range = 0;
            SoundRange = -1;
            Streaming = true;
            StreamingDescription = "Anal Voring";
            AsksPlayerDescription = "";
            AsksPlayer = false;
            UsedOnPrey = false;
            Hostile = true;
            AppearConditional = (a, t) =>
                a.VoreController.CouldVoreTarget(t, VoreType.Anal, DigestionAlias.Any);
            SuccessOdds = (a, t) => VoreCheckOdds(a, t, VoreType.Anal);
            SuccessEffect = (a, t) => Vore(a, t, VoreType.Anal);
            FailEffect = (a, t) => VoreRefused(a, t, a.VoreController.PartCurrentlyDigests(VoreLocation.Bowels));
        }
    }

    class AskToOralVore : InteractionBase
    {
        public AskToOralVore()
        {
            Name = "AskToOralVore";
            Description =
                "Ask them if they would be willing to let you eat them.  Non-fatal is assumed.";
            Class = ClassType.VoreAskThem;
            Type = InteractionType.AskToOralVore;
            Range = 0;
            SoundRange = -1;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "asks to oral vore you but not digest you";
            AsksPlayer = true;
            UsedOnPrey = false;
            Hostile = false;
            AppearConditional = (a, t) =>
                a.VoreController.CouldVoreTarget(t, VoreType.Oral, DigestionAlias.CanEndo);
            SuccessOdds = (a, t) => WillingVoreCheckOdds(this, a, t, VoreType.Oral);
            SuccessEffect = (a, t) => AskToVore(a, t, VoreType.Oral, false);
        }
    }

    class AskToUnbirth : InteractionBase
    {
        public AskToUnbirth()
        {
            Name = "AskToUnbirth";
            Description =
                "Ask them if they would be willing to let you unbirth them.  Non-fatal is assumed.";
            Class = ClassType.VoreAskThem;
            Type = InteractionType.AskToUnbirth;
            Range = 0;
            SoundRange = -1;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "asks to unbirth you but not melt you";
            AsksPlayer = true;
            UsedOnPrey = false;
            Hostile = false;
            AppearConditional = (a, t) =>
                a.VoreController.CouldVoreTarget(t, VoreType.Unbirth, DigestionAlias.CanEndo);
            SuccessOdds = (a, t) => WillingVoreCheckOdds(this, a, t, VoreType.Unbirth);
            SuccessEffect = (a, t) => AskToVore(a, t, VoreType.Unbirth, false);
        }
    }

    class AskToCockVore : InteractionBase
    {
        public AskToCockVore()
        {
            Name = "AskToCockVore";
            Description =
                "Ask them if they would be willing to let you cock vore them.  Non-fatal is assumed.";
            Class = ClassType.VoreAskThem;
            Type = InteractionType.AskToCockVore;
            Range = 0;
            SoundRange = -1;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "asks to cock vore you but not melt you";
            AsksPlayer = true;
            UsedOnPrey = false;
            Hostile = false;
            AppearConditional = (a, t) =>
                a.VoreController.CouldVoreTarget(t, VoreType.Cock, DigestionAlias.CanEndo);
            SuccessOdds = (a, t) => WillingVoreCheckOdds(this, a, t, VoreType.Cock);
            SuccessEffect = (a, t) => AskToVore(a, t, VoreType.Cock, false);
        }
    }

    class AskToAnalVore : InteractionBase
    {
        public AskToAnalVore()
        {
            Name = "AskToAnalVore";
            Description =
                "Ask them if they would be willing to let you anal vore them.  Non-fatal is assumed.";
            Class = ClassType.VoreAskThem;
            Type = InteractionType.AskToAnalVore;
            Range = 0;
            SoundRange = -1;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "asks to anal vore you but not digest you";
            AsksPlayer = true;
            UsedOnPrey = false;
            Hostile = false;
            AppearConditional = (a, t) =>
                a.VoreController.CouldVoreTarget(t, VoreType.Anal, DigestionAlias.CanEndo);
            SuccessOdds = (a, t) => WillingVoreCheckOdds(this, a, t, VoreType.Anal);
            SuccessEffect = (a, t) => AskToVore(a, t, VoreType.Anal, false);
        }
    }

    class AskToOralVoreDigest : InteractionBase
    {
        public AskToOralVoreDigest()
        {
            Name = "AskToOralVoreDigest";
            Description =
                "Ask them if they would be willing to let you eat them.  Digestion is assumed.";
            Class = ClassType.VoreAskThem;
            Type = InteractionType.AskToOralVoreDigest;
            Range = 0;
            SoundRange = -1;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "asks to oral vore you and digest you";
            AsksPlayer = true;
            UsedOnPrey = false;
            Hostile = false;
            AppearConditional = (a, t) =>
                a.VoreController.CouldVoreTarget(t, VoreType.Oral, DigestionAlias.CanVore);
            SuccessOdds = (a, t) => WillingVoreDigestCheckOdds(this, a, t, VoreType.Oral);
            SuccessEffect = (a, t) => AskToVore(a, t, VoreType.Oral, true);
        }
    }

    class AskToUnbirthDigest : InteractionBase
    {
        public AskToUnbirthDigest()
        {
            Name = "AskToUnbirthDigest";
            Description =
                "Ask them if they would be willing to let you unbirth them.  Digestion is assumed.";
            Class = ClassType.VoreAskThem;
            Type = InteractionType.AskToUnbirthDigest;
            Range = 0;
            SoundRange = -1;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "asks to unbirth you and melt you";
            AsksPlayer = true;
            UsedOnPrey = false;
            Hostile = false;
            AppearConditional = (a, t) =>
                a.VoreController.CouldVoreTarget(t, VoreType.Unbirth, DigestionAlias.CanVore);
            SuccessOdds = (a, t) => WillingVoreDigestCheckOdds(this, a, t, VoreType.Unbirth);
            SuccessEffect = (a, t) => AskToVore(a, t, VoreType.Unbirth, true);
        }
    }

    class AskToCockVoreDigest : InteractionBase
    {
        public AskToCockVoreDigest()
        {
            Name = "AskToCockVoreDigest";
            Description =
                "Ask them if they would be willing to let you cock vore them.  Digestion is assumed.";
            Class = ClassType.VoreAskThem;
            Type = InteractionType.AskToCockVoreDigest;
            Range = 0;
            SoundRange = -1;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "asks to cock vore you and melt you";
            AsksPlayer = true;
            UsedOnPrey = false;
            Hostile = false;
            AppearConditional = (a, t) =>
                a.VoreController.CouldVoreTarget(t, VoreType.Cock, DigestionAlias.CanVore);
            SuccessOdds = (a, t) => WillingVoreDigestCheckOdds(this, a, t, VoreType.Cock);
            SuccessEffect = (a, t) => AskToVore(a, t, VoreType.Cock, true);
        }
    }

    class AskToAnalVoreDigest : InteractionBase
    {
        public AskToAnalVoreDigest()
        {
            Name = "AskToAnalVoreDigest";
            Description =
                "Ask them if they would be willing to let you anal vore them.  Digestion is assumed.";
            Class = ClassType.VoreAskThem;
            Type = InteractionType.AskToAnalVoreDigest;
            Range = 0;
            SoundRange = -1;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "asks to anal vore you and digest you";
            AsksPlayer = true;
            UsedOnPrey = false;
            Hostile = false;
            AppearConditional = (a, t) =>
                a.VoreController.CouldVoreTarget(t, VoreType.Anal, DigestionAlias.CanVore);
            SuccessOdds = (a, t) => WillingVoreDigestCheckOdds(this, a, t, VoreType.Anal);
            SuccessEffect = (a, t) => AskToVore(a, t, VoreType.Anal, true);
        }
    }

    class AskToBeOralVored : InteractionBase
    {
        public AskToBeOralVored()
        {
            Name = "AskToBeOralVored";
            Description = "Offer yourself as a willing meal to this person.";
            Class = ClassType.VoreAskToBe;
            Type = InteractionType.AskToBeOralVored;
            Range = 0;
            SoundRange = -1;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "asks you to oral vore them and digest them";
            AsksPlayer = true;
            UsedOnPrey = false;
            Hostile = false;
            AppearConditional = (a, t) =>
                t.VoreController.CouldVoreTarget(a, VoreType.Oral, DigestionAlias.CanVore);
            SuccessOdds = (a, t) => AskToBeVoredOdds(this, a, t, false, VoreType.Oral);
            SuccessEffect = (a, t) => AskToBeVored(a, t, VoreType.Oral);
        }
    }

    class AskToBeOralVoredEndo : InteractionBase
    {
        public AskToBeOralVoredEndo()
        {
            Name = "AskToBeOralVoredEndo";
            Description = "Offer yourself to be swallowed and not digested.  ";
            Class = ClassType.VoreAskToBe;
            Type = InteractionType.AskToBeOralVoredEndo;
            Range = 0;
            SoundRange = -1;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "asks you to oral vore them but not digest them";
            AsksPlayer = true;
            UsedOnPrey = false;
            Hostile = false;
            AppearConditional = (a, t) =>
                t.VoreController.CouldVoreTarget(a, VoreType.Oral, DigestionAlias.CanEndo);
            SuccessOdds = (a, t) => AskToBeVoredOdds(this, a, t, true, VoreType.Oral);
            SuccessEffect = (a, t) => AskToBeVoredEndo(a, t, VoreType.Oral);
        }
    }

    class AskToBeUnbirthed : InteractionBase
    {
        public AskToBeUnbirthed()
        {
            Name = "AskToBeUnbirthed";
            Description = "Offer yourself to be melted in this person's womb.";
            Class = ClassType.VoreAskToBe;
            Type = InteractionType.AskToBeUnbirthed;
            Range = 0;
            SoundRange = -1;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "asks you to unbirth them and melt them";
            AsksPlayer = true;
            UsedOnPrey = false;
            Hostile = false;
            AppearConditional = (a, t) =>
                t.VoreController.CouldVoreTarget(a, VoreType.Unbirth, DigestionAlias.CanVore);
            SuccessOdds = (a, t) => AskToBeVoredOdds(this, a, t, false, VoreType.Unbirth);
            SuccessEffect = (a, t) => AskToBeVored(a, t, VoreType.Unbirth);
        }
    }

    class AskToBeUnbirthedEndo : InteractionBase
    {
        public AskToBeUnbirthedEndo()
        {
            Name = "AskToBeUnbirthedEndo";
            Description = "Offer yourself to be held in this person's womb.";
            Class = ClassType.VoreAskToBe;
            Type = InteractionType.AskToBeUnbirthedEndo;
            Range = 0;
            SoundRange = -1;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "asks you to unbirth them but not melt them";
            AsksPlayer = true;
            UsedOnPrey = false;
            Hostile = false;
            AppearConditional = (a, t) =>
                t.VoreController.CouldVoreTarget(a, VoreType.Unbirth, DigestionAlias.CanEndo);
            SuccessOdds = (a, t) => AskToBeVoredOdds(this, a, t, true, VoreType.Unbirth);
            SuccessEffect = (a, t) => AskToBeVoredEndo(a, t, VoreType.Unbirth);
        }
    }

    class AskToBeCockVored : InteractionBase
    {
        public AskToBeCockVored()
        {
            Name = "AskToBeCockVored";
            Description = "Offer yourself to be melted in this person's balls.";
            Class = ClassType.VoreAskToBe;
            Type = InteractionType.AskToBeCockVored;
            Range = 0;
            SoundRange = -1;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "asks you to cock vore them and melt them";
            AsksPlayer = true;
            UsedOnPrey = false;
            Hostile = false;
            AppearConditional = (a, t) =>
                t.VoreController.CouldVoreTarget(a, VoreType.Cock, DigestionAlias.CanVore);
            SuccessOdds = (a, t) => AskToBeVoredOdds(this, a, t, false, VoreType.Cock);
            SuccessEffect = (a, t) => AskToBeVored(a, t, VoreType.Cock);
        }
    }

    class AskToBeCockVoredEndo : InteractionBase
    {
        public AskToBeCockVoredEndo()
        {
            Name = "AskToBeCockVoredEndo";
            Description = "Offer yourself to be held in this person's balls.";
            Class = ClassType.VoreAskToBe;
            Type = InteractionType.AskToBeCockVoredEndo;
            Range = 0;
            SoundRange = -1;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "asks you to cock vore them but not melt them";
            AsksPlayer = true;
            UsedOnPrey = false;
            Hostile = false;
            AppearConditional = (a, t) =>
                t.VoreController.CouldVoreTarget(a, VoreType.Cock, DigestionAlias.CanEndo);
            SuccessOdds = (a, t) => AskToBeVoredOdds(this, a, t, true, VoreType.Cock);
            SuccessEffect = (a, t) => AskToBeVoredEndo(a, t, VoreType.Cock);
        }
    }

    class AskToBeAnalVored : InteractionBase
    {
        public AskToBeAnalVored()
        {
            Name = "AskToBeAnalVored";
            Description =
                "Offer yourself as a willing meal to this person, but not through the mouth.";
            Class = ClassType.VoreAskToBe;
            Type = InteractionType.AskToBeAnalVored;
            Range = 0;
            SoundRange = -1;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "asks you to anal vore them and digest them";
            AsksPlayer = true;
            UsedOnPrey = false;
            Hostile = false;
            AppearConditional = (a, t) =>
                t.VoreController.CouldVoreTarget(a, VoreType.Anal, DigestionAlias.CanVore);
            SuccessOdds = (a, t) => AskToBeVoredOdds(this, a, t, false, VoreType.Anal);
            SuccessEffect = (a, t) => AskToBeVored(a, t, VoreType.Anal);
        }
    }

    class AskToBeAnalVoredEndo : InteractionBase
    {
        public AskToBeAnalVoredEndo()
        {
            Name = "AskToBeAnalVoredEndo";
            Description = "Offer yourself to be anally taken in and not digested.";
            Class = ClassType.VoreAskToBe;
            Type = InteractionType.AskToBeAnalVoredEndo;
            Range = 0;
            SoundRange = -1;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "asks you to anal vore them but not digest them";
            AsksPlayer = true;
            UsedOnPrey = false;
            Hostile = false;
            AppearConditional = (a, t) =>
                t.VoreController.CouldVoreTarget(a, VoreType.Anal, DigestionAlias.CanEndo);
            SuccessOdds = (a, t) => AskToBeVoredOdds(this, a, t, true, VoreType.Anal);
            SuccessEffect = (a, t) => AskToBeVoredEndo(a, t, VoreType.Anal);
        }
    }

    class AskThemToStrip : InteractionBase
    {
        public AskThemToStrip()
        {
            Name = "AskThemToStrip";
            Description =
                "Ask them to strip.\n  They're most likely to agree if they are romantically involved, or if their promiscuity or prey willingness is high.  ";
            Class = ClassType.Romantic;
            Type = InteractionType.AskThemToStrip;
            Range = 0;
            SoundRange = -1;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "wants you to strip";
            AsksPlayer = true;
            UsedOnPrey = false;
            Hostile = false;
            AppearConditional = (a, t) => t.ClothingStatus != ClothingStatus.Nude;
            SuccessOdds = (a, t) => RomanticCheckOdds(this, a, t, .5f);
            SuccessEffect = (a, t) => Strip(t, true);
        }
    }

    class FreeStomachPrey : InteractionBase
    {
        public FreeStomachPrey()
        {
            Name = "FreeStomachPrey";
            Description = "Try to free a living prey from within them.  This uses up some energy.";
            Class = ClassType.Unfriendly;
            Type = InteractionType.FreeStomachPrey;
            Range = 0;
            SoundRange = -1;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "";
            AsksPlayer = false;
            UsedOnPrey = false;
            Hostile = true;
            AppearConditional = (a, t) => t.VoreController.GetLiving(VoreLocation.Stomach) != null;
            SuccessOdds = (a, t) => GetFreeingOdds(a, t, VoreLocation.Stomach);
            SuccessEffect = (a, t) => FreePrey(a, t, VoreLocation.Stomach);
            FailEffect = (a, t) => FailedFreePrey(a, t);
        }
    }

    class FreeWombPrey : InteractionBase
    {
        public FreeWombPrey()
        {
            Name = "FreeWombPrey";
            Description = "Try to free a living prey from within them.  This uses up some energy.";
            Class = ClassType.Unfriendly;
            Type = InteractionType.FreeWombPrey;
            Range = 0;
            SoundRange = -1;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "";
            AsksPlayer = false;
            UsedOnPrey = false;
            Hostile = true;
            AppearConditional = (a, t) => t.VoreController.GetLiving(VoreLocation.Womb) != null;
            SuccessOdds = (a, t) => GetFreeingOdds(a, t, VoreLocation.Womb);
            SuccessEffect = (a, t) => FreePrey(a, t, VoreLocation.Womb);
            FailEffect = (a, t) => FailedFreePrey(a, t);
        }
    }

    class FreeBallsPrey : InteractionBase
    {
        public FreeBallsPrey()
        {
            Name = "FreeBallsPrey";
            Description = "Try to free a living prey from within them.  This uses up some energy.";
            Class = ClassType.Unfriendly;
            Type = InteractionType.FreeBallsPrey;
            Range = 0;
            SoundRange = -1;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "";
            AsksPlayer = false;
            UsedOnPrey = false;
            Hostile = true;
            AppearConditional = (a, t) => t.VoreController.GetLiving(VoreLocation.Balls) != null;
            SuccessOdds = (a, t) => GetFreeingOdds(a, t, VoreLocation.Balls);
            SuccessEffect = (a, t) => FreePrey(a, t, VoreLocation.Balls);
            FailEffect = (a, t) => FailedFreePrey(a, t);
        }
    }

    class FreeAnalPrey : InteractionBase
    {
        public FreeAnalPrey()
        {
            Name = "FreeAnalPrey";
            Description = "Try to free a living prey from within them.  This uses up some energy.";
            Class = ClassType.Unfriendly;
            Type = InteractionType.FreeAnalPrey;
            Range = 0;
            SoundRange = -1;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "";
            AsksPlayer = false;
            UsedOnPrey = false;
            Hostile = true;
            AppearConditional = (a, t) => t.VoreController.GetLiving(VoreLocation.Bowels) != null;
            SuccessOdds = (a, t) => GetFreeingOdds(a, t, VoreLocation.Bowels);
            SuccessEffect = (a, t) => FreePrey(a, t, VoreLocation.Bowels);
            FailEffect = (a, t) => FailedFreePrey(a, t);
        }
    }

    class OutragedAtOralVore : InteractionBase
    {
        public OutragedAtOralVore()
        {
            Name = "OutragedAtOralVore";
            Description = "";
            Class = ClassType.Event;
            Type = InteractionType.OutragedAtOralVore;
            Range = 0;
            SoundRange = 2;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "";
            AsksPlayer = false;
            UsedOnPrey = false;
            Hostile = false;
            AppearConditional = (a, t) => false;
            SuccessOdds = (a, t) => 1;
        }
    }

    class PleasedAtOralVore : InteractionBase
    {
        public PleasedAtOralVore()
        {
            Name = "PleasedAtOralVore";
            Description = "";
            Class = ClassType.Event;
            Type = InteractionType.PleasedAtOralVore;
            Range = 0;
            SoundRange = 2;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "";
            AsksPlayer = false;
            UsedOnPrey = false;
            Hostile = false;
            AppearConditional = (a, t) => false;
            SuccessOdds = (a, t) => 1;
        }
    }

    class OutragedAtUnbirth : InteractionBase
    {
        public OutragedAtUnbirth()
        {
            Name = "OutragedAtUnbirth";
            Description = "";
            Class = ClassType.Event;
            Type = InteractionType.OutragedAtUnbirth;
            Range = 0;
            SoundRange = 2;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "";
            AsksPlayer = false;
            UsedOnPrey = false;
            Hostile = false;
            AppearConditional = (a, t) => false;
            SuccessOdds = (a, t) => 1;
        }
    }

    class PleasedAtUnbirth : InteractionBase
    {
        public PleasedAtUnbirth()
        {
            Name = "PleasedAtUnbirth";
            Description = "";
            Class = ClassType.Event;
            Type = InteractionType.PleasedAtUnbirth;
            Range = 0;
            SoundRange = 2;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "";
            AsksPlayer = false;
            UsedOnPrey = false;
            Hostile = false;
            AppearConditional = (a, t) => false;
            SuccessOdds = (a, t) => 1;
        }
    }

    class OutragedAtCockVore : InteractionBase
    {
        public OutragedAtCockVore()
        {
            Name = "OutragedAtCockVore";
            Description = "";
            Class = ClassType.Event;
            Type = InteractionType.OutragedAtCockVore;
            Range = 0;
            SoundRange = 2;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "";
            AsksPlayer = false;
            UsedOnPrey = false;
            Hostile = false;
            AppearConditional = (a, t) => false;
            SuccessOdds = (a, t) => 1;
        }
    }

    class PleasedAtCockVore : InteractionBase
    {
        public PleasedAtCockVore()
        {
            Name = "PleasedAtCockVore";
            Description = "";
            Class = ClassType.Event;
            Type = InteractionType.PleasedAtCockVore;
            Range = 0;
            SoundRange = 2;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "";
            AsksPlayer = false;
            UsedOnPrey = false;
            Hostile = false;
            AppearConditional = (a, t) => false;
            SuccessOdds = (a, t) => 1;
        }
    }

    class OutragedAtAnalVore : InteractionBase
    {
        public OutragedAtAnalVore()
        {
            Name = "OutragedAtAnalVore";
            Description = "";
            Class = ClassType.Event;
            Type = InteractionType.OutragedAtAnalVore;
            Range = 0;
            SoundRange = 2;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "";
            AsksPlayer = false;
            UsedOnPrey = false;
            Hostile = false;
            AppearConditional = (a, t) => false;
            SuccessOdds = (a, t) => 1;
        }
    }

    class PleasedAtAnalVore : InteractionBase
    {
        public PleasedAtAnalVore()
        {
            Name = "PleasedAtAnalVore";
            Description = "";
            Class = ClassType.Event;
            Type = InteractionType.PleasedAtAnalVore;
            Range = 0;
            SoundRange = 2;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "";
            AsksPlayer = false;
            UsedOnPrey = false;
            Hostile = false;
            AppearConditional = (a, t) => false;
            SuccessOdds = (a, t) => 1;
        }
    }

    class UnexpectedDigestion : InteractionBase
    {
        public UnexpectedDigestion()
        {
            Name = "UnexpectedDigestion";
            Description = "";
            Class = ClassType.Vore;
            Type = InteractionType.UnexpectedDigestion;
            Range = 0;
            SoundRange = 3;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "heats up as acids begin to tinge your body.\nAllow yourself to be digested?";
            AsksPlayer = true;
            UsedOnPrey = true;
            Hostile = false;
            AppearConditional = (a, t) => false;
            SuccessOdds = (a, t) => AllowDigestOdds(this, a, t) - 0.3f;
            SuccessEffect = (a, t) => AskIfCanDigest(a, t);
            FailEffect = (a, t) => UnexpectedDigestion(t, a);
        }
    }

    class UnexpectedHeat : InteractionBase
    {
        public UnexpectedHeat()
        {
            Name = "UnexpectedHeat";
            Description = "";
            Class = ClassType.Vore;
            Type = InteractionType.UnexpectedHeat;
            Range = 0;
            SoundRange = 3;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "heats up as the flesh around you squeezes tighter.\nAllow yourself to be melted?";
            AsksPlayer = true;
            UsedOnPrey = true;
            Hostile = false;
            AppearConditional = (a, t) => false;
            SuccessOdds = (a, t) => AllowDigestOdds(this, a, t) - 0.3f;
            SuccessEffect = (a, t) => AskIfCanDigest(a, t);
            FailEffect = (a, t) => UnexpectedDigestion(t, a);
        }
    }

    class PreyImmune : InteractionBase
    {
        public PreyImmune()
        {
            Name = "PreyImmune";
            Description = "";
            Class = ClassType.Event;
            Type = InteractionType.PreyImmune;
            Range = 0;
            SoundRange = 2;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "";
            AsksPlayer = false;
            UsedOnPrey = false;
            Hostile = false;
            AppearConditional = (a, t) => false;
            SuccessOdds = (a, t) => 1;
        }
    }

    class PreyEscapes : InteractionBase
    {
        public PreyEscapes()
        {
            Name = "PreyEscapes";
            Description = "";
            Class = ClassType.Event;
            Type = InteractionType.PreyEscapes;
            Range = 0;
            SoundRange = -1;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "";
            AsksPlayer = false;
            UsedOnPrey = false;
            Hostile = false;
            AppearConditional = (a, t) => false;
            SuccessOdds = (a, t) => 1;
            SuccessEffect = (a, t) =>
                a.VoreController.GetProgressOf(t)?.FreePrey(allowFreedMessage: false);
        }
    }

    class PreyShiftsIntoStomach : InteractionBase
    {
        public PreyShiftsIntoStomach()
        {
            Name = "PreyShiftsIntoStomach";
            Description = "";
            Class = ClassType.Event;
            Type = InteractionType.PreyShiftsIntoStomach;
            Range = 0;
            SoundRange = -1;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "";
            AsksPlayer = false;
            UsedOnPrey = false;
            Hostile = false;
            AppearConditional = (a, t) => false;
            SuccessOdds = (a, t) => 1;
        }
    }

    class PreyFreedIntoStomach : InteractionBase
    {
        public PreyFreedIntoStomach()
        {
            Name = "PreyFreedIntoStomach";
            Description = "";
            Class = ClassType.Event;
            Type = InteractionType.PreyFreedIntoStomach;
            Range = 0;
            SoundRange = -1;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "";
            AsksPlayer = false;
            UsedOnPrey = false;
            Hostile = false;
            AppearConditional = (a, t) => false;
            SuccessOdds = (a, t) => 1;
        }
    }

    class PreyShiftsIntoWomb : InteractionBase
    {
        public PreyShiftsIntoWomb()
        {
            Name = "PreyShiftsIntoWomb";
            Description = "";
            Class = ClassType.Event;
            Type = InteractionType.PreyShiftsIntoWomb;
            Range = 0;
            SoundRange = -1;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "";
            AsksPlayer = false;
            UsedOnPrey = false;
            Hostile = false;
            AppearConditional = (a, t) => false;
            SuccessOdds = (a, t) => 1;
        }
    }

    class PreyFreedIntoWomb : InteractionBase
    {
        public PreyFreedIntoWomb()
        {
            Name = "PreyFreedIntoWomb";
            Description = "";
            Class = ClassType.Event;
            Type = InteractionType.PreyFreedIntoWomb;
            Range = 0;
            SoundRange = -1;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "";
            AsksPlayer = false;
            UsedOnPrey = false;
            Hostile = false;
            AppearConditional = (a, t) => false;
            SuccessOdds = (a, t) => 1;
        }
    }

    class PreyShiftsIntoBalls : InteractionBase
    {
        public PreyShiftsIntoBalls()
        {
            Name = "PreyShiftsIntoBalls";
            Description = "";
            Class = ClassType.Event;
            Type = InteractionType.PreyShiftsIntoBalls;
            Range = 0;
            SoundRange = -1;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "";
            AsksPlayer = false;
            UsedOnPrey = false;
            Hostile = false;
            AppearConditional = (a, t) => false;
            SuccessOdds = (a, t) => 1;
        }
    }

    class PreyFreedIntoBalls : InteractionBase
    {
        public PreyFreedIntoBalls()
        {
            Name = "PreyFreedIntoBalls";
            Description = "";
            Class = ClassType.Event;
            Type = InteractionType.PreyFreedIntoBalls;
            Range = 0;
            SoundRange = -1;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "";
            AsksPlayer = false;
            UsedOnPrey = false;
            Hostile = false;
            AppearConditional = (a, t) => false;
            SuccessOdds = (a, t) => 1;
        }
    }

    class PreyShiftsIntoAnus : InteractionBase
    {
        public PreyShiftsIntoAnus()
        {
            Name = "PreyShiftsIntoAnus";
            Description = "";
            Class = ClassType.Event;
            Type = InteractionType.PreyShiftsIntoAnus;
            Range = 0;
            SoundRange = -1;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "";
            AsksPlayer = false;
            UsedOnPrey = false;
            Hostile = false;
            AppearConditional = (a, t) => false;
            SuccessOdds = (a, t) => 1;
        }
    }

    class PreyFreedIntoAnus : InteractionBase
    {
        public PreyFreedIntoAnus()
        {
            Name = "PreyFreedIntoAnus";
            Description = "";
            Class = ClassType.Event;
            Type = InteractionType.PreyFreedIntoAnus;
            Range = 0;
            SoundRange = -1;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "";
            AsksPlayer = false;
            UsedOnPrey = false;
            Hostile = false;
            AppearConditional = (a, t) => false;
            SuccessOdds = (a, t) => 1;
        }
    }

    class TellPreyTheyWillBeDigested : InteractionBase
    {
        public TellPreyTheyWillBeDigested()
        {
            Name = "TellPreyTheyWillBeDigested";
            Description = "";
            Class = ClassType.Event;
            Type = InteractionType.TellPreyTheyWillBeDigested;
            Range = 0;
            SoundRange = -1;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "";
            AsksPlayer = false;
            UsedOnPrey = false;
            Hostile = false;
            AppearConditional = (a, t) => false;
            SuccessOdds = (a, t) => 1;
        }
    }

    class TellPreyTheyWillBeMelted : InteractionBase
    {
        public TellPreyTheyWillBeMelted()
        {
            Name = "TellPreyTheyWillBeMelted";
            Description = "";
            Class = ClassType.Event;
            Type = InteractionType.TellPreyTheyWillBeMelted;
            Range = 0;
            SoundRange = -1;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "";
            AsksPlayer = false;
            UsedOnPrey = false;
            Hostile = false;
            AppearConditional = (a, t) => false;
            SuccessOdds = (a, t) => 1;
        }
    }

    class VoreTeased : InteractionBase
    {
        public VoreTeased()
        {
            Name = "VoreTeased";
            Description = "";
            Class = ClassType.Event;
            Type = InteractionType.VoreTeased;
            Range = 0;
            SoundRange = 1;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "";
            AsksPlayer = false;
            UsedOnPrey = false;
            Hostile = false;
            AppearConditional = (a, t) => false;
            SuccessOdds = (a, t) => 1;
        }
    }

    class VoreTeasedEndo : InteractionBase
    {
        public VoreTeasedEndo()
        {
            Name = "VoreTeasedEndo";
            Description = "";
            Class = ClassType.Event;
            Type = InteractionType.VoreTeasedEndo;
            Range = 0;
            SoundRange = 1;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "";
            AsksPlayer = false;
            UsedOnPrey = false;
            Hostile = false;
            AppearConditional = (a, t) => false;
            SuccessOdds = (a, t) => 1;
        }
    }

    class PreyKnockOutOtherPrey : InteractionBase
    {
        public PreyKnockOutOtherPrey()
        {
            Name = "PreyKnockOutOtherPrey";
            Description = "";
            Class = ClassType.Event;
            Type = InteractionType.PreyKnockOutOtherPrey;
            Range = 0;
            SoundRange = -1;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "";
            AsksPlayer = false;
            UsedOnPrey = false;
            Hostile = false;
            AppearConditional = (a, t) => false;
            SuccessOdds = (a, t) => 1;
        }
    }

    class SpitUpPrey : InteractionBase
    {
        public SpitUpPrey()
        {
            Name = "SpitUpPrey";
            Description =
                "Regurgitate your prey.  If they hate you they'll hate you somewhat less after you release them.";
            Class = ClassType.Friendly;
            Type = InteractionType.SpitUpPrey;
            Range = 0;
            SoundRange = -1;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "";
            AsksPlayer = false;
            UsedOnPrey = true;
            Hostile = true;
            AppearConditional = (a, t) =>
                a.VoreController.GetProgressOf(t, VoreLocation.Stomach)?.IsAlive() ?? false;
            SuccessOdds = (a, t) => 1;
            SuccessEffect = (a, t) => a.VoreController.GetProgressOf(t)?.FreePrey(true, false);
        }
    }

    class BirthPrey : InteractionBase
    {
        public BirthPrey()
        {
            Name = "BirthPrey";
            Description =
                "Birth your prey.  If they hate you they'll hate you somewhat less after you release them.";
            Class = ClassType.Friendly;
            Type = InteractionType.BirthPrey;
            Range = 0;
            SoundRange = -1;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "";
            AsksPlayer = false;
            UsedOnPrey = true;
            Hostile = true;
            AppearConditional = (a, t) =>
                a.VoreController.GetProgressOf(t, VoreLocation.Womb)?.IsAlive() ?? false;
            SuccessOdds = (a, t) => 1;
            SuccessEffect = (a, t) => a.VoreController.GetProgressOf(t)?.FreePrey(true, false);
        }
    }

    class ReleaseCockPrey : InteractionBase
    {
        public ReleaseCockPrey()
        {
            Name = "ReleaseCockPrey";
            Description =
                "Release your prey back out through your cock.  If they hate you they'll hate you somewhat less after you release them.";
            Class = ClassType.Friendly;
            Type = InteractionType.ReleaseCockPrey;
            Range = 0;
            SoundRange = -1;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "";
            AsksPlayer = false;
            UsedOnPrey = true;
            Hostile = true;
            AppearConditional = (a, t) =>
                a.VoreController.GetProgressOf(t, VoreLocation.Balls)?.IsAlive() ?? false;
            SuccessOdds = (a, t) => 1;
            SuccessEffect = (a, t) => a.VoreController.GetProgressOf(t)?.FreePrey(true, false);
        }
    }

    class ReleaseAnalPrey : InteractionBase
    {
        public ReleaseAnalPrey()
        {
            Name = "ReleaseAnalPrey";
            Description =
                "Release your prey back out through your anus.  If they hate you they'll hate you somewhat less after you release them.";
            Class = ClassType.Friendly;
            Type = InteractionType.ReleaseAnalPrey;
            Range = 0;
            SoundRange = -1;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "";
            AsksPlayer = false;
            UsedOnPrey = true;
            Hostile = true;
            AppearConditional = (a, t) =>
                a.VoreController.GetProgressOf(t, VoreLocation.Bowels)?.IsAlive() ?? false;
            SuccessOdds = (a, t) => 1;
            SuccessEffect = (a, t) => a.VoreController.GetProgressOf(t)?.FreePrey(true, false);
        }
    }

    class RubBelly : InteractionBase
    {
        public RubBelly()
        {
            Name = "RubBelly";
            Description = "Rub their expanded belly.";
            Class = ClassType.Friendly;
            Type = InteractionType.RubBelly;
            Range = 0;
            SoundRange = -1;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "wants to rub your belly";
            AsksPlayer = false;
            UsedOnPrey = false;
            Hostile = false;
            AppearConditional = (a, t) => t.VoreController.HasBellyPreySwallowed();
            SuccessOdds = (a, t) => BellyRubOdds(this, a, t, 0.1f);
            SuccessEffect = (a, t) => RubBelly(a, t);
        }
    }

    class RubBalls : InteractionBase
    {
        public RubBalls()
        {
            Name = "RubBalls";
            Description = "Rub their expanded balls.";
            Class = ClassType.Romantic;
            Type = InteractionType.RubBalls;
            Range = 0;
            SoundRange = -1;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "wants to rub your balls";
            AsksPlayer = true;
            UsedOnPrey = false;
            Hostile = false;
            AppearConditional = (a, t) => t.VoreController.HasBallsPreySwallowed();
            SuccessOdds = (a, t) => BellyRubOdds(this, a, t, 0);
            SuccessEffect = (a, t) => RubBalls(a, t);
        }
    }

    class AskForBellyRub : InteractionBase
    {
        public AskForBellyRub()
        {
            Name = "AskForBellyRub";
            Description = "Place their hands on your expanded belly.";
            Class = ClassType.Friendly;
            Type = InteractionType.AskForBellyRub;
            Range = 0;
            SoundRange = -1;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "wants you to rub their belly";
            AsksPlayer = true;
            UsedOnPrey = false;
            Hostile = false;
            AppearConditional = (a, t) => a.VoreController.HasBellyPreySwallowed();
            SuccessOdds = (a, t) => BellyRubAskOdds(this, a, t, 0.1f);
            SuccessEffect = (a, t) => AskRubBelly(a, t);
        }
    }

    class AskForBallsRub : InteractionBase
    {
        public AskForBallsRub()
        {
            Name = "AskForBallsRub";
            Description = "Place their hands on your expanded balls.";
            Class = ClassType.Friendly;
            Type = InteractionType.AskForBallsRub;
            Range = 0;
            SoundRange = -1;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "wants you to rub their balls";
            AsksPlayer = true;
            UsedOnPrey = false;
            Hostile = false;
            AppearConditional = (a, t) => a.VoreController.HasBallsPreySwallowed();
            SuccessOdds = (a, t) => BellyRubAskOdds(this, a, t, 0.1f);
            SuccessEffect = (a, t) => AskRubBalls(a, t);
        }
    }

    class AskIfCanDigest : InteractionBase
    {
        public AskIfCanDigest()
        {
            Name = "AskIfCanDigest";
            Description = "Ask your prey if they'd be willing to be digested.";
            Class = ClassType.Friendly;
            Type = InteractionType.AskIfCanDigest;
            Range = 0;
            SoundRange = -1;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "wants to digest you";
            AsksPlayer = true;
            UsedOnPrey = true;
            Hostile = false;
            AppearConditional = (a, t) =>
                t.BeingEaten
                && CanDigestPrey(a, t)
                && a.VoreController.GetProgressOf(t).ExpectingEndosoma
                && t.Dead == false
                && (
                    a.VoreController.GetProgressOf(t).Location == VoreLocation.Stomach
                    || a.VoreController.GetProgressOf(t).Location == VoreLocation.Bowels
                );
            SuccessOdds = (a, t) =>
                a.VoreController.GetProgressOf(t).TriedConverting ? 0 : AllowDigestOdds(this, a, t);
            SuccessEffect = (a, t) => AskIfCanDigest(a, t);
            FailEffect = (a, t) => a.VoreController.GetProgressOf(t).TriedConverting = true;
        }
    }

    class AskIfCanMelt : InteractionBase
    {
        public AskIfCanMelt()
        {
            Name = "AskIfCanMelt";
            Description = "Ask your prey if they'd be willing to be melted.";
            Class = ClassType.Friendly;
            Type = InteractionType.AskIfCanMelt;
            Range = 0;
            SoundRange = -1;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "wants to melt you";
            AsksPlayer = true;
            UsedOnPrey = true;
            Hostile = false;
            AppearConditional = (a, t) =>
                t.BeingEaten
                && CanDigestPrey(a, t)
                && a.VoreController.GetProgressOf(t).ExpectingEndosoma
                && t.Dead == false
                && (
                    a.VoreController.GetProgressOf(t).Location != VoreLocation.Stomach
                    && a.VoreController.GetProgressOf(t).Location != VoreLocation.Bowels
                );
            SuccessOdds = (a, t) =>
                a.VoreController.GetProgressOf(t).TriedConverting ? 0 : AllowDigestOdds(this, a, t);
            SuccessEffect = (a, t) => AskIfCanDigest(a, t);
            FailEffect = (a, t) => a.VoreController.GetProgressOf(t).TriedConverting = true;
        }
    }

    class TransferPreyToStomach : InteractionBase
    {
        public TransferPreyToStomach()
        {
            Name = "TransferPreyToStomach";
            Description = "Move your prey internally from your bowels into your stomach.";
            Class = ClassType.Vore;
            Type = InteractionType.TransferPreyToStomach;
            Range = 0;
            SoundRange = -1;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "";
            AsksPlayer = false;
            UsedOnPrey = true;
            Hostile = true;
            AppearConditional = (a, t) =>
                t.BeingEaten
                && t.FindMyPredator() == a
                && a.VoreController.GetProgressOf(t).Location == VoreLocation.Bowels;
            SuccessOdds = (a, t) => 1;
            SuccessEffect = (a, t) => MovePreyToStomach(a, t);
        }
    }

    class TransferPreyToBowels : InteractionBase
    {
        public TransferPreyToBowels()
        {
            Name = "TransferPreyToBowels";
            Description = "Move your prey internally from your stomach into your bowels.";
            Class = ClassType.Vore;
            Type = InteractionType.TransferPreyToBowels;
            Range = 0;
            SoundRange = -1;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "";
            AsksPlayer = false;
            UsedOnPrey = true;
            Hostile = true;
            AppearConditional = (a, t) =>
                t.BeingEaten
                && t.FindMyPredator() == a
                && a.VoreController.GetProgressOf(t).Location == VoreLocation.Stomach
                && State.World.Settings.AnalVoreGoesDirectlyToStomach == false;
            SuccessOdds = (a, t) => 1;
            SuccessEffect = (a, t) => MovePreyToBowels(a, t);
        }
    }

    class TalkWithPrey : InteractionBase
    {
        public TalkWithPrey()
        {
            Name = "TalkWithPrey";
            Description = "Talk with someone who's inside of someone else.";
            Class = ClassType.Friendly;
            Type = InteractionType.TalkWithPrey;
            Range = 0;
            SoundRange = 0;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "";
            AsksPlayer = false;
            UsedOnPrey = true;
            Hostile = false;
            AppearConditional = (a, t) =>
                t.BeingEaten
                && t.FindMyPredator().VoreController.GetProgressOf(t).Willing
                && t.Dead == false
                && (a.BeingEaten == false || t.FindMyPredator() == a);
            SuccessOdds = (a, t) => FriendshipCheckOdds(this, a, t, -.15f);
            SuccessEffect = (a, t) => IncreaseFriendshipSymetric(a, t, .015f, .015f);
        }
    }

    class PlayfulTeasePrey : InteractionBase
    {
        public PlayfulTeasePrey()
        {
            Name = "PlayfulTeasePrey";
            Description = "Playfully tease the prey inside of someone.";
            Class = ClassType.Friendly;
            Type = InteractionType.PlayfulTeasePrey;
            Range = 0;
            SoundRange = 0;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "";
            AsksPlayer = false;
            UsedOnPrey = true;
            Hostile = false;
            AppearConditional = (a, t) =>
                t.BeingEaten
                && t.FindMyPredator().VoreController.GetProgressOf(t).Willing
                && t.Dead == false
                && (a.BeingEaten == false || t.FindMyPredator() == a);
            SuccessOdds = (a, t) => 1;
            SuccessEffect = (a, t) => IncreaseFriendshipSymetric(t, a, .05f, .005f);
        }
    }

    class TauntPrey : InteractionBase
    {
        public TauntPrey()
        {
            Name = "TauntPrey";
            Description = "Taunt or insult prey inside of someone.";
            Class = ClassType.Unfriendly;
            Type = InteractionType.TauntPrey;
            Range = 0;
            SoundRange = -1;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "";
            AsksPlayer = false;
            UsedOnPrey = true;
            Hostile = true;
            AppearConditional = (a, t) =>
                t.BeingEaten
                && t.FindMyPredator().VoreController.GetProgressOf(t).Willing == false
                && t.Dead == false
                && (a.BeingEaten == false || t.FindMyPredator() == a);
            SuccessOdds = (a, t) => 1;
            SuccessEffect = (a, t) => Insult(a, t, 0.04f);
        }
    }

    class SoothePrey : InteractionBase
    {
        public SoothePrey()
        {
            Name = "SoothePrey";
            Description = "Try to soothe and relax prey inside of someone.";
            Class = ClassType.Friendly;
            Type = InteractionType.SoothePrey;
            Range = 0;
            SoundRange = -1;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "is trying to calm you down. Become willing?";
            AsksPlayer = true;
            UsedOnPrey = true;
            Hostile = false;
            AppearConditional = (a, t) =>
                t.BeingEaten
                && t.FindMyPredator().VoreController.GetProgressOf(t).Willing == false
                && t.Dead == false
                && a.BeingEaten == false;
            SuccessOdds = (a, t) => SoothePreyOdds(this, a, t, 0.2f);
            SuccessEffect = (a, t) => t.FindMyPredator().VoreController.GetProgressOf(t).Willing = true;
        }
    }

    class PreySootheOtherPrey : InteractionBase
    {
        public PreySootheOtherPrey()
        {
            Name = "PreySootheOtherPrey";
            Description = "Try to soothe and relax your fellow prey.";
            Class = ClassType.Vore;
            Type = InteractionType.PreySootheOtherPrey;
            Range = 0;
            SoundRange = -1;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "is trying to calm you down.";
            AsksPlayer = true;
            UsedOnPrey = true;
            Hostile = false;
            AppearConditional = (a, t) =>
                (a.FindMyPredator()?.VoreController.AreSisterPrey(a, t) ?? false)
                && a.BeingSwallowed == false
                && t.FindMyPredator().VoreController.GetProgressOf(a).Willing
                && t.FindMyPredator().VoreController.GetProgressOf(t).Willing == false
                && t.Dead == false;
            SuccessOdds = (a, t) => SoothePreyOdds(this, a, t, 0.1f);
            SuccessEffect = (a, t) => t.FindMyPredator().VoreController.GetProgressOf(t).Willing = true;
        }
    }

    class StopDigesting : InteractionBase
    {
        public StopDigesting()
        {
            Name = "StopDigesting";
            Description = "Stop digesting this and any sister prey.";
            Class = ClassType.Friendly;
            Type = InteractionType.StopDigesting;
            Range = 0;
            SoundRange = -1;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "";
            AsksPlayer = false;
            UsedOnPrey = true;
            Hostile = false;
            AppearConditional = (a, t) => false;
            SuccessOdds = (a, t) => 1;
            SuccessEffect = (a, t) => StopDigest(t, a);
        }
    }

    class PreyWillingYell : InteractionBase
    {
        public PreyWillingYell()
        {
            Name = "PreyWillingYell";
            Description = "Cry out in happy excitement.";
            Class = ClassType.Vore;
            Type = InteractionType.PreyWillingYell;
            Range = 999;
            SoundRange = 3;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "";
            AsksPlayer = false;
            UsedOnPrey = false;
            Hostile = false;
            AppearConditional = (a, t) =>
                a.FindMyPredator() == t
                && t.VoreController.GetProgressOf(a).Willing
                && a.Dead == false;
            SuccessOdds = (a, t) => 1;
        }
    }

    class PreyWillingSquirm : InteractionBase
    {
        public PreyWillingSquirm()
        {
            Name = "PreyWillingSquirm";
            Description = "Happily squirm within your predator.";
            Class = ClassType.Vore;
            Type = InteractionType.PreyWillingSquirm;
            Range = 999;
            SoundRange = 0;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "";
            AsksPlayer = false;
            UsedOnPrey = false;
            Hostile = false;
            AppearConditional = (a, t) =>
                a.FindMyPredator() == t
                && t.VoreController.GetProgressOf(a).Willing
                && a.Needs.Energy < .9f
                && a.Dead == false;
            SuccessOdds = (a, t) => 1;
            SuccessEffect = (a, t) =>
                PreyRub(a, t, useEnergy: 1.0f / 80.0f, willing: false, violent: false);
        }
    }

    class PreyWillingMasturbate : InteractionBase
    {
        public PreyWillingMasturbate()
        {
            Name = "PreyWillingMasturbate";
            Description = "Play with yourself within your predator.";
            Class = ClassType.Vore;
            Type = InteractionType.PreyWillingMasturbate;
            Range = 999;
            SoundRange = 0;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "";
            AsksPlayer = false;
            UsedOnPrey = false;
            Hostile = false;
            AppearConditional = (a, t) =>
                a.FindMyPredator() == t
                && t.VoreController.GetProgressOf(a).Willing
                && a.Dead == false;
            SuccessOdds = (a, t) => 1;
            SuccessEffect = (a, t) => Masturbate(a, t);
        }
    }

    class PreyWillingBellyRub : InteractionBase
    {
        public PreyWillingBellyRub()
        {
            Name = "PreyWillingBellyRub";
            Description = "Rub your predator's belly from the inside.";
            Class = ClassType.Vore;
            Type = InteractionType.PreyWillingBellyRub;
            Range = 999;
            SoundRange = 0;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "";
            AsksPlayer = false;
            UsedOnPrey = false;
            Hostile = false;
            AppearConditional = (a, t) =>
                a.FindMyPredator() == t
                && (t.VoreController.GetProgressOf(a, VoreLocation.Stomach)?.Willing ?? false)
                && a.Needs.Energy < .9f
                && a.Dead == false;
            SuccessOdds = (a, t) => 1;
            SuccessEffect = (a, t) => PreyWillingRub(a, t);
        }
    }

    class PreyWillingWombRub : InteractionBase
    {
        public PreyWillingWombRub()
        {
            Name = "PreyWillingWombRub";
            Description = "Rub your predator's womb from the inside.";
            Class = ClassType.Vore;
            Type = InteractionType.PreyWillingWombRub;
            Range = 999;
            SoundRange = 0;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "";
            AsksPlayer = false;
            UsedOnPrey = false;
            Hostile = false;
            AppearConditional = (a, t) =>
                a.FindMyPredator() == t
                && (t.VoreController.GetProgressOf(a, VoreLocation.Womb)?.Willing ?? false)
                && a.Needs.Energy < .9f
                && a.Dead == false;
            SuccessOdds = (a, t) => 1;
            SuccessEffect = (a, t) => PreyWillingRub(a, t);
        }
    }

    class PreyWillingBallsRub : InteractionBase
    {
        public PreyWillingBallsRub()
        {
            Name = "PreyWillingBallsRub";
            Description = "Rub your predator's balls from the inside.";
            Class = ClassType.Vore;
            Type = InteractionType.PreyWillingBallsRub;
            Range = 999;
            SoundRange = 0;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "";
            AsksPlayer = false;
            UsedOnPrey = false;
            Hostile = false;
            AppearConditional = (a, t) =>
                a.FindMyPredator() == t
                && (t.VoreController.GetProgressOf(a, VoreLocation.Balls)?.Willing ?? false)
                && a.Needs.Energy < .9f
                && a.Dead == false;
            SuccessOdds = (a, t) => 1;
            SuccessEffect = (a, t) => PreyWillingRub(a, t);
        }
    }

    class PreyWillingAnusRub : InteractionBase
    {
        public PreyWillingAnusRub()
        {
            Name = "PreyWillingAnusRub";
            Description = "Rub your predator's anus from the inside.";
            Class = ClassType.Vore;
            Type = InteractionType.PreyWillingAnusRub;
            Range = 999;
            SoundRange = 0;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "";
            AsksPlayer = false;
            UsedOnPrey = false;
            Hostile = false;
            AppearConditional = (a, t) =>
                a.FindMyPredator() == t
                && (t.VoreController.GetProgressOf(a, VoreLocation.Bowels)?.Willing ?? false)
                && a.Needs.Energy < .9f
                && a.Dead == false;
            SuccessOdds = (a, t) => 1;
            SuccessEffect = (a, t) => PreyWillingRub(a, t);
        }
    }

    class PreyWillingNap : InteractionBase
    {
        public PreyWillingNap()
        {
            Name = "PreyWillingNap";
            Description = "Take a brief nap within your predator.";
            Class = ClassType.Vore;
            Type = InteractionType.PreyWillingNap;
            Range = 999;
            SoundRange = 0;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "";
            AsksPlayer = false;
            UsedOnPrey = false;
            Hostile = false;
            AppearConditional = (a, t) =>
                a.FindMyPredator() == t
                && t.VoreController.GetProgressOf(a).Willing
                && t.VoreController.GetProgressOf(a).IsSwallowing() == false
                && a.Dead == false;
            SuccessOdds = (a, t) => 1;
            SuccessEffect = (a, t) => a.Needs.ChangeEnergy(-.04f);
        }
    }

    class PreyWillingAskToBeLetOut : InteractionBase
    {
        public PreyWillingAskToBeLetOut()
        {
            Name = "PreyWillingAskToBeLetOut";
            Description =
                "Ask to be let out of your predator.  Will not succeed if you're being digested, you need to use PreyBeg or PreyConvinceToSpare in that case.";
            Class = ClassType.VoreAsk;
            Type = InteractionType.PreyWillingAskToBeLetOut;
            Range = 999;
            SoundRange = 0;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "wants you to let them out";
            AsksPlayer = true;
            UsedOnPrey = false;
            Hostile = false;
            AppearConditional = (a, t) =>
                a.FindMyPredator() == t
                && t.VoreController.GetProgressOf(a).Willing
                && a.Dead == false;
            SuccessOdds = (a, t) => PreyAskToBeFreeOdds(this, a, t);
            SuccessEffect = (a, t) => t.VoreController.GetProgressOf(a).FreePrey(true, false);
            FailEffect = (a, t) => FailedPreyAsk(a, t);
        }
    }

    class PreyBecomeUnwilling : InteractionBase
    {
        public PreyBecomeUnwilling()
        {
            Name = "PreyBecomeUnwilling";
            Description = "Switch over to the unwilling line of actions.";
            Class = ClassType.Vore;
            Type = InteractionType.PreyBecomeUnwilling;
            Range = 999;
            SoundRange = 0;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "";
            AsksPlayer = false;
            UsedOnPrey = false;
            Hostile = false;
            AppearConditional = (a, t) =>
                a.FindMyPredator() == t
                && t.VoreController.GetProgressOf(a).Willing
                && a.Dead == false;
            SuccessOdds = (a, t) => 1;
            SuccessEffect = (a, t) => t.VoreController.GetProgressOf(a).Willing = false;
        }
    }

    class PreyScream : InteractionBase
    {
        public PreyScream()
        {
            Name = "PreyScream";
            Description = "Scream for help, hoping someone will hear you.";
            Class = ClassType.VoreUnwilling;
            Type = InteractionType.PreyScream;
            Range = 999;
            SoundRange = 3;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "";
            AsksPlayer = false;
            UsedOnPrey = false;
            Hostile = false;
            AppearConditional = (a, t) =>
                a.FindMyPredator() == t
                && t.VoreController.GetProgressOf(a).Willing == false
                && a.Dead == false;
            SuccessOdds = (a, t) => 1;
        }
    }

    class PreyBeg : InteractionBase
    {
        public PreyBeg()
        {
            Name = "PreyBeg";
            Description =
                "Beg for your life, hoping your predator will let you out or that someone will hear you.";
            Class = ClassType.VoreUnwilling;
            Type = InteractionType.PreyBeg;
            Range = 999;
            SoundRange = 2;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "wants you to let them out";
            AsksPlayer = true;
            UsedOnPrey = false;
            Hostile = false;
            AppearConditional = (a, t) =>
                a.FindMyPredator() == t
                && t.VoreController.GetProgressOf(a).Willing == false
                && a.Dead == false;
            SuccessOdds = (a, t) => BegCheckOdds(a, t);
            SuccessEffect = (a, t) => ReleasePrey(t, a);
            FailEffect = (a, t) => BegFail(a, t);
        }
    }

    class PreyConvinceToSpare : InteractionBase
    {
        public PreyConvinceToSpare()
        {
            Name = "PreyConvinceToSpare";
            Description = "Try to convince your predator to switch to endosoma.";
            Class = ClassType.VoreAsk;
            Type = InteractionType.PreyConvinceToSpare;
            Range = 999;
            SoundRange = 0;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "wants you to stop digesting them";
            AsksPlayer = true;
            UsedOnPrey = false;
            Hostile = false;
            AppearConditional = (a, t) =>
                a.FindMyPredator() == t
                && a.Dead == false
                && a.BeingSwallowed == false
                && t.VoreController.GetProgressOf(a).Willing == false
                && t.VoreController.TargetIsBeingDigested(a)
                && State.World.Settings.CheckDigestion(t, DigestionAlias.CanEndo);
            SuccessOdds = (a, t) => SpareCheckOdds(this, a, t);
            SuccessEffect = (a, t) => Spare(a, t);
            FailEffect = (a, t) => SpareFail(a, t);
        }
    }

    class PreyWait : InteractionBase
    {
        public PreyWait()
        {
            Name = "PreyWait";
            Description = "Quietly wait within your predator.";
            Class = ClassType.Vore;
            Type = InteractionType.PreyWait;
            Range = 999;
            SoundRange = 0;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "";
            AsksPlayer = false;
            UsedOnPrey = false;
            Hostile = false;
            AppearConditional = (a, t) => a.FindMyPredator() == t;
            SuccessOdds = (a, t) => 1;
        }
    }

    class PreyStruggle : InteractionBase
    {
        public PreyStruggle()
        {
            Name = "PreyStruggle";
            Description = "Expend some energy to try and free yourself from your predator.";
            Class = ClassType.Vore;
            Type = InteractionType.PreyStruggle;
            Range = 999;
            SoundRange = 1;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "";
            AsksPlayer = false;
            UsedOnPrey = false;
            Hostile = false;
            AppearConditional = (a, t) =>
                a.FindMyPredator() == t
                && t.VoreController.GetProgressOf(a).Willing == false
                && a.Needs.Energy < .9f
                && a.Dead == false;
            SuccessOdds = (a, t) => VoreEscapeCheckOdds(a, t, .9f);
            SuccessEffect = (a, t) => Struggle(a, t, success: true, violent: false);
            FailEffect = (a, t) => Struggle(a, t, success: false, violent: false);
        }
    }

    class PreyViolentStruggle : InteractionBase
    {
        public PreyViolentStruggle()
        {
            Name = "PreyViolentStruggle";
            Description =
                "Expend quite a bit of energy in a desperate attempt to free yourself from your predator.";
            Class = ClassType.Vore;
            Type = InteractionType.PreyViolentStruggle;
            Range = 999;
            SoundRange = 1;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "";
            AsksPlayer = false;
            UsedOnPrey = false;
            Hostile = false;
            AppearConditional = (a, t) =>
                a.FindMyPredator() == t
                && t.VoreController.GetProgressOf(a).Willing == false
                && a.Needs.Energy < .9f
                && a.Dead == false;
            SuccessOdds = (a, t) => VoreEscapeCheckOdds(a, t, 3f);
            SuccessEffect = (a, t) => Struggle(a, t, success: true, violent: true);
            FailEffect = (a, t) => Struggle(a, t, success: false, violent: true);
        }
    }

    class PreyRecover : InteractionBase
    {
        public PreyRecover()
        {
            Name = "PreyRecover";
            Description = "Rest for a few moments within your predator, but keeping your guard up.";
            Class = ClassType.Vore;
            Type = InteractionType.PreyRecover;
            Range = 999;
            SoundRange = 1;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "";
            AsksPlayer = false;
            UsedOnPrey = false;
            Hostile = false;
            AppearConditional = (a, t) =>
                a.FindMyPredator() == t
                && t.VoreController.GetProgressOf(a).Willing == false
                && a.Dead == false;
            SuccessOdds = (a, t) => 1;
            SuccessEffect = (a, t) => a.Needs.ChangeEnergy(-.025f);
        }
    }

    class PreyMasturbate : InteractionBase
    {
        public PreyMasturbate()
        {
            Name = "PreyMasturbate";
            Description = "Play with yourself within your predator.";
            Class = ClassType.Vore;
            Type = InteractionType.PreyMasturbate;
            Range = 999;
            SoundRange = 0;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "";
            AsksPlayer = false;
            UsedOnPrey = false;
            Hostile = false;
            AppearConditional = (a, t) =>
                a.FindMyPredator() == t
                && t.VoreController.GetProgressOf(a).Willing == false
                && a.Dead == false;
            SuccessOdds = (a, t) => 1;
            SuccessEffect = (a, t) => Masturbate(a, t);
        }
    }

    class PreyAskToBeDigested : InteractionBase
    {
        public PreyAskToBeDigested()
        {
            Name = "PreyAskToBeDigested";
            Description =
                "Ask your predator to digest you (They'll only instantly switch if they have every prey's permission)";
            Class = ClassType.VoreAskToBe;
            Type = InteractionType.PreyAskToBeDigested;
            Range = 999;
            SoundRange = 0;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "wants you to digest them";
            AsksPlayer = true;
            UsedOnPrey = false;
            Hostile = false;
            AppearConditional = (a, t) =>
                CanDigestPrey(t, a)
                && a.Dead == false
                && a.BeingSwallowed == false
                && (
                    t.VoreController.GetProgressOf(a).Location == VoreLocation.Stomach
                    || t.VoreController.GetProgressOf(a).Location == VoreLocation.Bowels
                );
            SuccessOdds = (a, t) => AskToBeDigestedOdds(a, t);
            SuccessEffect = (a, t) => ConvinceToDigest(a, t);
            FailEffect = (a, t) => FailedPreyAsk(a, t);
        }
    }

    class PreyAskToBeMelted : InteractionBase
    {
        public PreyAskToBeMelted()
        {
            Name = "PreyAskToBeMelted";
            Description =
                "Ask your predator to melt you (They'll only instantly switch if they have every prey's permission)";
            Class = ClassType.VoreAskToBe;
            Type = InteractionType.PreyAskToBeMelted;
            Range = 999;
            SoundRange = 0;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "wants you to melt them";
            AsksPlayer = true;
            UsedOnPrey = false;
            Hostile = false;
            AppearConditional = (a, t) =>
                CanDigestPrey(t, a)
                && a.Dead == false
                && a.BeingSwallowed == false
                && (
                    t.VoreController.GetProgressOf(a).Location != VoreLocation.Stomach
                    && t.VoreController.GetProgressOf(a).Location != VoreLocation.Bowels
                );
            SuccessOdds = (a, t) => AskToBeDigestedOdds(a, t);
            SuccessEffect = (a, t) => ConvinceToDigest(a, t);
            FailEffect = (a, t) => FailedPreyAsk(a, t);
        }
    }

    class PreyBecomeWilling : InteractionBase
    {
        public PreyBecomeWilling()
        {
            Name = "PreyBecomeWilling";
            Description = "Give in and switch to the willing line of actions.";
            Class = ClassType.Vore;
            Type = InteractionType.PreyBecomeWilling;
            Range = 999;
            SoundRange = 0;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "";
            AsksPlayer = false;
            UsedOnPrey = false;
            Hostile = false;
            AppearConditional = (a, t) =>
                a.FindMyPredator() == t
                && t.VoreController.GetProgressOf(a).Willing == false
                && a.Dead == false;
            SuccessOdds = (a, t) => 1;
            SuccessEffect = (a, t) => t.VoreController.GetProgressOf(a).Willing = true;
        }
    }

    class PreyMeet : InteractionBase
    {
        public PreyMeet()
        {
            Name = "PreyMeet";
            Description = "Introduce yourself to them.";
            Class = ClassType.Vore;
            Type = InteractionType.PreyMeet;
            Range = 999;
            SoundRange = 2;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "";
            AsksPlayer = false;
            UsedOnPrey = true;
            Hostile = false;
            AppearConditional = (a, t) =>
                a.HasRelationshipWith(t) == false
                && (a.FindMyPredator()?.VoreController.AreSisterPrey(a, t) ?? false)
                && a.BeingSwallowed == false;
            SuccessOdds = (a, t) => 1;
            SuccessEffect = (a, t) => Meet(a, t);
        }
    }

    class PreyTalkToAnotherPrey : InteractionBase
    {
        public PreyTalkToAnotherPrey()
        {
            Name = "PreyTalkToAnotherPrey";
            Description = "Talk to another prey.";
            Class = ClassType.VoreAsk;
            Type = InteractionType.PreyTalkToAnotherPrey;
            Range = 999;
            SoundRange = 1;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "";
            AsksPlayer = false;
            UsedOnPrey = true;
            Hostile = false;
            AppearConditional = (a, t) =>
                (a.FindMyPredator()?.VoreController.AreSisterPrey(a, t, true) ?? false)
                && a.BeingSwallowed == false;
            SuccessOdds = (a, t) => FriendshipCheckOdds(this, a, t, -.25f);
            SuccessEffect = (a, t) => IncreaseFriendshipSymetric(a, t, .015f, .015f);
        }
    }

    class PreyConvinceOtherPreyToBeDigested : InteractionBase
    {
        public PreyConvinceOtherPreyToBeDigested()
        {
            Name = "PreyConvinceOtherPreyToBeDigested";
            Description = "Convince another prey to want to be digested.";
            Class = ClassType.VoreAsk;
            Type = InteractionType.PreyConvinceOtherPreyToBeDigested;
            Range = 999;
            SoundRange = 1;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "wants to know if you'd let your predator digest you";
            AsksPlayer = true;
            UsedOnPrey = true;
            Hostile = false;
            AppearConditional = (a, t) =>
                (a.FindMyPredator()?.VoreController.AreSisterPrey(a, t) ?? false)
                && a.BeingSwallowed == false
                && CanDigestPrey(a.FindMyPredator(), t)
                && a.FindMyPredator().VoreController.TargetIsBeingDigested(t) == false;
            SuccessOdds = (a, t) => PreyConvinceAllowDigestOdds(this, a, t);
            SuccessEffect = (a, t) => ConvinceToDigest(t, a.FindMyPredator());
        }
    }

    class PreyKissOtherPrey : InteractionBase
    {
        public PreyKissOtherPrey()
        {
            Name = "PreyKissOtherPrey";
            Description = "Kiss another prey.";
            Class = ClassType.VoreAsk;
            Type = InteractionType.PreyKissOtherPrey;
            Range = 999;
            SoundRange = -1;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "wants to kiss you";
            AsksPlayer = true;
            UsedOnPrey = true;
            Hostile = false;
            AppearConditional = (a, t) =>
                (a.FindMyPredator()?.VoreController.AreSisterPrey(a, t) ?? false)
                && a.BeingSwallowed == false;
            SuccessOdds = (a, t) => RomanticCheckOdds(this, a, t, 0);
            SuccessEffect = (a, t) => IncreaseRomanticSymetric(a, t, .02f, .02f);
            FailEffect = (a, t) => RomanticRejection(a, t);
        }
    }

    class PreyFondleOtherPrey : InteractionBase
    {
        public PreyFondleOtherPrey()
        {
            Name = "PreyFondleOtherPrey";
            Description = "Fondle another prey.";
            Class = ClassType.VoreAsk;
            Type = InteractionType.PreyFondleOtherPrey;
            Range = 999;
            SoundRange = -1;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "wants to fondle you";
            AsksPlayer = true;
            UsedOnPrey = true;
            Hostile = false;
            AppearConditional = (a, t) =>
                (a.FindMyPredator()?.VoreController.AreSisterPrey(a, t) ?? false)
                && a.BeingSwallowed == false;
            SuccessOdds = (a, t) => RomanticCheckOdds(this, a, t, 0);
            SuccessEffect = (a, t) => IncreaseRomanticSymetric(a, t, .02f, .02f);
            FailEffect = (a, t) => RomanticRejection(a, t);
        }
    }

    class PreyEatOtherPrey : InteractionBase
    {
        public PreyEatOtherPrey()
        {
            Name = "PreyEatOtherPrey";
            Description = "Eat another prey.";
            Class = ClassType.Vore;
            Type = InteractionType.PreyEatOtherPrey;
            Range = 999;
            SoundRange = -1;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "";
            AsksPlayer = false;
            UsedOnPrey = true;
            Hostile = true;
            AppearConditional = (a, t) =>
                (a.FindMyPredator()?.VoreController.AreSisterPrey(a, t) ?? false)
                && a.BeingSwallowed == false
                && a.VoreController.CouldVoreTarget(t, VoreType.Oral, DigestionAlias.Any)
                && t.Dead == false
                && a.VoreController.Swallowing(VoreLocation.Stomach) == false;
            SuccessOdds = (a, t) => VoreCheckOdds(a, t, VoreType.Oral);
            SuccessEffect = (a, t) => PreyEat(a, t, VoreType.Oral);
        }
    }

    class PreyUnbirthOtherPrey : InteractionBase
    {
        public PreyUnbirthOtherPrey()
        {
            Name = "PreyUnbirthOtherPrey";
            Description = "Unbirth another prey.";
            Class = ClassType.Vore;
            Type = InteractionType.PreyUnbirthOtherPrey;
            Range = 999;
            SoundRange = -1;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "";
            AsksPlayer = false;
            UsedOnPrey = true;
            Hostile = true;
            AppearConditional = (a, t) =>
                (a.FindMyPredator()?.VoreController.AreSisterPrey(a, t) ?? false)
                && a.BeingSwallowed == false
                && a.VoreController.CouldVoreTarget(t, VoreType.Unbirth, DigestionAlias.Any)
                && t.Dead == false
                && a.VoreController.Swallowing(VoreLocation.Womb) == false;
            SuccessOdds = (a, t) => VoreCheckOdds(a, t, VoreType.Unbirth);
            SuccessEffect = (a, t) => PreyEat(a, t, VoreType.Unbirth);
        }
    }

    class PreyCockVoreOtherPrey : InteractionBase
    {
        public PreyCockVoreOtherPrey()
        {
            Name = "PreyCockVoreOtherPrey";
            Description = "Cock Vore another prey.";
            Class = ClassType.Vore;
            Type = InteractionType.PreyCockVoreOtherPrey;
            Range = 999;
            SoundRange = -1;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "";
            AsksPlayer = false;
            UsedOnPrey = true;
            Hostile = true;
            AppearConditional = (a, t) =>
                (a.FindMyPredator()?.VoreController.AreSisterPrey(a, t) ?? false)
                && a.BeingSwallowed == false
                && a.VoreController.CouldVoreTarget(t, VoreType.Cock, DigestionAlias.Any)
                && t.Dead == false
                && a.VoreController.Swallowing(VoreLocation.Balls) == false;
            SuccessOdds = (a, t) => VoreCheckOdds(a, t, VoreType.Cock);
            SuccessEffect = (a, t) => PreyEat(a, t, VoreType.Cock);
        }
    }

    class PreyAnalVoreOtherPrey : InteractionBase
    {
        public PreyAnalVoreOtherPrey()
        {
            Name = "PreyAnalVoreOtherPrey";
            Description = "Anal Vore another prey.";
            Class = ClassType.Vore;
            Type = InteractionType.PreyAnalVoreOtherPrey;
            Range = 999;
            SoundRange = -1;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "";
            AsksPlayer = false;
            UsedOnPrey = true;
            Hostile = true;
            AppearConditional = (a, t) =>
                (a.FindMyPredator()?.VoreController.AreSisterPrey(a, t) ?? false)
                && a.BeingSwallowed == false
                && a.VoreController.CouldVoreTarget(t, VoreType.Anal, DigestionAlias.Any)
                && t.Dead == false
                && a.VoreController.Swallowing(VoreLocation.Stomach) == false;
            SuccessOdds = (a, t) => VoreCheckOdds(a, t, VoreType.Anal);
            SuccessEffect = (a, t) => PreyEat(a, t, VoreType.Anal);
        }
    }

    class PreyAskToOralVore : InteractionBase
    {
        public PreyAskToOralVore()
        {
            Name = "PreyAskToOralVore";
            Description =
                "Ask them if they would be willing to let you eat them.  Non-fatal is assumed.";
            Class = ClassType.VoreAskThem;
            Type = InteractionType.PreyAskToOralVore;
            Range = 999;
            SoundRange = -1;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "asks to oral vore you but not digest you";
            AsksPlayer = true;
            UsedOnPrey = true;
            Hostile = false;
            AppearConditional = (a, t) =>
                (a.FindMyPredator()?.VoreController.AreSisterPrey(a, t) ?? false)
                && a.VoreController.CouldVoreTarget(t, VoreType.Oral, DigestionAlias.CanEndo);
            SuccessOdds = (a, t) => WillingVoreCheckOdds(this, a, t, VoreType.Oral);
            SuccessEffect = (a, t) => PreyAskToVore(a, t, VoreType.Oral, false);
        }
    }

    class PreyAskToUnbirth : InteractionBase
    {
        public PreyAskToUnbirth()
        {
            Name = "PreyAskToUnbirth";
            Description =
                "Ask them if they would be willing to let you unbirth them.  Non-fatal is assumed.";
            Class = ClassType.VoreAskThem;
            Type = InteractionType.PreyAskToUnbirth;
            Range = 999;
            SoundRange = -1;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "asks to unbirth you but not melt you";
            AsksPlayer = true;
            UsedOnPrey = true;
            Hostile = false;
            AppearConditional = (a, t) =>
                (a.FindMyPredator()?.VoreController.AreSisterPrey(a, t) ?? false)
                && a.VoreController.CouldVoreTarget(t, VoreType.Unbirth, DigestionAlias.CanEndo);
            SuccessOdds = (a, t) => WillingVoreCheckOdds(this, a, t, VoreType.Unbirth);
            SuccessEffect = (a, t) => PreyAskToVore(a, t, VoreType.Unbirth, false);
        }
    }

    class PreyAskToCockVore : InteractionBase
    {
        public PreyAskToCockVore()
        {
            Name = "PreyAskToCockVore";
            Description =
                "Ask them if they would be willing to let you cock vore them.  Non-fatal is assumed.";
            Class = ClassType.VoreAskThem;
            Type = InteractionType.PreyAskToCockVore;
            Range = 999;
            SoundRange = -1;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "asks to cock vore you but not melt you";
            AsksPlayer = true;
            UsedOnPrey = true;
            Hostile = false;
            AppearConditional = (a, t) =>
                (a.FindMyPredator()?.VoreController.AreSisterPrey(a, t) ?? false)
                && a.VoreController.CouldVoreTarget(t, VoreType.Cock, DigestionAlias.CanEndo);
            SuccessOdds = (a, t) => WillingVoreCheckOdds(this, a, t, VoreType.Cock);
            SuccessEffect = (a, t) => PreyAskToVore(a, t, VoreType.Cock, false);
        }
    }

    class PreyAskToAnalVore : InteractionBase
    {
        public PreyAskToAnalVore()
        {
            Name = "PreyAskToAnalVore";
            Description =
                "Ask them if they would be willing to let you anal vore them.  Non-fatal is assumed.";
            Class = ClassType.VoreAskThem;
            Type = InteractionType.PreyAskToAnalVore;
            Range = 999;
            SoundRange = -1;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "asks to anal vore you but not digest you";
            AsksPlayer = true;
            UsedOnPrey = true;
            Hostile = false;
            AppearConditional = (a, t) =>
                (a.FindMyPredator()?.VoreController.AreSisterPrey(a, t) ?? false)
                && a.VoreController.CouldVoreTarget(t, VoreType.Anal, DigestionAlias.CanEndo);
            SuccessOdds = (a, t) => WillingVoreCheckOdds(this, a, t, VoreType.Anal);
            SuccessEffect = (a, t) => PreyAskToVore(a, t, VoreType.Anal, false);
        }
    }

    class PreyAskToOralVoreDigest : InteractionBase
    {
        public PreyAskToOralVoreDigest()
        {
            Name = "PreyAskToOralVoreDigest";
            Description =
                "Ask them if they would be willing to let you eat them.  Digestion is assumed.";
            Class = ClassType.VoreAskThem;
            Type = InteractionType.PreyAskToOralVoreDigest;
            Range = 999;
            SoundRange = -1;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "asks to oral vore you and digest you";
            AsksPlayer = true;
            UsedOnPrey = true;
            Hostile = false;
            AppearConditional = (a, t) =>
                (a.FindMyPredator()?.VoreController.AreSisterPrey(a, t) ?? false)
                && a.VoreController.CouldVoreTarget(t, VoreType.Oral, DigestionAlias.CanVore);
            SuccessOdds = (a, t) => WillingVoreDigestCheckOdds(this, a, t, VoreType.Oral);
            SuccessEffect = (a, t) => PreyAskToVore(a, t, VoreType.Oral, true);
        }
    }

    class PreyAskToUnbirthDigest : InteractionBase
    {
        public PreyAskToUnbirthDigest()
        {
            Name = "PreyAskToUnbirthDigest";
            Description =
                "Ask them if they would be willing to let you unbirth them.  Digestion is assumed.";
            Class = ClassType.VoreAskThem;
            Type = InteractionType.PreyAskToUnbirthDigest;
            Range = 999;
            SoundRange = -1;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "asks to unbirth you and melt you";
            AsksPlayer = true;
            UsedOnPrey = true;
            Hostile = false;
            AppearConditional = (a, t) =>
                (a.FindMyPredator()?.VoreController.AreSisterPrey(a, t) ?? false)
                && a.VoreController.CouldVoreTarget(t, VoreType.Unbirth, DigestionAlias.CanVore);
            SuccessOdds = (a, t) => WillingVoreDigestCheckOdds(this, a, t, VoreType.Unbirth);
            SuccessEffect = (a, t) => PreyAskToVore(a, t, VoreType.Unbirth, true);
        }
    }

    class PreyAskToCockVoreDigest : InteractionBase
    {
        public PreyAskToCockVoreDigest()
        {
            Name = "PreyAskToCockVoreDigest";
            Description =
                "Ask them if they would be willing to let you cock vore them.  Digestion is assumed.";
            Class = ClassType.VoreAskThem;
            Type = InteractionType.PreyAskToCockVoreDigest;
            Range = 999;
            SoundRange = -1;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "asks to cock vore you and melt you";
            AsksPlayer = true;
            UsedOnPrey = true;
            Hostile = false;
            AppearConditional = (a, t) =>
                (a.FindMyPredator()?.VoreController.AreSisterPrey(a, t) ?? false)
                && a.VoreController.CouldVoreTarget(t, VoreType.Cock, DigestionAlias.CanVore);
            SuccessOdds = (a, t) => WillingVoreDigestCheckOdds(this, a, t, VoreType.Cock);
            SuccessEffect = (a, t) => PreyAskToVore(a, t, VoreType.Cock, true);
        }
    }

    class PreyAskToAnalVoreDigest : InteractionBase
    {
        public PreyAskToAnalVoreDigest()
        {
            Name = "PreyAskToAnalVoreDigest";
            Description =
                "Ask them if they would be willing to let you anal vore them.  Digestion is assumed.";
            Class = ClassType.VoreAskThem;
            Type = InteractionType.PreyAskToAnalVoreDigest;
            Range = 999;
            SoundRange = -1;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "asks to anal vore you and digest you";
            AsksPlayer = true;
            UsedOnPrey = true;
            Hostile = false;
            AppearConditional = (a, t) =>
                (a.FindMyPredator()?.VoreController.AreSisterPrey(a, t) ?? false)
                && a.VoreController.CouldVoreTarget(t, VoreType.Anal, DigestionAlias.CanVore);
            SuccessOdds = (a, t) => WillingVoreDigestCheckOdds(this, a, t, VoreType.Anal);
            SuccessEffect = (a, t) => PreyAskToVore(a, t, VoreType.Anal, true);
        }
    }

    class Following : InteractionBase
    {
        public Following()
        {
            Name = "Following";
            Description = "";
            Class = ClassType.Event;
            Type = InteractionType.Following;
            Range = 0;
            SoundRange = -1;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "";
            AsksPlayer = false;
            UsedOnPrey = false;
            Hostile = false;
            AppearConditional = (a, t) => false;
            SuccessOdds = (a, t) => 1;
        }
    }

    class EventDisposalBathroom : InteractionBase
    {
        public EventDisposalBathroom()
        {
            Name = "EventDisposalBathroom";
            Description = "";
            Class = ClassType.Event;
            Type = InteractionType.EventDisposalBathroom;
            Range = 999;
            SoundRange = -1;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "";
            AsksPlayer = false;
            UsedOnPrey = false;
            Hostile = false;
            AppearConditional = (a, t) => false;
            SuccessOdds = (a, t) => 1;
        }
    }

    class EventDisposalGround : InteractionBase
    {
        public EventDisposalGround()
        {
            Name = "EventDisposalGround";
            Description = "";
            Class = ClassType.Event;
            Type = InteractionType.EventDisposalGround;
            Range = 999;
            SoundRange = -1;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "";
            AsksPlayer = false;
            UsedOnPrey = false;
            Hostile = false;
            AppearConditional = (a, t) => false;
            SuccessOdds = (a, t) => 1;
        }
    }

    class EventDisposalCockBathroom : InteractionBase
    {
        public EventDisposalCockBathroom()
        {
            Name = "EventDisposalCockBathroom";
            Description = "";
            Class = ClassType.Event;
            Type = InteractionType.EventDisposalCockBathroom;
            Range = 999;
            SoundRange = -1;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "";
            AsksPlayer = false;
            UsedOnPrey = false;
            Hostile = false;
            AppearConditional = (a, t) => false;
            SuccessOdds = (a, t) => 1;
        }
    }

    class EventDisposalCockGround : InteractionBase
    {
        public EventDisposalCockGround()
        {
            Name = "EventDisposalCockGround";
            Description = "";
            Class = ClassType.Event;
            Type = InteractionType.EventDisposalCockGround;
            Range = 999;
            SoundRange = -1;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "";
            AsksPlayer = false;
            UsedOnPrey = false;
            Hostile = false;
            AppearConditional = (a, t) => false;
            SuccessOdds = (a, t) => 1;
        }
    }
    class EventDisposalCockOrgasm : InteractionBase
    {
        public EventDisposalCockOrgasm()
        {
            Name = "EventDisposalCockOrgasm";
            Description = "";
            Class = ClassType.Event;
            Type = InteractionType.EventDisposalCockOrgasm;
            Range = 999;
            SoundRange = -1;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "";
            AsksPlayer = false;
            UsedOnPrey = false;
            Hostile = false;
            AppearConditional = (a, t) => false;
            SuccessOdds = (a, t) => 1;
        }
    }

    class EventDisposalUnbirthBathroom : InteractionBase
    {
        public EventDisposalUnbirthBathroom()
        {
            Name = "EventDisposalUnbirthBathroom";
            Description = "";
            Class = ClassType.Event;
            Type = InteractionType.EventDisposalUnbirthBathroom;
            Range = 999;
            SoundRange = -1;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "";
            AsksPlayer = false;
            UsedOnPrey = false;
            Hostile = false;
            AppearConditional = (a, t) => false;
            SuccessOdds = (a, t) => 1;
        }
    }

    class EventDisposalUnbirthGround : InteractionBase
    {
        public EventDisposalUnbirthGround()
        {
            Name = "EventDisposalUnbirthGround";
            Description = "";
            Class = ClassType.Event;
            Type = InteractionType.EventDisposalUnbirthGround;
            Range = 999;
            SoundRange = -1;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "";
            AsksPlayer = false;
            UsedOnPrey = false;
            Hostile = false;
            AppearConditional = (a, t) => false;
            SuccessOdds = (a, t) => 1;
        }
    }

    class EventDisposalUnbirthOrgasm : InteractionBase
    {
        public EventDisposalUnbirthOrgasm()
        {
            Name = "EventDisposalUnbirthOrgasm";
            Description = "";
            Class = ClassType.Event;
            Type = InteractionType.EventDisposalUnbirthOrgasm;
            Range = 999;
            SoundRange = -1;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "";
            AsksPlayer = false;
            UsedOnPrey = false;
            Hostile = false;
            AppearConditional = (a, t) => false;
            SuccessOdds = (a, t) => 1;
        }
    }

    class KissVore : InteractionBase
    {
        public KissVore()
        {
            Name = "KissVore";
            Description = "Try to eat them without their consent.";
            Class = ClassType.VoreConsuming;
            Type = InteractionType.KissVore;
            Range = 0;
            SoundRange = -1;
            Streaming = true;
            StreamingDescription = "Eating";
            AsksPlayerDescription = "";
            AsksPlayer = false;
            UsedOnPrey = false;
            Hostile = true;
            AppearConditional = (a, t) => false;
            SuccessOdds = (a, t) => VoreCheckOdds(a, t, VoreType.Oral);
            SuccessEffect = (a, t) => Vore(a, t, VoreType.Oral, true);
            FailEffect = (a, t) => VoreRefused(a, t, a.VoreController.PartCurrentlyDigests(VoreLocation.Stomach));
        }
    }

    class SexUnbirth : InteractionBase
    {
        public SexUnbirth()
        {
            Name = "SexUnbirth";
            Description = "Try to unbirth them without their consent.";
            Class = ClassType.VoreConsuming;
            Type = InteractionType.SexUnbirth;
            Range = 0;
            SoundRange = -1;
            Streaming = true;
            StreamingDescription = "Unbirthing";
            AsksPlayerDescription = "";
            AsksPlayer = false;
            UsedOnPrey = false;
            Hostile = true;
            AppearConditional = (a, t) => false;
            SuccessOdds = (a, t) => VoreCheckOdds(a, t, VoreType.Unbirth);
            SuccessEffect = (a, t) => Vore(a, t, VoreType.Unbirth, true);
            FailEffect = (a, t) => VoreRefused(a, t, a.VoreController.PartCurrentlyDigests(VoreLocation.Womb));
        }
    }

    class SexCockVore : InteractionBase
    {
        public SexCockVore()
        {
            Name = "SexCockVore";
            Description = "Try to cock vore them without their consent.";
            Class = ClassType.VoreConsuming;
            Type = InteractionType.SexCockVore;
            Range = 0;
            SoundRange = -1;
            Streaming = true;
            StreamingDescription = "Cock Voring";
            AsksPlayerDescription = "";
            AsksPlayer = false;
            UsedOnPrey = false;
            Hostile = true;
            AppearConditional = (a, t) => false;
            SuccessOdds = (a, t) => VoreCheckOdds(a, t, VoreType.Cock);
            SuccessEffect = (a, t) => Vore(a, t, VoreType.Cock, true);
            FailEffect = (a, t) => VoreRefused(a, t, a.VoreController.PartCurrentlyDigests(VoreLocation.Balls));
        }
    }

    class SexAnalVore : InteractionBase
    {
        public SexAnalVore()
        {
            Name = "SexAnalVore";
            Description = "Try to anal vore them without their consent.";
            Class = ClassType.VoreConsuming;
            Type = InteractionType.SexAnalVore;
            Range = 0;
            SoundRange = -1;
            Streaming = true;
            StreamingDescription = "Anal Voring";
            AsksPlayerDescription = "";
            AsksPlayer = false;
            UsedOnPrey = false;
            Hostile = true;
            AppearConditional = (a, t) => false;
            SuccessOdds = (a, t) => VoreCheckOdds(a, t, VoreType.Anal);
            SuccessEffect = (a, t) => Vore(a, t, VoreType.Anal, true);
            FailEffect = (a, t) => VoreRefused(a, t, a.VoreController.PartCurrentlyDigests(VoreLocation.Bowels));
        }
    }

    class SexAskToOralVore : InteractionBase
    {
        public SexAskToOralVore()
        {
            Name = "SexAskToOralVore";
            Description =
                "Ask them if they would be willing to let you eat them.  Non-fatal is assumed.";
            Class = ClassType.VoreAskThem;
            Type = InteractionType.SexAskToOralVore;
            Range = 0;
            SoundRange = -1;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "asks to oral vore you but not digest you";
            AsksPlayer = true;
            UsedOnPrey = false;
            Hostile = false;
            AppearConditional = (a, t) => false;
            SuccessOdds = (a, t) => WillingVoreCheckOdds(this, a, t, VoreType.Oral);
            SuccessEffect = (a, t) => AskToVore(a, t, VoreType.Oral, false, true);
        }
    }

    class SexAskToUnbirth : InteractionBase
    {
        public SexAskToUnbirth()
        {
            Name = "SexAskToUnbirth";
            Description =
                "Ask them if they would be willing to let you unbirth them.  Non-fatal is assumed.";
            Class = ClassType.VoreAskThem;
            Type = InteractionType.SexAskToUnbirth;
            Range = 0;
            SoundRange = -1;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "asks to unbirth you but not melt you";
            AsksPlayer = true;
            UsedOnPrey = false;
            Hostile = false;
            AppearConditional = (a, t) => false;
            SuccessOdds = (a, t) => WillingVoreCheckOdds(this, a, t, VoreType.Unbirth);
            SuccessEffect = (a, t) => AskToVore(a, t, VoreType.Unbirth, false, true);
        }
    }

    class SexAskToCockVore : InteractionBase
    {
        public SexAskToCockVore()
        {
            Name = "SexAskToCockVore";
            Description =
                "Ask them if they would be willing to let you cock vore them.  Non-fatal is assumed.";
            Class = ClassType.VoreAskThem;
            Type = InteractionType.SexAskToCockVore;
            Range = 0;
            SoundRange = -1;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "asks to cock vore you but not melt you";
            AsksPlayer = true;
            UsedOnPrey = false;
            Hostile = false;
            AppearConditional = (a, t) => false;
            SuccessOdds = (a, t) => WillingVoreCheckOdds(this, a, t, VoreType.Cock);
            SuccessEffect = (a, t) => AskToVore(a, t, VoreType.Cock, false, true);
        }
    }

    class SexAskToAnalVore : InteractionBase
    {
        public SexAskToAnalVore()
        {
            Name = "SexAskToAnalVore";
            Description =
                "Ask them if they would be willing to let you anal vore them.  Non-fatal is assumed.";
            Class = ClassType.VoreAskThem;
            Type = InteractionType.SexAskToAnalVore;
            Range = 0;
            SoundRange = -1;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "asks to anal vore you but not digest you";
            AsksPlayer = true;
            UsedOnPrey = false;
            Hostile = false;
            AppearConditional = (a, t) => false;
            SuccessOdds = (a, t) => WillingVoreCheckOdds(this, a, t, VoreType.Anal);
            SuccessEffect = (a, t) => AskToVore(a, t, VoreType.Anal, false, true);
        }
    }

    class SexAskToOralVoreDigest : InteractionBase
    {
        public SexAskToOralVoreDigest()
        {
            Name = "SexAskToOralVoreDigest";
            Description =
                "Ask them if they would be willing to let you eat them.  Non-fatal is assumed.";
            Class = ClassType.VoreAskThem;
            Type = InteractionType.SexAskToOralVoreDigest;
            Range = 0;
            SoundRange = -1;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "asks to oral vore you and digest you";
            AsksPlayer = true;
            UsedOnPrey = false;
            Hostile = false;
            AppearConditional = (a, t) => false;
            SuccessOdds = (a, t) => WillingVoreDigestCheckOdds(this, a, t, VoreType.Oral);
            SuccessEffect = (a, t) => AskToVore(a, t, VoreType.Oral, true, true);
        }
    }

    class SexAskToUnbirthDigest : InteractionBase
    {
        public SexAskToUnbirthDigest()
        {
            Name = "SexAskToUnbirthDigest";
            Description =
                "Ask them if they would be willing to let you unbirth them.  Non-fatal is assumed.";
            Class = ClassType.VoreAskThem;
            Type = InteractionType.SexAskToUnbirthDigest;
            Range = 0;
            SoundRange = -1;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "asks to unbirth you and melt you";
            AsksPlayer = true;
            UsedOnPrey = false;
            Hostile = false;
            AppearConditional = (a, t) => false;
            SuccessOdds = (a, t) => WillingVoreDigestCheckOdds(this, a, t, VoreType.Unbirth);
            SuccessEffect = (a, t) => AskToVore(a, t, VoreType.Unbirth, true, true);
        }
    }

    class SexAskToCockVoreDigest : InteractionBase
    {
        public SexAskToCockVoreDigest()
        {
            Name = "SexAskToCockVoreDigest";
            Description =
                "Ask them if they would be willing to let you cock vore them.  Non-fatal is assumed.";
            Class = ClassType.VoreAskThem;
            Type = InteractionType.SexAskToCockVoreDigest;
            Range = 0;
            SoundRange = -1;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "asks to cock vore you and melt you";
            AsksPlayer = true;
            UsedOnPrey = false;
            Hostile = false;
            AppearConditional = (a, t) => false;
            SuccessOdds = (a, t) => WillingVoreDigestCheckOdds(this, a, t, VoreType.Cock);
            SuccessEffect = (a, t) => AskToVore(a, t, VoreType.Cock, true, true);
        }
    }

    class SexAskToAnalVoreDigest : InteractionBase
    {
        public SexAskToAnalVoreDigest()
        {
            Name = "SexAskToAnalVoreDigest";
            Description =
                "Ask them if they would be willing to let you anal vore them.  Non-fatal is assumed.";
            Class = ClassType.VoreAskThem;
            Type = InteractionType.SexAskToAnalVoreDigest;
            Range = 0;
            SoundRange = -1;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "asks to anal vore you and digest you";
            AsksPlayer = true;
            UsedOnPrey = false;
            Hostile = false;
            AppearConditional = (a, t) => false;
            SuccessOdds = (a, t) => WillingVoreDigestCheckOdds(this, a, t, VoreType.Anal);
            SuccessEffect = (a, t) => AskToVore(a, t, VoreType.Anal, true, true);
        }
    }

    class PredDistracted : InteractionBase
    {
        public PredDistracted()
        {
            Name = "PredDistracted";
            Description = "";
            Class = ClassType.Event;
            Type = InteractionType.PredDistracted;
            Range = 0;
            SoundRange = -1;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "";
            AsksPlayer = false;
            UsedOnPrey = false;
            Hostile = false;
            AppearConditional = (a, t) => false;
            SuccessOdds = (a, t) => 1;
        }
    }

    // SPELLCASTING INTERACTIONS

    class CastDisrobe : InteractionBase
    {
        public CastDisrobe()
        {
            Name = "CastDisrobe";
            Description = "Use magic to remove and destroy their clothing. Will make target upset.";
            Class = ClassType.CastTarget;
            Type = InteractionType.CastDisrobe;
            Range = 0;
            SoundRange = 1;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "";
            AsksPlayer = false;
            UsedOnPrey = false;
            Hostile = true;
            AppearConditional = (a, t) => a.ClothingStatus != ClothingStatus.Nude && CanUseMagic(a);
            SuccessOdds = (a, t) => SpellcastOdds(this, a, t, 0.0f);
            SuccessEffect = (a, t) => CastDisrobe(a, t);
            FailEffect = (a, t) => CastDecreaseFriendship(t, a, 0.2f);
        }
    }

    class CastHeal : InteractionBase
    {
        public CastHeal()
        {
            Name = "CastHeal";
            Description =
                "Use magic to heal the target's wounds. Makes target very happy, unless you're currently digesting them.";
            Class = ClassType.CastTarget;
            Type = InteractionType.CastHeal;
            Range = 0;
            SoundRange = 1;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "";
            AsksPlayer = false;
            UsedOnPrey = true;
            Hostile = false;
            AppearConditional = (a, t) => t.Health < (Constants.HealthMax / 1.5) && CanUseMagic(a);
            SuccessOdds = (a, t) => 1.0f;
            SuccessEffect = (a, t) => CastHeal(a, t);
        }
    }

    class CastGrow : InteractionBase
    {
        public CastGrow()
        {
            Name = "CastGrow";
            Description =
                "Use magic to enlarge the target for a while. Targets who are currently prey won't know who cast it.";
            Class = ClassType.CastTarget;
            Type = InteractionType.CastGrow;
            Range = 0;
            SoundRange = 1;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "";
            AsksPlayer = false;
            UsedOnPrey = true;
            Hostile = false;
            AppearConditional = (a, t) =>
                State.World.Settings.SizeChangeMod != 1
                && State.World.Settings.SpellDurationMod != 0
                && CanUseMagic(a);
            SuccessOdds = (a, t) => SpellcastOdds(this, a, t, 0.2f);
            SuccessEffect = (a, t) => CastGrow(a, t);
        }
    }

    class CastShrink : InteractionBase
    {
        public CastShrink()
        {
            Name = "CastShrink";
            Description =
                "Use magic to shrink the target for a while. Targets unwilling to be prey will be upset. Targets who are currently prey won't know who cast it.";
            Class = ClassType.CastTarget;
            Type = InteractionType.CastShrink;
            Range = 0;
            SoundRange = 1;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "";
            AsksPlayer = false;
            UsedOnPrey = true;
            Hostile = true;
            AppearConditional = (a, t) =>
                State.World.Settings.SizeChangeMod != 1
                && State.World.Settings.SpellDurationMod != 0
                && CanUseMagic(a);
            SuccessOdds = (a, t) => SpellcastOdds(this, a, t, 0.3f);
            SuccessEffect = (a, t) => CastShrink(a, t);
            FailEffect = (a, t) => CastDecreaseFriendship(t, a, 0.3f);
        }
    }

    class CastArouse : InteractionBase
    {
        public CastArouse()
        {
            Name = "CastArouse";
            Description =
                "Use magic to cause the target to be uncontrollably horny for a few turns. If successful or if target is prey, the target won't know you cast it.";
            Class = ClassType.CastTarget;
            Type = InteractionType.CastArouse;
            Range = 0;
            SoundRange = 1;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "";
            AsksPlayer = false;
            UsedOnPrey = true;
            Hostile = false;
            AppearConditional = (a, t) => CanUseMagic(a);
            SuccessOdds = (a, t) => SpellcastOdds(this, a, t, 0.0f);
            SuccessEffect = (a, t) => CastArouse(a, t);
            FailEffect = (a, t) => CastDecreaseFriendship(t, a, 0.5f);
        }
    }

    class CastCharm : InteractionBase
    {
        public CastCharm()
        {
            Name = "CastCharm";
            Description =
                "Use magic to cause the target to be highly suggestible for a few turns. When it ends, they will be extremely upset!";
            Class = ClassType.CastTarget;
            Type = InteractionType.CastCharm;
            Range = 0;
            SoundRange = 1;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "";
            AsksPlayer = false;
            UsedOnPrey = true;
            Hostile = false;
            AppearConditional = (a, t) => CanUseMagic(a);
            SuccessOdds = (a, t) => SpellcastOdds(this, a, t, 0.4f);
            SuccessEffect = (a, t) => CastCharm(a, t);
            FailEffect = (a, t) => CastDecreaseFriendship(t, a, 0.5f);
        }
    }

    class CastFreeze : InteractionBase
    {
        public CastFreeze()
        {
            Name = "CastFreeze";
            Description =
                "Use magic to prevent the target from moving. Hostile, will upset the target. Range 2.";
            Class = ClassType.CastTarget;
            Type = InteractionType.CastFreeze;
            Range = 2;
            SoundRange = 3;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "";
            AsksPlayer = false;
            UsedOnPrey = false;
            Hostile = true;
            AppearConditional = (a, t) => CanUseMagic(a) && LOS.Check(a.Position, t.Position);
            SuccessOdds = (a, t) => SpellcastOdds(this, a, t, 0.1f);
            SuccessEffect = (a, t) => CastFreeze(a, t);
            FailEffect = (a, t) => CastDecreaseFriendship(t, a, 0.3f);
        }
    }

    class CastHunger : InteractionBase
    {
        public CastHunger()
        {
            Name = "CastHunger";
            Description =
                "Use magic to invoke an unnatural hunger. If the target is already hungry, may cause them to immediately hunt for prey. If successful, the target won't know you cast it. Range 1.";
            Class = ClassType.CastTarget;
            Type = InteractionType.CastHunger;
            Range = 1;
            SoundRange = 2;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "";
            AsksPlayer = false;
            UsedOnPrey = false;
            Hostile = false;
            AppearConditional = (a, t) => CanUseMagic(a) && LOS.Check(a.Position, t.Position);
            SuccessOdds = (a, t) => SpellcastOdds(this, a, t, 0.1f);
            SuccessEffect = (a, t) => CastHunger(a, t);
            FailEffect = (a, t) => CastDecreaseFriendship(t, a, 0.3f);
        }
    }

    class CastPreyCurse : InteractionBase
    {
        public CastPreyCurse()
        {
            Name = "CastPreyCurse";
            Description =
                "Use magic to cause the target to look especially appealing to potential predators for a while. Hostile, will upset the target. Range 1.";
            Class = ClassType.CastTarget;
            Type = InteractionType.CastPreyCurse;
            Range = 1;
            SoundRange = 2;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "";
            AsksPlayer = false;
            UsedOnPrey = false;
            Hostile = false;
            AppearConditional = (a, t) => CanUseMagic(a) && LOS.Check(a.Position, t.Position);
            SuccessOdds = (a, t) => SpellcastOdds(this, a, t, 0.1f);
            SuccessEffect = (a, t) => CastPreyCurse(a, t);
            FailEffect = (a, t) => CastDecreaseFriendship(t, a, 0.3f);
        }
    }

    class CastAssetThief : InteractionBase
    {
        public CastAssetThief()
        {
            Name = "CastAssetThief";
            Description =
                "Use magic to siphon a target's breast, balls, or cock size. Hostile, will upset the target.";
            Class = ClassType.CastTarget;
            Type = InteractionType.CastAssetThief;
            Range = 0;
            SoundRange = 2;
            Streaming = false;
            StreamingDescription = "";
            AsksPlayerDescription = "";
            AsksPlayer = false;
            UsedOnPrey = false;
            Hostile = false;
            AppearConditional = (a, t) => CanUseMagic(a) && LOS.Check(a.Position, t.Position) && AssetThiefCheck(a, t);
            SuccessOdds = (a, t) => SpellcastOdds(this, a, t, 0.1f);
            SuccessEffect = (a, t) => CastAssetThief(a, t);
            FailEffect = (a, t) => CastDecreaseFriendship(t, a, 0.5f);
        }
    }
}
