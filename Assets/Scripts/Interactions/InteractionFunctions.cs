﻿using System.Linq;
using static HelperFunctions;

static class InteractionFunctions
{
    internal static void OutragedAtSex(Person a, Person t)
    {
        var rel = a.GetRelationshipWith(t);
        rel.RomanticLevel = Utility.PushTowardsNegativeOne(rel.RomanticLevel, .8f);
        rel.FriendshipLevel = Utility.PushTowardsNegativeOne(rel.FriendshipLevel, .3f);
        a.Romance.Dating = null;
        t.Romance.Dating = null;
    }

    internal static void OutragedAtDisposal(Person a, Person t)
    {
        var rel = a.GetRelationshipWith(t);
        rel.RomanticLevel = Utility.PushTowardsNegativeOne(rel.RomanticLevel, .05f);
        rel.FriendshipLevel = Utility.PushTowardsNegativeOne(rel.FriendshipLevel, .03f);
    }

    internal static void UnexpectedDigestion(Person a, Person t)
    {
        t.VoreController.GetProgressOf(a).Willing = false;
        t.VoreController.GetProgressOf(a).ExpectingEndosoma = false;
        var rel = a.GetRelationshipWith(t);
        rel.FriendshipLevel = Utility.PushTowardsNegativeOne(
            t.GetRelationshipWith(a).FriendshipLevel,
            .75f * State.World.Settings.VoreAnger
        );
        rel.RomanticLevel = Utility.PushTowardsNegativeOne(
            t.GetRelationshipWith(a).RomanticLevel,
            .5f * State.World.Settings.VoreAnger
        );
        rel.VoreBetrayed();
        if (a.VoreTracking.Any())
            a.VoreTracking.Last().Betrayed = true;
    }

    /// <summary>
    /// Decreases friendship by value, unless target has masochist trait. Also has a chance to trigger vendetta.
    /// </summary>
    internal static void Insult(Person a, Person t, float value, bool vendettaChance = false)
    {
        if (t.HasTrait(Traits.Masochist))
        {
            DecreaseFriendship(t, a, value/2);
            if (t.Romance.CanSafelyRomance(a))
            {
                t.Needs.Horniness = Utility.PushTowardOne(t.Needs.Horniness, value/4);
                IncreaseRomantic(t, a, value/8);
            }
        }
        else
        {
            DecreaseFriendship(t, a, value);

            if (t.HasTrait(Traits.Vengeful) && vendettaChance == true)
                if (Rand.NextFloat(-1, 0) > t.GetRelationshipWith(a).FriendshipLevel)
                    t.GetRelationshipWith(a).Vendetta = true;
        }
    }

    internal static void Push(Person a, Person t)
    {
        t.Stunned = true;
        Insult(a, t, 0.08f, true);
    }

    internal static void Meet(Person a, Person t)
    {
        a.GetRelationshipWith(t).Met = true;
        t.GetRelationshipWith(a).Met = true;
    }

    internal static void Compliment(Person a, Person t)
    {
        IncreaseFriendship(t, a, .005f);
        IncreaseFriendshipSymetric(a, t, .005f, .010f);
        if (t.Romance.CanSafelyRomance(a) && t.Romance.RomanticDesperation > 0.5)
            IncreaseRomantic(t, a, .002f);
    }

    internal static void FriendlyHug(Person a, Person t)
    {
        IncreaseFriendshipSymetric(a, t, .01f, .01f);
        if (t.Romance.CanSafelyRomance(a) && t.Romance.RomanticDesperation > 0.5)
            IncreaseRomantic(t, a, .003f);
    }

    internal static void AskIfSingle(Person a, Person t)
    {
        a.GetRelationshipWith(t).KnowledgeAbout.DatingTarget = t.Romance.Dating;
        a.GetRelationshipWith(t).KnowledgeAbout.IsDating = t.Romance.Dating != null;
    }

    internal static void AskOut(Person a, Person t)
    {
        a.Romance.Dating = t;
        t.Romance.Dating = a;
    }

    internal static void AskOutFail(Person a, Person t)
    {
        RomanticRejection(a, t);
        a.GetRelationshipWith(t).LastAskedAboutDating = 0;
        t.GetRelationshipWith(a).LastAskedAboutDating = 0;
    }

    internal static void Flirt(Person a, Person t)
    {
        IncreaseRomanticSymetric(a, t, .01f, .01f);
        t.Needs.Horniness = Utility.PushTowardOne(t.Needs.Horniness, .002f);
    }

    internal static void Kiss(Person a, Person t)
    {
        IncreaseRomanticSymetric(a, t, .02f, .02f);
        t.Needs.Horniness = Utility.PushTowardOne(t.Needs.Horniness, .005f);
    }

    internal static void ComplimentAppearance(Person a, Person t)
    {
        IncreaseFriendshipSymetric(a, t, .005f, .01f);
        IncreaseRomanticSymetric(a, t, .005f, .01f);
        IncreaseRomantic(t, a, .0075f);
    }

    internal static void StartSex(Person a, Person t)
    {
        // stat tracking
        if (!a.MiscStats.UniqueSexPartners.Contains(t))
            a.MiscStats.UniqueSexPartners.Add(t);
        if (!t.MiscStats.UniqueSexPartners.Contains(a))
            t.MiscStats.UniqueSexPartners.Add(a);
        a.MiscStats.TimesHadSex++;
        t.MiscStats.TimesHadSex++;
        a.GetRelationshipWith(t).HaveHadSex = true;
        t.GetRelationshipWith(a).HaveHadSex = true;

        a.StreamingSelfAction = SelfActionType.None;
        t.StreamingSelfAction = SelfActionType.None;
        
        // start sex
        if (a.ActiveSex == null)
        {
            a.ActiveSex = new ActiveSex(a, t);
            t.ActiveSex = new ActiveSex(t, a);
        }
    }

    internal static void AskToBeVored(Person a, Person t, VoreType voreType)
    {
        // clear any previous vore progress, allows this to work if both chars are prey
        if (a.GetMyVoreProgress() != null)
        {
            Person PreviousPred = a.FindMyPredator();
            PreviousPred.VoreController.RemovePrey(a);
        }
        if (t.VoreController.HasPrey(voreType) == false)
            t.VoreController.SetPartDigest(voreType, true);
        VoreProgress vore = new VoreProgress(t, a, false, 0, true);
        vore.Willing = true;
        a.GetRelationshipWith(t).VoreWilling();
        t.VoreController.AddPrey(vore, voreType);
        t.StreamingTarget = a;
        switch (voreType)
        {
            case VoreType.Oral:
                t.StreamingAction = InteractionType.OralVore;
                break;
            case VoreType.Anal:
                t.StreamingAction = InteractionType.AnalVore;
                break;
            case VoreType.Unbirth:
                t.StreamingAction = InteractionType.Unbirth;
                break;
            case VoreType.Cock:
                t.StreamingAction = InteractionType.CockVore;
                break;
        }
        a.BeingEaten = true;
        a.Needs.Cleanliness += .1f;

        VoreTrackingRecord VoreRecord = new VoreTrackingRecord(
            t,
            a,
            preyAskedToBeEaten: true,
            eatenDuringSex: t.EatenDuringSex,
            willing: vore.Willing
        );
        GetLocation(t, a, VoreRecord);
        a.VoreTracking.Add(VoreRecord);
    }

    private static void GetLocation(Person a, Person t, VoreTrackingRecord VoreRecord)
    {
        if (a.Position == a.MyRoom)
        {
            VoreRecord.EatenIn = "PredBedroom";
        }
        else if (t.Position == t.MyRoom)
        {
            VoreRecord.EatenIn = "OwnBedroom";
        }
        else if (a.ZoneContainsBed())
        {
            VoreRecord.EatenIn = "VacentBedroom";
        }
        else if (a.ZoneContainsShower())
        {
            VoreRecord.EatenIn = "Shower";
        }
        else if (a.ZoneContainsBathroom())
        {
            VoreRecord.EatenIn = "Bathroom";
        }
        else if (a.ZoneContainsFood())
        {
            VoreRecord.EatenIn = "Cafeteria";
        }
        else if (a.ZoneContainsGym())
        {
            VoreRecord.EatenIn = "Gym";
        }
        else if (a.ZoneContainsLibrary())
        {
            VoreRecord.EatenIn = "Library";
        }
        else if (a.ZoneContainsNurseOffice())
        {
            VoreRecord.EatenIn = "NurseOffice";
        }
        else
        {
            VoreRecord.EatenIn = "Public";
        }
    }

    internal static void AskToBeVoredEndo(Person a, Person t, VoreType voreType)
    {
        // clear any previous vore progress, allows this to work if both chars are prey
        if (a.GetMyVoreProgress() != null)
        {
            Person PreviousPred = a.FindMyPredator();
            PreviousPred.VoreController.RemovePrey(a);
        }
        //if (t.VoreController.HasPrey(voreType) == false)
        t.VoreController.SetPartDigest(voreType, false); //Always cancel to guarantee safety.
        VoreProgress vore = new VoreProgress(t, a, true, 0, true);
        vore.Willing = true;
        a.GetRelationshipWith(t).VoreWilling();
        t.VoreController.AddPrey(vore, voreType);
        t.StreamingTarget = a;
        switch (voreType)
        {
            case VoreType.Oral:
                t.StreamingAction = InteractionType.OralVore;
                break;
            case VoreType.Anal:
                t.StreamingAction = InteractionType.AnalVore;
                break;
            case VoreType.Unbirth:
                t.StreamingAction = InteractionType.Unbirth;
                break;
            case VoreType.Cock:
                t.StreamingAction = InteractionType.CockVore;
                break;
        }
        a.BeingEaten = true;
        a.Needs.Cleanliness += .1f;

        VoreTrackingRecord VoreRecord = new VoreTrackingRecord(
            t,
            a,
            preyAskedToBeEaten: true,
            eatenDuringSex: t.EatenDuringSex,
            willing: vore.Willing
        );
        GetLocation(t, a, VoreRecord);
        a.VoreTracking.Add(VoreRecord);
    }

    internal static void AskToVore(
        Person a,
        Person t,
        VoreType voreType,
        bool digest,
        bool EatenDuringSex = false
    )
    {
        if (a.VoreController.HasPrey(voreType) == false || digest == false)
        {
            a.VoreController.SetPartDigest(voreType, digest);
        }
        if (a.VoreController.GetProgressOf(t) == null)
        {
            VoreProgress vore = new VoreProgress(a, t, !digest);
            a.VoreController.AddPrey(vore, voreType);
            t.GetRelationshipWith(a).VoreWilling();
            t.BeingEaten = true;
            vore.Willing = true;
            t.Needs.Cleanliness += .1f;

            VoreTrackingRecord VoreRecord = new VoreTrackingRecord(
                a,
                t,
                predAskedToEat: true,
                eatenDuringSex: EatenDuringSex,
                willing: vore.Willing
            );
            GetLocation(a, t, VoreRecord);
            t.VoreTracking.Add(VoreRecord);
        }
        else
        {
            UnityEngine.Debug.Log("I didn't think this was suppose to happen");
        }
    }

    internal static void Vore(Person a, Person t, VoreType voreType, bool EatenDuringSex = false)
    {
        if (a.VoreController.GetProgressOf(t) == null)
        {
            VoreProgress vore = new VoreProgress(a, t, false);
            a.VoreController.AddPrey(vore, voreType);
            if (a.VoreController.TargetIsBeingDigested(t) == false)
            {
                a.VoreController.GetProgressOf(t).ExpectingEndosoma = true;
            }
            t.BeingEaten = true;
            if (
                t.IsWillingPreyToTarget(a, a.VoreController.TargetIsBeingDigested(t))
                || (t.HasTrait(Quirks.GutSlut) && t != State.World.ControlledPerson)
            )
            {
                if (t.HasTrait(Quirks.GutSlut))
                    IncreaseFriendship(t, a, .3f);
                var selfAction = SelfActionList.List[SelfActionType.WillingScream];
                State.World.EndOfTurnCallBacks.Add(() => selfAction.OnDo(t));
                vore.Willing = true;
                t.GetRelationshipWith(a).VoreWilling();
            }
            else
            {
                if (a.VoreController.GetProgressOf(t).ExpectingEndosoma)
                {
                    t.GetRelationshipWith(a).FriendshipLevel = Utility.PushTowardsNegativeOne(
                        t.GetRelationshipWith(a).FriendshipLevel,
                        .4f * State.World.Settings.VoreAnger
                    );
                    t.GetRelationshipWith(a).RomanticLevel = Utility.PushTowardsNegativeOne(
                        t.GetRelationshipWith(a).RomanticLevel,
                        .3f * State.World.Settings.VoreAnger
                    );
                    t.GetRelationshipWith(a).VoreBetrayed();
                }
                else
                {
                    t.GetRelationshipWith(a).FriendshipLevel = Utility.PushTowardsNegativeOne(
                        t.GetRelationshipWith(a).FriendshipLevel,
                        .8f * State.World.Settings.VoreAnger
                    );
                    t.GetRelationshipWith(a).RomanticLevel = Utility.PushTowardsNegativeOne(
                        t.GetRelationshipWith(a).RomanticLevel,
                        .6f * State.World.Settings.VoreAnger
                    );
                    t.GetRelationshipWith(a).VoreBetrayed();
                }

                var selfAction = SelfActionList.List[SelfActionType.Scream];
                State.World.EndOfTurnCallBacks.Add(() => selfAction.OnDo(t));
            }
            if (State.World.Settings.FlexibleStats)
                a.Personality.Voracity = Utility.PushTowardOne(a.Personality.Voracity, .05f);
            t.Needs.Cleanliness += .1f;

            VoreTrackingRecord VoreRecord = new VoreTrackingRecord(
                a,
                t,
                eatenDuringSex: EatenDuringSex,
                willing: vore.Willing
            );
            GetLocation(a, t, VoreRecord);
            t.VoreTracking.Add(VoreRecord);
        }
        else
        {
            if (
                a.VoreController.TargetIsBeingDigested(t)
                && a.VoreController.GetProgressOf(t).ExpectingEndosoma
                && t.IsWillingPreyToTarget(a, a.VoreController.TargetIsBeingDigested(t)) == false
            )
            {
                if (a.Dead)
                {
                    a.VoreController.GetProgressOf(t).ExpectingEndosoma = true;
                }
                else if (voreType == VoreType.Oral || voreType == VoreType.Anal)
                {
                    if (a.VoreController.GetProgressOf(t).Stage > 0)
                        InteractionList.List[InteractionType.TellPreyTheyWillBeDigested].OnSucceed(
                            a,
                            t,
                            true
                        );
                    InteractionList.List[InteractionType.UnexpectedDigestion].OnFail(a, t);
                }
                else
                {
                    if (a.VoreController.GetProgressOf(t).Stage > 0)
                        InteractionList.List[InteractionType.TellPreyTheyWillBeMelted].OnSucceed(
                            a,
                            t,
                            true
                        );
                    InteractionList.List[InteractionType.UnexpectedHeat].OnFail(a, t);
                }
            }
            if (a.VoreController.TargetIsBeingDigested(t))
                t.Needs.Cleanliness += .05f;
            else
                t.Needs.Cleanliness += .02f;
        }
    }

    internal static void VoreRefused(Person a, Person t, bool digest = true)
    {
        if (t.IsWillingPreyToTarget(a, digest) == false)
        {
            t.GetRelationshipWith(a).FriendshipLevel = Utility.PushTowardsNegativeOne(
                t.GetRelationshipWith(a).FriendshipLevel,
                .4f * State.World.Settings.VoreAnger
            );
            t.GetRelationshipWith(a).RomanticLevel = Utility.PushTowardsNegativeOne(
                t.GetRelationshipWith(a).RomanticLevel,
                .3f * State.World.Settings.VoreAnger
            );
            t.GetRelationshipWith(a).VoreBetrayed();
        }
    }

    internal static void FreePrey(Person a, Person t, VoreLocation location)
    {
        if (t.VoreController.GetLiving(location) == null)
            return;
        var target = t.VoreController.CurrentSwallow(location);
        if (target == null)
            target = t.VoreController.GetLiving(location);
        IncreaseFriendship(target.Target, a, .4f);
        IncreaseFriendshipSymetric(a, target.Target, .05f, 0);
        DecreaseFriendship(t, a, .15f);
        a.Needs.ChangeEnergy(.03f);
        target.FreePrey(allowFreedMessage: false);
    }

    internal static void FailedFreePrey(Person a, Person t)
    {
        DecreaseFriendship(t, a, .04f);
        a.Needs.ChangeEnergy(.03f);
    }

    internal static void ReleasePrey(Person a, Person t)
    {
        var progress = a.VoreController.GetProgressOf(t);
        State.World.EndOfTurnCallBacks.Add(() => a.AI.SpitUpPreyEquivalent(progress));
    }

    internal static void AskIfCanDigest(Person a, Person t)
    {
        a.VoreController.GetProgressOf(t).ExpectingEndosoma = false;
        if (a.VoreController.GetAllProgress(a.VoreController.GetProgressOf(t).Location).Count == 1)
            a.VoreController.SetPartDigest(a.VoreController.GetProgressOf(t).Location, true);
        t.VoreTracking.Last().PredAskedToDigest = true;
    }

    internal static void MovePreyToStomach(Person a, Person t)
    {
        var progress = a.VoreController.GetProgressOf(t);
        if (progress == null)
            return;
        a.VoreController.MovePrey(progress, VoreLocation.Stomach);
    }

    internal static void MovePreyToBowels(Person a, Person t)
    {
        var progress = a.VoreController.GetProgressOf(t);
        if (progress == null)
            return;
        a.VoreController.MovePrey(progress, VoreLocation.Bowels);
    }

    internal static void ShoveInPrey(Person a, Person t)
    {
        var progress = t.VoreController.CurrentSwallow(VoreLocation.Any);
        if (progress == null)
            return;
        progress.AdvanceStage();
        IncreaseFriendship(t, a, .04f);
        if (progress.Willing == false)
            DecreaseFriendshipAndRomantic(
                progress.Target,
                a,
                .05f * State.World.Settings.VoreAnger
            );
    }

    internal static void Taste(Person a, Person t)
    {
        a.Needs.Horniness = Utility.PushTowardOne(a.Needs.Horniness, .05f);
        t.Needs.Horniness = Utility.PushTowardOne(t.Needs.Horniness, .05f);
        t.GetRelationshipWith(a).VoreTaste();
    }

    internal static void Masturbate(Person a, Person t)
    {
        if (a.ClothingStatus == ClothingStatus.Normal)
            a.Needs.Horniness += .02f;
        else if (a.ClothingStatus == ClothingStatus.Underwear)
            a.Needs.Horniness += .025f;
        else
            a.Needs.Horniness += .03f;
        if (a.Needs.Horniness > 1)
        {
            State.World.EndOfTurnCallBacks.Add(
                () => SelfActionList.List[SelfActionType.Orgasm].OnDo(a)
            );
        }
    }

    internal static void FailedPreyAsk(Person a, Person t)
    {
        t.VoreController.GetProgressOf(a).TurnsSinceLastAsk = 0;
    }

    internal static void ConvinceToDigest(Person a, Person t)
    {
        t.VoreController.GetProgressOf(a).ExpectingEndosoma = false;
        t.VoreController.CheckIfShouldDigest(t.VoreController.GetProgressOf(a).Location);
        a.VoreTracking.Last().PreyAskedToBeDigested = true;
    }

    internal static void StopDigest(Person a, Person t)
    {
        t.VoreController.SetPartDigest(t.VoreController.GetProgressOf(a).Location, false);
    }

    internal static void Spare(Person a, Person t)
    {
        t.VoreController.GetProgressOf(a).TimesBegged++;
        t.VoreController.SetPartDigest(t.VoreController.GetProgressOf(a).Location, false);
        t.VoreController.GetProgressOf(a).ExpectingEndosoma = true;
    }

    internal static void SpareFail(Person a, Person t)
    {
        t.VoreController.GetProgressOf(a).TimesBegged++;
    }

    internal static void BegFail(Person a, Person t)
    {
        t.VoreController.GetProgressOf(a).TimesBegged++;
    }

    internal static void Struggle(Person a, Person t, bool success, bool violent)
    {
        PreyRub(a, t, useEnergy: (violent ? .15f : .04f), willing: false, violent: violent);
        if (success)
            State.World.EndOfTurnCallBacks.Add(
                () => InteractionList.List[InteractionType.PreyEscapes].OnSucceed(t, a)
            );
    }

    internal static void PreyEat(
        Person a,
        Person t,
        VoreType voreType,
        bool PredAskedToEat = false,
        bool PredAskedToDigest = false,
        bool PreyAskedToBeEaten = false,
        bool PreyAskedToBeDigested = false,
        bool EatenDuringSex = false
    )
    {
        if (a.VoreController.GetProgressOf(t) == null)
        {
            Person PreviousPred = t.FindMyPredator();
            PreviousPred.VoreController.RemovePrey(t);
            VoreProgress vore = new VoreProgress(a, t, false);
            a.VoreController.AddPrey(vore, voreType);
            t.BeingEaten = true;
            if (t.IsWillingPreyToTarget(a, a.VoreController.TargetIsBeingDigested(t)))
            {
                var selfAction = SelfActionList.List[SelfActionType.WillingScream];
                State.World.EndOfTurnCallBacks.Add(() => selfAction.OnDo(t));
                vore.Willing = true;
                t.GetRelationshipWith(a).VoreWilling();
            }
            else
            {
                t.GetRelationshipWith(a).FriendshipLevel = Utility.PushTowardsNegativeOne(
                    t.GetRelationshipWith(a).FriendshipLevel,
                    .8f * State.World.Settings.VoreAnger
                );
                t.GetRelationshipWith(a).RomanticLevel = Utility.PushTowardsNegativeOne(
                    t.GetRelationshipWith(a).RomanticLevel,
                    .6f * State.World.Settings.VoreAnger
                );
                t.GetRelationshipWith(a).VoreBetrayed();
                var selfAction = SelfActionList.List[SelfActionType.Scream];
                State.World.EndOfTurnCallBacks.Add(() => selfAction.OnDo(t));
            }
            t.Needs.Cleanliness += .1f;

            // VoreTrackingRecord
            t.VoreTracking.Add(
                new VoreTrackingRecord(
                    a,
                    t,
                    prevPred: PreviousPred,
                    predAskedToEat: PredAskedToEat,
                    predAskedToDigest: PredAskedToDigest,
                    preyAskedToBeEaten: PreyAskedToBeEaten,
                    preyAskedToBeDigested: PreyAskedToBeDigested,
                    eatenDuringSex: EatenDuringSex,
                    preyEatPrey: true,
                    eatenIn: "InsidePred",
                    willing: vore.Willing
                )
            );
        }
    }

    internal static void PreyAskToVore(
        Person a,
        Person t,
        VoreType voreType,
        bool digest,
        bool PredAskedToEat = false,
        bool PredAskedToDigest = false,
        bool PreyAskedToBeEaten = false,
        bool PreyAskedToBeDigested = false,
        bool EatenDuringSex = false
    )
    {
        if (a.VoreController.HasPrey(voreType) == false || digest == false)
        {
            a.VoreController.SetPartDigest(voreType, digest);
        }
        if (a.VoreController.GetProgressOf(t) == null)
        {
            Person PreviousPred = t.FindMyPredator();
            PreviousPred.VoreController.RemovePrey(t);
            VoreProgress vore = new VoreProgress(a, t, !digest);
            a.VoreController.AddPrey(vore, voreType);
            t.BeingEaten = true;
            vore.Willing = true;
            t.GetRelationshipWith(a).VoreWilling();
            a.VoreController.SetPartDigest(voreType, digest);
            t.Needs.Cleanliness += .1f;

            // VoreTrackingRecord
            t.VoreTracking.Add(
                new VoreTrackingRecord(
                    a,
                    t,
                    prevPred: PreviousPred,
                    predAskedToEat: PredAskedToEat,
                    predAskedToDigest: PredAskedToDigest,
                    preyAskedToBeEaten: PreyAskedToBeEaten,
                    preyAskedToBeDigested: PreyAskedToBeDigested,
                    eatenDuringSex: EatenDuringSex,
                    preyEatPrey: true,
                    eatenIn: "InsidePred",
                    willing: vore.Willing
                )
            );
        }
    }

    internal static void BreakUp(Person a, Person t)
    {
        a.Romance.Dating = null;
        t.Romance.Dating = null;
    }

    internal static void Strip(Person a, bool asked = false)
    {
        if (asked)
            a.AI.StripTurns = 5;
        if (a.ClothingStatus == ClothingStatus.Normal)
        {
            a.ClothingStatus = ClothingStatus.Underwear;
            a.ClothesInTile = ClothingStatus.Normal;
        }
        else if (a.ClothingStatus == ClothingStatus.Underwear)
        {
            a.ClothingStatus = ClothingStatus.Nude;
            if (a.ClothesInTile != ClothingStatus.Normal)
                a.ClothesInTile = ClothingStatus.Underwear;
        }
    }

    internal static void Clothe(Person a)
    {
        if (a.ClothingStatus == ClothingStatus.Nude)
            a.ClothingStatus = ClothingStatus.Underwear;
        else if (a.ClothingStatus == ClothingStatus.Underwear)
            a.ClothingStatus = ClothingStatus.Normal;
    }

    internal static void Masturbate(Person a)
    {
        if (a.ClothingStatus == ClothingStatus.Normal)
            a.Needs.Horniness += .02f;
        else if (a.ClothingStatus == ClothingStatus.Underwear)
            a.Needs.Horniness += .025f;
        else
            a.Needs.Horniness += .03f;
        if (a.Needs.Horniness > 1)
        {
            SelfActionList.List[SelfActionType.Orgasm].OnDo(a);
        }
    }

    internal static void EatFood(Person a)
    {
        if (a.Magic.Duration_Hunger <= 0)
            a.Needs.Hunger -= .12f;
    }

    internal static void Exercise(Person a)
    {
        a.Needs.ChangeEnergy(.005f);
        a.Needs.Horniness -= .015f;
        a.GymSatisfaction += 16;

        if (State.World.Settings.FlexibleStats && a.Needs.Energy < 1)
            a.Personality.Strength = Utility.PushTowardOne(a.Personality.Strength, .0015f);
    }

    internal static void ResearchCommunication(Person a)
    {
        a.LibrarySatisfaction += 16;
        if (State.World.Settings.FlexibleStats && a.Needs.Energy < 1)
            a.Personality.Charisma = Utility.PushTowardOne(a.Personality.Charisma, .0005f);
    }

    internal static void BrowseWeb(Person a)
    {
        a.LibrarySatisfaction += 16;
    }

    internal static void StudyArcane(Person a)
    {
        a.LibrarySatisfaction += 16;
        a.Magic.Mana += (0.05f * (a.Magic.ManaRegen + a.Boosts.ManaRegen));

        if (State.World.Settings.FlexibleStats && a.Needs.Energy < 1)
            a.Magic.Proficiency = Utility.PushTowardOne(a.Magic.Proficiency, .002f);
    }

    internal static void Meditate(Person a)
    {
        a.GymSatisfaction += 16;
        a.Magic.Mana += (0.05f * (a.Magic.ManaRegen + a.Boosts.ManaRegen));

        if (State.World.Settings.FlexibleStats && a.Needs.Energy < 1)
            a.Magic.Willpower = Utility.PushTowardOne(a.Magic.Willpower, .002f);
    }

    internal static void AreaVoreTease(Person a, bool endo)
    {
        foreach (var target in GetPeopleWithinXSquares(a, 3))
        {
            if (
                target == State.World.ControlledPerson
                || target.BeingEaten
                || target.VoreController.CurrentSwallow(VoreLocation.Any) != null
            )
                continue;
            if (
                (target.Position == a.Position || State.World.GetZone(a.Position).Accepts(target))
                && target.StreamingTarget == null
            )
            {
                // ENDO CHECKS
                if (endo)
                {
                    // first, check if characters want to endo the teaser and then have them hunt them down endo
                    if ((
                        target.VoreController.CouldVoreTarget(a, VoreType.Oral, DigestionAlias.CanEndo)
                        || target.VoreController.CouldVoreTarget(a, VoreType.Anal, DigestionAlias.CanEndo)
                        || target.VoreController.CouldVoreTarget(a, VoreType.Cock, DigestionAlias.CanEndo)
                        || target.VoreController.CouldVoreTarget(a, VoreType.Unbirth, DigestionAlias.CanEndo))
                        &&
                        (target.AI.Desires.DesireToEndoTarget(a) > Rand.NextFloat(0.3f, 1.4f)
                        || target.AI.Desires.DesireToForciblyEndoTarget(a) > Rand.NextFloat(0.3f, 1.4f))
                    )
                    {
                        target.AI.HuntDownWillingPrey(a, true);
                        State.World.EndOfTurnCallBacks.Add(() => InteractionList.List[InteractionType.VoreTeasedEndo].OnSucceed(target, a));
                    }
                    // if not, extra chance for them to force digestion vore on the teaser
                    else if (
                        (target.VoreController.CouldVoreTarget(a, VoreType.Oral, DigestionAlias.CanVore)
                        || target.VoreController.CouldVoreTarget(a, VoreType.Anal, DigestionAlias.CanVore)
                        || target.VoreController.CouldVoreTarget(a, VoreType.Cock, DigestionAlias.CanVore)
                        || target.VoreController.CouldVoreTarget(a, VoreType.Unbirth, DigestionAlias.CanVore))
                        && target.AI.Desires.DesireToForciblyEatAndDigestTarget(a) > Rand.NextFloat(0.7f, 1.8f)
                        )
                    {
                        target.AI.HuntDownWillingPrey(a, false);
                    }
                }
                // DIGESTION CHECKS
                else if (
                    target.VoreController.CouldVoreTarget(a, VoreType.Oral, DigestionAlias.CanVore)
                    || target.VoreController.CouldVoreTarget(a, VoreType.Anal, DigestionAlias.CanVore)
                    || target.VoreController.CouldVoreTarget(a, VoreType.Cock, DigestionAlias.CanVore)
                    || target.VoreController.CouldVoreTarget(a, VoreType.Unbirth, DigestionAlias.CanVore))
                {
                    if (target.AI.Desires.DesireToVoreAndDigestTarget(a) > Rand.NextFloat(0.3f, 1.4f)
                        || target.AI.Desires.DesireToForciblyEatAndDigestTarget(a) > Rand.NextFloat(0.3f, 1.4f))
                    {
                        target.AI.HuntDownWillingPrey(a, false);
                        State.World.EndOfTurnCallBacks.Add(() => InteractionList.List[InteractionType.VoreTeased].OnSucceed(target, a));
                    }
                }
            }
        }
    }

    internal static void TreatWounds(Person a)
    {
        a.Health += Constants.NurseHeal;
        if (a.Needs.Hunger > 1)
            a.Needs.Hunger = 0.9f;
        if (a.Health > Constants.HealthMax)
            a.Health = Constants.HealthMax;
    }

    internal static void Orgasm(Person a)
    {
        // if there is balls prey or womb prey, dispose it
        if (State.World.Settings.DisposeWithOrgasm)
        {
            if (a.NeedsBallsDisposal())
                Dispose(a, VoreLocation.Balls, true);
            else if (a.NeedsWombDisposal())
                Dispose(a, VoreLocation.Womb, true);
        }

        // deals damage to any digesting prey inside the nutter
        if (State.World.Settings.PredOrgasmAlwaysDamages)
            a.VoreController.DigestDamage(5.0f * State.World.Settings.PredOrgasmDigestionMod * (a.HasTrait(Quirks.LustfulDigestion) ? 2 : 1), VoreLocation.Any);
        else
        {
            a.VoreController.DigestDamage(5.0f * State.World.Settings.PredOrgasmDigestionMod * (a.HasTrait(Quirks.LustfulDigestion) ? 2 : 1), VoreLocation.Balls);
            a.VoreController.DigestDamage(5.0f * State.World.Settings.PredOrgasmDigestionMod * (a.HasTrait(Quirks.LustfulDigestion) ? 2 : 1), VoreLocation.Womb);
        }

        // deals damage to self if orgasming inside digesting predator
        if (a.FindMyPredator() != null)
                if (a.FindMyPredator().VoreController.TargetIsBeingDigested(a) == true)
                {
                var damageMod = (Constants.DigestionDamage
                        * State.World.Settings.AbsorptionSpeed
                        * a.FindMyPredator().Boosts.OutgoingDigestionSpeed
                        * a.Boosts.IncomingDigestionSpeed
                        * State.World.Settings.PreyOrgasmDigestionMod
                        );
                a.Health -= (int)(5 * damageMod);
             }

        if (a.ActiveSex != null)
        {
            if (a.ActiveSex.Other.HasTrait(Quirks.ManaVampire) || a.HasTrait(Quirks.ManaDonor))
                a.ActiveSex.Other.Magic.Mana += 1;
            if (a.ActiveSex.Other.HasTrait(Quirks.ManaVampire))
                a.Magic.Mana -= 1;
        }
        if (a.HasTrait(Quirks.Insatiable))
            a.Needs.Horniness = .5f;
        else
            a.Needs.Horniness = .2f;

        a.Needs.Cleanliness += .12f;
        a.TurnsSinceOrgasm = 0;
        a.MiscStats.TimesHadOrgasm++;
    }

    internal static void EndSex(Person a, Person t)
    {
        a.EndStreamingActions();
        t.EndStreamingActions();
    }

    internal static void MakeOut(Person a, Person t)
    {
        IncreaseRomanticSymetric(a, t, .01f, .01f);
        a.Needs.Horniness = Utility.PushTowardOne(a.Needs.Horniness, .01f);
        t.Needs.Horniness = Utility.PushTowardOne(t.Needs.Horniness, .01f);
    }

    internal static void RubBelly(Person a, Person t)
    {
        IncreaseFriendshipSymetric(t, a, .04f, 0);

        // deals damage to any digesting prey inside belly
        t.VoreController.DigestDamage(1.0f * State.World.Settings.RubDigestionMod, VoreLocation.Stomach);
        t.VoreController.DigestDamage(1.0f * State.World.Settings.RubDigestionMod, VoreLocation.Womb);
        t.VoreController.DigestDamage(1.0f * State.World.Settings.RubDigestionMod, VoreLocation.Bowels);
    }

    internal static void RubBalls(Person a, Person t)
    {
        IncreaseFriendshipSymetric(t, a, .04f, 0);

        // deals damage to any digesting prey inside balls
        t.VoreController.DigestDamage(1.0f * State.World.Settings.RubDigestionMod, VoreLocation.Balls);
    }

    internal static void AskRubBelly(Person a, Person t)
    {
        State.World.EndOfTurnCallBacks.Add(
            () => InteractionList.List[InteractionType.RubBelly].OnSucceed(t, a)
        );
    }

    internal static void AskRubBalls(Person a, Person t)
    {
        State.World.EndOfTurnCallBacks.Add(
            () => InteractionList.List[InteractionType.RubBalls].OnSucceed(t, a)
        );
    }

    /// <summary>
    /// Have the pred react to a prey rubbing or struggling inside of them.
    /// </summary>
    /// <param name="prey"></param>
    /// <param name="pred"></param>
    /// <param name="useEnergy">The amount of energy used by the prey to perform this rub.</param>
    /// <param name="willing">Unambiguously willing, such as a belly rub.</param>
    /// <param name="violent">True if this is a violent struggle.</param>
    internal static void PreyRub(
        Person prey,
        Person pred,
        float useEnergy,
        bool willing,
        bool violent
    )
    {
        if (prey.HasTrait(Quirks.SmartStruggle))
            useEnergy /= 2;
        prey.Needs.ChangeEnergy(useEnergy);
        if (!violent)
        {
            IncreaseFriendship(pred, prey, (willing ? .015f : .01f));
        }
        pred.Needs.Horniness = Utility.PushTowardOne(
            pred.Needs.Horniness,
            .05f * pred.Personality.Voraphilia
                + .05f
                    * pred.Personality.Voraphilia
                    * (willing ? 1 - pred.Personality.Dominance : pred.Personality.Dominance)
        );
    }

    /// <summary>
    /// Shorthand for a willing belly rub.
    /// </summary>
    internal static void PreyWillingRub(Person prey, Person pred)
    {
        PreyRub(prey, pred, useEnergy: 1.0f / 100.0f, willing: true, violent: false);
    }

    internal static void SexEvent(
        Person a,
        Person t,
        float relationship,
        float horniness,
        float cleanliness
    )
    {
        IncreaseRomanticSymetric(a, t, relationship, relationship);
        a.Needs.Horniness += horniness;
        t.Needs.Horniness += horniness;
        a.Needs.Cleanliness += cleanliness;
        t.Needs.Cleanliness += cleanliness;
    }

    internal static void Breastfeed(
        Person a,
        Person t,
        float relationship,
        float horniness,
        float hunger
    )
    {
        IncreaseRomanticSymetric(a, t, relationship, relationship);
        a.Needs.Horniness += horniness;
        t.Needs.Horniness += horniness;
        t.Needs.Hunger -= hunger;

        if (t.HasTrait(Quirks.ManaVampire) || a.HasTrait(Quirks.ManaDonor))
            t.Magic.Mana += 0.05f;
        if (t.HasTrait(Quirks.ManaVampire))
            a.Magic.Mana -= 0.05f;
    }

    internal static void SexEventSplit(
        Person a,
        Person t,
        float relationship,
        float horninessA,
        float horninessT,
        float cleanlinessA,
        float cleanlinessT
    )
    {
        IncreaseRomanticSymetric(a, t, relationship, relationship);
        a.Needs.Horniness += horninessA;
        t.Needs.Horniness += horninessT;
        a.Needs.Cleanliness += cleanlinessA;
        t.Needs.Cleanliness += cleanlinessT;
    }

    internal static void SwitchPosition(Person a, Person t, SexPosition position)
    {
        a.ActiveSex.Position = position;
        t.ActiveSex.Position = position;
        a.ActiveSex.LastPositionChange = 0;
        t.ActiveSex.LastPositionChange = 0;
    }

    internal static void SwitchPosition(
        Person a,
        Person t,
        SexPosition actorPos,
        SexPosition targetPos
    )
    {
        a.ActiveSex.Position = actorPos;
        t.ActiveSex.Position = targetPos;
        a.ActiveSex.LastPositionChange = 0;
        t.ActiveSex.LastPositionChange = 0;
    }

    internal static void Dispose(Person a, VoreLocation location, bool fromOrgasm = false)
    {
        DisposalData disp;
        if (location == VoreLocation.Stomach)
            disp = a.Disposals
                .Where(s => s.Location == VoreLocation.Stomach || s.Location == VoreLocation.Bowels)
                .FirstOrDefault();
        else
            disp = a.Disposals.Where(s => s.Location == location).FirstOrDefault();
        if (disp == null)
            return;

        // disposal reaction if it is turn 2 (mid-disposal) or if it is an orgasm disposal
        if (disp.TurnsDone == 2 || fromOrgasm)
            DisposalReaction(a, disp);
        // increment disposal counter, end if disposal is finished
        State.World.EndOfTurnCallBacks.Add(() =>
        {
            disp.TurnsDone++;
            if (disp.TurnsDone > 3)
            {
                if (fromOrgasm == false)
                    a.EndStreamingActions();
                
                a.Disposals.Remove(disp);
            }
        });
    }
    /// <summary>
    /// Initiate disposal reactions for anyone with LOS on person a
    /// </summary>
    internal static void DisposalReaction(Person a, DisposalData disp)
    {
        foreach (Person person in State.World.GetPeople(true))
        {
            if (person == a)
                continue;
            if (person.BeingEaten == false)
            {
                if (LOS.Check(person.Position, a.Position))
                {
                    if (person.Dead)
                        continue;
                    if (
                        disp.Location == VoreLocation.Stomach
                        || disp.Location == VoreLocation.Bowels
                    )
                    {
                        // enjoy it if voraphile and friendly
                        if (
                            person.Personality.Voraphilia >= 0.7
                            && person.GetRelationshipWith(a).FriendshipLevel >= 0.3
                        )
                            InteractionList.List[InteractionType.PleasedAtDisposal].RunCheck(
                                person,
                                a
                            );
                        // get angry if not voraphile or unfriendly, and not disposing at bathroom
                        else if (
                            (
                                person.Personality.Voraphilia< 0.5
                                || person.GetRelationshipWith(a).FriendshipLevel< 0
                            )
                            && a.ZoneContainsObject(ObjectType.Bathroom) == false
                        )
                            InteractionList.List[InteractionType.OutragedAtDisposal].RunCheck(
                                person,
                                a
                            );
                    }
                    else if (disp.Location == VoreLocation.Balls)
                    {
                        // enjoy it if horny and romantically interested
                        if (
                            person.Romance.CanSafelyRomance(a)
                            && (
                                person.Needs.Horniness >= 0.7
                                || person.GetRelationshipWith(a).RomanticLevel >= 0.3
                            )
                        )
                            InteractionList.List[
                                InteractionType.PleasedAtCockDisposal
                            ].RunCheck(person, a);
                        // get angry if prude or unfriendly, and not disposing at bathroom
                        else if (
                            (
                                person.Personality.Promiscuity < 0.4
                                || person.HasTrait(Quirks.Prude)
                                || person.GetRelationshipWith(a).FriendshipLevel < 0
                            )
                            && a.ZoneContainsObject(ObjectType.Bathroom) == false
                        )
                            InteractionList.List[
                                InteractionType.OutragedAtCockDisposal
                            ].RunCheck(person, a);
                    }
                    else if (disp.Location == VoreLocation.Womb)
                    {
                        // enjoy it if horny and romantically interested
                        if (
                            person.Romance.CanSafelyRomance(a)
                            && (
                                person.Needs.Horniness >= 0.7
                                || person.GetRelationshipWith(a).RomanticLevel >= 0.3
                            )
                        )
                            InteractionList.List[
                                InteractionType.PleasedAtWombDisposal
                            ].RunCheck(person, a);
                        // get angry if prude or unfriendly, and not disposing at bathroom
                        else if (
                            (
                                person.Personality.Promiscuity < 0.4
                                || person.HasTrait(Quirks.Prude)
                                || person.GetRelationshipWith(a).FriendshipLevel < 0
                            )
                            && a.ZoneContainsObject(ObjectType.Bathroom) == false
                        )
                            InteractionList.List[
                                InteractionType.OutragedAtWombDisposal
                            ].RunCheck(person, a);
                    }
                }
            }
        }
    }

    // TARGET SPELLS

    internal static void CastDisrobe(Person a, Person t)
    {
        if (t.ClothingStatus == ClothingStatus.Normal)
        {
            if (a.Magic.Potency > 0.4)
                t.ClothingStatus = ClothingStatus.Nude;
            else
                t.ClothingStatus = ClothingStatus.Underwear;
        }
        else if (t.ClothingStatus == ClothingStatus.Underwear)
        {
            t.ClothingStatus = ClothingStatus.Nude;
        }

        t.Stunned = true;
        CastDecreaseFriendship(t, a, 0.3f);
    }

    internal static void CastHeal(Person a, Person t)
    {
        t.Health += (Constants.HealthMax / 2) + ((int)a.Magic.Potency * (Constants.HealthMax / 2));

        if (
            a != t
            && (
                t.BeingEaten == false
                || t.FindMyPredator().VoreController.GetProgressOf(t).Willing
                || t.FindMyPredator().VoreController.AreSisterPrey(t, a)
            )
        )
            IncreaseFriendship(t, a, 0.2f);
    }

    internal static void CastArouse(Person a, Person t)
    {
        int duration = (int)((16 + (a.Magic.Potency * 12)) * State.World.Settings.SpellDurationMod);

        if (State.World.Settings.ExtendSpellDurations)
            t.Magic.Duration_Aroused += duration;
        else if (t.Magic.Duration_Aroused <= duration)
            // Gives long duration "Aroused" status effect which increases arousal
            // Resets the duration if re-cast
            t.Magic.Duration_Aroused = duration;

        // Also makes target very horny, potentially causing orgasm instantly
        float boost = 0.4f + (a.Magic.Potency * 0.6f);

        if (t.Needs.Horniness > 0.9f)
            SelfActionList.List[SelfActionType.Orgasm].OnDo(t);
        else if (t.Needs.Horniness + boost > 0.95f)
            t.Needs.Horniness = 0.95f;
        else
            t.Needs.Horniness += boost;
    }

    internal static void CastCharm(Person a, Person t)
    {
        int duration = (int)((8 + (a.Magic.Potency * 6)) * State.World.Settings.SpellDurationMod);

        // Sets the "CharmedBy" flag to the caster (charm only boosts effects for this person)
        t.Magic.CharmedBy = a;

        // If target has negative relationship, set it back to 0
        // When charm effect ends, there will be a big friendship penalty
        if (t.GetRelationshipWith(a).FriendshipLevel < 0)
            t.GetRelationshipWith(a).FriendshipLevel = 0;
        if (t.GetRelationshipWith(a).RomanticLevel < 0)
            t.GetRelationshipWith(a).RomanticLevel = 0;

        if (State.World.Settings.ExtendSpellDurations)
            t.Magic.Duration_Charm += duration;
        else if (t.Magic.Duration_Charm <= duration)
            // Gives long duration "Charm" status effect which increases suceptibility to all types of interaction
            // Resets the duration if re-cast
            t.Magic.Duration_Charm = duration;

        // If casting it on target as unwilling prey, sets them to be willing
        if (t.FindMyPredator() == a && a.VoreController.GetProgressOf(t).Willing == false && t.Dead == false)
            InteractionList.List[InteractionType.PreyBecomeWilling].OnSucceed(t, a);

        // Also stun the target for a turn so that the charmer can do a follow-up action
        t.Stunned = true;
    }

    internal static void CastGrow(Person a, Person t)
    {
        // HEIGHT/WEIGHT MODDING/RESETTING HAPPENS IN Magic.cs
        SelfActionList.List[SelfActionType.JustGrew].OnDo(t);

        // INCREASES DURATION OF EFFECT
        int boost = (int)((12 + (a.Magic.Potency * 8)) * State.World.Settings.SpellDurationMod);

        if (t.Magic.Resize_Level < 0)
        {
            t.Magic.Resize_Level = 0;
            t.Magic.Duration_Resized = 0;
            SelfActionList.List[SelfActionType.ResetShrink].OnDo(t);
        }
        else
        {
            t.Magic.Resize_Level += 1;
            t.Magic.Duration_Resized = boost;
        }

        if (
            a != t
            && (t.BeingEaten == false || t.FindMyPredator().VoreController.AreSisterPrey(t, a))
        )
            IncreaseFriendship(t, a, 0.1f);
    }

    internal static void CastShrink(Person a, Person t)
    {
        // HEIGHT/WEIGHT MODDING/RESETTING HAPPENS IN Magic.cs
        SelfActionList.List[SelfActionType.JustShrunk].OnDo(t);

        // INCREASES DURATION OF EFFECT
        int boost = (int)((12 + (a.Magic.Potency * 8)) * State.World.Settings.SpellDurationMod);

        if (t.Magic.Resize_Level > 0)
        {
            t.Magic.Resize_Level = 0;
            t.Magic.Duration_Resized = 0;
            SelfActionList.List[SelfActionType.ResetGrow].OnDo(t);
        }
        else
        {
            t.Magic.Resize_Level -= 1;
            t.Magic.Duration_Resized = boost;
        }

        if (a != t)
        {
            t.Stunned = true;
            if (
                t.Personality.PreyWillingness < State.World.Settings.WillingThreshold
                && (t.BeingEaten == false || t.FindMyPredator().VoreController.AreSisterPrey(t, a))
            )
                CastDecreaseFriendship(t, a, 0.3f);
        }
    }

    internal static void CastFreeze(Person a, Person t)
    {
        // INCREASES DURATION OF EFFECT
        int boost = (int)((4 + (a.Magic.Potency * 4)) * State.World.Settings.SpellDurationMod);
        if (State.World.Settings.ExtendSpellDurations)
            t.Magic.Duration_Freeze += boost;
        else if (t.Magic.Duration_Freeze <= boost) 
            t.Magic.Duration_Freeze = boost;
        t.Stunned = true;
        CastDecreaseFriendship(t, a, 0.3f);
    }

    internal static void CastHunger(Person a, Person t)
    {
        float boost = 0.4f + (a.Magic.Potency * 0.6f);
        int duration = (int)((20 + (20 * boost)) * State.World.Settings.SpellDurationMod);

        if (t.VoreController.CapableOfVore())
        {
            
            if (t.Magic.Duration_Hunger > 0)
                // Attempts to start a hunt if they already have the effect
                t.AI.HuntDownPrey();

            if (State.World.Settings.ExtendSpellDurations)
                t.Magic.Duration_Hunger += duration;
            else if (t.Magic.Duration_Hunger <= duration)
                // Gives long duration "Unnatural Hunger" status effect which drastically raises odds of AI attempting vore and nerfs the cafeteria
                // Resets the duration if re-cast
                t.Magic.Duration_Hunger = duration;
        }
        
        // Also makes target very hungry
        if (t.Needs.Hunger < 0.5)
            t.Needs.Hunger = 0.5f + (boost / 2);
        else
            t.Needs.Hunger += boost / 3;
    }

    internal static void CastPreyCurse(Person a, Person t)
    {
        float boost = 0.4f + (a.Magic.Potency * 0.6f);
        int duration = (int)((25 + (25 * boost)) * State.World.Settings.SpellDurationMod);

        if (State.World.Settings.ExtendSpellDurations)
            t.Magic.Duration_PreyCurse += duration;
        else if (t.Magic.Duration_PreyCurse <= duration)
            // Gives long duration "Prey Curse" status effect which drastically raises odds of other AI attempting to vore this character
            // Resets the duration if re-cast
            t.Magic.Duration_PreyCurse = duration;

        if (a != t)
        {
            // Also stuns the target and makes them upset, provided it isn't self-cast
            CastDecreaseFriendship(t, a, 0.3f);
            t.Stunned = true;
        }
        
    }

    internal static bool AssetThiefCheck(Person a, Person t)
    {
        if (t.PartList.BreastSize > 0 && t.GenderType.HasBreasts && a.GenderType.HasBreasts)
            return true;
        if (t.GenderType.HasDick && a.GenderType.HasDick)
            if (t.PartList.DickSize > 0 || t.PartList.BallSize > 0)
                return true;
        return false;
    }

    internal static void CastAssetThief(Person a, Person t)
    {
        float boost = 0.2f + (a.Magic.Potency * 0.3f);

        if (t.PartList.BreastSize > 0 && t.GenderType.HasBreasts && a.GenderType.HasBreasts)
        {
            float breastval = t.PartList.BreastSize * boost;
            t.PartList.BreastSize -= breastval;
            a.PartList.BreastSize += breastval;
        }
        if (t.PartList.DickSize > 0 && t.GenderType.HasDick && a.GenderType.HasDick)
        {
            float dickval = t.PartList.DickSize * boost;
            t.PartList.DickSize -= dickval;
            a.PartList.DickSize += dickval;
        }
        if (t.PartList.BallSize > 0 && t.GenderType.HasDick && a.GenderType.HasDick)
        {
            float ballsval = t.PartList.BallSize * boost;
            t.PartList.BallSize -= ballsval;
            a.PartList.BallSize += ballsval;
        }

        // Also stuns the target and makes them upset
        CastDecreaseFriendship(t, a, 0.5f);
        t.Stunned = true;
    }

    // SELF SPELLS

    internal static void CastPassdoor(Person a)
    {
        // INCREASES DURATION OF EFFECT
        int boost = (int)((7 + (a.Magic.Potency * 5)) * State.World.Settings.SpellDurationMod);
        if (State.World.Settings.ExtendSpellDurations)
            a.Magic.Duration_Passdoor += boost;
        else if (a.Magic.Duration_Passdoor <= boost)
            a.Magic.Duration_Passdoor = boost;
    }

    internal static void CastHeal(Person a)
    {
        CastHeal(a, a);
    }

    internal static void CastGrow(Person a)
    {
        CastGrow(a, a);
    }

    internal static void CastShrink(Person a)
    {
        CastShrink(a, a);
    }

    internal static void CastPreyCurse(Person a)
    {
        CastPreyCurse(a, a);
    }

    internal static void CharmEnded(Person a)
    {
        Person t = a.Magic.CharmedBy;

        a.EndStreamingActions();

        // If the character has guinea pig, none of this should happen
        if (a.HasTrait(Traits.GuineaPig) == false)
        {
            // Charmed char should hate charmer
            CastDecreaseFriendship(a, t, 0.7f);
            DecreaseRomantic(a, t, 0.3f);

            // End sex if was having it with charmer
            if (a.ActiveSex != null && a.ActiveSex.Other == t)
                SexInteractionList.List[SexInteractionType.EndSex].OnDo(a, t);

            // Become unwilling if pred is charmer
            if (a.FindMyPredator() == t && t.VoreController.GetProgressOf(a).Willing && a.Dead == false)
                InteractionList.List[InteractionType.PreyBecomeUnwilling].OnSucceed(a, t);

            // Break up if partner is charmer
            if (a.Romance.Dating == t)
                BreakUp(a, t);
        }

        // If the character would never have been willing (ie: charmed into cheating or ignoring orientation) extra romance penalty
        if (!a.Romance.CanSafelyRomance(t, true))
        {
            DecreaseRomantic(a, t, 0.7f);
        }

    }
}