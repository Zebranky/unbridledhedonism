﻿struct RepeatAction
{
    bool LastInt;
    InteractionBase LastIntType;

    bool LastSex;
    SexInteractionBase LastSexType;

    bool LastSelf;
    SelfActionBase LastSelfType;

    bool LastWait;

    Person LastTarget;

    internal void SetLastWait()
    {
        LastWait = true;
        LastInt = false;
        LastSex = false;
        LastSelf = false;
        LastIntType = null;
        LastTarget = null;
    }

    internal void SetLast(InteractionBase type, Person target)
    {
        LastWait = false;
        LastInt = true;
        LastSex = false;
        LastSelf = false;
        LastIntType = type;
        LastTarget = target;
    }

    internal void SetLast(SexInteractionBase type, Person target)
    {
        LastWait = false;
        LastInt = false;
        LastSex = true;
        LastSelf = false;
        LastSexType = type;
        LastTarget = target;
    }

    internal void SetLast(SelfActionBase type)
    {
        LastWait = false;
        LastInt = false;
        LastSex = false;
        LastSelf = true;
        LastSelfType = type;
        LastTarget = null;
    }

    internal void RepeatLast()
    {
        if (LastWait && State.GameManager.TurnButtons[0].gameObject.activeSelf)
        {
            State.GameManager.MoveController.SkipTurn();
        }
        if (State.World.ControlledPerson.Health < Constants.EndHealth)
        {
            Clear();
            return;
        }
        if (State.World.ControlledPerson.Dead)
        {
            if (LastIntType != null && LastIntType.Type == InteractionType.PreyWait)
            {
                LastIntType.RunCheck(State.World.ControlledPerson, LastTarget);
                State.World.NextTurn();
                return;
            }
            Clear();
            return;
        }
        if (LastInt && State.World.ControlledPerson.BeingEaten)
        {
            if (
                LastIntType.Class == ClassType.VoreAskThem
                || LastIntType.Class == ClassType.VoreAskToBe
                || LastIntType.Class == ClassType.VoreConsuming
            )
            {
                LastInt = false;
            }
        }

        if (State.World.ControlledPerson.ActiveSex != null)
        {
            LastInt = false;
            LastSelf = false;
        }
        if (
            State.GameManager.ContinueActionPanel.activeSelf
            && State.GameManager.ContinueActionButton.interactable
        )
        {
            State.GameManager.ContinueActionButton.onClick.Invoke();
            return;
        }
        if (LastSex)
        {
            if (
                State.World.ControlledPerson.ActiveSex != null
                && LastSexType.AppearConditional(State.World.ControlledPerson, LastTarget)
                && LastSexType.PositionAllows(State.World.ControlledPerson)
            )
            {
                LastSexType.OnDo(State.World.ControlledPerson, LastTarget);
                State.World.NextTurn();
                return;
            }
        }
        if (
            State.World.ControlledPerson.StreamingAction != InteractionType.None
            && State.World.ControlledPerson.StreamingAction != InteractionType.StartSex
        )
        {
            if (
                State.World.ControlledPerson.StreamingTarget == null
                || InteractionList.List[State.World.ControlledPerson.StreamingAction].Range
                    < State.World.ControlledPerson.Position.GetNumberOfMovesDistance(
                        State.World.ControlledPerson.StreamingTarget.Position
                    )
            )
            {
                State.World.ControlledPerson.EndStreamingActions();
                Clear();
                return;
            }
            var action = InteractionList.List[State.World.ControlledPerson.StreamingAction];
            action.OnSucceed(
                State.World.ControlledPerson,
                State.World.ControlledPerson.StreamingTarget,
                true
            );
            State.World.NextTurn();
            return;
        }
        else if (State.World.ControlledPerson.StreamingSelfAction != SelfActionType.None)
        {
            var action = SelfActionList.List[State.World.ControlledPerson.StreamingSelfAction];
            action.OnDo(State.World.ControlledPerson, true);
            State.World.NextTurn();
            return;
        }
        if (LastInt)
        {
            if (LastIntType.UsedOnPrey && LastTarget.BeingEaten == false)
                return;
            if (
                LastIntType.AppearConditional(State.World.ControlledPerson, LastTarget)
                && LastIntType.Range
                    >= State.World.ControlledPerson.Position.GetNumberOfMovesDistance(
                        LastTarget.Position
                    )
            )
            {
                LastIntType.RunCheck(State.World.ControlledPerson, LastTarget);
                State.World.NextTurn();
                return;
            }
        }

        if (LastSelf)
        {
            if (LastSelfType.AppearConditional(State.World.ControlledPerson))
            {
                LastSelfType.OnDo(State.World.ControlledPerson);
                State.World.NextTurn();
                return;
            }
        }
    }

    internal void Clear()
    {
        LastWait = false;
        LastInt = false;
        LastSelf = false;
        LastSex = false;
        LastTarget = null;
    }
}
