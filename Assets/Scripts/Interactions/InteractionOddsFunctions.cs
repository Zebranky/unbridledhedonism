﻿using GeneratedInteractions;
using System;
using UnityEngine;
using static UnityEngine.GraphicsBuffer;

static class InteractionOddsFunctions
{
    public static float FriendshipCheckOdds(
        InteractionBase interaction,
        Person actor,
        Person target,
        float difficulty
    )
    {
        float successOdds =
            .25f
            + (actor.Personality.Charisma * 0.4f)
            + (target.GetRelationshipWith(actor).FriendshipLevel * 0.75f)
            - difficulty;

        if ((interaction.Type == InteractionType.Talk) && target.HasTrait(Traits.Talkative))
            successOdds += 0.3f;
        if ((interaction.Type == InteractionType.FriendlyHug) && target.HasTrait(Traits.LovesHugs))
            successOdds *= 1.3f;

        // special checks for AskToEnter
        if (interaction.Type == InteractionType.AskToEnter)
        {
            // give a bonus based on romantic level
            successOdds += (target.GetRelationshipWith(actor).RomanticLevel * 0.3f);

            // if target is doing something horny and isn't promiscuous, fail
            if ((target.ActiveSex != null || target.StreamingSelfAction == SelfActionType.Masturbate)
                && (
                    target.Personality.Promiscuity < 0.8
                    || target.HasTrait(Traits.LovesPrivateSex)
                    || target.HasTrait(Quirks.Prude)
                )
                && target.HasTrait(Traits.Exhibitionist) == false
                && target.Magic.Duration_Aroused != 0
                && target.Magic.Duration_Charm != 0
            )
                successOdds = 0;

            // if dating, auto-succeed
            if (target.Romance.Dating == actor)
                successOdds = 1;

            // big penalty if target is sleeping and heavy sleeper
            if (target.HasTrait(Traits.HeavySleeper) && target.StreamingSelfAction == SelfActionType.Rest)
                successOdds *= 0.1f;
        }

        if (target.Magic.IsCharmedBy(actor))
            successOdds = .75f + (successOdds / 4);

        // reduce odds based on recent rejections
        if (actor.GetRelationshipWith(target).Rejections.ContainsKey(interaction.Type))
            successOdds *= RejectionMod(interaction, actor, target);

        if (successOdds <= 0)
            successOdds = 0;

        return successOdds;
    }

    public static float BellyRubAskOdds(InteractionBase interaction, Person actor, Person target, float difficulty)
    {
        float worshipMod = 0;
        float successOdds;
        Relationship relationship = target.GetRelationshipWith(actor);

        if (target.HasTrait(Quirks.PredWorship))
            worshipMod += 0.5f;
        if (target.HasTrait(Traits.BellyFixation))
            worshipMod += 0.3f;
        if (actor.HasTrait(Traits.Intimidating))
            successOdds =
                0.25f
                + worshipMod
                + (actor.Personality.Strength * 0.5f)
                + ((1 - target.Personality.Dominance) * 0.2f)
                + (relationship.GetRomanticLevelToTarget())
                + (relationship.FriendshipLevel * 0.2f)
                + (target.Personality.Voraphilia * (target.Needs.Horniness + 0.2f))
                - 0.3f
                - difficulty;
        else
            successOdds =
                worshipMod
                + (actor.Personality.Charisma * 0.2f)
                + ((1 - target.Personality.Dominance) * 0.2f)
                + (relationship.GetRomanticLevelToTarget())
                + (relationship.FriendshipLevel * 0.2f)
                + (target.Personality.Voraphilia * (target.Needs.Horniness + 0.2f))
                - 0.3f
                - difficulty;
        
        // reduce odds based on recent rejections
        if (actor.GetRelationshipWith(target).Rejections.ContainsKey(interaction.Type))
            successOdds *= RejectionMod(interaction, actor, target);

        if (target.Magic.IsCharmedBy(actor))
            successOdds = .75f + (successOdds / 4);

        if (successOdds <= 0)
            successOdds = 0;

        return successOdds;
    }

    public static float BellyRubOdds(InteractionBase interaction, Person actor, Person target, float difficulty)
    {
        float worshipMod = 0;
        float successOdds;
        Relationship relationship = target.GetRelationshipWith(actor);
        if (target.HasTrait(Traits.BellyFixation))
            worshipMod += 0.3f;
        if (target.HasTrait(Traits.DemandsWorship))
            return 1;
        else
            successOdds =
                (actor.Personality.Charisma * 0.2f)
                + (relationship.GetRomanticLevelToTarget())
                + (relationship.FriendshipLevel * 0.2f)
                + (target.Personality.Voraphilia * (target.Needs.Horniness + 0.5f))
                + worshipMod
                - difficulty;

        if (target.Magic.IsCharmedBy(actor))
            successOdds = .75f + (successOdds / 4);

        // reduce odds based on recent rejections
        if (actor.GetRelationshipWith(target).Rejections.ContainsKey(interaction.Type))
            successOdds *= RejectionMod(interaction, actor, target);

        // limit odds to 0
        if (successOdds <= 0)
            successOdds = 0;

        return successOdds;
    }

    public static float StrengthCheckOdds(Person actor, Person target, float difficulty)
    {
        float otherMods = 0;
        float successOdds;

        if (target.Needs.Energy > .4f)
            otherMods += Math.Max(Math.Min(.4f * target.Needs.Energy - .16f, .2f), 0);
        if (actor.Needs.Energy > .4f)
            otherMods -= Math.Max(Math.Min(.4f * actor.Needs.Energy - .16f, .2f), 0);
        successOdds =
            .5f
            + (actor.Personality.Strength * 0.6f)
            - (target.Personality.Strength * 0.6f)
            + otherMods
            - difficulty;

        if (successOdds <= 0)
            successOdds = 0;
        return successOdds;
    }

    public static float SpellcastOdds(
        InteractionBase interaction,
        Person actor,
        Person target,
        float difficulty
    )
    {
        if (actor.HasTrait(Quirks.PerfectSpells))
            return 1;

        float otherMods = 0;
        float successOdds;

        if (actor.HasTrait(Quirks.MagicalLust) && actor.Needs.Horniness >= 0.5f)
            otherMods += ((actor.Needs.Horniness - 0.5f) / 2);
        if (
            actor.HasTrait(Quirks.Transmuter)
            && (
                (interaction.Type == InteractionType.CastGrow)
                || (interaction.Type == InteractionType.CastShrink)
            )
        )
            otherMods += 0.15f;
        if (actor.HasTrait(Quirks.Charmer) && (interaction.Type == InteractionType.CastCharm))
            otherMods += 0.1f;

        successOdds =
            0.6f
            + (actor.Personality.Charisma * 0.6f)
            + (actor.Magic.Proficiency * 0.3f)
            - (target.Personality.Dominance * 0.6f)
            - (target.Magic.Willpower * 0.3f)
            + otherMods
            - difficulty;

        if (interaction.Type == InteractionType.CastGrow)
            successOdds += target.Personality.PredWillingness / 2;
        if (interaction.Type == InteractionType.CastShrink)
            successOdds += target.Personality.PreyWillingness / 2;
        if (actor.HasTrait(Quirks.Intelligent))
            successOdds *= 1.2f;
        if (target.HasTrait(Quirks.WeakWilled))
            successOdds *= 1.2f;
        if (target.Magic.IsCharmedBy(actor))
            successOdds *= 1.5f;
        if (successOdds <= 0)
            successOdds = 0;
        return successOdds;
    }

    public static float SelfSpellcastOdds(SelfActionBase selfAction, Person actor, float difficulty)
    {
        if (actor.HasTrait(Quirks.PerfectSpells))
            return 1;

        float otherMods = 0;
        float successOdds;

        if (actor.HasTrait(Quirks.MagicalLust) && actor.Needs.Horniness >= 0.5f)
            otherMods += ((actor.Needs.Horniness - 0.5f) / 2);
        if (
            actor.HasTrait(Quirks.Transmuter)
            && (
                (selfAction.Type == SelfActionType.CastGrowSelf)
                || (selfAction.Type == SelfActionType.CastShrinkSelf)
            )
        )
            otherMods += 0.15f;
        if (actor.BeingEaten && actor.FindMyPredator().VoreController.TargetIsBeingDigested(actor))
        {
            if (
                selfAction.Type == SelfActionType.CastHealSelf
                || selfAction.Type == SelfActionType.CastGrowSelf
            )
                otherMods -= 0.3f;
        }

        successOdds = 0.8f + ((actor.Magic.Proficiency) * 0.3f) + otherMods - difficulty;

        if (actor.HasTrait(Quirks.Intelligent))
            successOdds *= 1.2f;

        if (successOdds <= 0)
            successOdds = 0;
        return successOdds;
    }

    public static float VoreCheckOdds(Person pred, Person prey, VoreType type)
    {
        float otherMods = 0;
        if (prey.TurnsSinceOrgasm < 4)
            otherMods += .2f - prey.TurnsSinceOrgasm * .05f;
        if (
            prey.StreamingSelfAction != SelfActionType.None
            || prey.StreamingAction != InteractionType.None
        )
            otherMods += .1f;
        if (pred.HasTrait(Quirks.LoveBites) && pred.Needs.Horniness > .6f)
            otherMods += (pred.Needs.Horniness - .6f) / 4;
        if (pred.HasTrait(Quirks.GreatHunger) && pred.Needs.Hunger > .4f)
            otherMods += (pred.Needs.Hunger - .4f) / 6;
        if (
            pred.HasTrait(Quirks.ViciousPredator)
            && pred.GetRelationshipWith(prey).FriendshipLevel < 0
        )
            otherMods += .1f;
        if (pred.HasTrait(Quirks.BiggerFish) && prey.VoreController.CapableOfVore())
            otherMods += .1f;
        if (pred.HasTrait(Quirks.LowOnTheChain) && prey.VoreController.CapableOfVore() == false)
            otherMods += .1f;
        if (pred.HasTrait(Quirks.PredInPrivate) && HelperFunctions.InPrivateArea(pred, prey))
            otherMods += .1f;
        if (prey.HasTrait(Quirks.SlipperyWhenWet) && prey.Needs.Horniness > .6f)
            otherMods -= Math.Max(Math.Min(.6f * pred.Needs.Energy - .36f, .2f), 0);
        if (pred.HasTrait(Quirks.FearsomeReputation))
            otherMods += .008f * Math.Min(pred.MiscStats.TimesDigestedOther, 20);

        if (prey.Needs.Energy > .4f)
            otherMods += Math.Max(Math.Min(.4f * prey.Needs.Energy - .16f, .2f), 0);
        if (pred.Needs.Energy > .4f)
            otherMods -= Math.Max(Math.Min(.4f * pred.Needs.Energy - .16f, .2f), 0);
        if (prey.ActiveSex != null)
            otherMods += .2f;
        if (prey.Magic.Duration_PreyCurse > 0)
            otherMods += .3f;

        if (prey.ClothingStatus == ClothingStatus.Nude)
            otherMods += .2f;
        if (prey.ClothingStatus == ClothingStatus.Underwear)
            otherMods += .1f;

        if (prey.StreamingSelfAction == SelfActionType.Rest)
            otherMods += .3f;
        if (prey.StreamingSelfAction == SelfActionType.Masturbate)
            otherMods += .1f;

        otherMods += pred.VoreController.VorePower * .04f;
        otherMods -= prey.VoreController.VorePower * .04f;
        var predSize = (float)
            Math.Pow(pred.PartList.Height / prey.PartList.Height, State.World.Settings.SizeFactor);
        if (pred.HasTrait(Quirks.ElasticPred) && predSize < 1)
            predSize = 1;
        otherMods += (float)Math.Log(predSize, 10.0f);

        var odds =
            .3f
            + (prey.GetRelationshipWith(pred).FriendshipLevel * .3f)
            + ((pred.Personality.Strength + 2 * pred.Personality.Voracity) / 3)
            - (prey.Personality.Strength * (prey.Magic.IsCharmedBy(pred) ? 0.2f : 1))
            + otherMods;

        if (pred.Needs.Energy >= 1f)
            odds *= 0.5f;
        if (prey.Needs.Energy >= 1f)
            odds *= 1.5f;
        if (prey.Health <= Constants.HealthMax * 0.8f)
            odds *= 1.2f;

        if (type == VoreType.Oral && pred.LastTarget == prey)
        {
            if (pred.LastAction == InteractionType.Kiss)
                odds *= 1.1f;
            if (pred.LastAction == InteractionType.Hug)
                odds *= 1.1f;
            if (pred.LastAction == InteractionType.MakeOut)
                odds *= 1.2f;
            if (pred.LastAction == InteractionType.Taste)
                odds *= 1.5f;
        }

        switch (type)
        {
            case VoreType.Oral:
                odds /= State.World.Settings.OralVoreDifficulty;
                break;
            case VoreType.Unbirth:
                odds /= State.World.Settings.UnbirthDifficulty;
                break;
            case VoreType.Cock:
                odds /= State.World.Settings.CockVoreDifficulty;
                break;
            case VoreType.Anal:
                odds /= State.World.Settings.AnalVoreDifficulty;
                break;
        }
        if (pred.HasTrait(Quirks.PerfectAttack))
        {
            if (prey.HasTrait(Quirks.PerfectDefense) == false)
                odds = 1;
        }
        else if (prey.HasTrait(Quirks.PerfectDefense))
            odds = 0;

        if (odds <= 0)
            odds = 0;

        return odds;
    }

    public static float AllowDigestOdds(InteractionBase interaction, Person pred, Person prey)
    {
        float odds = prey.AI.Desires.DesireToBeDigested(pred);

        // reduce odds based on recent rejections
        if (prey.GetRelationshipWith(pred).Rejections.ContainsKey(interaction.Type))
            odds *= RejectionMod(interaction, pred, prey);

        if (odds <= 0)
            odds = 0;

        return odds;
    }

    public static float PreyConvinceAllowDigestOdds(InteractionBase interaction, Person actor, Person target)
    {
        float odds = target.AI.Desires.DesireToBeDigested(target.FindMyPredator());

        // friendship based modifier
        odds *= target.AI.Desires.MiddleOverallAttachment(actor);

        // reduce odds based on recent rejections
        if (actor.GetRelationshipWith(target).Rejections.ContainsKey(interaction.Type))
            odds *= RejectionMod(interaction, actor, target);

        if (odds <= 0)
            odds = 0;

        return odds;
    }

    public static float PreyAskToBeFreeOdds(InteractionBase interaction, Person actor, Person target)
    {
        float odds = target.AI.Desires.AskedDesireToReleaseEndoPrey(actor);

        // reduce odds based on recent rejections
        if (actor.GetRelationshipWith(target).Rejections.ContainsKey(interaction.Type))
            odds *= RejectionMod(interaction, actor, target);

        if (odds <= 0)
            odds = 0;

        return odds;
    }

    public static float AskToBeVoredOdds(InteractionBase interaction, Person actor, Person target, bool endo, VoreType type)
    {
        float odds;
        VoreLocation loc = target.VoreController.GetRespectiveStartingPoint(type);

        // ENDO CHECKS
        if (endo)
        {
            odds = target.AI.Desires.DesireToEndoTarget(actor);
            
            // modify value if target already has digesting prey
            if (target.VoreController.PartCurrentlyDigests(loc) && target.VoreController.HasPrey(loc))
                foreach (VoreProgress progress in target.VoreController.GetAllProgress(loc))
                    odds *= SpareCheckOdds(progress.Target, target);
        }

        // DIGESTION CHECKS
        else
        {
            odds = target.AI.Desires.DesireToVoreAndDigestTargetOffer(actor);

            // modify value if target already has endo prey
            if (target.VoreController.PartCurrentlyDigests(loc) == false && target.VoreController.HasPrey(loc))
                foreach (VoreProgress progress in target.VoreController.GetAllProgress(loc))
                    odds *= target.AI.Desires.DesireToForciblyEatAndDigestTarget(progress.Target);
        }

        // modify value based on vore type interest
        float typeinterest = 2;
        switch (type)
        {
            case VoreType.Oral:
                typeinterest *= target.Personality.OralVoreInterest;
                break;
            case VoreType.Unbirth:
                typeinterest *= target.Personality.UnbirthInterest;
                break;
            case VoreType.Cock:
                typeinterest *= target.Personality.CockVoreInterest;
                break;
            case VoreType.Anal:
                typeinterest *= target.Personality.AnalVoreInterest;
                break;
        }
        typeinterest -= 1;
        float interestMod = (odds * typeinterest) / 5;
        float finalOdds = odds + interestMod;

        // reduce odds based on recent rejections
        if (actor.GetRelationshipWith(target).Rejections.ContainsKey(interaction.Type))
            finalOdds *= RejectionMod(interaction, actor, target);

        if (finalOdds <= 0)
            finalOdds = 0;

        return finalOdds;
    }

    public static float VoreEscapeCheckOdds(Person prey, Person pred, float multiplier)
    {
        float successOdds;
        float otherMods = 0;

        if (prey.Needs.Energy > .4f)
            otherMods -= Math.Max(Math.Min(.4f * prey.Needs.Energy - .16f, .2f), 0);
        if (pred.Needs.Energy > .4f)
            otherMods += Math.Max(Math.Min(.4f * pred.Needs.Energy - .16f, .2f), 0);
        if (pred.HasTrait(Quirks.SkilledEscape))
        {
            if (prey.Needs.Energy > .5f)
                otherMods -= Math.Max(Math.Min(.5f * prey.Needs.Energy - .2f, .2f), 0);
            if (prey.Needs.Energy < .5f)
                otherMods += Math.Max(Math.Min(.5f * pred.Needs.Energy - .2f, .2f), 0);
        }
        multiplier *= prey.Boosts.EscapeSkill;
        multiplier *= State.World.Settings.EscapeRate;

        otherMods += prey.VoreController.VorePower * .02f;
        otherMods -= pred.VoreController.VorePower * .02f;
        var preySize = (float)
            Math.Pow(prey.PartList.Height / pred.PartList.Height, State.World.Settings.SizeFactor);
        if (pred.HasTrait(Quirks.ElasticPred) && preySize > 1)
            preySize = 1;
        multiplier *= preySize;

        if (prey.HasTrait(Quirks.PerfectEscape))
        {
            if (pred.HasTrait(Quirks.PerfectReflexes) == false)
                return 1;
        }
        else if (pred.HasTrait(Quirks.PerfectReflexes))
            return 0;

        successOdds =
            multiplier
            * (
                .05f
                - ((pred.Personality.Strength + 2 * pred.Personality.Voracity) / 60)
                + prey.Personality.Strength / 20
                + otherMods / 8
            );

        if (successOdds <= 0)
            successOdds = 0;

        return successOdds;
    }

    public static float BegCheckOdds(Person prey, Person pred)
    {
        float successOdds;

        // fail if pred is endo dom, vore is endo, and endo is set to last forever
        if (
            pred.Personality.EndoDominator
            && State.World.Settings.EndoDominatorsHoldForever
            && pred.VoreController.TargetIsBeingDigested(prey) == false
        )
            return 0;

        // fail if pred is blackoutpred, digesting, and in bedroom
        if (
            pred.HasTrait(Traits.BlackoutPred)
            && pred.ZoneContainsBed()
            && !prey.BeingSwallowed
            && pred.VoreController.TargetIsBeingDigested(prey)
        )
            return 0;

        // fail if pred is heavy sleeper and sleeping
        if (pred.HasTrait(Traits.HeavySleeper)
            && pred.StreamingSelfAction == SelfActionType.Rest)
            return 0;

        // fail if pred has unyielding trait
        if (pred.HasTrait(Traits.Unyielding))
            return 0;

        // odds calculation
        successOdds =
            0.35f
            * ((pred.Personality.Kindness * 2) - 1)
            * (1 - State.World.Settings.PredConviction)
            / (0.15f + pred.AI.Desires.DesireToVoreTargetWithoutPredWillingness(prey))
            / (float)Math.Pow(1 + pred.VoreController.GetProgressOf(prey).TimesBegged, 2)
            / (pred.HasTrait(Traits.Traitor) ? 1.5f : 1);

        if (pred.Magic.IsCharmedBy(prey))
            successOdds = .75f + (successOdds / 4);

        if (successOdds <= 0)
            successOdds = 0;

        return successOdds;
    }

    public static float SpareCheckOdds(Person prey, Person pred)
    {
        float successOdds;

        // fail if there is other digesting prey in same location
        if (pred.VoreController
                .GetAllProgress(pred.VoreController.GetProgressOf(prey).Location)
                .Count > 1)
            return 0;
        
        // fail if pred is only interested in digestion
        if (pred.Personality.VorePreference == VorePreference.Digestion)
            return 0;

        // fail if pred is blackoutpred, digesting, and in bedroom
        if (
            pred.HasTrait(Traits.BlackoutPred)
            && pred.ZoneContainsBed()
            && !prey.BeingSwallowed
            && pred.VoreController.TargetIsBeingDigested(prey)
        )
            return 0;

        // fail if pred is heavy sleeper and sleeping
        if (pred.HasTrait(Traits.HeavySleeper)
            && pred.StreamingSelfAction == SelfActionType.Rest)
            return 0;

        // fail if pred has unyielding trait
        if (pred.HasTrait(Traits.Unyielding))
            return 0;

        // odds calculation
        successOdds =
            0.325f
            * ((pred.Personality.Kindness * 2) - 1)
            * (1 - State.World.Settings.PredConviction)
            / (0.15f + pred.AI.Desires.DesireToDigestTarget(prey))
            / (float)Math.Pow(1 + pred.VoreController.GetProgressOf(prey).TimesBegged, 2)
            / (pred.HasTrait(Traits.Traitor) ? 1.5f : 1);

        if (pred.Magic.IsCharmedBy(prey))
            successOdds = .75f + (successOdds / 4);

        if (successOdds <= 0)
            successOdds = 0;

        return successOdds;
    }

    public static float SpareCheckOdds(InteractionBase interaction, Person prey, Person pred)
    {
        float successOdds = SpareCheckOdds(prey, pred);

        // reduce odds based on recent rejections
        if (prey.GetRelationshipWith(pred).Rejections.ContainsKey(interaction.Type))
            successOdds *= RejectionMod(interaction, prey, pred);

        return successOdds;
    }

    public static float WillingVoreDigestCheckOdds(InteractionBase interaction, Person actor, Person target, VoreType type)
    {
        // digestive vore odds check
        float finalOdds;
        float difficulty = State.World.Settings.AskToVoreDifficulty;

        // set odds based on desire to be digest vored (factors in willingness, personality, and friendship)
        float successOdds = target.AI.Desires.DesireToBeVored(actor) - difficulty;
        successOdds *= 0.8f;

        // modify odds based on vore type interest
        float interest = 2;
        switch (type)
        {
            case VoreType.Oral:
                interest *= target.Personality.OralVoreInterest;
                break;
            case VoreType.Unbirth:
                interest *= target.Personality.UnbirthInterest;
                break;
            case VoreType.Cock:
                interest *= target.Personality.CockVoreInterest;
                break;
            case VoreType.Anal:
                interest *= target.Personality.AnalVoreInterest;
                break;
        }
        interest -= 1;
        float interestMod = (successOdds * interest) / 5;
        finalOdds = successOdds + interestMod;

        // reduce odds based on recent rejections
        if (actor.GetRelationshipWith(target).Rejections.ContainsKey(interaction.Type))
            finalOdds *= RejectionMod(interaction, actor, target);

        if (finalOdds <= 0)
            finalOdds = 0;

        return finalOdds;
    }

    public static float WillingVoreCheckOdds(InteractionBase interaction, Person actor, Person target, VoreType type)
    {
        // if endo is romantic only and prey doesn't like pred, refuse instantly
        if (State.World.Settings.EndoisOnlyRomantic)
        {
            if (target.Romance.DesiresGender(actor.GenderType) == false)
                return 0;
        }

        // endo vore odds check
        float finalOdds;
        float difficulty = State.World.Settings.AskToVoreDifficulty;

        // set odds based on desire to be endo vored (factors in willingness, personality, and friendship)
        float successOdds = target.AI.Desires.DesireToBeVoredEndo(actor) - difficulty;

        // modify value based on trust
        var trust = target.GetRelationshipWith(actor).Trust;
        if (trust < 0)
            trust *= 2;
        trust *= .25f;

        successOdds *= 1 + (trust / 2);

        // additional modification if endo is romantic only
        if (State.World.Settings.EndoisOnlyRomantic)
        {
            successOdds *= 1
                + (target.GetRelationshipWith(actor).GetRomanticLevelToTarget() * 0.5f)
                + ((target.Personality.Promiscuity - 0.5f) / 4);

            if (target.Romance.CanSafelyRomance(actor) == false)
                successOdds = successOdds * .8f - .15f;
        }

        // modify value based on vore type interest
        float typeinterest = 2;
        switch (type)
        {
            case VoreType.Oral:
                typeinterest *= target.Personality.OralVoreInterest;
                break;
            case VoreType.Unbirth:
                typeinterest *= target.Personality.UnbirthInterest;
                break;
            case VoreType.Cock:
                typeinterest *= target.Personality.CockVoreInterest;
                break;
            case VoreType.Anal:
                typeinterest *= target.Personality.AnalVoreInterest;
                break;
        }
        typeinterest -= 1;
        float interestMod = (successOdds * typeinterest) / 5;
        finalOdds = successOdds + interestMod;

        // reduce odds based on recent rejections
        if (actor.GetRelationshipWith(target).Rejections.ContainsKey(interaction.Type))
            finalOdds *= RejectionMod(interaction, actor, target);

        if (finalOdds <= 0)
            finalOdds = 0;

        return finalOdds;
    }

    public static float TasteCheckOdds(InteractionBase interaction, Person actor, Person target)
    {
        if (target.Romance.DesiresGender(actor.GenderType) == false)
            return 0;

        float difficulty = State.World.Settings.AskToVoreDifficulty / 3 * 2;

        // set odds based on desire to be endo vored (factors in willingness, personality, and friendship)
        float successOdds = target.AI.Desires.DesireToBeVoredEndo(actor) - difficulty;

        successOdds *= 1
            + (target.GetRelationshipWith(actor).GetRomanticLevelToTarget() * 0.5f)
            + ((target.Personality.Promiscuity - 0.5f) / 4);

        if (target.Romance.CanSafelyRomance(actor) == false)
            successOdds = successOdds * .8f - .15f;

        float interestMod = (successOdds * target.Personality.OralVoreInterest) / 5;
        float finalOdds = successOdds + interestMod;

        // increase odds if action exists in character's romantic history
        if (State.World.Settings.RomanceHistoryPenalty < 1)
        {
            if (interaction.Type >= InteractionType.ComplimentAppearance && interaction.Type <= InteractionType.StartSex)
            {
                float firstTimePenalty = State.World.Settings.RomanceHistoryPenalty;
                foreach (InteractionType type in actor.GetRelationshipWith(target).RomanceHistory)
                {
                    // if action exists in history, no penalty
                    if (type == interaction.Type)
                    {
                        firstTimePenalty = 1;
                        break;
                    }
                    // if a bigger romantic action exists, only a small penalty
                    else if (type > interaction.Type)
                    {
                        firstTimePenalty = (State.World.Settings.RomanceHistoryPenalty + 1) / 2;
                        break;
                    }
                }
                finalOdds *= firstTimePenalty;
            }
        }

        // reduce odds based on recent rejections
        if (actor.GetRelationshipWith(target).Rejections.ContainsKey(interaction.Type))
            finalOdds *= RejectionMod(interaction, actor, target);

        if (finalOdds <= 0)
            finalOdds = 0;

        return finalOdds;
    }

    public static float RomanticCheckOdds(
        InteractionBase interaction,
        Person actor,
        Person target,
        float difficulty
    )
    {
        // auto pass or fail conditions
        if (target.Romance.CanSafelyRomance(actor) == false)
            return 0;
        if (interaction.Type == InteractionType.AskOut && target.Romance.IsDating)
            return 0;
        if (interaction.Type == InteractionType.StartSex && target.HasTrait(Traits.DownToFuck) && target.ZoneContainsBed())
            return 1;

        float hornymod = target.Needs.Horniness;
        if (interaction.Type == InteractionType.AskOut)
            hornymod = target.Romance.RomanticDesperation;

        Relationship relationship = target.GetRelationshipWith(actor);
        float successOdds =
            (actor.Personality.Charisma * 0.2f)
            + (target.Romance.RomanticDesperation * 0.25f)
            + relationship.GetRomanticLevelToTarget()
            + (relationship.FriendshipLevel * 0.2f)
            + (target.Personality.Promiscuity * hornymod)
            + (target.Magic.Duration_Aroused > 0 ? .2f : 0)
            - difficulty;

        // increase odds if action exists in character's romantic history
        if (State.World.Settings.RomanceHistoryPenalty < 1)
        {
            if (interaction.Type >= InteractionType.ComplimentAppearance && interaction.Type <= InteractionType.StartSex)
            {
                float firstTimePenalty = State.World.Settings.RomanceHistoryPenalty;
                foreach (InteractionType type in actor.GetRelationshipWith(target).RomanceHistory)
                {
                    // if action exists in history, no penalty
                    if (type == interaction.Type)
                    {
                        firstTimePenalty = 1;
                        break;
                    }
                    // if a bigger romantic action exists, only a small penalty
                    else if (type > interaction.Type)
                    {
                        firstTimePenalty = (State.World.Settings.RomanceHistoryPenalty + 1) / 2;
                        break;
                    }
                }
                successOdds *= firstTimePenalty;
            }
        }

        // for intense romantic actions, lower odds inversely with promiscuity if not exhibitionist or in private room
        if (
            interaction.Type == InteractionType.AskThemToStrip
            || interaction.Type == InteractionType.StartSex
            || interaction.Type == InteractionType.MakeOut
        )
            if (
                (
                    target.HasTrait(Traits.Exhibitionist)
                    || HelperFunctions.InPrivateArea(actor, target)
                ) == false
            )
                successOdds *= target.Personality.Promiscuity 
                    + (target.Magic.Duration_Aroused > 0 ? .3f : 0);

        // some adjustments related to traits
        if (target.HasTrait(Traits.TentativeRomance))
            successOdds *= 1.4f;
        if (
            (
                interaction.Type == InteractionType.Kiss
                || interaction.Type == InteractionType.MakeOut
                || interaction.Type == InteractionType.PreyKissOtherPrey
            ) && target.HasTrait(Traits.LovesKissing)
        )
            successOdds *= 1.3f;
        
        if ((interaction.Type == InteractionType.Hug) && target.HasTrait(Traits.LovesHugs))
            successOdds *= 1.3f;
        
        if (interaction.Type == InteractionType.StartSex)
        {
            if (target.HasTrait(Traits.SexAddict))
                successOdds *= 1.3f;
            if (target.HasTrait(Traits.DownToFuck))
                successOdds *= 1.5f;
            if (target.HasTrait(Traits.LovesPrivateSex) && target.ZoneContainsBed())
                successOdds *= 2f;
        }

        if (target.HasTrait(Traits.Slutty) 
            && actor.GetRelationshipWith(target).RomanceHistory.Contains(interaction.Type) == false
            && interaction.Type >= InteractionType.Kiss && interaction.Type <= InteractionType.StartSex)
            successOdds *= 2.5f;

        if (
            (
                interaction.Type == InteractionType.Flirt
                || interaction.Type == InteractionType.ComplimentAppearance
            ) && target.HasTrait(Traits.Flirtatious)
        )
            successOdds *= 1.3f;
        
        if (target.HasTrait(Traits.HornyWhenFull) && target.VoreController.HasAnyPrey())
            successOdds *= 1.3f;

        // contextual adjustment for sex and makeout odds
        if (
            (
                interaction.Type == InteractionType.StartSex
                || interaction.Type == InteractionType.MakeOut
            )
            && target.StreamingSelfAction == SelfActionType.Masturbate
        )
            successOdds *= 1.5f; // if target masturbating, asksex and makeout odds increased
        
        if (
            (
                interaction.Type == InteractionType.StartSex
                && target.StreamingAction == InteractionType.MakeOut
                && target.StreamingTarget == actor
            )
        )
            successOdds *= 1f + (target.StreamedTurns / 10); // if making out with target, asksex odds are increased each turn

        // strip odds adjustments
        if (interaction.Type == InteractionType.AskThemToStrip)
        {
            if (target.Personality.Promiscuity > .5f)
                successOdds += (target.Personality.Promiscuity - .5f) / 2;
            if (
                target.Personality.PreferredClothing != ClothingStatus.Normal
                && target.ClothingStatus == ClothingStatus.Normal
            )
                successOdds *= 1.3f;
            if (target.HasTrait(Traits.EasyShow))
                successOdds *= 1.3f;
            if (target.IsWillingPreyToTarget(actor) && actor.VoreController.CapableOfVore())
                successOdds += 0.3f;
        }

        // reduce odds based on recent rejections
        if (actor.GetRelationshipWith(target).Rejections.ContainsKey(interaction.Type))
            successOdds *= RejectionMod(interaction, actor, target);

        if (target.Magic.IsCharmedBy(actor))
            successOdds = .75f + (successOdds / 4);

        if (successOdds <= 0)
            successOdds = 0;

        return successOdds;
    }

    public static float SoothePreyOdds(InteractionBase interaction, Person actor, Person target, float difficulty)
    {
        float successOdds;
        Person pred = target.FindMyPredator();

        // odds start with target's desire to be vored
        if (pred.VoreController.TargetIsBeingDigested(target))
            successOdds = target.AI.Desires.DesireToBeVored(pred);
        else
            successOdds = target.AI.Desires.DesireToBeVoredEndo(pred);

        // increase odds the longer prey has been held for, caps out at +0.4 (20% increase after difficulty calc)
        successOdds += Mathf.Min(0.005f * pred.VoreController.GetProgressOf(target).TurnsHeld, 0.4f) * (actor.HasTrait(Traits.StockholmSyndrome) ? 2 : 1);

        // factor in difficulty
        successOdds = (successOdds - difficulty) / 2;

        // if actor is not pred, also factor in actor's friendship
        if (actor != pred)
            successOdds *= target.AI.Desires.MiddleOverallAttachment(actor);

        // if actor is motherly, boost odds 1.5x
        if (actor.HasTrait(Quirks.Motherly))
            successOdds *= 1.5f;

        // reduce odds based on recent rejections
        if (actor.GetRelationshipWith(target).Rejections.ContainsKey(interaction.Type))
            successOdds *= RejectionMod(interaction, actor, target);

        if (successOdds <= 0)
            successOdds = 0;

        return successOdds;
    }

    public static float AskToBeDigestedOdds(Person actor, Person target)
    {
        if (target.Personality.VorePreference == VorePreference.Endosoma)
            return 0;
        if (State.World.Settings.CheckDigestion(target, DigestionAlias.CanVore))
        {
            return target.AI.Desires.DesireToDigestTarget(actor);
        }
        return 0;
    }

    internal static float GetFreeingOdds(Person actor, Person target, VoreLocation loc)
    {
        var consume = target.VoreController.CurrentSwallow(loc);
        if (consume != null)
            return consume.AttemptFreeOdds(actor);
        consume = target.VoreController.GetLiving(loc);
        if (consume != null)
            return consume.AttemptFreeOdds(actor);
        return 0;
    }

    internal static float RejectionMod(InteractionBase interaction, Person actor, Person target)
    {
        if (State.World.Settings.RejectionPenalty == 0)
            return 0;
        float rejectionCooldown = actor.GetRelationshipWith(target).Rejections[interaction.Type];
        if (rejectionCooldown >= Math.Min(State.World.Settings.RejectionCooldown * 10, 10))
            return State.World.Settings.RejectionPenalty;
        else
            return 1 / ((1/State.World.Settings.RejectionPenalty) * (rejectionCooldown + 1) * 0.1f);
    }
}
