﻿using Assets.Scripts.TextGeneration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEngine;
using static TextGenerator;

public enum SuccessRequirement
{
    Either,
    Success,
    Fail
}

class GenericEventString<T>
{
    internal Func<T, string> GetString;
    internal Predicate<T> Conditional;
    internal int Priority;

    internal int TempPriority;

    string ActorRace;
    string TargetRace;

    string Text;
    string Cond;

    internal float FallThrough;

    internal SuccessRequirement SuccessRequirement;

    /// <summary>Suppression status, used to prevent repeat dialogs.</summary>
    internal bool Suppressed;

    /// <summary>True if this interaction has an auto-assigned non-zero priority.</summary>
    internal bool AutoPriority;

    public GenericEventString(
        string text,
        string actorRace,
        string targetRace,
        string file, // used only to provide context for debug messages
        int priority = 0,
        float fallthrough = 0,
        string cond = "",
        SuccessRequirement successRequirement = SuccessRequirement.Either,
        bool suppressed = false,
        bool autoPriority = false
    )
    {
        Priority = priority;

        Text = text;
        Cond = cond;

        FallThrough = fallthrough;

        if (string.IsNullOrWhiteSpace(actorRace))
            ActorRace = "Any";
        else
            ActorRace = actorRace;

        if (string.IsNullOrWhiteSpace(targetRace))
            TargetRace = "Any";
        else
            TargetRace = targetRace;

        GetString = (s) => ReadyString(s, Text);

        Conditional = (s) =>
            ConditionalProcessor.ProcessConditional(s, Cond, ActorRace, TargetRace, file, text);

        SuccessRequirement = successRequirement;

        Suppressed = suppressed;
        AutoPriority = autoPriority;
    }

    internal void CalculateTempPriority(string actorRace, string targetRace)
    {
        TempPriority = Priority;

        // raise priority by +2 if actor race matches, or +1 if actor is a child race of the specified actor race
        if (RaceManager.RaceDescendants(actorRace, ActorRace) != -1)
        {
            TempPriority += 1;

            if (RaceManager.RaceDescendants(actorRace, ActorRace) == 0)
                TempPriority += 1;
        }

        // raise priority by +2 if target race matches, or +1 if target is a child race of the specified target race
        if (RaceManager.RaceDescendants(targetRace, TargetRace) != -1)
        {
            TempPriority += 1;

            if (RaceManager.RaceDescendants(targetRace, TargetRace) == 0)
                TempPriority += 1;
        }

        // the fallthrough check - if failed, banish this line to negative priority
        if (Rand.NextFloat(0, 1) < FallThrough)
            TempPriority = -1;

        // the suppression check - if line is suppressed by repetition reduction, banish it further
        if (Suppressed)
            TempPriority = -2;
    }

    internal string TestGetActor => ActorRace;
    internal string TestGetTarget => TargetRace;

    //public GenericEventString(Func<T, string> getString, int priority = 0, Predicate<T> conditional = null, SuccessRequirement successRequirement = SuccessRequirement.Either)
    //{
    //    Priority = priority;

    //    GetString = getString;
    //    if (conditional == null)
    //        Conditional = (s) => true;
    //    else
    //        Conditional = conditional;
    //    SuccessRequirement = successRequirement;
    //}

    string ReadyString(T action, string text)
    {
        if (action is Interaction i)
        {
            return CleanText(i.Actor, i.Target, text);
        }
        else if (action is SexInteraction iSx)
        {
            return CleanText(iSx.Actor, iSx.Target, text);
        }
        else if (action is SelfAction iSe)
        {
            return CleanText(iSe.Actor, iSe.Actor, text);
        }
        UnityEngine.Debug.LogWarning("Fall through...");
        return "";
    }

    string CleanText(Person actor, Person target, string text)
    {
        text = text.Replace('\u0060', '\''); //Replaces the funny ’ with the proper '

        // Simple tag substitutions.
        var substitutions = new Dictionary<string, string>
        {
            { "[ActorName]", actor.GetFirstNameWithLink() },
            { "[TargetName]", target.GetFirstNameWithLink() },
            { "[ActorHeIs]", GPP.HeIs(actor) },
            { "[!ActorHeIs]", GPP.HeIs(actor, true) },
            { "[ActorHeHas]", GPP.HeHas(actor) },
            { "[!ActorHeHas]", GPP.HeHas(actor, true) },
            { "[ActorHis]", GPP.His(actor) },
            { "[!ActorHis]", GPP.His(target).ToTitleCase() },
            { "[ActorHisCapitalized]", GPP.His(target).ToTitleCase() },
            { "[ActorHim]", GPP.Him(actor) },
            { "[ActorHimself]", GPP.Himself(actor) },
            { "[ActorHe]", GPP.He(actor) },
            { "[!ActorHe]", GPP.He(actor).ToTitleCase() },
            { "[ActorBastard]", GPP.Bastard(actor) },
            { "[TargetHeIs]", GPP.HeIs(target) },
            { "[!TargetHeIs]", GPP.HeIs(target, true) },
            { "[TargetHeHas]", GPP.HeHas(target) },
            { "[!TargetHeHas]", GPP.HeHas(target, true) },
            { "[TargetHis]", GPP.His(target) },
            { "[!TargetHis]", GPP.His(target).ToTitleCase() },
            { "[TargetHim]", GPP.Him(target) },
            { "[TargetHimself]", GPP.Himself(target) },
            { "[TargetHe]", GPP.He(target) },
            { "[!TargetHe]", GPP.He(target).ToTitleCase() },
            { "[TargetBastard]", GPP.Bastard(target) },
            { "[SIfActorSingular]", GPP.SIfSingular(actor) },
            { "[ESIfActorSingular]", GPP.ESIfSingular(actor) },
            { "[IESIfActorSingular]", GPP.IESIfSingular(actor) },
            { "[ActorBoobSize]", GetWord.BreastSize(actor.PartList.BreastSize) },
            { "[TargetBoobSize]", GetWord.BreastSize(target.PartList.BreastSize) },
            { "[ActorBoobsSize]", GetWord.BreastSize(actor.PartList.BreastSize) },
            { "[TargetBoobsSize]", GetWord.BreastSize(target.PartList.BreastSize) },
            { "[ActorDickSize]", GetWord.DickSize(actor.PartList.DickSize) },
            { "[TargetDickSize]", GetWord.DickSize(target.PartList.DickSize) },
            { "[ActorBallSize]", GetWord.BallSize(actor.PartList.BallSize) },
            { "[TargetBallSize]", GetWord.BallSize(target.PartList.BallSize) },
            { "[ActorBallsSize]", GetWord.BallSize(actor.PartList.BallSize) },
            { "[TargetBallsSize]", GetWord.BallSize(target.PartList.BallSize) },
            { "[ActorSize]", GetWord.SizeWord(actor, target) },
            { "[TargetSize]", GetWord.SizeWord(target, actor) },
            { "[ActorFatness]", GetWord.FatnessWord(actor) },
            { "[TargetFatness]", GetWord.FatnessWord(target) },
            { "[PredatorOfTarget]", target.FindMyPredator()?.FirstName ?? "<No Pred>" },
            { "[PredOfTarget]", target.FindMyPredator()?.FirstName ?? "<No Pred>" },
            { "[PredatorOfActor]", actor.FindMyPredator()?.FirstName ?? "<No Pred>" },
            { "[PredOfActor]", actor.FindMyPredator()?.FirstName ?? "<No Pred>" },
            { "[NemesisOfTarget]", target.FindNemesis()?.FirstName ?? "<No Nemesis>" },
            { "[NemesisOfActor]", actor.FindNemesis()?.FirstName ?? "<No Nemesis>" },
            { "[BestieOfTarget]", target.FindBestFriend()?.FirstName ?? "<No Bestie>" },
            { "[BestieOfActor]", actor.FindBestFriend()?.FirstName ?? "<No Bestie>" },
            { "[VendettaOfTarget]", target.FindVendetta()?.FirstName ?? "<No Vendetta>" },
            { "[VendettaOfActor]", actor.FindVendetta()?.FirstName ?? "<No Vendetta>" },
            { "[CrushOfTarget]", target.FindCrush()?.FirstName ?? "<No Crush>" },
            { "[CrushOfActor]", actor.FindCrush()?.FirstName ?? "<No Crush>" },
            { "[DateOfTarget]", target.Romance.Dating?.FirstName ?? "<No Date>" },
            { "[DateOfActor]", actor.Romance.Dating?.FirstName ?? "<No Date>" },
            { "[TargetCharmedBy]", target.Magic.CharmedBy?.FirstName ?? "<Not Charmed>" },
            { "[ActorCharmedBy]", actor.Magic.CharmedBy?.FirstName ?? "<Not Charmed>" },
            { "[SexPartnerOfTarget]", target.SexPartner()?.FirstName ?? "<No Sex Partner>" },
            { "[SexPartnerOfActor]", actor.SexPartner()?.FirstName ?? "<No Sex Partner>" },
            { "[Breasts]", BreastsSyn },
            { "[Pussy]", PussySyn },
            { "[Pussies]", PussiesSyn },
            { "[Dick]", DickSyn },
            { "[Balls]", BallsSyn },
            { "[Clit]", ClitSyn },
            { "[Clits]", ClitsSyn },
            { "[Anus]", AnusSyn },
            { "[Ass]", AssSyn },
            { "[Belly]", BellySyn },
            { "[Soft]", SoftSyn },
            { "[Firm]", FirmSyn },
            { "[DigestAdj]", DigestAdjSyn },
            { "[FullAdj]", FullAdjSyn },
            { "[TargetEyeColor]", target.PartList.EyeColor },
            { "[TargetHairColor]", target.PartList.HairColor },
            { "[TargetHairLength]", target.PartList.HairLength },
            { "[TargetHairStyle]", target.PartList.HairStyle },
            {
                "[ActorLocationInPred]",
                actor.GetMyVoreProgress()?.Location.ToString().ToLower() ?? "insides"
            },
            {
                "[TargetLocationInPred]",
                target.GetMyVoreProgress()?.Location.ToString().ToLower() ?? "insides"
            },
            { "[ShoulderAdjective]", target.PartList.ShoulderDescription },
            { "[WaistAdjective]", target.PartList.HipDescription },
        };

        /// Parse tag into an output.
        string HandleOutputTag(string tag, string inner)
        {
            if (substitutions.ContainsKey(tag))
                return substitutions[tag];

            var sections = inner.Split(':');
            var head = sections.ElementAtOrDefault(0);
            if (head == "?" || head == "either") // Pick random element: [?:a:b:...]
            {
                return sections[Rand.Next(1, sections.Length)];
            }

            sections = inner.Split('-'); // Older tags are split with "-".
            head = sections.ElementAtOrDefault(0);
            if (head == "ActorCustom") // [ActorCustom-custom_tag]
            {
                if (actor.PartList.Tags.ContainsKey(sections.ElementAtOrDefault(1)))
                    return actor.PartList.Tags[sections[1]];
                Debug.LogError($"Actor is missing '{sections.ElementAtOrDefault(1)}' tag.");
            }
            if (head == "TargetCustom") // [TargetCustom-custom_tag]
            {
                if (target.PartList.Tags.ContainsKey(sections.ElementAtOrDefault(1)))
                    return target.PartList.Tags[sections[1]];
                Debug.LogError($"Target is missing '{sections.ElementAtOrDefault(1)}' tag.");
            }

            return $"<invalid tag:{inner}>";
        }

        var depth = 0;
        while (true)
        {
            // Matches the innermost tag and slices it from the string.
            var match = Regex.Match(text, @"(?<begin>.*)(?<tag>\[(?<inner>[^\]]*)\])(?<end>.*)");
            if (!match.Success) // All tags are parsed.
                break;
            text = string.Format(
                "{0}{1}{2}",
                match.Groups["begin"],
                HandleOutputTag(match.Groups["tag"].Value, match.Groups["inner"].Value),
                match.Groups["end"]
            );
            depth += 1;
            if (depth >= 500)
            {
                Debug.LogError($"Tag recursion limit reached:\n{text}");
                break;
            }
        }
        return text;
    }
}

internal static class MessageManager
{
    static readonly MessageData Data = new MessageData();

    /// <summary>
    /// Process collected messages from an action and return the best result.
    /// </summary>
    /// <returns>The final processed dialog string.</returns>
    static internal string CalculateBestMessage<InteractionT>(
        InteractionT action,
        List<GenericEventString<InteractionT>> messages,
        string actorRace,
        string targetRace
    )
    {
        foreach (var message in messages)
        {
            message.CalculateTempPriority(actorRace, targetRace);
        }

        // look only at lines with maximum priority among all lines that exist
        int priority = messages.Max(s => s.TempPriority);

        // if current priority == minimum possible priority of all available messages, aside from -1 (auto fail), then reset repetition suppressed lines and allow them to be shown again
        if (
            priority
            == messages
                .Where(s => s.TempPriority >= Math.Min(-1, priority))
                .Min(s => s.TempPriority)
        )
            foreach (var message in messages)
                message.Suppressed = false;

        // cast all lines with above priority to an array
        var array = messages.Where(s => s.TempPriority == priority).ToArray();

        // pick a random line among the array of available lines
        var selectedLine = array[Rand.Next(array.Length)];

        // if the selected line has AutoPriority, suppress it
        if (selectedLine.AutoPriority)
            selectedLine.Suppressed = true;

        // return the text of the chosen line
        return selectedLine.GetString(action);
    }

    static internal string GetText(SexInteraction action)
    {
        if (Data.SexTexts.ContainsKey(action.Type))
        {
            var messages = Data.SexTexts[action.Type].Where(s => s.Conditional(action)).ToList();
            if (messages.Any() == false)
            {
                UnityEngine.Debug.LogWarning(
                    $"Fell out of block with no available messages {action.Actor.FirstName} {action.Target.FirstName} {action.Position} {action.Type}"
                );
                return "";
            }
            return CalculateBestMessage(action, messages, action.Actor.Race, action.Target.Race);
        }
        return "";
    }

    static internal string GetText(SelfAction action)
    {
        if (
            (action.Type >= SelfActionType.ScatDisposalBathroom
            && action.Type <= SelfActionType.UnbirthDisposalFloor)
            || (action.Type == SelfActionType.Orgasm && State.World.Settings.DisposeWithOrgasm && (action.Actor.NeedsBallsDisposal() || action.Actor.NeedsWombDisposal()))
        )
        {
            return DisposalText(action);
        }
        else if (Data.SelfTexts.ContainsKey(action.Type))
        {
            var messages = Data.SelfTexts[action.Type].Where(s => s.Conditional(action)).ToList();
            if (messages.Any() == false)
            {
                UnityEngine.Debug.LogWarning(
                    $"Fell out of block with no available messages {action.Actor.FirstName} {action.Type}"
                );
                return "";
            }
            return CalculateBestMessage(action, messages, action.Actor.Race, "Any");
        }
        return "";
    }

    static internal string GetText(Interaction action)
    {
        if (
            InteractionList.List[action.Type].Class == ClassType.VoreConsuming
            && (
                InteractionList.List[action.Type].Type < InteractionType.KissVore
                || InteractionList.List[action.Type].Type > InteractionType.SexAskToAnalVoreDigest
            )
        )
        {
            return VoreText(action);
        }
        if (action.Type == InteractionType.MakeOut && action.Success == true)
        {
            SexInteraction sex = new SexInteraction(
                action.Actor,
                action.Target,
                SexInteractionType.DeepKiss
            );
            return GetText(sex);
        }

        if (Data.InteractionTexts.ContainsKey(action.Type))
        {
            var messages = Data.InteractionTexts[action.Type]
                .Where(
                    s =>
                        s.Conditional(action)
                        && (
                            s.SuccessRequirement == SuccessRequirement.Either
                            || (
                                s.SuccessRequirement == SuccessRequirement.Success && action.Success
                            )
                            || (
                                s.SuccessRequirement == SuccessRequirement.Fail
                                && action.Success == false
                            )
                        )
                )
                .ToList();
            if (messages.Any() == false)
            {
                UnityEngine.Debug.LogWarning(
                    $"Fell out of block with no available messages {action.Actor.FirstName} {action.Target.FirstName} {action.Success} {action.Type}"
                );
                return "";
            }
            return CalculateBestMessage(action, messages, action.Actor.Race, action.Target.Race);
        }
        return "";
    }

    static internal string GetText(Interaction action, VoreMessageType voreMessageType)
    {
        if (Data.VoreTexts.ContainsKey(voreMessageType))
        {
            var messages = Data.VoreTexts[voreMessageType]
                .Where(s => s.Conditional(action))
                .ToList();
            if (messages.Any() == false)
            {
                //Disregard optional texts
                if (
                    voreMessageType != VoreMessageType.VoreCleanliness
                    && voreMessageType != VoreMessageType.VoreClothing
                    && voreMessageType != VoreMessageType.VoreHunger
                )
                    UnityEngine.Debug.LogWarning(
                        $"Fell out of block with no available messages {action.Actor.FirstName} {action.Target.FirstName} {action.Success} {voreMessageType}"
                    );
                return "";
            }
            return CalculateBestMessage(action, messages, action.Actor.Race, action.Target.Race);
        }
        return "";
    }

    static internal string GetText(Interaction action, BodyPartDescriptionType bodyMessageType)
    {
        if (Data.BodyPartTexts.ContainsKey(bodyMessageType))
        {
            var messages = Data.BodyPartTexts[bodyMessageType]
                .Where(s => s.Conditional(action))
                .ToList();
            if (messages.Any() == false)
            {
                //UnityEngine.Debug.LogWarning($"Fell out of block with no available messages {action.Actor.FirstName} {action.Target.FirstName} {action.Success} {bodyMessageType}");
                return "";
            }
            return CalculateBestMessage(action, messages, action.Actor.Race, action.Target.Race);
        }
        return "";
    }

    static string DisposalText(SelfAction self)
    {
        InteractionType type = InteractionType.EventDisposalBathroom;
        DisposalData data = null;

        if (self.Type == SelfActionType.ScatDisposalBathroom)
        {
            type = InteractionType.EventDisposalBathroom;
            data = self.Actor.Disposals
                .Where(s => s.Location == VoreLocation.Stomach || s.Location == VoreLocation.Bowels)
                .FirstOrDefault();
        }
        if (self.Type == SelfActionType.ScatDisposalFloor)
        {
            type = InteractionType.EventDisposalGround;
            data = self.Actor.Disposals
                .Where(s => s.Location == VoreLocation.Stomach || s.Location == VoreLocation.Bowels)
                .FirstOrDefault();
        }
        else if (self.Type == SelfActionType.CockDisposalBathroom)
        {
            type = InteractionType.EventDisposalCockBathroom;
            data = self.Actor.Disposals
                .Where(s => s.Location == VoreLocation.Balls)
                .FirstOrDefault();
        }
        else if (self.Type == SelfActionType.CockDisposalFloor)
        {
            type = InteractionType.EventDisposalCockGround;
            data = self.Actor.Disposals
                .Where(s => s.Location == VoreLocation.Balls)
                .FirstOrDefault();
        }
        else if (self.Type == SelfActionType.UnbirthDisposalBathroom)
        {
            type = InteractionType.EventDisposalUnbirthBathroom;
            data = self.Actor.Disposals
                .Where(s => s.Location == VoreLocation.Womb)
                .FirstOrDefault();
        }
        else if (self.Type == SelfActionType.UnbirthDisposalFloor)
        {
            type = InteractionType.EventDisposalUnbirthGround;
            data = self.Actor.Disposals
                .Where(s => s.Location == VoreLocation.Womb)
                .FirstOrDefault();
        }
        else if (self.Type == SelfActionType.Orgasm && self.Actor.NeedsBallsDisposal())
        {
            type = InteractionType.EventDisposalCockOrgasm;
            data = self.Actor.Disposals
                .Where(s => s.Location == VoreLocation.Balls)
                .FirstOrDefault();
            data.TurnsDone = 4;
        }
        else if (self.Type == SelfActionType.Orgasm && self.Actor.NeedsWombDisposal())
        {
            type = InteractionType.EventDisposalUnbirthOrgasm;
            data = self.Actor.Disposals
                .Where(s => s.Location == VoreLocation.Womb)
                .FirstOrDefault();
            data.TurnsDone = 4;
        }

        if (data == null)
        {
            UnityEngine.Debug.Log("Disposal Error");
            return "";
        }

        Interaction action = new Interaction(self.Actor, data.Person, true, type, data.TurnsDone);

        if (Data.InteractionTexts.ContainsKey(type))
        {
            var messages = Data.InteractionTexts[type].Where(s => s.Conditional(action)).ToList();
            if (messages.Any() == false)
            {
                UnityEngine.Debug.LogWarning(
                    $"Fell out of block with no available messages {action.Actor.FirstName} {action.Target.FirstName} {action.Success} {action.Type}"
                );
                return "";
            }
            return CalculateBestMessage(action, messages, action.Actor.Race, action.Target.Race);
        }
        return "";
    }

    static string VoreText(Interaction action)
    {
        if (action.Type == InteractionType.Unbirth || action.Type == InteractionType.SexUnbirth)
            return VoreUnbirth(action);
        if (action.Type == InteractionType.CockVore || action.Type == InteractionType.SexCockVore)
            return VoreCock(action);
        if (action.Type == InteractionType.AnalVore || action.Type == InteractionType.SexAnalVore)
            return VoreAnal(action);
        //if (action.Type == InteractionType.OralVore)
        return VoreOral(action);
    }

    private static string VoreOral(Interaction action)
    {
        if (action.Success)
        {
            if (action.Stage == 0)
                return GetText(action, VoreMessageType.OralBegin);
            else if (action.Stage == 1)
                return GetText(
                        action,
                        action.Target.PartList.Parts[action.Stage - 1].GetBPD(
                            BodyPartDescription.OralVore
                        )
                    ) + GetText(action, VoreMessageType.VoreCleanliness);
            else if (
                action.Stage == 2
                && action.Actor.VoreController.TargetIsBeingDigested(action.Target)
            )
                return GetText(
                        action,
                        action.Target.PartList.Parts[action.Stage - 1].GetBPD(
                            BodyPartDescription.OralVore
                        )
                    ) + GetText(action, VoreMessageType.VoreHunger);
            else if (action.Stage == 3)
                return GetText(
                        action,
                        action.Target.PartList.Parts[action.Stage - 1].GetBPD(
                            BodyPartDescription.OralVore
                        )
                    ) + GetText(action, VoreMessageType.VoreClothing);
            else if (
                action.Stage == 4
                && action.Actor.LastSex == action.Target
                && action.Actor.LastSexTurn >= State.World.Turn - 6
            )
                return GetText(
                        action,
                        action.Target.PartList.Parts[action.Stage - 1].GetBPD(
                            BodyPartDescription.OralVore
                        )
                    ) + GetText(action, VoreMessageType.VoreAfterSex);
            else if (action.Stage <= action.Target.PartList.Parts.Count)
                return GetText(
                    action,
                    action.Target.PartList.Parts[action.Stage - 1].GetBPD(
                        BodyPartDescription.OralVore
                    )
                );
            else if (action.Actor.VoreController.TargetIsBeingDigested(action.Target) == false)
            {
                if (Rand.NextFloat(0, 1) > Config.SuppressEndoMessages)
                    return GetText(action, VoreMessageType.InStomachEndo);
                else
                    return "{SUPPRESS}";
            }
            else
                return GetText(action, VoreMessageType.InStomach);
        }
        else
        {
            return GetText(action, VoreMessageType.OralVoreRefused);
        }
    }

    private static string VoreCock(Interaction action)
    {
        if (action.Success)
        {
            if (action.Stage == 0)
                return GetText(action, VoreMessageType.CockVoreBegin);
            else if (action.Stage == 1)
                return GetText(
                    action,
                    action.Target.PartList.Parts[action.Stage - 1].GetBPD(
                        BodyPartDescription.CockVore
                    )
                );
            else if (
                action.Stage == 2
                && action.Actor.VoreController.TargetIsBeingDigested(action.Target)
            )
                return GetText(
                        action,
                        action.Target.PartList.Parts[action.Stage - 1].GetBPD(
                            BodyPartDescription.CockVore
                        )
                    ) + GetText(action, VoreMessageType.VoreHunger);
            else if (action.Stage == 3)
                return GetText(
                        action,
                        action.Target.PartList.Parts[action.Stage - 1].GetBPD(
                            BodyPartDescription.CockVore
                        )
                    ) + GetText(action, VoreMessageType.VoreClothing);
            else if (
                action.Stage == 4
                && action.Actor.LastSex == action.Target
                && action.Actor.LastSexTurn >= State.World.Turn - 6
            )
                return GetText(
                        action,
                        action.Target.PartList.Parts[action.Stage - 1].GetBPD(
                            BodyPartDescription.CockVore
                        )
                    ) + GetText(action, VoreMessageType.VoreAfterSex);
            else if (action.Stage <= action.Target.PartList.Parts.Count)
                return GetText(
                    action,
                    action.Target.PartList.Parts[action.Stage - 1].GetBPD(
                        BodyPartDescription.CockVore
                    )
                );
            else if (action.Actor.VoreController.TargetIsBeingDigested(action.Target) == false)
            {
                if (Rand.NextFloat(0, 1) > Config.SuppressEndoMessages)
                    return GetText(action, VoreMessageType.InBallsEndo);
                else
                    return "{SUPPRESS}";
            }
            else
                return GetText(action, VoreMessageType.InBalls);
        }
        else
        {
            return GetText(action, VoreMessageType.CockVoreRefused);
        }
    }

    private static string VoreUnbirth(Interaction action)
    {
        if (action.Success)
        {
            if (action.Stage == 0)
                return GetText(action, VoreMessageType.UnbirthBegin);
            else if (action.Stage == 1)
                return GetText(
                    action,
                    action.Target.PartList.Parts[action.Stage - 1].GetBPD(
                        BodyPartDescription.Unbirth
                    )
                );
            else if (
                action.Stage == 2
                && action.Actor.VoreController.TargetIsBeingDigested(action.Target)
            )
                return GetText(
                        action,
                        action.Target.PartList.Parts[action.Stage - 1].GetBPD(
                            BodyPartDescription.Unbirth
                        )
                    ) + GetText(action, VoreMessageType.VoreHunger);
            else if (action.Stage == 3)
                return GetText(
                        action,
                        action.Target.PartList.Parts[action.Stage - 1].GetBPD(
                            BodyPartDescription.Unbirth
                        )
                    ) + GetText(action, VoreMessageType.VoreClothing);
            else if (
                action.Stage == 4
                && action.Actor.LastSex == action.Target
                && action.Actor.LastSexTurn >= State.World.Turn - 6
            )
                return GetText(
                        action,
                        action.Target.PartList.Parts[action.Stage - 1].GetBPD(
                            BodyPartDescription.Unbirth
                        )
                    ) + GetText(action, VoreMessageType.VoreAfterSex);
            else if (action.Stage <= action.Target.PartList.Parts.Count)
                return GetText(
                    action,
                    action.Target.PartList.Parts[action.Stage - 1].GetBPD(
                        BodyPartDescription.Unbirth
                    )
                );
            else if (action.Actor.VoreController.TargetIsBeingDigested(action.Target) == false)
            {
                if (Rand.NextFloat(0, 1) > Config.SuppressEndoMessages)
                    return GetText(action, VoreMessageType.InWombEndo);
                else
                    return "{SUPPRESS}";
            }
            else
                return GetText(action, VoreMessageType.InWomb);
        }
        else
        {
            return GetText(action, VoreMessageType.UnbirthRefused);
        }
    }

    private static string VoreAnal(Interaction action)
    {
        if (action.Success)
        {
            if (action.Stage == 0)
                return GetText(action, VoreMessageType.AnalVoreBegin);
            else if (action.Stage == 1)
                return GetText(
                    action,
                    action.Target.PartList.Parts[action.Stage - 1].GetBPD(
                        BodyPartDescription.AnalVore
                    )
                );
            else if (
                action.Stage == 2
                && action.Actor.VoreController.TargetIsBeingDigested(action.Target)
            )
                return GetText(
                        action,
                        action.Target.PartList.Parts[action.Stage - 1].GetBPD(
                            BodyPartDescription.AnalVore
                        )
                    ) + GetText(action, VoreMessageType.VoreHunger);
            else if (action.Stage == 3)
                return GetText(
                        action,
                        action.Target.PartList.Parts[action.Stage - 1].GetBPD(
                            BodyPartDescription.AnalVore
                        )
                    ) + GetText(action, VoreMessageType.VoreClothing);
            else if (
                action.Stage == 4
                && action.Actor.LastSex == action.Target
                && action.Actor.LastSexTurn >= State.World.Turn - 6
            )
                return GetText(
                        action,
                        action.Target.PartList.Parts[action.Stage - 1].GetBPD(
                            BodyPartDescription.AnalVore
                        )
                    ) + GetText(action, VoreMessageType.VoreAfterSex);
            else if (action.Stage <= action.Target.PartList.Parts.Count)
                return GetText(
                    action,
                    action.Target.PartList.Parts[action.Stage - 1].GetBPD(
                        BodyPartDescription.AnalVore
                    )
                );
            else if (
                action.Actor.VoreController.GetProgressOf(action.Target).Location
                == VoreLocation.Stomach
            )
                return VoreOral(action); //Divert to stomach messages if in stomach
            else if (action.Actor.VoreController.TargetIsBeingDigested(action.Target) == false)
            {
                if (Rand.NextFloat(0, 1) > Config.SuppressEndoMessages)
                    return GetText(action, VoreMessageType.InAnusEndo);
                else
                    return "{SUPPRESS}";
            }
            else
                return GetText(action, VoreMessageType.InAnus);
        }
        else
        {
            return GetText(action, VoreMessageType.AnalVoreRefused);
        }
    }

    static internal string GetPopUpText(Interaction action)
    {
        if (Enum.TryParse($"PlayerPop_{action.Type.ToString()}", out InteractionType PlayerPopType))
        {
            if (Data.InteractionTexts.ContainsKey(PlayerPopType))
            {
                var messages = Data.InteractionTexts[PlayerPopType]
                    .Where(
                        s =>
                            s.Conditional(action)
                            && (
                                s.SuccessRequirement == SuccessRequirement.Either
                                || (
                                    s.SuccessRequirement == SuccessRequirement.Success
                                    && action.Success
                                )
                                || (
                                    s.SuccessRequirement == SuccessRequirement.Fail
                                    && action.Success == false
                                )
                            )
                    )
                    .ToList();
                if (messages.Any() == false)
                {
                    //UnityEngine.Debug.LogWarning($"Fell out of block with no available messages {action.Actor.FirstName} {action.Target.FirstName} {action.Success} PlayerPop_{action.Type}");
                    return "";
                }
                return CalculateBestMessage(
                    action,
                    messages,
                    action.Actor.Race,
                    action.Target.Race
                );
            }
        }
        return "";
    }

    internal static void BulkTest()
    {
        Person A = new Person("A", "First", 0, Orientation.All, true, new Vec2(0, 0));
        Person B = new Person("B", "Second", 0, Orientation.All, true, new Vec2(0, 0));

        SexInteraction sexInter = new SexInteraction(A, B, SexInteractionType.CaressFace);
        foreach (var value in Data.SexTexts)
        {
            foreach (var entry in value.Value)
            {
                A.Race = entry.TestGetActor;
                B.Race = entry.TestGetTarget;
                entry.Conditional(sexInter);
            }
        }
        SelfAction selfInter = new SelfAction(A, SelfActionType.Rest);
        foreach (var value in Data.SelfTexts)
        {
            foreach (var entry in value.Value)
            {
                A.Race = entry.TestGetActor;
                B.Race = entry.TestGetTarget;
                entry.Conditional(selfInter);
            }
        }
        Interaction action = new Interaction(A, B, true, InteractionType.Talk);
        foreach (var value in Data.InteractionTexts)
        {
            foreach (var entry in value.Value)
            {
                A.Race = entry.TestGetActor;
                B.Race = entry.TestGetTarget;
                entry.Conditional(action);
            }
        }
        foreach (var value in Data.VoreTexts)
        {
            foreach (var entry in value.Value)
            {
                A.Race = entry.TestGetActor;
                B.Race = entry.TestGetTarget;
                entry.Conditional(action);
            }
        }
        foreach (var value in Data.BodyPartTexts)
        {
            foreach (var entry in value.Value)
            {
                A.Race = entry.TestGetActor;
                B.Race = entry.TestGetTarget;
                entry.Conditional(action);
            }
        }

        if (UnityEngine.Object.FindObjectOfType<MessageBox>() == null)
            State.GameManager.CreateMessageBox("No errors found");
    }
}
