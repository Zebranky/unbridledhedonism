﻿using OdinSerializer;
using System.Collections.Generic;

class RaceWeights
{
    [OdinSerialize]
    [Category("General")]
    internal Dictionary<string, int> Weights = new Dictionary<string, int>();

    public RaceWeights()
    {
        foreach (var race in RaceManager.PickableRaces)
        {
            if (race == "Human")
                Weights[race] = 500;
            else
                Weights[race] = 500;
        }
    }

    internal Race GetRandomRace()
    {
        WeightedList<string> rand = new WeightedList<string>();
        foreach (var entry in Weights)
        {
            rand.Add(entry.Key, entry.Value);
        }
        return RaceManager.GetRace(rand.GetResult());
    }
}
