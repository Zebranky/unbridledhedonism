﻿using UnityEngine;
using UnityEngine.EventSystems;
using System;
using UnityEngine.UI;
using TMPro;
using System.Linq;
using System.Collections.Generic;
using System.IO;

public enum MapEditorTypes
{
    Ignore,
    Clear,
    IndoorHallway,
    Grass,
    OutdoorPath,
    DormRoom,
    Cafeteria,
    Shower,
    NurseOffice,
    Bathroom,
    Gym,
    Library,
}

public enum WallTypes
{
    Ignore,
    Wall,
    Door,
    Window,
    Nothing,
}

public class MapEditor : MonoBehaviour
{
    public TMP_Dropdown TileTypeDropdown;
    public TMP_Dropdown WallTypeDropdown;

    public bool PlacingMode => TileTypeDropdown.value > 0 || WallTypeDropdown.value > 0;

    bool continueActionState;

    World BeforeResize;

    MapResizePanel ResizeUI;

    public TextMeshProUGUI Tooltip;

    public Toggle OverrideName;
    public TMP_InputField TileName;

    public TMP_Dropdown BrushType;

    public Button UndoButton;
    public Button UndoResizeButton;

    public GameObject ResizeObject;

    List<UndoMapAction> UndoActions = new List<UndoMapAction>();
    UndoMapAction LastActionBuilder;

    bool EnteredFromTitleScreen;

    bool Warned = false;

    internal void Open(bool titleScreen)
    {
        EnteredFromTitleScreen = titleScreen;
        gameObject.SetActive(true);
        State.GameManager.PlayerText.transform.parent.parent.parent.gameObject.SetActive(false);
        State.GameManager.TargetText.transform.parent.gameObject.SetActive(false);
        State.GameManager.LogText.transform.parent.parent.parent.gameObject.SetActive(false);
        continueActionState = State.GameManager.ContinueActionPanel.activeSelf;
        State.GameManager.ContinueActionPanel.SetActive(false);
    }

    void Start()
    {
        TileTypeDropdown.ClearOptions();
        foreach (MapEditorTypes type in (MapEditorTypes[])Enum.GetValues(typeof(MapEditorTypes)))
        {
            TileTypeDropdown.options.Add(new TMP_Dropdown.OptionData(type.ToString()));
        }
        TileTypeDropdown.RefreshShownValue();

        WallTypeDropdown.ClearOptions();
        foreach (WallTypes type in (WallTypes[])Enum.GetValues(typeof(WallTypes)))
        {
            WallTypeDropdown.options.Add(new TMP_Dropdown.OptionData(type.ToString()));
        }
        WallTypeDropdown.RefreshShownValue();
        UndoActions = new List<UndoMapAction>();
    }

    public void UpdateTileType()
    {
        if (TileTypeDropdown.value > 0)
        {
            WallTypeDropdown.value = 0;
            WallTypeDropdown.RefreshShownValue();
        }
    }

    public void UpdateWallType()
    {
        if (WallTypeDropdown.value > 0)
        {
            TileTypeDropdown.value = 0;
            TileTypeDropdown.RefreshShownValue();
        }
    }

    void Update()
    {
        if (gameObject.activeSelf == false)
            return;
        if (State.KeyManager.CancelPressed)
        {
            TileTypeDropdown.value = 0;
            WallTypeDropdown.value = 0;
            TileTypeDropdown.RefreshShownValue();
            WallTypeDropdown.RefreshShownValue();
        }
        UndoButton.interactable = UndoActions.Any();
        UndoResizeButton.gameObject.SetActive(BeforeResize != null);
        if (EventSystem.current.IsPointerOverGameObject() == false) //Makes sure mouse isn't over a UI element
        {
            Vector2 currentMousePos = State.GameManager.Camera.ScreenToWorldPoint(
                Input.mousePosition
            );

            int x = (int)(currentMousePos.x + 0.5f);
            int y = (int)(currentMousePos.y + 0.5f);
            if (
                x >= 0
                && x < State.World.Zones.GetLength(0)
                && y >= 0
                && y < State.World.Zones.GetLength(1)
            )
            {
                UpdateTooltips(x, y);
                if (Input.GetMouseButtonDown(0))
                    ProcessClick(x, y);
                else if (Input.GetMouseButton(0))
                    ProcessClick(x, y, true);
                if (Input.GetMouseButtonDown(1))
                    ProcessRightClick(x, y);
            }
        }
    }

    private void ProcessRightClick(int x, int y) { }

    private void UpdateTooltips(int x, int y) { }

    internal void ClearUndo()
    {
        UndoActions.Clear();
    }

    private void ProcessClick(int x, int y, bool held = false)
    {
        if (held == false)
        {
            LastActionBuilder = new UndoMapAction();
            if (UndoActions.Count > 15)
                UndoActions.RemoveAt(0);
            UndoActions.Add(LastActionBuilder);
        }

        TileCheck(x, y);
        WallCheck(x, y);
    }

    private void TileCheck(int x, int y)
    {
        if ((MapEditorTypes)TileTypeDropdown.value != MapEditorTypes.Ignore)
        {
            if (BrushType.value == 0)
            {
                PaintTile(x, y);
            }
            else if (BrushType.value <= 4)
            {
                int radius = BrushType.value;
                for (int xAdjust = -radius; xAdjust <= radius; xAdjust++)
                {
                    for (int yAdjust = -radius; yAdjust <= radius; yAdjust++)
                    {
                        if (x + xAdjust >= State.World.Zones.GetLength(0) || x + xAdjust < 0)
                            continue;
                        if (y + yAdjust >= State.World.Zones.GetLength(1) || y + yAdjust < 0)
                            continue;
                        PaintTile(x + xAdjust, y + yAdjust);
                    }
                }
            }
            else if (BrushType.value == 5)
            {
                Fill(x, y);
            }

            State.GameManager.TileManager.DrawWorld();
        }
    }

    private void Fill(int startX, int startY)
    {
        bool deadTile = false;

        var zone = State.World.GetZone(new Vec2(startX, startY));
        var Zones = State.World.Zones;

        ZoneType fillOverType = ZoneType.Grass;
        if (zone != null)
        {
            fillOverType = State.World.GetZone(new Vec2(startX, startY)).Type;
        }
        else
            deadTile = true;
        Vec2 q = new Vec2(startX, startY);
        int h = Zones.GetLength(1);
        int w = Zones.GetLength(0);

        List<Vec2> visited = new List<Vec2>();

        Stack<Vec2> stack = new Stack<Vec2>();
        stack.Push(q);
        while (stack.Count > 0)
        {
            Vec2 p = stack.Pop();
            int x = p.x;
            int y = p.y;
            if (y < 0 || y > h - 1 || x < 0 || x > w - 1)
                continue;
            if (visited.Contains(p))
            {
                continue;
            }
            if (deadTile)
            {
                if (Zones[x, y] != null)
                    continue;
            }
            else
            {
                if (Zones[x, y] == null || Zones[x, y].Type != fillOverType)
                    continue;
            }

            visited.Add(p);
            PaintTile(x, y);
            stack.Push(new Vec2(x + 1, y));
            stack.Push(new Vec2(x - 1, y));
            stack.Push(new Vec2(x, y + 1));
            stack.Push(new Vec2(x, y - 1));
        }
    }

    private void PaintTile(int x, int y)
    {
        Zone oldZone = State.World.Zones[x, y];
        LastActionBuilder.Add(() => State.World.Zones[x, y] = oldZone);
        if (State.World.Zones[x, y]?.Objects.Contains(ObjectType.Bed) ?? false)
        {
            List<World.DormRoom> dorms = State.World.DormRooms
                .Where(s => s.Position.x == x && s.Position.y == y)
                .ToList();
            foreach (var dorm in dorms)
            {
                State.World.DormRooms.Remove(dorm);
            }
            LastActionBuilder.Add(() => State.World.DormRooms.AddRange(dorms));
        }

        State.World.Zones[x, y] = GetZoneOfType((MapEditorTypes)TileTypeDropdown.value);
        if (State.World.Zones[x, y] != null)
        {
            if (OverrideName.isOn)
                State.World.Zones[x, y].Name = TileName.text;
            if (
                State.World.Zones[x, y].Objects.Contains(ObjectType.Bed)
                && State.World.DormRooms.Where(s => s.Position == new Vec2(x, y)).Any() == false
            )
            {
                var newDorm = new World.DormRoom(new Vec2(x, y), World.Facing.North);
                State.World.DormRooms.Add(newDorm);
                LastActionBuilder.Add(() => State.World.DormRooms.Remove(newDorm));
            }
        }
    }

    private void WallCheck(int x, int y)
    {
        if ((WallTypes)WallTypeDropdown.value != WallTypes.Ignore)
        {
            Vector2 currentMousePos = State.GameManager.Camera.ScreenToWorldPoint(
                Input.mousePosition
            );
            float xf = currentMousePos.x - .5f;
            float yf = currentMousePos.y - .5f;
            float xfr = (1 + xf) % 1;
            float yfr = (1 + yf) % 1;
            bool north = false;

            Vec2 vec = new Vec2(-1, -1);
            if (xfr < .2f && yfr > .2f && yfr < .8f)
            {
                vec.x = (int)Math.Floor(xf);
                vec.y = y;
            }
            else if (xfr > .8f && yfr > .2f && yfr < .8f)
            {
                vec.x = (int)Math.Floor(1 + xf);
                vec.y = y;
            }
            else if (yfr < .2f && xfr > .2f && xfr < .8f)
            {
                vec.y = (int)Math.Floor(yf);
                vec.x = x;
                north = true;
            }
            else if (yfr > .8f && xfr > .2f && xfr < .8f)
            {
                vec.y = (int)Math.Floor(1 + yf);
                vec.x = x;
                north = true;
            }

            if (
                vec.x >= 0
                && vec.x < State.World.Zones.GetLength(0)
                && vec.y >= 0
                && vec.y < State.World.Zones.GetLength(1)
            )
            {
                switch ((WallTypes)WallTypeDropdown.value)
                {
                    case WallTypes.Ignore:
                        break;
                    case WallTypes.Wall:
                        UpdateWall(vec, north, new Border(true, true, 1));
                        break;
                    case WallTypes.Door:
                        UpdateWall(vec, north, new Border(true, false, 1));
                        break;
                    case WallTypes.Window:
                        UpdateWall(vec, north, new Border(false, true, 1));
                        break;
                    case WallTypes.Nothing:
                        if (north)
                        {
                            var lastWall = State.World.BorderController.GetBorder(
                                vec,
                                vec + new Vec2(0, 1)
                            );
                            LastActionBuilder.Add(
                                () => State.World.BorderController.SetNorthBorder(vec, lastWall)
                            );
                        }
                        else
                        {
                            var lastWall = State.World.BorderController.GetBorder(
                                vec,
                                vec + new Vec2(1, 0)
                            );
                            LastActionBuilder.Add(
                                () => State.World.BorderController.SetEastBorder(vec, lastWall)
                            );
                        }
                        State.World.BorderController.ClearBorder(vec, north);
                        break;
                }
            }

            State.GameManager.TileManager.DrawWorld();
        }
    }

    Zone GetZoneOfType(MapEditorTypes type)
    {
        switch (type)
        {
            case MapEditorTypes.IndoorHallway:
                return new Zone("Hallway", ZoneType.IndoorHallway);
            case MapEditorTypes.Grass:
                return new Zone("Outdoor", ZoneType.Grass);
            case MapEditorTypes.OutdoorPath:
                return new Zone("Outdoor Path", ZoneType.OutdoorPath);
            case MapEditorTypes.DormRoom:
                return new Zone(
                    "Dorm room",
                    ZoneType.DormRoom,
                    new List<ObjectType>() { ObjectType.Bed, ObjectType.Clothes }
                );
            case MapEditorTypes.Ignore:
                break;
            case MapEditorTypes.Cafeteria:
                return new Zone(
                    "Cafeteria",
                    ZoneType.Cafeteria,
                    new List<ObjectType>() { ObjectType.Food }
                );
            case MapEditorTypes.Shower:
                return new Zone(
                    "Shower",
                    ZoneType.Shower,
                    new List<ObjectType>() { ObjectType.Shower }
                );
            case MapEditorTypes.Clear:
                return null;
            case MapEditorTypes.NurseOffice:
                return new Zone(
                    "Nurse's Office",
                    ZoneType.NurseOffice,
                    new List<ObjectType>() { ObjectType.NurseOffice }
                );
            case MapEditorTypes.Bathroom:
                return new Zone(
                    "Bathroom",
                    ZoneType.Bathroom,
                    new List<ObjectType>() { ObjectType.Bathroom }
                );
            case MapEditorTypes.Gym:
                return new Zone("Gym", ZoneType.Gym, new List<ObjectType>() { ObjectType.Gym });
            case MapEditorTypes.Library:
                return new Zone(
                    "Library",
                    ZoneType.Library,
                    new List<ObjectType>() { ObjectType.Library }
                );
        }
        return null;
    }

    void UpdateWall(Vec2 pos, bool north, Border border)
    {
        if (north)
        {
            var lastWall = State.World.BorderController.GetBorder(pos, pos + new Vec2(0, 1));
            LastActionBuilder.Add(() => State.World.BorderController.SetNorthBorder(pos, lastWall));
            State.World.BorderController.SetNorthBorder(pos, border);
        }
        else
        {
            var lastWall = State.World.BorderController.GetBorder(pos, pos + new Vec2(1, 0));
            LastActionBuilder.Add(() => State.World.BorderController.SetEastBorder(pos, lastWall));
            State.World.BorderController.SetEastBorder(pos, border);
        }
    }

    public void LoadMapPicker()
    {
        var ui = Instantiate(State.GameManager.LoadPicker).GetComponent<FileLoaderUI>();
        new SimpleFileLoader(
            State.MapDirectory,
            "map",
            ui,
            true,
            SimpleFileLoader.LoaderType.MapEditor
        );
    }

    public void OpenResizePanel()
    {
        if (ResizeUI == null)
        {
            ResizeUI = Instantiate(ResizeObject, UndoButton.transform.parent.parent)
                .GetComponent<MapResizePanel>();
            ResizeUI.SetSize.onClick.AddListener(() => Resize());
            ResizeUI.Cancel.onClick.AddListener(() =>
            {
                ResizeUI.gameObject.SetActive(false);
                BeforeResize = null;
            });
        }

        ResizeUI.gameObject.SetActive(true);
        ResizeUI.NewSizeX.text = State.World.Zones.GetLength(0).ToString();
        ResizeUI.NewSizeY.text = State.World.Zones.GetLength(1).ToString();
    }

    public void Resize()
    {
        int x;
        int y;
        try
        {
            x = Convert.ToInt32(ResizeUI.NewSizeX.text);
            y = Convert.ToInt32(ResizeUI.NewSizeY.text);
        }
        catch (Exception e)
        {
            Debug.LogException(e);
            State.GameManager.CreateMessageBox("Invalid value for one of the values");
            return;
        }
        if (x < 4 || y < 4)
        {
            State.GameManager.CreateMessageBox("Can't have a dimension less than 4");
            return;
        }

        int oldX = State.World.Zones.GetLength(0);
        int oldY = State.World.Zones.GetLength(1);

        BeforeResize = Utility.SerialClone(State.World);

        Zone[,] newZones = new Zone[x, y];
        int diffX = 0;
        int diffY = 0;
        if (ResizeUI.AddRemoveX.value == 0)
            diffX = x - oldX;
        if (ResizeUI.AddRemoveY.value == 1)
            diffY = y - oldY;
        if (ResizeUI.AddRemoveX.value == 2)
            diffX = (x - oldX) / 2;
        if (ResizeUI.AddRemoveY.value == 2)
            diffY = (y - oldY) / 2;

        Vec2 diff = new Vec2(diffX, diffY);
        if (Warned == false)
        {
            foreach (var person in State.World.GetPeople(true))
            {
                var newPos = person.Position + diff;
                if (newPos.x < 0 || newPos.x >= x || newPos.y < 0 || newPos.y >= y)
                {
                    State.GameManager.CreateMessageBox(
                        "This resize would place a person outside of the map boundaries and kill them. If intended, do that action again. This warning won't appear again until you restart the game."
                    );
                    Warned = true;
                    return;
                }
            }
        }

        UndoActions.Clear();

        for (int i = 0; i < x; i++)
        {
            for (int j = 0; j < y; j++)
            {
                Vec2 oldVec = new Vec2(i, j);
                Vec2 newVec = new Vec2(i + diffX, j + diffY);
                if (
                    i < State.World.Zones.GetLength(0) + diffX
                    && j < State.World.Zones.GetLength(1) + diffY
                    && i - diffX >= 0
                    && i - diffX <= oldX - 1
                    && j - diffY >= 0
                    && j - diffY <= oldY - 1
                )
                    newZones[i, j] = State.World.Zones[i - diffX, j - diffY];
                else
                    newZones[i, j] = null;
            }
        }

        State.World.BorderController.ShiftBorders(
            diff,
            newZones.GetLength(0) - 1,
            newZones.GetLength(1) - 1
        );

        for (int i = 0; i < State.World.DormRooms.Count; i++)
        {
            State.World.DormRooms[i] = new World.DormRoom(
                State.World.DormRooms[i].Position + diff,
                State.World.DormRooms[i].DoorSide
            );
        }

        for (int i = State.World.DormRooms.Count - 1; i >= 0; i--)
        {
            if (
                State.World.DormRooms[i].Position.x < 0
                || State.World.DormRooms[i].Position.x >= x
                || State.World.DormRooms[i].Position.y < 0
                || State.World.DormRooms[i].Position.y >= y
            )
                State.World.DormRooms.RemoveAt(i);
        }

        foreach (var person in State.World.GetPeople(true).ToList())
        {
            person.Position += diff;
            person.MyRoom += diff;
            if (
                person.Position.x < 0
                || person.Position.x >= x
                || person.Position.y < 0
                || person.Position.y >= y
            )
            {
                State.World.RemovePerson(person);
            }
        }

        State.World.Zones = newZones;

        State.GameManager.TileManager.DrawWorld();

        State.GameManager.RefreshCameraSettings();

        ResizeUI.gameObject.SetActive(false);
    }

    public void UndoResize()
    {
        if (BeforeResize != null)
            State.World = BeforeResize;
        State.GameManager.TileManager.DrawWorld();
        State.GameManager.RefreshCameraSettings();
        BeforeResize = null;
    }

    void TidyUp()
    {
        for (int i = State.World.DormRooms.Count - 1; i >= 0; i--)
        {
            if (
                State.World.GetZone(
                    new Vec2(
                        State.World.DormRooms[i].Position.x,
                        State.World.DormRooms[i].Position.y
                    )
                ) == null
            )
                State.World.DormRooms.RemoveAt(i);
        }

        foreach (var person in State.World.GetPeople(true).ToList())
        {
            if (State.World.GetZone(person.Position) == null)
            {
                State.World.RemovePerson(person);
            }
        }
    }

    public void Close()
    {
        gameObject.SetActive(false);
        State.GameManager.PlayerText.transform.parent.parent.parent.gameObject.SetActive(true);
        State.GameManager.TargetText.transform.parent.gameObject.SetActive(true);
        State.GameManager.LogText.transform.parent.parent.parent.gameObject.SetActive(true);
        State.GameManager.ContinueActionPanel.SetActive(continueActionState);
        TidyUp();
        PathFinder.Initialized = false;
        UndoActions.Clear();
        BeforeResize = null;
        if (EnteredFromTitleScreen)
            State.GameManager.MenuScreen.MainMenu();
    }

    public void SaveMapPrompt()
    {
        var ui = Instantiate(State.GameManager.InputBoxPrefab).GetComponent<InputBox>();
        ui.SetData(
            (string s) => TrySave(Path.Combine(State.MapDirectory, $"{s}.map")),
            "Save",
            "Cancel",
            "Enter name to save map as.",
            20
        );
    }

    void TrySave(string name)
    {
        if (File.Exists(name))
        {
            var box = Instantiate(State.GameManager.DialogBoxPrefab).GetComponent<DialogBox>();
            box.SetData(
                () => SaveMap(name),
                "Overwrite",
                "Cancel",
                "Map with that name already exists, overwrite it?"
            );
        }
        else
        {
            SaveMap(name);
        }
    }

    private void SaveMap(string name)
    {
        State.World.DormRooms = State.World.DormRooms.Distinct().ToList();
        TidyUp();
        State.Save(name);
    }

    public void AttemptUndo()
    {
        if (UndoActions.Any())
        {
            UndoActions[UndoActions.Count - 1].Undo();
            ;
            UndoActions.RemoveAt(UndoActions.Count - 1);
        }
    }

    class UndoMapAction
    {
        List<Action> Actions;

        public UndoMapAction()
        {
            Actions = new List<Action>();
        }

        public void Add(Action action)
        {
            Actions.Add(action);
        }

        public void Undo()
        {
            for (int i = Actions.Count - 1; i >= 0; i--)
            {
                Actions[i].Invoke();
            }
            State.GameManager.TileManager.DrawWorld();
        }
    }
}
