using OdinSerializer;
using System;

public enum DigestType
{
    Both,
    DigestionOnly,
    EndoOnly,
    OnlyPlayerCanDigest,
}

internal enum DigestionAlias
{
    CanVore,
    CanSwitch,
    CanEndo,
    Any
}

internal enum LoadCharacterType
{
    Random,
    Saved,
    Templates,
}

public enum MagicAvailability
{
    Default,
    Everyone,
    NoMagic,
}

public enum AutoDecide
{
    Disabled,
    Enabled,
    SucceedOnly,
    FailOnly,
    Immersive,
}

public class Settings
{
    // GENERAL

    [OdinSerialize, ProperName("Max Sight Range")]
    [Description(
        "At longer than this distance, events won't be witnessed (show up the in the log), or trigger responses, note that the ai's 'discovered vore' interaction has its own max length set to 6 tiles."
    )]
    [IntegerRange(6, 100)]
    [Category("General")]
    internal int MaxSightRange = 20;

    [OdinSerialize, ProperName("See Through Walls Enabled")]
    [Description(
        "Whether events will be witnessed through walls (show up the in the log), or trigger responses, note that the ai's 'discovered vore' interaction has its own max length set to 6 tiles."
    )]
    [Category("General")]
    internal bool SeeThroughWallsEnabled = false;


    [OdinSerialize, ProperName("Player Auto Decision Type")]
    [Description(
        "If enabled, will check the auto succeed, auto fail, and fully willing threshold values below before showing an \"ask player\" dialog box. SuccessOnly = only allows auto-succeed. FailOnly = only allows auto-failure. Immersive = enables auto fail for \"hostile\" vore and spell attack actions only."
    )]
    [Category("General")]
    internal AutoDecide DecideType = AutoDecide.Disabled;

    [OdinSerialize, ProperName("Auto Succeed")]
    [Description(
        "If auto decide is enabled and an action made against the player character would have above this % chance to succeed, the player character will agree to it automatically and a dialog box will not be shown."
    )]
    [Category("General")]
    [FloatRange(0, 1)]
    internal float PlayerAutoSucceedThreshold = 0.9f;

    [OdinSerialize, ProperName("Auto Fail")]
    [Description(
        "If auto decide is enabled and an action made against the player character would have below this % chance to succeed, the player character will refuse it automatically and a dialog box will not be shown."
    )]
    [Category("General")]
    [FloatRange(0, 1)]
    internal float PlayerAutoFailThreshold = 0.1f;

    [OdinSerialize, ProperName("Sneaky Digestion")]
    [Description(
        "If enabled, players will not recieve a warning if their endo predator switches to digestion without telling them."
    )]
    [Category("General")]
    internal bool SneakyDigestion = false;


    [OdinSerialize, ProperName("Log All Text")]
    [Description(
        "Causes the game to log all text to a file as it happens.  Note, these can take up a fair bit of space.  They're stored in the saves & content directory in the \"Logs\" folder."
    )]
    [Category("General")]
    internal bool LogAllText = false;

    [OdinSerialize, ProperName("Log File Name")]
    [Description("The file name the logs will be saved to (you don't need any extension)")]
    [Category("General")]
    internal string LogFileName = "LogName";


    // VORE GENERAL
    // world
    [OdinSerialize, ProperName("Vore Knowledge")]
    [Description(
        "How well vore is known about, mostly just cosmetic, affects how surprised people act when they see someone being eaten."
    )]
    [Category("Vore")]
    public bool VoreKnowledge = true;

    [OdinSerialize, ProperName("Secretive Vore")]
    [Description(
        "AI predators will never vore in front of others, decreasing public vore and increasing how often prey is lured to dorm rooms or secluded spots.  Note that characters with a vendetta will ignore this rule, also that this will greatly reduce the number of vore attempts."
    )]
    [Category("Vore")]
    public bool SecretiveVore = true;

    [OdinSerialize, ProperName("Vore Follows Orientation")]
    [Description(
        "If enabled, people will only eat people that they are attracted to (I.e. straight females would only eat males.)"
    )]
    [Category("Vore")]
    internal bool VoreFollowsOrientiation = false;


        // vore types enable/disables
    [OdinSerialize, ProperName("Oral Vore Enabled")]
    [Description("Whether oral vore in general is enabled")]
    [Category("Vore")]
    internal bool OralVoreEnabled = true;

    [OdinSerialize, ProperName("Oral Vore Difficulty")]
    [Description(
        "Affects the relative difficulty of performing this action, higher values lower the odds of success.  It's recommended to only make minor tweaks to this."
    )]
    [Category("Vore")]
    [FloatRange(.25f, 4)]
    internal float OralVoreDifficulty = 1;

    [OdinSerialize, ProperName("Cock Vore Enabled")]
    [Description("Whether cock vore in general is enabled")]
    [Category("Vore")]
    internal bool CockVoreEnabled = true;

    [OdinSerialize, ProperName("Cock Vore Difficulty")]
    [Description(
        "Affects the relative difficulty of performing this action, higher values lower the odds of success.  It's recommended to only make minor tweaks to this."
    )]
    [Category("Vore")]
    [FloatRange(.25f, 4)]
    internal float CockVoreDifficulty = 1;

    [OdinSerialize, ProperName("Unbirth Enabled")]
    [Description("Whether unbirth in general is enabled")]
    [Category("Vore")]
    internal bool UnbirthEnabled = true;

    [OdinSerialize, ProperName("Unbirth Difficulty")]
    [Description(
        "Affects the relative difficulty of performing this action, higher values lower the odds of success.  It's recommended to only make minor tweaks to this."
    )]
    [Category("Vore")]
    [FloatRange(.25f, 4)]
    internal float UnbirthDifficulty = 1;

    [OdinSerialize, ProperName("Anal Vore Enabled")]
    [Description("Whether anal vore in general is enabled")]
    [Category("Vore")]
    internal bool AnalVoreEnabled = false;

    [OdinSerialize, ProperName("Anal Vore Difficulty")]
    [Description(
        "Affects the relative difficulty of performing this action, higher values lower the odds of success.  It's recommended to only make minor tweaks to this."
    )]
    [Category("Vore")]
    [FloatRange(.25f, 4)]
    internal float AnalVoreDifficulty = 1;

    [OdinSerialize, ProperName("Anal Vore goes directly to stomach.")]
    [Description(
        "If disabled, anal voring someone will put them in a unique location, the bowels.  That has its own text, though it still shares capacity with the stomach.  People there will have the option to be moved to the stomach.  If enabled, anal vored prey just go directly to the stomach."
    )]
    [Category("Vore")]
    internal bool AnalVoreGoesDirectlyToStomach = false;


        // endo
    [OdinSerialize, ProperName("Endo is Only Romantic")]
    [Description(
        "Whether Endo is considered a romantic option instead of just a Friendly option (Endo dominators ignore this)"
    )]
    [Category("Vore")]
    internal bool EndoisOnlyRomantic = true;

    [OdinSerialize, ProperName("Endo Desired Length")]
    [Description(
        "Controls how long characters prefer to hold their prey in endosoma mode.  The pred will start wanting to release the prey on their own after this many turns.  (This number is doubled for endo dominators)"
    )]
    [Category("Vore")]
    [IntegerRange(1, 1000)]
    internal int EndoPreferredTurns = 150;

    [OdinSerialize, ProperName("Endo Dominators Hold Forever")]
    [Description(
        "If this is set, endo dominators will never release their prey voluntarily, prey can only struggle out, or be freed"
    )]
    [Category("Vore")]
    internal bool EndoDominatorsHoldForever = false;

    [OdinSerialize, ProperName("Predator Tier Also Restricts Endo")]
    [Description(
        "Characters will also be incapable of using endo on higher tiers, i.e. there will be no vore actions at all against higher tiers."
    )]
    [Category("Vore")]
    internal bool PredatorTierAlsoRestrictsEndo = true;


		// vore statistics
    [OdinSerialize, ProperName("Pred Capacity")]
    [Description(
        "The lower-bound of any preds capacity.  High voracity will quadruple this value and certain quirks may increase it farther."
    )]
    [Category("Vore")]
    [FloatRange(.0f, 2)]
    internal float PredCapacity = 1;

    [OdinSerialize, ProperName("Vore Anger")]
    [FloatRange(0, 1)]
    [Description(
        "How upset people will be witnessing vore actions, particularly with people they care about or themselves, at 0 there will be no relationship drops from vore.  "
    )]
    [Category("Vore")]
    internal float VoreAnger = 1;

    [OdinSerialize, ProperName("Escape Rate")]
    [FloatRange(0, 4)]
    [Description(
        "How adept prey are at escaping using the struggle commands.  Set it to 0 to make those completely ineffective."
    )]
    [Category("Vore")]
    internal float EscapeRate = 1;

    [OdinSerialize, ProperName("Escape Stun")]
    [Description(
        "Causes predators to be stunned for a turn after prey forcibly escapes, to allow them to flee easier."
    )]
    [Category("Vore")]
    public bool EscapeStun = false;

    [OdinSerialize, ProperName("Freeing Odds")]
    [FloatRange(0, 4)]
    [Description(
        "A straight multiplier to the odds of someone freeing prey from someone else.   Note that partially consumed prey are significantly easier to free."
    )]
    [Category("Vore")]
    internal float FreeingOdds = 1;

    [OdinSerialize, ProperName("Pred Conviction")]
    [Description(
        "How firmly predators are in their intent to digest.  At 1 they won't consider those requests, at 0 they're easily persuaded, the default is .5"
    )]
    [Category("Vore")]
    internal float PredConviction = 1;

    [OdinSerialize, ProperName("Fully Willing Threshold")]
    [Description(
        "Anyone with a prey willingness of at least this value will be willing when eaten, even if forced.  If vore is not endo, also factors in the prey's digestion interest."
    )]
    [Category("Vore")]
    public float WillingThreshold = .7f;

    [OdinSerialize, ProperName("Ask to Vore Difficulty")]
    [FloatRange(0, 2)]
    [Description(
        "Raise or lower the difficulty of \"Ask to Vore\" and Taste interactions. Higher numbers = harder to persuade characters to be prey."
    )]
    [Category("Vore")]
    public float AskToVoreDifficulty = .5f;


        // ai biases
    [OdinSerialize, ProperName("Vore Hunting Bias")]
    [FloatRange(0, 2)]
    [Description(
        "How often preds will enter hunting mode when hungry.  Note that is the main source of forced vore, there is only vendettas and vore during sex besides hunting for prey."
    )]
    [Category("Vore")]
    internal float VoreHuntingBias = 1;

    [OdinSerialize, ProperName("Pred Ask Bias")]
    [FloatRange(0, 8)]
    [Description(
        "How much predators value asking prey to be eaten.  Setting it higher will also increase the amount of total vore."
    )]
    [Category("Vore")]
    internal float AskPredWillingness = 1;

    [OdinSerialize, ProperName("Pred Forced Bias")]
    [FloatRange(0, 8)]
    [Description(
        "How much predators like using forced vore. Hunting is the main source of this, and setting it higher means they're more likely to pick people that they see as targets.  If set below .2, they will start mixing in asking into hunting (To give them something to do if hunting is still enabled but they can't really force).  If set to 0 they will avoid it completely, except for characters with vendettas."
    )]
    [Category("Vore")]
    internal float ForcedPredWillingness = 1;

    [OdinSerialize, ProperName("Digestion Bias")]
    [FloatRange(0, 2)]
    [Description(
        "How much predators like to digest prey.  This changes both how often it happens in relation to endo, and also how common it is in general"
    )]
    [Category("Vore")]
    internal float DigestionBias = 1;

    [OdinSerialize, ProperName("Endo Bias")]
    [FloatRange(0, 2)]
    [Description(
        "How much predators like to have prey that they don't digest.  This changes both how often it happens in relation to digestion, and also how common it is in general"
    )]
    [Category("Vore")]
    internal float EndoBias = 1;

    [OdinSerialize, ProperName("Help Prey Bias")]
    [FloatRange(0, 4)]
    [Description(
        "How often people will try to help prey.  Setting it to 0 disables this entirely.  The actual result depends on a few factors, including how well they know the predator and prey."
    )]
    [Category("Vore")]
    internal float HelpPreyBias = 1;


    [OdinSerialize, ProperName("Willing Offer Bias")]
    [FloatRange(0, 30)]
    [Description(
        "How often a willing prey will spontaneously offer themselves to someone.  1 is the default as somewhat rare."
    )]
    [Category("Vore")]
    internal float WillingOfferOdds = 1;



// DIGESTION

    [OdinSerialize, ProperName("Digestion Type")]
    [Description("The digestion types available to the player / AI.")]
    [Category("Digestion")]
    internal DigestType DigestType = DigestType.Both;

    [OdinSerialize, ProperName("Digestion Speed")]
    [Description(
        "Change how fast digestion happens. A value of 1 gives digestion in about 17 turns on a full health target. Higher values mean less time."
    )]
    [Category("Digestion")]
    [FloatRange(.01f, 4)]
    internal float DigestionSpeed = 0.5f;

    [OdinSerialize, ProperName("Absorption Speed")]
    [Description(
        "Change how fast absorption happens (the time from when a prey dies until they're gone). At the default settings absorption takes about 13 turns. Higher values mean less time."
    )]
    [Category("Digestion")]
    [FloatRange(.01f, 4)]
    internal float AbsorptionSpeed = 1;

    [OdinSerialize, ProperName("Prey Orgasm Digest Mod")]
    [Description(
        "Roughly how many turns of digestion damage should a prey take when they orgasm inside another. A value of 1 equals roughly 5 turns of damage. Set to 0 to disable."
    )]
    [Category("Digestion")]
    [FloatRange(0, 10)]
    internal float PreyOrgasmDigestionMod = 1f;

    [OdinSerialize, ProperName("Pred Orgasm Digest Mod")]
    [Description(
        "Roughly how many turns of digestion damage should a pred inflict take when they orgasm with prey inside. A value of 1 equals roughly 5 turns of damage. Set to 0 to disable."
    )]
    [Category("Digestion")]
    [FloatRange(0, 10)]
    internal float PredOrgasmDigestionMod = 1f;

    [OdinSerialize, ProperName("Pred Orgasm Always Damages")]
    [Description(
        "If enabled, a pred will always inflict digestion damage on digesting prey when having an orgasm. If false, damage is only applied for womb or balls prey."
    )]
    [Category("Digestion")]
    internal bool PredOrgasmAlwaysDamages = false;

    [OdinSerialize, ProperName("Rub Digestion Mod")]
    [Description(
        "Roughly how much digestion damage should be inflicted when rubbing digesting bellies or balls. A value of 1 equals 1 turn of damage. Set to 0 to disable."
    )]
    [Category("Digestion")]
    [FloatRange(0, 10)]
    internal float RubDigestionMod = 1f;

    [OdinSerialize, ProperName("Disable AI - Ask to Digest")]
    [Description("Prevents the AI from using the ask to digest interactions.")]
    [Category("Digestion")]
    internal bool PreventAIAskToDigest = false;

    [OdinSerialize, ProperName("Disable AI - Ask to be Digested")]
    [Description("Prevents the AI from using the ask to be digested interactions.")]
    [Category("Digestion")]
    internal bool PreventAIAskToBeDigested = false;



// SIZE & GAIN

    [OdinSerialize, ProperName("Weight Gain Enabled")]
    [Description("Enables / Disables all weight gain (the next four options)")]
    [Category("Size & Gain")]
    public bool WeightGain = false;

    [OdinSerialize, ProperName("Size Factor")]
    [Description(
        "Affects the bonus/penalty of size differences.  0 disables size differences, 1 gives size difference a linear bonus and is fairly balanced, 2 gives a squared bonus and is fairly realistic, 3 gives an extreme cubic bonus."
    )]
    [Category("Size & Gain")]
    [FloatRange(.0f, 3)]
    internal float SizeFactor = 1;

    [OdinSerialize, ProperName("Body Weight Gain Rate")]
    [FloatRange(0, 10)]
    [Description(
        "Affects the rate at which characters gain / lose body weight (They slowly lose weight while extremely hungry).  A value of 1 is based on 'realistic gain'.   0 disables this entirely."
    )]
    [Category("Size & Gain")]
    public float WeightGainBody = 0;

    [OdinSerialize, ProperName("Max Weight Gain Limit")]
    [IntegerRange(0, 4000)]
    [Description("Weight gain slows down in relation to this, and will never exceed this value.")]
    [Category("Size & Gain")]
    public int TheoreticalMaxWeight = 800;

    [OdinSerialize, ProperName("Height Gain Rate")]
    [FloatRange(0, 10)]
    [Description(
        "Affects the rate at which characters gain height through vore.  0 disables this entirely."
    )]
    [Category("Size & Gain")]
    public float WeightGainHeight = 0;

    [OdinSerialize, ProperName("Max Height Gain Limit")]
    [IntegerRange(0, 300)]
    [Description("Height gain slows down in relation to this, and will never exceed this value.")]
    [Category("Size & Gain")]
    public int TheoreticalMaxHeight = 96;

    [OdinSerialize, ProperName("Boob Size Gain Rate")]
    [FloatRange(0, 10)]
    [Description(
        "Affects the rate at which characters gain breast size through vore.  0 disables this entirely."
    )]
    [Category("Size & Gain")]
    public float WeightGainBoob = 0;

    [OdinSerialize, ProperName("Dick Size Gain Rate")]
    [FloatRange(0, 10)]
    [Description(
        "Affects the rate at which characters gain dick size through vore.  0 disables this entirely."
    )]
    [Category("Size & Gain")]
    public float WeightGainDick = 0;

    [OdinSerialize, ProperName("Dick Growth From CV Only")]
    [Description("If enabled, dick size is only increased via cock vore / balls digestion.")]
    [Category("Size & Gain")]
    internal bool DickGainFromCVOnly = true;


// RELATIONSHIPS

    [OdinSerialize, ProperName("Friendship Multiplier"), FloatRange(.1f, 4.0f)]
    [Description(
        "How fast friendships increase.  Faster speeds reduce grinding, but may be seen as too fast.  The default is 1.  This only affects smaller changes, big changes like saving someone are unaffected."
    )]
    [Category("Relationships")]
    internal float FriendshipMultiplier = 1;

    [OdinSerialize, ProperName("Romantic Multiplier"), FloatRange(.1f, 4.0f)]
    [Description(
        "How fast romantic feelings increase.  Faster speeds reduce grinding, but may be seen as too fast.  The default is 2."
    )]
    [Category("Relationships")]
    internal float RomanticMultiplier = 2;

    [OdinSerialize, ProperName("Rejection Cooldown"), FloatRange(0, 4)]
    [Description(
        "How long it takes after being friendly/romantically rejected before odds return to normal.  Set to 0 to disable rejection penalty."
    )]
    [Category("Relationships")]
    internal float RejectionCooldown = 1;

    [OdinSerialize, ProperName("Rejection Penalty"), FloatRange(0, 1)]
    [Description(
        "The odds multiplier on rejections.  When nearing the end of the rejection, will scale back to full odds.  Set to 1 for no penalty.  Set to 0 to always fail rejected actions."
    )]
    [Category("Relationships")]
    internal float RejectionPenalty = 0.1f;

    [OdinSerialize, ProperName("First Time Penalty"), FloatRange(0.1f, 1)]
    [Description(
        "An odds multiplier applied for basic romantic actions that two characters have not done together yet.  Think of this as a \"First Kiss Difficulty\" of sorts, that is removed after success.  Lower numbers = bigger penalty.  Set to 1 to remove this penalty."
    )]
    [Category("Relationships")]
    internal float RomanceHistoryPenalty = 0.7f;

    [OdinSerialize, ProperName("Relationship Decay")]
    [Description(
        "If active, friendship and relationship values will slowly decay over time, requiring regular interactions to keep the values up."
    )]
    [Category("Relationships")]
    internal bool RelationshipDecay = false;

    [OdinSerialize, ProperName("Allow Dating Dorm Entry")]
    [Description("Allows characters who are dating to enter each other's dorm rooms freely.")]
    [Category("Relationships")]
    internal bool DatingEntry = true;



// STATS & NEEDS

    [OdinSerialize, ProperName("Flexible Stats")]
    [Description(
        "Whether or not personality and stats can change during the game. Some examples are people getting better at being a predator with experience, or gaining strength from digesting prey.  "
    )]
    [Category("Stats & Needs")]
    internal bool FlexibleStats = true;

    [OdinSerialize, ProperName("Flexble Stat Decay Speed")]
    [Description(
        "Affects how fast flexible stats fall over time, setting to 0 disables this entirely.   Note that stats don't decay if there is no relevant building to raise them, or if flexible stats is disabled."
    )]
    [Category("Stats & Needs")]
    [FloatRange(0, 4)]
    internal float FlexibleStatDecaySpeed = 1;

    [OdinSerialize, ProperName("Hunger Rate")]
    [Description(
        "Change how much hunger increases passively each turn.  High values mean characters get hungry faster."
    )]
    [Category("Stats & Needs")]
    [FloatRange(.1f, 10)]
    internal float HungerRate = 1;

    [OdinSerialize, ProperName("Dirtiness Rate")]
    [Description(
        "Change how much cleanliness decreases passively each turn.  High values mean characters get dirty faster.  Actions which affect dirtiness are unaffected."
    )]
    [Category("Stats & Needs")]
    [FloatRange(.1f, 10)]
    internal float CleanlinessRate = 1;

    [OdinSerialize, ProperName("Tiredness Rate")]
    [Description(
        "Change how much tiredness increases passively each turn.  High values mean characters get hungry faster.  Actions which affect tiredness are unaffected."
    )]
    [Category("Stats & Needs")]
    [FloatRange(.1f, 10)]
    internal float TirednessRate = 1;

    [OdinSerialize, ProperName("Horniness Rate")]
    [Description(
        "Change how much horniness changes passively each turn.  High values mean a bigger change.  Note: This is strongly affected by sex drive - characters with low sex drive will lose horniness every turn if not stimulated."
    )]
    [Category("Stats & Needs")]
    [FloatRange(.1f, 10)]
    internal float HorninessRate = 1;

    [OdinSerialize, ProperName("Cafeteria Usage Rate")]
    [Description(
        "Change how frequently an AI controlled character wants to use the cafeteria."
    )]
    [Category("Stats & Needs")]
    [FloatRange(.1f, 5)]
    internal float CafeteriaUsageRate = 1;
    
    [OdinSerialize, ProperName("Shower Usage Rate")]
    [Description(
        "Change how frequently an AI controlled character wants to use the shower."
    )]
    [Category("Stats & Needs")]
    [FloatRange(.1f, 5)]
    internal float ShowerUsageRate = 1;
    
    [OdinSerialize, ProperName("Bed Usage Rate")]
    [Description(
        "Change how frequently an AI controlled character wants to rest in their bed."
    )]
    [Category("Stats & Needs")]
    [FloatRange(.1f, 5)]
    internal float BedUsageRate = 1;

    [OdinSerialize, ProperName("Seek Orgasm Rate")]
    [Description(
        "Change how frequently an AI controlled character looks to have sex or masturbate."
    )]
    [Category("Stats & Needs")]
    [FloatRange(.1f, 5)]
    internal float SeekOrgasmRate = 1;

    [OdinSerialize, ProperName("Gym Usage Rate")]
    [Description(
        "Change how frequently an AI controlled character wants to use the gym."
    )]
    [Category("Stats & Needs")]
    [FloatRange(.1f, 5)]
    internal float GymUsageRate = 1;
    
    [OdinSerialize, ProperName("Library Usage Rate")]
    [Description(
        "Change how frequently an AI controlled character wants to use the library."
    )]
    [Category("Stats & Needs")]
    [FloatRange(.1f, 5)]
    internal float LibraryUsageRate = 1;


    // SEX

    [OdinSerialize, ProperName("Sex Promiscuity Threshold")]
    [Description(
        "Anyone with a promiscuity lower than this value will refuse to have sex outside of a bedroom."
    )]
    [Category("Sex")]
    public float PromiscuityThreshold = .5f;

    [OdinSerialize, ProperName("Allow Navelfuck")]
    [Description("Allows the Player / AI to use the navelfuck action during sex.")]
    [Category("Sex")]
    internal bool EnableNavelfuck = true;

    [OdinSerialize, ProperName("Allow Rimjob")]
    [Description("Allows the Player / AI to use the rimjob action during sex.")]
    [Category("Sex")]
    internal bool EnableRimjob = true;

    [OdinSerialize, ProperName("Allow Partner Kinky Sex Acts")]
    [Description(
        "Allows a character to perform kinky sex actions (footjob, kiss armpit, etc) if their partner has the associated trait."
    )]
    [Category("Sex")]
    internal bool PartnerKinkySex = true;

    [OdinSerialize, ProperName("Unbirth Lactation Enabled")]
    [Description(
        "If a character has womb prey, enables the breastfeeding sex action and text describing lactation."
    )]
    [Category("Sex")]
    public bool UnbirthLactation = true;


// MAGIC

    [OdinSerialize, ProperName("Magic Setting")]
    [Description(
        "If \"Default\", characters can use magic if it is enabled in their character settings. If \"Everyone\", magic can be performed by all characters regardless of settings. If \"NoMagic\", magic is disabled in this world."
    )]
    [Category("Magic")]
    public MagicAvailability MagicType = MagicAvailability.Default;

    [OdinSerialize, ProperName("Fizzle Mana Consumption")]
    [Description("If enabled, failing a spell will still consume mana.")]
    [Category("Magic")]
    public bool FizzleMana = true;

    [OdinSerialize, ProperName("Perma Size Change")]
    [Description(
        "If enabled, size changing spells will last forever (or until they are counteracted with the opposing spell)."
    )]
    [Category("Magic")]
    public bool PermaSizeChange = false;

    [OdinSerialize, ProperName("Mana Regen Rate")]
    [Description(
        "A multiplier for how quickly a character generates mana. High values mean faster mana generation. A value of 0 means mana will not regenerate naturally and must be gained from digestion."
    )]
    [Category("Magic")]
    [FloatRange(0, 4)]
    internal float ManaRegenMod = 1f;

    [OdinSerialize, ProperName("Spell Duration")]
    [Description(
        "A multiplier for how long spell effects last. High values mean longer lasting spell effects. Setting to 0 disables duration spells."
    )]
    [Category("Magic")]
    [FloatRange(0, 4)]
    internal float SpellDurationMod = 1f;

    [OdinSerialize, ProperName("Extend Spell Durations")]
    [Description(
        "If enabled, casting a spell twice will extend its duration.  If disabled, duration will be reset instead."
    )]
    [Category("Magic")]
    public bool ExtendSpellDurations = false;

    [OdinSerialize, ProperName("Size Change Modifier")]
    [Description(
        "A multiplier for how effective the grow and shrink spells are. Higher values mean larger changes. Changing this mid-game may have unintended effects. Setting to 1 disables the grow/shrink spells."
    )]
    [Category("Magic")]
    [FloatRange(1, 10)]
    internal float SizeChangeMod = 1.5f;

    [OdinSerialize, ProperName("Player Charm Threshold")]
    [Description(
        "If the player character is under the charmed effect, any interaction made towards you above this % chance will auto-succeed without asking first."
    )]
    [Category("Magic")]
    [FloatRange(0, 1)]
    internal float PlayerCharmThreshold = 0.0f;


// DISPOSAL OPTIONS

    [OdinSerialize, ProperName("Disposal Enabled")]
    [Description("Whether Disposal (scat) in general is enabled")]
    [Category("Disposal")]
    public bool DisposalEnabled = false;

    [OdinSerialize, ProperName("Cock Disposal Enabled")]
    [Description("Whether cock disposal in general is enabled")]
    [Category("Disposal")]
    public bool DisposalCockEnabled = false;

    [OdinSerialize, ProperName("Unbirth Disposal Enabled")]
    [Description("Whether unbirth disposal in general is enabled")]
    [Category("Disposal")]
    public bool DisposalUnbirthEnabled = false;

    [OdinSerialize, ProperName("Bones Disposal Enabled")]
    [Description("Whether bones can appear in disposal")]
    [Category("Disposal")]
    public bool DisposalBonesEnabled = false;

    [OdinSerialize, ProperName("Dispose with Orgasm")]
    [Description("Characters will automatically dispose melted cock or womb prey when having an orgasm.")]
    [Category("Disposal")]
    public bool DisposeWithOrgasm = true;

    [OdinSerialize, ProperName("Public Disposal Rate")]
    [Description(
        "Affects how often the AI will do disposals in public compared to bathrooms.   0 means always bathrooms, 1 means never bathrooms."
    )]
    [Category("Disposal")]
    [FloatRange(0, 1)]
    internal float PublicDisposalRate = 0.2f;



// WORLD SETTINGS

    [OdinSerialize, ProperName("Nurse Station Resurrection")]
    [Description(
        "If active, the nurse's office will serve as a resurrection point, bringing anyone who was digested back to life.  If off, digested people are gone."
    )]
    [Category("World")]
    internal bool NursesActive = false;

    [OdinSerialize, ProperName("New People Arrive")]
    [Description("If active, new people will occasionally show up to replace eaten people")]
    [Category("World")]
    internal bool NewArrivals = false;

    [OdinSerialize, ProperName("New Arrival Delay")]
    [Description(
        "New people will arrive on average every this many turns (the actual number of turns will be 50-150% of this)."
    )]
    [Category("World")]
    [IntegerRange(3, 1000)]
    internal int NewArrivalAverageTime = 40;

    [OdinSerialize, ProperName("New Arrival Maximum")]
    [Description(
        "New arrivals will only spawn if the world has less than this many people in it.  Note that the maximum is also limited by the number of dorm rooms"
    )]
    [Category("World")]
    [IntegerRange(4, 250)]
    internal int MaxPopulation = 100;

    [OdinSerialize, ProperName("New Arrival Type")]
    [Description(
        "The type of new characters that will spawn - Random is just pure random and Saved will pick from the saved characters, and only them.  Templates will generate a semi-random character from your saved templates. Note that saved characters are eligible for being re-added as soon as they're gone."
    )]
    [Category("World")]
    internal LoadCharacterType NewType = LoadCharacterType.Random;

    [OdinSerialize, ProperName("New Arrival Tag Specifier")]
    [Description(
        "Only has an effect if New Arrival type is set to named.  Leave blank to pick from all saved characters, but you can also add tags, that work the same way as they do in the saved character screen, i.e. 'main' would get characters that use that in their tags."
    )]
    [Category("World")]
    internal string NewTypeTag = "";

    [OdinSerialize, ProperName("Randomize Dorm Placement")]
    [Description(
        "If enabled, characters will be placed in a random dorm room instead of the next dorm room internally."
    )]
    [Category("World")]
    internal bool RandomizeDormPlacement = true;

    [OdinSerialize, ProperName("World Frozen")]
    [Description(
        "A Debug/testing option that causes only the currently selected player to be able to move. Pauses needs and digestion, though performing consuming actions (like swallowing) will perform normal digestion on all prey inside on any turn its performed."
    )]
    [Category("World")]
    internal bool WorldFrozen = false;




        // version (not a setting)
    [OdinSerialize, VariableEditorIgnores]
    string version;


    internal bool CheckDigestion(bool player, DigestionAlias type)
    {
        if (type == DigestionAlias.Any)
            return true;
        if (player)
        {
            switch (type)
            {
                case DigestionAlias.CanVore:
                    if (
                        DigestType == DigestType.Both
                        || DigestType == DigestType.DigestionOnly
                        || DigestType == DigestType.OnlyPlayerCanDigest
                    )
                        return true;
                    break;
                case DigestionAlias.CanSwitch:
                    if (
                        DigestType == DigestType.Both
                        || DigestType == DigestType.OnlyPlayerCanDigest
                    )
                        return true;
                    break;
                case DigestionAlias.CanEndo:
                    if (
                        DigestType == DigestType.Both
                        || DigestType == DigestType.EndoOnly
                        || DigestType == DigestType.OnlyPlayerCanDigest
                    )
                        return true;
                    break;
            }
        }
        else
        {
            switch (type)
            {
                case DigestionAlias.CanVore:
                    if (DigestType == DigestType.Both || DigestType == DigestType.DigestionOnly)
                        return true;
                    break;
                case DigestionAlias.CanSwitch:
                    if (DigestType == DigestType.Both)
                        return true;
                    break;
                case DigestionAlias.CanEndo:
                    if (
                        DigestType == DigestType.Both
                        || DigestType == DigestType.EndoOnly
                        || DigestType == DigestType.OnlyPlayerCanDigest
                    )
                        return true;
                    break;
            }
        }
        return false;
    }

    internal bool CheckDigestion(Person actor, DigestionAlias type)
    {
        return CheckDigestion(actor == State.World.ControlledPerson, type);
    }

    internal void CheckUpdateVersion()
    {
        if (string.Compare(version, "7D") < 0)
        {
            VoreHuntingBias = 1;
            ForcedPredWillingness = 1;
            AskPredWillingness = 1;
            VoreAnger = 1;
            HelpPreyBias = 1;
            WillingThreshold = .7f;
        }
        if (string.Compare(version, "7E") < 0)
        {
            PredConviction = .5f;
            EscapeRate = 1;
        }
        if (string.Compare(version, "8") < 0)
        {
            DigestionBias = 1;
            EndoBias = 1;
            TheoreticalMaxWeight = 800;
            TheoreticalMaxHeight = 96;
        }
        if (string.Compare(version, "8C") < 0)
        {
            FreeingOdds = 1;
        }
        if (string.Compare(version, "9B") < 0)
        {
            if (AbsorptionSpeed <= 0.06f)
                AbsorptionSpeed = DigestionSpeed;
            if (WillingOfferOdds == 0)
                WillingOfferOdds = 1;
        }
        if (string.Compare(version, "9F RH 4A1", StringComparison.Ordinal) < 0)
        {
            EnableNavelfuck = true;
            EnableRimjob = true;
            PartnerKinkySex = true;
            UnbirthLactation = true;
            DatingEntry = true;
            SizeFactor = 2f;
            FizzleMana = true;
            ManaRegenMod = 1f;
            SpellDurationMod = 1f;
            SizeChangeMod = 1.5f;
        }
        if (string.Compare(version, "9F RH 4D", StringComparison.Ordinal) < 0)
        {
            RubDigestionMod = 1f;
            PreyOrgasmDigestionMod = 1f;
            PredOrgasmDigestionMod = 1f;
            PredOrgasmAlwaysDamages = false;
        }
        if (string.Compare(version, "9F RH 5B", StringComparison.Ordinal) < 0)
        {
            PredCapacity = 1f;
        }
        if (string.Compare(version, "9F RH 5C", StringComparison.Ordinal) < 0)
        {
            SneakyDigestion = false;
            PlayerCharmThreshold = 0.5f;
            PlayerAutoSucceedThreshold = 0.9f;
        }
        if (string.Compare(version, "9F RH 6A", StringComparison.Ordinal) < 0)
        {
            PlayerAutoFailThreshold = 0.1f;
            DecideType = AutoDecide.Disabled;
        }
        if (string.Compare(version, "9F RH 8A", StringComparison.Ordinal) < 0)
        {
            DisposeWithOrgasm = true;
            PromiscuityThreshold = 0.5f;
        }
        if (string.Compare(version, "9F RH 9A", StringComparison.Ordinal) < 0)
        {
            AskToVoreDifficulty = 0.5f;
            RejectionCooldown = 1f;
            RejectionPenalty = 0.1f;
            CleanlinessRate = 1f;
            TirednessRate = 1f;
            HorninessRate = 1f;
            ExtendSpellDurations = false;
            CafeteriaUsageRate = 1f;
            ShowerUsageRate = 1f;
            BedUsageRate = 1f;
            LibraryUsageRate = 1f;
            GymUsageRate = 1f;
            SeekOrgasmRate = 1f;
            RomanceHistoryPenalty = 0.7f;
        }

        version = State.Version;
    }
}
