﻿using OdinSerializer;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using static TemplateController;

public class World
{
    [OdinSerialize]
    internal Zone[,] Zones;

    [OdinSerialize]
    internal BorderController BorderController;

    [OdinSerialize]
    readonly List<Person> People;

    [OdinSerialize]
    internal TemplateContainer TemplateContainer;

    [OdinSerialize]
    public Settings Settings;

    [OdinSerialize]
    internal Person ControlledPerson;

    [OdinSerialize]
    internal Person CurrentTurn;

    [OdinSerialize]
    internal int Turn;

    [OdinSerialize]
    internal int NextID = 1;

    [OdinSerialize]
    internal bool AskedToStay;

    [OdinSerialize]
    internal bool WaitingQuestion;

    [OdinSerialize]
    internal int RemainingSkippedTurns;

    [OdinSerialize]
    internal string SaveVersion;

    internal Vec2 AutoDestination;

    [OdinSerialize]
    internal List<DigestionRecordItem> Digestions;

    [OdinSerialize]
    internal List<DormRoom> DormRooms;

    [OdinSerialize]
    internal GenderList GenderList;

    [OdinSerialize]
    internal RaceWeights RaceWeights;

    [OdinSerialize]
    internal TraitWeights TraitWeights;

    [OdinSerialize]
    internal string MapName;

    [OdinSerialize]
    internal Person VisionAttachedTo;

    [OdinSerialize]
    internal int NextArrivalTime;

    [OdinSerialize]
    internal bool LastPlayerTurnWasAI;

    [OdinSerialize]
    internal Vec2 OldLocation;

    [OdinSerialize]
    internal Person SoughtPerson;

    internal bool HasCafeteria;
    internal bool HasLibrary;
    internal bool HasGym;
    internal bool HasNurse;

    internal bool ClearedNextTurn;
    bool Repeat;
    bool SingleSkip;

    int TurnsToAutoSave;

    bool PlayerAlreadyInObserver = false;

    internal bool TestingMode = false;

    private List<Action> endOfTurnCallBacks = new List<Action>();

    internal List<Action> EndOfTurnCallBacks
    {
        get
        {
            if (endOfTurnCallBacks == null)
                endOfTurnCallBacks = new List<Action>();
            return endOfTurnCallBacks;
        }
        set => endOfTurnCallBacks = value;
    }

    internal struct DormRoom
    {
        [OdinSerialize]
        internal Vec2 Position;

        [OdinSerialize]
        internal Facing DoorSide;

        public DormRoom(Vec2 position, Facing doorSide)
        {
            Position = position;
            DoorSide = doorSide;
        }
    }

    internal enum Facing
    {
        North,
        East,
        West,
        South
    }

    /// <summary>
    /// Gets the specified zone, returning null if there is no zone, or if it's outside of boundaries.
    /// </summary>
    /// <param name="position"></param>
    /// <returns></returns>
    internal Zone GetZone(Vec2 position)
    {
        if (
            position.x < 0
            || position.x > Zones.GetUpperBound(0)
            || position.y < 0
            || position.y > Zones.GetUpperBound(1)
        )
            return null;
        return Zones[position.x, position.y];
    }

    internal World(
        List<Person> people,
        Settings settings,
        GenderList genderList,
        World map,
        RaceWeights raceWeights,
        TraitWeights traitWeights
    )
    {
        Settings = settings;
        People = people;
        GenderList = genderList;
        ControlledPerson = people[0];
        CurrentTurn = people[0];
        MapName = map.MapName;
        RaceWeights = raceWeights;
        TraitWeights = traitWeights;
        TemplateContainer = Utility.SerialClone(State.TemplateController.Container);
        UseCustomLayout(map);
        for (int i = 0; i < people.Count; i++)
        {
            People[i].Position = people[i].MyRoom;
        }
        RelabelPeople();
        PathFinder.Initialized = false;
        Digestions = new List<DigestionRecordItem>();
    }

    internal World(List<Person> people, Settings settings, GenderList genderList, bool expanded)
    {
        Settings = settings;
        People = people;
        if (GenderList == null)
            GenderList = new GenderList();
        else
            GenderList = genderList;
        ControlledPerson = people[0];
        CurrentTurn = people[0];
        RaceWeights = State.RaceWeights;
        TraitWeights = State.TraitWeights;
        TemplateContainer = new TemplateContainer();
        UseDefaultLayout(expanded);
        //UseCustomLayout();
        for (int i = 0; i < people.Count; i++)
        {
            People[i].Position = new Vec2(8 + Rand.Next(5), 7 + Rand.Next(5));
        }
        RelabelPeople();
        PathFinder.Initialized = false;
        Digestions = new List<DigestionRecordItem>();
    }

    internal World()
    {
        GenderList = new GenderList();
        Settings = new Settings();
        RaceWeights = State.RaceWeights;
        TraitWeights = State.TraitWeights;
        ControlledPerson = new Person(
            "Gwen",
            "Greene",
            1,
            Orientation.FemaleOnly,
            true,
            new Vec2(10, 9)
        );
        CurrentTurn = ControlledPerson;
        People = new List<Person>
        {
            ControlledPerson,
            new Person("Ellie", "Smith", 1, Orientation.FemaleOnly, true, new Vec2(9, 8)),
        };
        TemplateContainer = new TemplateContainer();
        RelabelPeople();
        PathFinder.Initialized = false;
        Digestions = new List<DigestionRecordItem>();
        UseDefaultLayout(false);
    }

    void UseDefaultLayout(bool expanded)
    {
        Zones = new Zone[20, 20];
        DormRooms = new List<DormRoom>
        {
            new DormRoom(new Vec2(4, 5), Facing.East),
            new DormRoom(new Vec2(4, 6), Facing.East),
            new DormRoom(new Vec2(4, 7), Facing.East),
            new DormRoom(new Vec2(4, 11), Facing.East),
            new DormRoom(new Vec2(4, 12), Facing.East),
            new DormRoom(new Vec2(4, 13), Facing.East),
            new DormRoom(new Vec2(6, 5), Facing.West),
            new DormRoom(new Vec2(6, 6), Facing.West),
            new DormRoom(new Vec2(6, 7), Facing.West),
            new DormRoom(new Vec2(6, 11), Facing.West),
            new DormRoom(new Vec2(6, 12), Facing.West),
            new DormRoom(new Vec2(6, 13), Facing.West),
        };
        BorderController = new BorderController();
        if (expanded)
        {
            CreateSwatch("", 3, 14, 3, 14, ZoneType.Grass);
            Zones[3, 8] = null;
            Zones[3, 10] = null;
            Zones[4, 8] = null;
            Zones[4, 10] = null;
            Zones[6, 8] = null;
            Zones[6, 10] = null;
            Zones[7, 8] = null;
            Zones[7, 10] = null;
        }

        CreateSwatch("Courtyard", 8, 12, 7, 11, ZoneType.Grass);
        CreateSwatch(
            "Cafeteria",
            9,
            11,
            12,
            14,
            ZoneType.Cafeteria,
            new List<ObjectType>() { ObjectType.Food }
        );
        CreateSwatch("Dorm Hallway", 4, 7, 9, 9, ZoneType.IndoorHallway);
        CreateSwatch("Dorm Hallway", 5, 5, 5, 13, ZoneType.IndoorHallway);
        Zones[3, 9] = new Zone(
            $"Shower Room",
            ZoneType.Shower,
            new List<ObjectType>() { ObjectType.Shower }
        );
        BorderController.SetEastBorder(new Vec2(3, 9), new Border(true, false, 1));

        Zones[8, 9].FloorType = FloorType.Path;
        Zones[9, 9].FloorType = FloorType.Path;
        Zones[10, 9].FloorType = FloorType.Path;
        Zones[10, 10].FloorType = FloorType.Path;
        Zones[10, 11].FloorType = FloorType.Path;

        for (int i = 0; i < DormRooms.Count; i++)
        {
            BorderController.SetEastBorder(DormRooms[i].Position, new Border(true, true, 1));
            BorderController.SetNorthBorder(DormRooms[i].Position, new Border(true, true, 1));
            BorderController.SetEastBorder(
                DormRooms[i].Position + new Vec2(-1, 0),
                new Border(true, true, 1)
            );
            BorderController.SetNorthBorder(
                DormRooms[i].Position + new Vec2(0, -1),
                new Border(true, true, 1)
            );
            switch (DormRooms[i].DoorSide)
            {
                case Facing.North:
                    BorderController.SetNorthBorder(
                        DormRooms[i].Position,
                        new Border(true, false, 1)
                    );
                    break;
                case Facing.East:
                    BorderController.SetEastBorder(
                        DormRooms[i].Position,
                        new Border(true, false, 1)
                    );
                    break;
                case Facing.West:
                    BorderController.SetEastBorder(
                        DormRooms[i].Position + new Vec2(-1, 0),
                        new Border(true, false, 1)
                    );
                    break;
                case Facing.South:
                    BorderController.SetNorthBorder(
                        DormRooms[i].Position + new Vec2(0, -1),
                        new Border(true, false, 1)
                    );
                    break;
            }
            if (People.Count > i)
            {
                ClaimDormRoom(People[i], DormRooms[i]);
            }
            else
                Zones[DormRooms[i].Position.x, DormRooms[i].Position.y] = new Zone(
                    $"Vacant Room",
                    ZoneType.DormRoom,
                    new List<ObjectType>() { ObjectType.Bed },
                    new List<Person>()
                );
        }
        State.GameManager?.RefreshCameraSettings();
        NextArrivalTime =
            Settings.NewArrivalAverageTime / 2 + Rand.Next(Settings.NewArrivalAverageTime);

        //FixZones();
        //UseCustomLayout();
    }

    void UseCustomLayout(World tempWorld)
    {
        Zones = tempWorld.Zones;
        BorderController = tempWorld.BorderController;
        DormRooms = tempWorld.DormRooms;
        DormRooms = DormRooms.Distinct().ToList();
        State.GameManager?.RefreshCameraSettings();

        List<DormRoom> roomList;

        if (Settings.RandomizeDormPlacement)
        {
            roomList = DormRooms.ToList();
            roomList.Shuffle();
        }
        else
            roomList = DormRooms.ToList();

        for (int i = 0; i < roomList.Count; i++)
        {
            if (People.Count > i)
            {
                ClaimDormRoom(People[i], roomList[i]);
            }
            else
                Zones[roomList[i].Position.x, roomList[i].Position.y] = new Zone(
                    $"Vacant Room",
                    ZoneType.DormRoom,
                    new List<ObjectType>() { ObjectType.Bed },
                    new List<Person>()
                );
        }
    }

    /// <summary>
    /// An alternate version that includes the player even if they're a ghost.
    /// </summary>
    internal List<Person> GetAllPeople(bool includePrey = true)
    {
        if (PlayerIsObserver() == false)
            return GetPeople(includePrey);
        if (includePrey == false)
            return People
                .Where(s => s.BeingEaten == false)
                .Append(State.World.ControlledPerson)
                .ToList();
        else
            return People.Append(State.World.ControlledPerson).ToList();
    }

    internal List<Person> GetPeople(bool includePrey)
    {
        if (includePrey == false)
            return People.Where(s => s.BeingEaten == false).ToList();
        else
            return People;
    }

    internal void RemovePerson(Person person)
    {
        if (State.World.ControlledPerson == person)
        {
            People.Remove(person);
            AttachPred();
        }
        else if (
            person.AlwaysReform == false
            && (Settings.NursesActive == false || GetZone(person.MyRoom) == null)
        )
        {
            person.Gone = true;
            People.Remove(person);
            ClearRoom(person, true);
            if (VisionAttachedTo == person)
            {
                AttachPred();
            }
        }
        else
        {
            Resurrect(person);
        }

        void AttachPred()
        {
            var pred = person.FindMyPredator();
            if (pred != null)
                VisionAttachedTo = pred;
        }
    }

    /// <summary>
    /// Fixes Zones from before V7 to V7.
    /// </summary>
    internal void FixZones()
    {
        foreach (Zone zone in Zones)
        {
            if (zone == null)
                continue;
            if (zone.Objects.Contains(ObjectType.Bed))
                zone.Type = ZoneType.DormRoom;
            else if (zone.Objects.Contains(ObjectType.Bathroom))
                zone.Type = ZoneType.Bathroom;
            else if (zone.Objects.Contains(ObjectType.Shower))
                zone.Type = ZoneType.Shower;
            else if (zone.Objects.Contains(ObjectType.NurseOffice))
                zone.Type = ZoneType.NurseOffice;
            else if (zone.Objects.Contains(ObjectType.Food))
                zone.Type = ZoneType.Cafeteria;
            else if (zone.FloorType == FloorType.Wood)
                zone.Type = ZoneType.IndoorHallway;
            else if (zone.FloorType == FloorType.Grass)
                zone.Type = ZoneType.Grass;
            else if (zone.FloorType == FloorType.Path)
                zone.Type = ZoneType.OutdoorPath;
        }
    }

    internal void ClearRoom(Person person, bool leaveName)
    {
        var roomZone = GetZone(person.MyRoom);
        if (roomZone != null)
        {
            roomZone.AllowedPeople.Clear();
            roomZone.Objects.Remove(ObjectType.Clothes);
            if (leaveName)
                roomZone.Name = $"{person.FirstName}'s former room";
            else
                roomZone.Name = $"Vacant Room";
        }
    }

    internal void Resurrect(Person person)
    {
        Vec2 respawnPos = RandomSquareOfType(ObjectType.NurseOffice);
        if (respawnPos.x == -1)
            respawnPos = person.MyRoom;
        person.Health = Constants.ResurrectHealth;
        person.Position = respawnPos;
        person.BeingEaten = false;
        person.EatenDuringSex = false;
        person.VoreTracking.Clear();
        if (People.Contains(person) == false)
            People.Add(person);
        person.EndStreamingActions();
        person.AI.ClearTasks();
        person.MiscStats.TurnsAlive = 0;
    }

    internal Vec2 RandomSquareOfType(ObjectType type)
    {
        List<Vec2> tiles = new List<Vec2>();
        for (int x = 0; x <= State.World.Zones.GetUpperBound(0); x++)
        {
            for (int y = 0; y <= State.World.Zones.GetUpperBound(1); y++)
            {
                if (State.World.Zones[x, y] != null)
                {
                    if (State.World.Zones[x, y].Objects.Contains(type))
                    {
                        tiles.Add(new Vec2(x, y));
                    }
                }
            }
        }
        if (tiles.Any() == false)
        {
            return new Vec2(-1, -1);
        }
        return tiles[Rand.Next(tiles.Count)];
    }

    internal Vec2 RandomOwnedSquare(Person person)
    {
        List<Vec2> tiles = new List<Vec2>();
        for (int x = 0; x <= State.World.Zones.GetUpperBound(0); x++)
        {
            for (int y = 0; y <= State.World.Zones.GetUpperBound(1); y++)
            {
                if (State.World.Zones[x, y] != null)
                {
                    if (State.World.Zones[x, y].AllowedPeople.Contains(person))
                    {
                        tiles.Add(new Vec2(x, y));
                    }
                }
            }
        }
        if (tiles.Any() == false)
        {
            return new Vec2(-1, -1);
        }
        return tiles[Rand.Next(tiles.Count)];
    }

    internal void CheckNextTurn()
    {
        if (State.GameManager.MenuScreen.gameObject.activeSelf)
            RemainingSkippedTurns = 0;

        if (
            ClearedNextTurn == false
            && CurrentTurn == ControlledPerson
            && AutoDestination != default
            && WaitingQuestion == false
        )
        {
            ClearedNextTurn = true;
            Repeat = false;
            ProcessNextTurn();
        }

        while (ClearedNextTurn)
        {
            ClearedNextTurn = false;
            if (WaitingQuestion)
                return;
            RunWaitingCallbacks();
            State.GameManager.ContinueActionPanel.SetActive(false);

            if (People.Any() == false)
                return;

            if (Repeat == false)
            {
                State.GameManager.TurnsSkipped++;
                int next = People.IndexOf(CurrentTurn) + 1;
                if (next >= People.Count)
                {
                    if (PlayerAlreadyInObserver && PlayerIsObserver())
                    {
                        CurrentTurn = State.World.ControlledPerson;
                        NextCycle();
                        LastPlayerTurnWasAI = true;
                        State.GameManager.DisplayInfo();
                        if (TurnsToAutoSave < 0)
                        {
                            TurnsToAutoSave = 40;
                            State.Save(Path.Combine(State.SaveDirectory, "AutoSave.sav"));
                        }
                        return;
                    }
                    next = 0;
                    NextCycle();
                }
                CurrentTurn = People[next];

                if (CurrentTurn == ControlledPerson && CurrentTurn.Stunned)
                {
                    SelfActionList.List[SelfActionType.Stunned].OnDo(CurrentTurn);
                    CurrentTurn.Stunned = false;
                    NextTurn();
                    return;
                }

                if (CurrentTurn == ControlledPerson && AutoDestination != default)
                {
                    return; //Force to to restart every cycle so it only skips one turn at a time.
                }

                if (People.Contains(ControlledPerson) == false)
                {
                    if (PlayerAlreadyInObserver == false)
                    {
                        PlayerAlreadyInObserver = true;
                        RemainingSkippedTurns = 0;
                    }
                    if (Config.AutoCenterCamera && VisionAttachedTo != null)
                    {
                        State.GameManager.CenterCameraOnTile(VisionAttachedTo.Position);
                    }
                }
                else
                    PlayerAlreadyInObserver = false;

                CheckNudity();
            }
            else
                Repeat = false;

            ProcessNextTurn();
        }
    }

    internal void ProcessNextTurn()
    {
        if (Settings.WorldFrozen)
        {
            CurrentTurn = ControlledPerson;
            UpdatePlayerUI();
            LastPlayerTurnWasAI = false;
            return;
        }

        if (CurrentTurn.Stunned)
        {
            SelfActionList.List[SelfActionType.Stunned].OnDo(CurrentTurn);
            CurrentTurn.Stunned = false;
            NextTurn();
            return;
        }

        if (CurrentTurn == ControlledPerson && AutoDestination != default)
        {
            if (CurrentTurn.AI.TryMove(AutoDestination))
            {
                NextTurn();
                return;
            }
            else
            {
                AutoDestination = default;
                NextTurn(true);
                return;
            }
        }

        if (CurrentTurn != ControlledPerson || SingleSkip)
        {
            if (SingleSkip)
            {
                SingleSkip = false;
                LastPlayerTurnWasAI = true;
            }
            CurrentTurn.AI.Execute();
            //State.GameManager.DisplayInfo();
            NextTurn();
        }
        else
        {
            if (SoughtPerson != null && SoughtPerson.Position == OldLocation)
            {
                State.World.ControlledPerson.AI.TryMove(OldLocation);
            }
            SoughtPerson = null;
            if (TurnsToAutoSave < 0)
            {
                TurnsToAutoSave = 40;
                State.Save(Path.Combine(State.SaveDirectory, "AutoSave.sav"));
            }
            if (PlayerIsObserver())
            {
                UpdatePlayerUI();
                return;
            }
            if (People.Count == 1 && AskedToStay == false)
            {
                var box = State.GameManager.CreateDialogBox();
                AskedToStay = true;
                box.SetData(
                    State.GameManager.MenuScreen.MainMenu,
                    "Return to Main Menu",
                    "Stay in game",
                    "You're the only person left on campus, you sort of win (though you'll have a lonely campus life).  Do you want to return to the main menu or stay in game?"
                );
            }

            UpdatePlayerUI();
            LastPlayerTurnWasAI = false;
        }
    }

    internal void NextTurn(bool repeat = false, bool singleSkip = false)
    {
        Repeat = repeat;
        SingleSkip = singleSkip;
        ClearedNextTurn = true;
    }

    internal void RunWaitingCallbacks()
    {
        for (int i = 0; i < 10; i++) //Done this way for safety incase an end of turn callback manages to create another.
        {
            if (EndOfTurnCallBacks.Any())
                EndOfTurnCallBacks[0].Invoke();
            else
                break;
            EndOfTurnCallBacks.RemoveAt(0);
        }
        EndOfTurnCallBacks.Clear();
    }

    internal void NextTurnTesting(bool end = false)
    {
        RemainingSkippedTurns = 4000;

        bool endNext = false;

        int next = People.IndexOf(CurrentTurn) + 1;
        if (next >= People.Count)
        {
            next = 0;
            NextCycle();
            endNext = true;
        }

        CurrentTurn = People[next];
        CheckNudity();

        if (end)
            return;

        CurrentTurn.AI.Execute();
        if (end == false)
            NextTurnTesting(endNext);
    }

    internal bool PlayerIsObserver()
    {
        return !State.World.GetPeople(true).Contains(ControlledPerson);
    }

    internal void WorldChanged()
    {
        foreach (var person in GetAllPeople(true))
        {
            person.AI.ClearPath();
        }

        HasCafeteria = false;
        HasGym = false;
        HasLibrary = false;

        foreach (var zone in Zones)
        {
            if (zone == null)
                continue;
            if (zone.Type == ZoneType.Cafeteria)
                HasCafeteria = true;
            else if (zone.Type == ZoneType.Gym)
                HasGym = true;
            else if (zone.Type == ZoneType.Library)
                HasLibrary = true;
            else if (zone.Type == ZoneType.NurseOffice)
                HasNurse = true;
        }
    }

    internal void UpdatePlayerUI()
    {
        if (ControlledPerson == null)
        {
            if (People == null || People.Any() == false)
                return;
            ControlledPerson = People[0];
        }

        State.GameManager.ContinueActionPanel.SetActive(false);
        State.GameManager.DisplayInfo();

        if (Config.AutoCenterCamera)
        {
            State.GameManager.CenterCameraOnTile(ControlledPerson.Position);
        }

        if (PlayerIsObserver())
        {
            return;
        }

        if (ControlledPerson.BeingEaten)
        {
            foreach (Button button in State.GameManager.TurnButtons)
            {
                button.gameObject.SetActive(false);
            }
            var pred = ControlledPerson.FindMyPredator();
            if (pred == null) //This was a guess as to what the exception in this function might be from.
            {
                ControlledPerson.BeingEaten = false;
                ControlledPerson.EndStreamingActions();
                UpdatePlayerUI();
                return;
            }
            State.GameManager.ContinueActionPanel.SetActive(true);
            State.GameManager.ContinueActionButton.onClick.RemoveAllListeners();
            State.GameManager.ContinueActionText.text =
                $"You're being eaten by {pred.FirstName} - Select them to choose options";
            State.GameManager.ContinueActionButton.interactable = false;
            if (
                State.GameManager.ClickedPerson == null
                || State.GameManager.ClickedPerson.BeingEaten == false
                || State.GameManager.ClickedPerson.Dead
                || State.GameManager.ClickedPerson.FindMyPredator()
                    != ControlledPerson.FindMyPredator()
            )
            {
                State.GameManager.ClickedPerson = pred;
            }

            if (
                ControlledPerson.VoreController.HasPrey(VoreLocation.Any)
                && ControlledPerson.VoreController.Swallowing(VoreLocation.Any)
            )
            {
                AskAboutSwallowing();
                return;
            }
            if (
                ControlledPerson.VoreController.HasPrey(VoreLocation.Any)
                && Settings.WorldFrozen == false
            )
            {
                ControlledPerson.VoreController.AdvanceStages();
            }

            State.GameManager.DisplayInfo();
            return;
        }
        foreach (Button button in State.GameManager.TurnButtons)
        {
            button.gameObject.SetActive(true);
        }
        if (
            ControlledPerson.VoreController.HasPrey(VoreLocation.Any)
            && ControlledPerson.VoreController.Swallowing(VoreLocation.Any)
        )
        {
            AskAboutSwallowing();
            return;
        }

        if (
            ControlledPerson.VoreController.HasPrey(VoreLocation.Any)
            && Settings.WorldFrozen == false
        )
        {
            ControlledPerson.VoreController.AdvanceStages();
        }

        if (
            ControlledPerson.StreamingAction != InteractionType.None
            && InteractionList.List[ControlledPerson.StreamingAction].Class
                == ClassType.VoreConsuming
        )
        {
            ControlledPerson.EndStreamingActions(); //Shouldn't have the option to continue the 'eating' option after swallowing.
        }

        //Fail out early so it will do something else if possible.
        if (
            ControlledPerson.AI.FollowingPersonExternal() != null
            && ControlledPerson.Position.GetNumberOfMovesDistance(
                ControlledPerson.AI.FollowingPersonExternal().Position
            ) > 1
        )
            ControlledPerson.AI.FollowPerson(null);

        // If the player controlled char reaches someone else's bedroom during a follow, end the follow
        if (
            ControlledPerson.AI.FollowingPersonExternal() != null
            && ControlledPerson.ZoneContainsBed()
        )
            ControlledPerson.AI.FollowPerson(null);

        if (ControlledPerson.AI.FollowingPersonExternal() != null)
        {
            if (RemainingSkippedTurns > 0)
            {
                return;
            }
            State.GameManager.ContinueActionPanel.SetActive(true);
            State.GameManager.ContinueActionButton.onClick.RemoveAllListeners();
            State.GameManager.ContinueActionText.text =
                $"Continue to follow {ControlledPerson.AI.FollowingPersonExternal().FirstName} (option will vanish if you get more than 1 tile away)";

            State.GameManager.ContinueActionButton.interactable = true;
            State.GameManager.ContinueActionButton.onClick.AddListener(() =>
            {
                if (ControlledPerson.AI.TryFollowExternal())
                    NextTurn();
                else if (
                    ControlledPerson.Position
                    == ControlledPerson.AI.FollowingPersonExternal().Position
                )
                    NextTurn();
            });
        }
        else if (ControlledPerson.StreamingAction != InteractionType.None)
        {
            var action = InteractionList.List[ControlledPerson.StreamingAction];
            if (
                ControlledPerson.StreamingTarget == null
                || action.Range
                    < ControlledPerson.Position.GetNumberOfMovesDistance(
                        ControlledPerson.StreamingTarget.Position
                    )
            )
            {
                ControlledPerson.EndStreamingActions();
                return;
            }
            State.GameManager.ContinueActionPanel.SetActive(true);
            State.GameManager.ContinueActionButton.onClick.RemoveAllListeners();

            if (ControlledPerson.StreamingAction == InteractionType.StartSex)
            {
                State.GameManager.ContinueActionText.text =
                    $"You're having sex with {ControlledPerson.StreamingTarget.FirstName} - Select them to choose options";
                State.GameManager.ContinueActionButton.interactable = false;
                foreach (Button button in State.GameManager.TurnButtons)
                {
                    button.gameObject.SetActive(false);
                }
            }
            else
            {
                string desc = InteractionList.List[
                    ControlledPerson.StreamingAction
                ].StreamingDescription;
                if (desc != "")
                    State.GameManager.ContinueActionText.text =
                        $"You're in the middle of {desc} with {ControlledPerson.StreamingTarget.FirstName}, continue?";
                else
                    State.GameManager.ContinueActionText.text =
                        $"You're in the middle of {action.Name} with {ControlledPerson.StreamingTarget.FirstName}, continue?";
                State.GameManager.ContinueActionButton.interactable = true;
                State.GameManager.ContinueActionButton.onClick.AddListener(() =>
                {
                    action.OnSucceed(ControlledPerson, ControlledPerson.StreamingTarget, true);
                    NextTurn();
                });
            }
        }
        else if (ControlledPerson.StreamingSelfAction != SelfActionType.None)
        {
            var action = SelfActionList.List[ControlledPerson.StreamingSelfAction];
            if (
                ControlledPerson.StreamedTurns >= action.MaxStreamLength
                || action.AppearConditional(ControlledPerson) == false
            )
            {
                ControlledPerson.EndStreamingActions();
            }
            else
            {
                State.GameManager.ContinueActionPanel.SetActive(true);
                string desc = SelfActionList.List[
                    ControlledPerson.StreamingSelfAction
                ].StreamingDescription;
                if (desc != "")
                    State.GameManager.ContinueActionText.text =
                        $"You're in the middle of {desc}, continue?";
                else
                    State.GameManager.ContinueActionText.text =
                        $"You're in the middle of {action.Name}, continue?";

                State.GameManager.ContinueActionButton.interactable = true;

                State.GameManager.ContinueActionButton.onClick.RemoveAllListeners();
                State.GameManager.ContinueActionButton.onClick.AddListener(() =>
                {
                    action.OnDo(ControlledPerson, true);
                    NextTurn();
                });
            }
        }
        if (RemainingSkippedTurns > 0)
            State.GameManager.ContinueActionButton.interactable = false;
    }

    private void AskAboutSwallowing()
    {
        if (RemainingSkippedTurns <= 0)
        {
            State.GameManager.ContinueActionPanel.SetActive(true);
            State.GameManager.ContinueActionButton.onClick.RemoveAllListeners();
            State.GameManager.ContinueActionText.text =
                $"You're currently consuming {ControlledPerson.VoreController.CurrentSwallow(VoreLocation.Any).Target.FirstName} - Selecting any other action will free them.";
            State.GameManager.ContinueActionButton.interactable = true;
            State.GameManager.ContinueActionButton.onClick.AddListener(() =>
            {
                ControlledPerson.VoreController.AdvanceStages();
                NextTurn();
            });
            //var box = State.GameManager.CreateDialogBox();
            //box.SetData(
            //    () =>
            //    {
            //        ControlledPerson.VoreController.AdvanceStages();
            //        NextTurn();
            //    },
            //    "Continue Swallowing",
            //    "Spit them up",
            //    $"You're currently swallowing {ControlledPerson.VoreController.CurrentSwallow.Target.FirstName}, keep swallowing them or spit them back up so that you can move again",
            //     () =>
            //     {
            //         ControlledPerson.VoreController.CurrentSwallow.FreePrey(true);
            //         ControlledPerson.VoreController.AdvanceStages();
            //         NextTurn();
            //     }
            //    );
        }
        else
        {
            ControlledPerson.VoreController.AdvanceStages();
            NextTurn();
        }
    }

    private void CheckNudity()
    {
        int spotted = 0;
        foreach (Person person in People)
        {
            if (person == CurrentTurn)
                continue;
            if (person.BeingEaten || CurrentTurn.BeingEaten)
                continue;
            if (
                person.ClothingStatus == ClothingStatus.Nude
                && CurrentTurn.Romance.DesiresGender(person.GenderType)
            )
            {
                if (LOS.Check(person.Position, CurrentTurn.Position))
                {
                    spotted++;
                }
            }
        }
        if (spotted > 0)
        {
            SelfActionList.List[SelfActionType.TurnedOnByNudity].OnDo(CurrentTurn);
            CurrentTurn.Needs.Horniness += .003f * (1f + spotted) / 2;
        }
    }

    void NextCycle()
    {
        if (Settings.WorldFrozen)
            return;

        foreach (var person in People)
        {
            person.Update();
        }
        if (NextArrivalTime > Settings.NewArrivalAverageTime * 1.5f)
        {
            NextArrivalTime =
                Settings.NewArrivalAverageTime / 2 + Rand.Next(Settings.NewArrivalAverageTime);
        }
        NextArrivalTime--;
        if (
            Settings.NewArrivals
            && NextArrivalTime <= 0
            && GetPeople(true).Count < Settings.MaxPopulation
        )
        {
            TryAddPerson();
        }
        Turn++;
        TurnsToAutoSave--;
        if (RemainingSkippedTurns > 0)
        {
            RemainingSkippedTurns--;
        }
    }

    internal bool ManualAddPerson(Person person)
    {
        if (person == null)
            return false;
        List<DormRoom> openRooms = new List<DormRoom>();
        foreach (var dorm in DormRooms)
        {
            var zone = GetZone(dorm.Position);
            if (zone?.AllowedPeople == null)
                continue;
            if (zone.AllowedPeople.Any() == false)
                openRooms.Add(dorm);
        }
        if (openRooms.Any())
        {
            if (Settings.RandomizeDormPlacement)
                openRooms.Shuffle();
            var room = openRooms[Rand.Next(openRooms.Count)];
            person.Position = room.Position;

            ClaimDormRoom(person, room);
            People.Add(person);
        }
        else
            return false;
        if (person.Personality == null)
            person.Personality = new Personality();
        person.ID = NextID;
        NextID++;
        person.MiscStats.TurnAdded = Turn;
        person.GymSatisfaction = Rand.Next(80, 120);
        person.LibrarySatisfaction = Rand.Next(80, 120);
        GetFreeLabelFor(person);
        return true;
    }

    internal void ClaimDormRoomCurrentlyIn(Person person)
    {
        foreach (var room in DormRooms)
        {
            if (person.Position == room.Position)
            {
                ClaimDormRoom(person, room);
                return;
            }
        }
        Debug.Log("Claim Room failed!");
    }

    private void ClaimDormRoom(Person person, DormRoom room)
    {
        Zones[room.Position.x, room.Position.y] = new Zone(
            $"{person.FirstName}'s Room",
            ZoneType.DormRoom,
            new List<ObjectType>() { ObjectType.Bed, ObjectType.Clothes },
            new List<Person>() { person }
        );
        person.MyRoom = room.Position;
    }

    private void TryAddPerson()
    {
        NextArrivalTime =
            Settings.NewArrivalAverageTime / 2 + Rand.Next(Settings.NewArrivalAverageTime);
        List<DormRoom> openRooms = new List<DormRoom>();
        foreach (var dorm in DormRooms)
        {
            var zone = GetZone(dorm.Position);
            if (zone?.AllowedPeople == null)
                continue;
            if (zone.AllowedPeople.Any() == false)
                openRooms.Add(dorm);
        }
        if (openRooms.Any())
        {
            if (Settings.RandomizeDormPlacement)
                openRooms.Shuffle();
            var room = openRooms[Rand.Next(openRooms.Count)];
            Person newPerson;
            if (State.World.Settings.NewType == LoadCharacterType.Random)
            {
                int gender = GenderList.GetRandomGender();
                string name = "Nameless";
                string label = "";
                for (int i = 0; i < 500; i++)
                {
                    name = GenderList.List[gender].FeminineName
                        ? NameGenerator.GetRandomFemaleName()
                        : NameGenerator.GetRandomMaleName();
                    if (GetAllPeople().Where(s => s.FirstName == name).Any())
                        continue;
                    break;
                }
                for (int i = 0; i < 100; i++)
                {
                    label = ((char)(65 + i)).ToString();
                    if (GetAllPeople().Where(s => s.Label == label).Any())
                        continue;
                    break;
                }

                newPerson = new Person(
                    name,
                    NameGenerator.GetRandomLastName(),
                    gender,
                    (Orientation)GenderList.GetRandomOrientation(gender),
                    true,
                    room.Position,
                    RaceWeights.GetRandomRace()
                );
                newPerson.Label = label;
            }
            else if (State.World.Settings.NewType == LoadCharacterType.Templates)
            {
                var obj = State.GameManager.StartScreen.GetPersonFromTemplates(
                    GenderList,
                    RaceWeights
                );
                if (obj == null)
                    return;
                newPerson = StartScreen.CreatePerson(obj);
                if (newPerson == null)
                    return;
                for (int i = 0; i < 500; i++)
                {
                    newPerson.FirstName = GenderList.List[newPerson.Gender].FeminineName
                        ? NameGenerator.GetRandomFemaleName()
                        : NameGenerator.GetRandomMaleName();
                    if (GetAllPeople().Where(s => s.FirstName == newPerson.FirstName).Any())
                        continue;
                    break;
                }
                for (int i = 0; i < 100; i++)
                {
                    newPerson.Label = ((char)(65 + i)).ToString();
                    if (GetAllPeople().Where(s => s.Label == newPerson.Label).Any())
                        continue;
                    break;
                }
                if (newPerson.PartList == null)
                    newPerson.PartList = Create.Random(
                        newPerson.GenderType,
                        RaceManager.GetRace(newPerson.Race)
                    );
                newPerson.LastName = NameGenerator.GetRandomLastName();
                GameObject.Destroy(obj.gameObject);
                newPerson.Position = room.Position;
            }
            else
            {
                newPerson = Utility.SerialClone(
                    State.SavedPersonController.GetRandomCharacterNotInList(GetAllPeople(true))
                );
                if (newPerson == null)
                    return;
                GetFreeLabelFor(newPerson);
                newPerson.Position = room.Position;
            }
            if (newPerson.Personality == null)
                newPerson.Personality = new Personality();
            newPerson.ID = NextID;
            NextID++;
            newPerson.MiscStats.TurnAdded = Turn;
            newPerson.GymSatisfaction = Rand.Next(80, 120);
            newPerson.LibrarySatisfaction = Rand.Next(80, 120);
            ClaimDormRoom(newPerson, room);
            People.Add(newPerson);
        }
    }

    internal void GetFreeLabelFor(Person newPerson)
    {
        if (string.IsNullOrWhiteSpace(newPerson.Label))
        {
            string label = ".";
            for (int i = 0; i < 62; i++)
            {
                label = ((char)(65 + i)).ToString();
                if (GetAllPeople().Where(s => s.Label == label).Any())
                    continue;
                break;
            }
            newPerson.Label = label;
        }
    }

    internal void RelabelPeople()
    {
        int j = 0;
        for (int i = 0; i < People.Count; i++)
        {
            People[i].ID = NextID;
            NextID++;
            if (string.IsNullOrWhiteSpace(People[i].Label) == false)
            {
                j--;
                continue;
            }

            for (int k = 0; k < People.Count; k++)
            {
                if (People[k].Label == $"{(char)(65 + i + j)}")
                {
                    j++;
                    k = -1;
                    continue;
                }
            }
            People[i].Label = ((char)(65 + i + j)).ToString();
        }
    }

    void CreateSwatch(
        string name,
        int minX,
        int maxX,
        int minY,
        int maxY,
        ZoneType type,
        List<ObjectType> objects
    )
    {
        for (int x = minX; x <= maxX; x++)
        {
            for (int y = minY; y <= maxY; y++)
            {
                Zones[x, y] = new Zone(name, type, objects);
            }
        }
    }

    internal void AskPlayer(Person actor, InteractionBase interaction, bool endsTurn = true)
    {
        if (
            TestingMode || (RemainingSkippedTurns > 0 && interaction.Class != ClassType.VoreAskThem)
        )
        {
            interaction.RunCheck(actor, ControlledPerson, true);
            if (endsTurn)
                NextTurn();
            return;
        }
        WaitingQuestion = true;
        string question = MessageManager.GetPopUpText(
            new Interaction(actor, ControlledPerson, true, interaction.Type)
        );

        if (
            ControlledPerson.VoreController.CurrentSwallow(VoreLocation.Any) != null
            && interaction.Streaming
        )
        {
            if (question == "")
                question =
                    $"{actor.GetFullName()} {interaction.AsksPlayerDescription}\n(You're currently consuming someone, agreeing would spit your prey back up.)";
            else
                question =
                    $"...{actor.GetFullName()}...\n{question}\n(You're currently consuming someone, agreeing would spit your prey back up.)";
        }
        else if (ControlledPerson.ActiveSex != null && interaction.Streaming)
        {
            if (question == "")
                question =
                    $"{actor.GetFullName()} {interaction.AsksPlayerDescription}\n(You're currently having sex, agreeing would end that.)";
            else
                question =
                    $"...{actor.GetFullName()}...\n{question}\n(You're currently having sex, agreeing would end that.)";
        }
        else
        {
            if (question == "")
                question = $"{actor.GetFullName()} {interaction.AsksPlayerDescription}.";
            else
                question = $"...{actor.GetFullName()}...\n{question}";
        }
        var box = State.GameManager.CreateThreeDialogBox();
        box.SetData(
            () =>
            {
                interaction.OnSucceed(actor, ControlledPerson);
                WaitingQuestion = false;
                if (endsTurn)
                    NextTurn();
                else
                    RunWaitingCallbacks();
            },
            "Agree",
            "Refuse",
            "AI decide",
            question,
            () =>
            {
                interaction.OnFail(actor, ControlledPerson, interruptOnFail: false);
                WaitingQuestion = false;
                if (endsTurn)
                    NextTurn();
                else
                    RunWaitingCallbacks();
            },
            () =>
            {
                interaction.RunCheck(actor, ControlledPerson, true, interruptOnFail: false);
                WaitingQuestion = false;
                if (endsTurn)
                    NextTurn();
                else
                    RunWaitingCallbacks();
            }
        );

        RemainingSkippedTurns = 0;
    }

    internal void AskPlayerWillingPrey(Person actor, InteractionBase interaction)
    {
        if (RemainingSkippedTurns > 0)
            RemainingSkippedTurns = 0;
        WaitingQuestion = true;
        State.World.ControlledPerson.EndStreamingActions();
        var box = State.GameManager.CreateThreeDialogBox();
        string voreType = "eat";
        string digestion = " and hold ";

        switch (interaction.Type)
        {
            case InteractionType.OralVore:
            case InteractionType.KissVore:
                voreType = "eat";
                if (actor.VoreController.PartCurrentlyDigests(VoreLocation.Stomach))
                    digestion = " and digest ";
                break;
            case InteractionType.CockVore:
            case InteractionType.SexCockVore:
                voreType = "cock vore";
                if (actor.VoreController.PartCurrentlyDigests(VoreLocation.Balls))
                    digestion = " and melt ";
                break;
            case InteractionType.Unbirth:
            case InteractionType.SexUnbirth:
                voreType = "unbirth";
                if (actor.VoreController.PartCurrentlyDigests(VoreLocation.Womb))
                    digestion = " and melt ";
                break;
            case InteractionType.AnalVore:
            case InteractionType.SexAnalVore:
                voreType = "anal vore";
                if (actor.VoreController.PartCurrentlyDigests(VoreLocation.Bowels))
                    digestion = " and digest ";
                break;
        }
        if (Config.DebugViewGoals == false)
            digestion = " ";
        box.SetData(
            () =>
            {
                float OldWillingness = ControlledPerson.Personality.PreyWillingness;
                ControlledPerson.Personality.PreyWillingness = 2;
                interaction.OnSucceed(actor, ControlledPerson);
                ControlledPerson.Personality.PreyWillingness = OldWillingness;
                WaitingQuestion = false;
                NextTurn();
            },
            "Willing",
            "Unwilling",
            "AI Decide",
            $"{actor.GetFullName()} wants to {voreType}{digestion}you, do you go willingly or fight back?",
            () =>
            {
                float OldWillingness = ControlledPerson.Personality.PreyWillingness;
                if (
                    ControlledPerson.Personality.PreyWillingness
                    >= State.World.Settings.WillingThreshold
                )
                    ControlledPerson.Personality.PreyWillingness =
                        State.World.Settings.WillingThreshold - .005f;
                interaction.RunCheck(actor, ControlledPerson, true);
                ControlledPerson.Personality.PreyWillingness = OldWillingness;
                WaitingQuestion = false;
                NextTurn();
            },
            () =>
            {
                interaction.RunCheck(actor, ControlledPerson, true);
                WaitingQuestion = false;
                NextTurn();
            }
        );
    }

    internal void AskPlayerWillingMagic(Person actor, InteractionBase interaction)
    {
        if (RemainingSkippedTurns > 0)
            RemainingSkippedTurns = 0;
        WaitingQuestion = true;
        State.World.ControlledPerson.EndStreamingActions();
        var box = State.GameManager.CreateThreeDialogBox();

        string spelltype = interaction.Name.ToLowerInvariant();
        if (spelltype.Contains("cast"))
            spelltype = spelltype.Replace("cast", "");

        box.SetData(
            () =>
            {
                interaction.OnSucceed(actor, ControlledPerson);
                WaitingQuestion = false;
                NextTurn();
            },
            "Allow",
            "Resist",
            "AI Decide",
            $"{actor.GetFullName()} casts a {spelltype} spell on you, do you resist the effect?",
            () =>
            {
                interaction.RunCheck(actor, ControlledPerson, true);
                WaitingQuestion = false;
                NextTurn();
            },
            () =>
            {
                interaction.RunCheck(actor, ControlledPerson, true);
                WaitingQuestion = false;
                NextTurn();
            }
        );
    }

    void CreateSwatch(
        string name,
        int minX,
        int maxX,
        int minY,
        int maxY,
        ZoneType type = ZoneType.Grass
    )
    {
        for (int x = minX; x <= maxX; x++)
        {
            for (int y = minY; y <= maxY; y++)
            {
                Zones[x, y] = new Zone(name, type);
            }
        }
    }

    internal bool TileExists(int x, int y)
    {
        if (x >= 0 && x <= Zones.GetUpperBound(0))
        {
            if (y >= 0 && y <= Zones.GetUpperBound(1))
            {
                return Zones[x, y] != null;
            }
        }
        return false;
    }

    internal bool CanMove(Person person, int changeX, int changeY, bool forceEntry = false)
    {
        if (person == null)
            return false;
        if (TileExists(person.Position.x + changeX, person.Position.y + changeY) == false)
            return false;
        if (person.Magic.Duration_Freeze > 0)
            return false;
        if (forceEntry)
            return true;
        if (
            State.World.BorderController
                .GetBorder(
                    person.Position,
                    new Vec2(person.Position.x + changeX, person.Position.y + changeY)
                )
                .BlocksTravel
        )
            return false;
        return Zones[person.Position.x + changeX, person.Position.y + changeY].Accepts(person);
    }

    internal bool Move(Person person, int changeX, int changeY, bool forceEntry = false)
    {
        if (CanMove(person, changeX, changeY, forceEntry))
        {
            person.Position.x += changeX;
            person.Position.y += changeY;
            foreach (var progress in person.VoreController.GetAllSubPrey())
            {
                progress.Target.Position = person.Position;
            }
            person.EndStreamingActions();
            person.ClothesInTile = ClothingStatus.Nude;
            return true;
        }
        return false;
    }
}
