﻿using UnityEngine;
using UnityEngine.Tilemaps;
using System.Collections.Generic;
using System;
using System.IO;

public enum FloorType
{
    Grass,
    Path,
    Wood,
    Concrete,
    Tile
}

public class TileManager : MonoBehaviour
{
    public Tilemap WorldMap;
    public Tilemap ObjectMap;

    public Tile Sample;
    public Sprite DefaultSprite;

    public Tilemap BackMap;

    public Tile Dark;

    Tile ToiletTile;
    Tile BedTile;
    Tile NurseBedTile;
    Tile Books;
    Tile GymObject;

    Tile VoreDisplay;
    Tile SexDisplay;
    Tile DisposalDisplay;

    internal Dictionary<ZoneType, Tile> TileDict;

    private void Awake()
    {
        Dictionary<string, float> sizes = new Dictionary<string, float>();
        if (File.Exists(Path.Combine(Application.streamingAssetsPath, "Tiles", "sizes.txt")))
        {
            var lines = File.ReadLines(
                Path.Combine(Application.streamingAssetsPath, "Tiles", "sizes.txt")
            );
            foreach (var line in lines)
            {
                var split = line.Split(',');
                split[0] = split[0].Trim();
                if (float.TryParse(split[1], out float result))
                {
                    sizes[split[0]] = 1 / result;
                }
            }
        }

        TileDict = new Dictionary<ZoneType, Tile>();
        foreach (ZoneType type in (ZoneType[])Enum.GetValues(typeof(ZoneType)))
        {
            var tile = Instantiate(Sample);
            Process(tile, type.ToString());
            TileDict.Add(type, tile);
        }

        //Process(GrassTile, "grass");
        //Process(PathTile, "path");
        //Process(WoodTile, "wood");
        //Process(ConcreteTile, "concrete");
        //Process(TileTile, "tile");


        ToiletTile = Instantiate(Sample);
        BedTile = Instantiate(Sample);
        NurseBedTile = Instantiate(Sample);
        Books = Instantiate(Sample);
        GymObject = Instantiate(Sample);
        VoreDisplay = Instantiate(Sample);
        SexDisplay = Instantiate(Sample);
        DisposalDisplay = Instantiate(Sample);

        Process(ToiletTile, "Toilet");
        Process(BedTile, "Bed");
        Process(NurseBedTile, "Nursebed");
        Process(Books, "Books");
        Process(GymObject, "Gymobject");

        Process(VoreDisplay, "Voredisplay");
        Process(SexDisplay, "Sexdisplay");
        Process(DisposalDisplay, "Disposaldisplay");

        void Process(Tile tile, string type)
        {
            float modifier = 1.04f;
            if (sizes.ContainsKey(type))
            {
                modifier = sizes[type];
            }
            string nextFile = Path.Combine(Application.streamingAssetsPath, "Tiles", $"{type}.png");
            if (File.Exists(nextFile))
            {
                tile.sprite = LoadPNG(nextFile, modifier);
            }
            else if (
                File.Exists(Path.Combine(Application.streamingAssetsPath, "Tiles", $"{type}.jpg"))
            )
            {
                tile.sprite = LoadPNG(
                    Path.Combine(Application.streamingAssetsPath, "Tiles", $"{type}.jpg"),
                    modifier
                );
            }
            else
                tile.sprite = DefaultSprite;
        }
    }

    public void DrawWorld()
    {
        WorldMap.ClearAllTiles();
        ObjectMap.ClearAllTiles();

        for (int x = 0; x <= State.World.Zones.GetUpperBound(0); x++)
        {
            for (int y = 0; y <= State.World.Zones.GetUpperBound(1); y++)
            {
                if (State.World.Zones[x, y] != null)
                {
                    WorldMap.SetTile(
                        new Vector3Int(x, y, 0),
                        TileDict[State.World.Zones[x, y].Type]
                    );
                    if (State.World.Zones[x, y].Type == ZoneType.DormRoom)
                        ObjectMap.SetTile(new Vector3Int(x, y, 0), BedTile);
                    if (State.World.Zones[x, y].Type == ZoneType.Bathroom)
                        ObjectMap.SetTile(new Vector3Int(x, y, 0), ToiletTile);
                    if (State.World.Zones[x, y].Type == ZoneType.NurseOffice)
                        ObjectMap.SetTile(new Vector3Int(x, y, 0), NurseBedTile);
                    if (State.World.Zones[x, y].Type == ZoneType.Gym)
                        ObjectMap.SetTile(new Vector3Int(x, y, 0), GymObject);
                    if (State.World.Zones[x, y].Type == ZoneType.Library)
                        ObjectMap.SetTile(new Vector3Int(x, y, 0), Books);
                }
            }
        }

        State.World.BorderController.DrawBorders(
            State.GameManager.BorderDisplay.NorthBorderMap,
            State.GameManager.BorderDisplay.EastBorderMap
        );
        UpdateSpecial();
    }

    public void UpdateSpecial()
    {
        BackMap.ClearAllTiles();
        if (Config.HighlightVore)
        {
            foreach (var person in State.World.GetAllPeople())
            {
                if (person.VoreController.CurrentSwallow(VoreLocation.Any) != null)
                {
                    Vector3Int loc = new Vector3Int(person.Position.x, person.Position.y, 0);
                    BackMap.SetTile(loc, VoreDisplay);
                }
            }
        }
        if (Config.HighlightSex)
        {
            foreach (var person in State.World.GetAllPeople())
            {
                if (person.ActiveSex != null)
                {
                    Vector3Int loc = new Vector3Int(person.Position.x, person.Position.y, 0);
                    BackMap.SetTile(loc, SexDisplay);
                }
            }
        }
        if (Config.HighlightDisposal)
        {
            foreach (var person in State.World.GetAllPeople())
            {
                if (
                    person.StreamingSelfAction >= SelfActionType.ScatDisposalBathroom
                    && person.StreamingSelfAction <= SelfActionType.UnbirthDisposalFloor
                )
                {
                    Vector3Int loc = new Vector3Int(person.Position.x, person.Position.y, 0);
                    BackMap.SetTile(loc, DisposalDisplay);
                }
            }
        }
        if (Config.PlayerVisionActive())
        {
            for (int x = 0; x < State.World.Zones.GetLength(0); x++)
            {
                for (int y = 0; y < State.World.Zones.GetLength(1); y++)
                {
                    if (State.World.Zones[x, y] == null)
                        continue;
                    if (LOS.Check(State.World.ControlledPerson, new Vec2(x, y)) == false)
                    {
                        Vector3Int loc = new Vector3Int(x, y, 0);
                        BackMap.SetTile(loc, Dark);
                    }
                }
            }
        }
    }

    static Sprite LoadPNG(string filePath, float modifier)
    {
        Texture2D tex = null;
        byte[] fileData;

        if (File.Exists(filePath))
        {
            fileData = File.ReadAllBytes(filePath);
            tex = new Texture2D(2, 2, TextureFormat.BGRA32, false);
            tex.LoadImage(fileData); //..this will auto-resize the texture dimensions.
        }
        if (tex == null)
            return null;
        Rect rect = new Rect(new Vector2(0, 0), new Vector2(tex.width, tex.height));
        Vector2 pivot = new Vector2(0.5f, 0.5f);
        int higherDimension = Math.Max(tex.width, tex.height);
        Sprite sprite = Sprite.Create(tex, rect, pivot, higherDimension * modifier);
        return sprite;
    }
}
