﻿using OdinSerializer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public enum VoreLocation
{
    Any,
    Stomach,
    Womb,
    Balls,
    Bowels
}

internal enum VoreType
{
    Oral,
    Unbirth,
    Cock,
    Anal
}

public class VoreController : ISerializationCallbackReceiver
{
    /// Locations considered to be a general internal location.
    static internal readonly HashSet<VoreLocation> VoreBellyLocations = new HashSet<VoreLocation>
    {
        VoreLocation.Stomach,
        VoreLocation.Womb,
        VoreLocation.Bowels
    };

    [OdinSerialize, VariableEditorIgnores]
    readonly Person Self;

    [OdinSerialize, VariableEditorIgnores]
    internal bool StomachDigestsPrey;

    [OdinSerialize, VariableEditorIgnores]
    internal bool WombAbsorbsPrey;

    [OdinSerialize, VariableEditorIgnores]
    internal bool BallsAbsorbPrey;

    [OdinSerialize, VariableEditorIgnores]
    internal bool BowelsDigestPrey;

    // Obsolete variables which exist for backwards save compatibility.
    // These can eventually be removed, they can not be referenced by new code.
    [Obsolete, OdinSerialize, VariableEditorIgnores]
    List<VoreProgress> AllVoredTargets
    {
        get => VoreTargets.ToList();
        set { VoreTargets = new HashSet<VoreProgress>(value); }
    }

    [Obsolete, OdinSerialize, VariableEditorIgnores]
    List<VoreProgress> StomachTargets
    {
        get => VoreTargets.Where(x => x.Location == VoreLocation.Stomach).ToList();
        set { }
    }

    [Obsolete, OdinSerialize, VariableEditorIgnores]
    List<VoreProgress> BowelTargets
    {
        get => VoreTargets.Where(x => x.Location == VoreLocation.Bowels).ToList();
        set { }
    }

    [Obsolete, OdinSerialize, VariableEditorIgnores]
    List<VoreProgress> WombTargets
    {
        get => VoreTargets.Where(x => x.Location == VoreLocation.Womb).ToList();
        set { }
    }

    [Obsolete, OdinSerialize, VariableEditorIgnores]
    List<VoreProgress> BallsTargets
    {
        get => VoreTargets.Where(x => x.Location == VoreLocation.Balls).ToList();
        set { }
    }

    /// <summary> All references to prey held by this controller. </summary>
    [OdinSerialize, VariableEditorIgnores]
    HashSet<VoreProgress> VoreTargets = new HashSet<VoreProgress>();

    [OdinSerialize, ProperName("Vore Capable")]
    [Description(
        "Controls whether this character is capable of vore or not.  Disabling will disable all vore types."
    )]
    [Category("Vore")]
    public bool GeneralVoreCapable;

    [OdinSerialize, ProperName("Oral Vore Capable")]
    [Description("Controls whether this character is capable of oral vore.")]
    [Category("Vore")]
    public bool OralVoreCapable = true;

    [OdinSerialize, ProperName("Cock Vore Capable")]
    [Description("Controls whether this character is capable of cock vore.")]
    [Category("Vore")]
    public bool CockVoreCapable = true;

    [OdinSerialize, ProperName("Unbirth Capable")]
    [Description("Controls whether this character is capable of unbirth.")]
    [Category("Vore")]
    public bool UnbirthCapable = true;

    [OdinSerialize, ProperName("Anal Vore Capable")]
    [Description("Controls whether this character is capable of anal vore.")]
    [Category("Vore")]
    public bool AnalVoreCapable = true;

    [OdinSerialize, ProperName("Vore Power")]
    [IntegerRange(-5, 5)]
    [Description(
        "Positive values will boost vore and escape odds, negative lowers.  Is only manually set.  Can be used to simulate size differences, or whatever you want.  "
    )]
    [Category("Vore")]
    internal int VorePower;

    [OdinSerialize, ProperName("Total Digestions")]
    [IntegerRange(0, 9999)]
    [Description(
        "The total number of people this character has digested.  It's largely just flavor text, aside from lowering how likely people are to try and rescue someone from you."
    )]
    [Category("Vore")]
    internal int TotalDigestions;

    public bool CapableOfVore()
    {
        if (GeneralVoreCapable == false)
            return false;
        if (Self.GenderType.VoreDisabled)
            return false;
        return true;
    }

    public VoreController(Person self, bool canVore)
    {
        Self = self;
        GeneralVoreCapable = canVore;
    }

    void ISerializationCallbackReceiver.OnAfterDeserialize()
    {
        if (VoreTargets == null)
            VoreTargets = new HashSet<VoreProgress>();
    }

    void ISerializationCallbackReceiver.OnBeforeSerialize() { }

    internal VoreLocation GetRespectiveStartingPoint(VoreType voreType)
    {
        switch (voreType)
        {
            case VoreType.Oral:
                return VoreLocation.Stomach;
            case VoreType.Unbirth:
                return VoreLocation.Womb;
            case VoreType.Cock:
                return VoreLocation.Balls;
            case VoreType.Anal:
                if (State.World.Settings.AnalVoreGoesDirectlyToStomach)
                    return VoreLocation.Stomach;
                else
                    return VoreLocation.Bowels;
        }
        return VoreLocation.Stomach;
    }

    /// <summary>
    /// Iterates over the vore items in a specific location.
    /// </summary>
    IEnumerable<VoreProgress> GetRespectiveList(VoreLocation location)
    {
        if (location == VoreLocation.Any)
            return VoreTargets.AsEnumerable();
        return VoreTargets.Where(it => it.Location == location).AsEnumerable();
    }

    /// <summary>
    /// Returns true or false, throws an exception if the target isn't present.
    /// </summary>
    /// <param name="target"></param>
    /// <returns></returns>
    public bool TargetIsBeingDigested(Person target)
    {
        var ret = GetProgressOf(target);

        if (target.Health <= 0)
            return true;
        if (target.DigestionImmune)
            return false;
        if (Self.Team != 0 && Self.Team == target.Team)
            return false;
        if (target.PredatorTier > Self.PredatorTier)
            return false;

        return PartCurrentlyDigests(ret.Location);
    }

    internal bool PartCurrentlyDigests(VoreType voreType) =>
        PartCurrentlyDigests(GetRespectiveStartingPoint(voreType));

    /// <summary>
    /// Accounts for bowel redirection
    /// </summary>
    /// <param name="location"></param>
    /// <returns></returns>
    internal bool PartCurrentlyDigests(VoreLocation location)
    {
        switch (location)
        {
            case VoreLocation.Stomach:
                return StomachDigestsPrey;
            case VoreLocation.Womb:
                return WombAbsorbsPrey;
            case VoreLocation.Balls:
                return BallsAbsorbPrey;
            case VoreLocation.Bowels:
                if (State.World.Settings.AnalVoreGoesDirectlyToStomach)
                    return StomachDigestsPrey;
                return BowelsDigestPrey;
        }
        return false;
    }

    internal void SetPartDigest(VoreType voreType, bool active)
    {
        SetPartDigest(GetRespectiveStartingPoint(voreType), active);
    }

    internal void SetPartDigest(VoreLocation location, bool active)
    {
        if (
            State.World.Settings.CheckDigestion(Self, DigestionAlias.CanEndo) == false
            && active == false
        )
        {
            UnityEngine.Debug.Log("Tried to set a unit that can only digest to endo");
            active = true;
        }
        if (State.World.Settings.CheckDigestion(Self, DigestionAlias.CanVore) == false && active)
        {
            UnityEngine.Debug.Log("Tried to set a unit that can only endo to digest");
            active = false;
        }
        switch (location)
        {
            case VoreLocation.Stomach:
                StomachDigestsPrey = active;
                return;
            case VoreLocation.Womb:
                WombAbsorbsPrey = active;
                return;
            case VoreLocation.Balls:
                BallsAbsorbPrey = active;
                return;
            case VoreLocation.Bowels:
                BowelsDigestPrey = active;
                return;
        }
        UnityEngine.Debug.LogWarning("Missing Part Reference for setting digest!");
        return;
    }

    public bool IsDigesting()
    {
        foreach (var progress in VoreTargets)
        {
            if (TargetIsBeingDigested(progress.Target))
                return true;
        }
        return false;
    }

    public bool IsAbsorbing()
    {
        foreach (var progress in VoreTargets)
        {
            if (progress.Target.Health < 0)
                return true;
        }
        return false;
    }

    public bool IsAbsorbingBellyPrey()
    {
        foreach (var progress in VoreTargets.Where(it => VoreBellyLocations.Contains(it.Location)))
        {
            if (progress.Target.Health < 0)
                return true;
        }
        return false;
    }

    public bool IsAbsorbingStomachPrey()
    {
        foreach (var progress in VoreTargets.Where(it => it.Location == VoreLocation.Stomach))
        {
            if (progress.Target.Health < 0)
                return true;
        }
        return false;
    }

    public bool IsAbsorbingBowelsPrey()
    {
        foreach (var progress in VoreTargets.Where(it => it.Location == VoreLocation.Bowels))
        {
            if (progress.Target.Health < 0)
                return true;
        }
        return false;
    }

    public bool IsAbsorbingWombPrey()
    {
        foreach (var progress in VoreTargets.Where(it => it.Location == VoreLocation.Womb))
        {
            if (progress.Target.Health < 0)
                return true;
        }
        return false;
    }

    public bool IsAbsorbingBallsPrey()
    {
        foreach (var progress in VoreTargets.Where(it => it.Location == VoreLocation.Balls))
        {
            if (progress.Target.Health < 0)
                return true;
        }
        return false;
    }

    internal void CheckIfShouldDigest(VoreLocation part = VoreLocation.Any)
    {
        if (State.World.Settings.CheckDigestion(false, DigestionAlias.CanSwitch) == false)
            return;
        if (part == VoreLocation.Any || part == VoreLocation.Stomach)
            CheckPart(VoreLocation.Stomach);
        if (part == VoreLocation.Any || part == VoreLocation.Balls)
            CheckPart(VoreLocation.Balls);
        if (part == VoreLocation.Any || part == VoreLocation.Womb)
            CheckPart(VoreLocation.Womb);
        if (part == VoreLocation.Any || part == VoreLocation.Bowels)
            CheckPart(VoreLocation.Bowels);

        void CheckPart(VoreLocation location)
        {
            if (PartCurrentlyDigests(location) == false && GetRespectiveList(location).Any())
            {
                if (
                    GetRespectiveList(location)
                        .Where(
                            s =>
                                (s.Target.Dead == false)
                                && (
                                    s.ExpectingEndosoma
                                    || Self.AI.Desires.DesireToDigestTarget(s.Target) < 0.04f
                                )
                        )
                        .Any() == false
                )
                    SetPartDigest(location, true);
            }
        }
    }

    internal void MovePrey(VoreProgress progress, VoreLocation location)
    {
        progress.Location = location;
        switch (location)
        {
            case VoreLocation.Stomach:
                progress.OriginalType = VoreType.Oral;
                break;
            case VoreLocation.Womb:
                progress.OriginalType = VoreType.Unbirth;
                break;
            case VoreLocation.Balls:
                progress.OriginalType = VoreType.Cock;
                break;
            case VoreLocation.Bowels:
                progress.OriginalType = VoreType.Anal;
                break;
        }
    }

    internal void AddPrey(VoreProgress progress, VoreType voreType)
    {
        VoreLocation location = GetRespectiveStartingPoint(voreType);
        VoreTargets.Add(progress);
        progress.Location = location;
        progress.OriginalType = voreType;
        State.World.EndOfTurnCallBacks.Add(() => progress.Target.EndStreamingActions());
    }

    internal void AddPrey(VoreProgress progress, VoreLocation location)
    {
        VoreType assignedType = VoreType.Oral;
        switch (location)
        {
            case VoreLocation.Stomach:
                if (OralVoreCapable == false || State.World.Settings.OralVoreEnabled == false)
                    assignedType = VoreType.Anal;
                else
                    assignedType = VoreType.Oral;
                break;
            case VoreLocation.Womb:
                assignedType = VoreType.Unbirth;
                break;
            case VoreLocation.Balls:
                assignedType = VoreType.Cock;
                break;
            case VoreLocation.Bowels:
                assignedType = VoreType.Anal;
                break;
        }
        VoreTargets.Add(progress);
        progress.Location = location;
        progress.OriginalType = assignedType;
        State.World.EndOfTurnCallBacks.Add(() => progress.Target.EndStreamingActions());
    }

    internal void RemovePrey(VoreProgress progress) => VoreTargets.Remove(progress);

    internal void RemovePrey(Person person) => VoreTargets.RemoveWhere(s => s.Target == person);

    internal List<VoreProgress> GetAllProgress(VoreLocation location) =>
        GetRespectiveList(location).ToList();

    internal bool HasPrey(VoreLocation location, bool IncludeSwallowing = true)
    {
        if (IncludeSwallowing)
            return GetRespectiveList(location).Any();
        else
            return GetRespectiveList(location).Any((s) => !s.IsSwallowing());
    }

    internal bool HasPrey(VoreType voreType, bool IncludeSwallowing = true)
    {
        if (IncludeSwallowing)
            return GetRespectiveList(GetRespectiveStartingPoint(voreType)).Any();
        else
            return GetRespectiveList(GetRespectiveStartingPoint(voreType)).Any((s) => !s.IsSwallowing());
    }

    internal bool HasUnwillingPrey(VoreLocation location, bool IncludeSwallowing = true)
    {
        if (IncludeSwallowing)
            return GetRespectiveList(location).Any((s) => !s.Willing);
        else
            return GetRespectiveList(location).Any((s) => !s.IsSwallowing() && !s.Willing);
    }

    internal bool HasWillingPrey(VoreLocation location, bool IncludeSwallowing = true)
    {
        if (IncludeSwallowing)
            return GetRespectiveList(location).Any((s) => s.Willing);
        else
            return GetRespectiveList(location).Any((s) => !s.IsSwallowing() && s.Willing);
    }

    internal VoreProgress GetLiving(VoreLocation location) =>
        GetRespectiveList(location).FirstOrDefault(s => s.IsAlive());

    internal bool ContainsPerson(Person target, VoreLocation location) =>
        GetRespectiveList(location).Any(s => s.Target == target);

    public VoreProgress GetProgressOf(Person target) =>
        VoreTargets.FirstOrDefault(s => s.Target == target);

    public VoreProgress GetProgressOf(Person target, VoreLocation location) =>
        GetRespectiveList(location).FirstOrDefault(s => s.Target == target);

    public bool TargetIsInMyStomach(Person target) //Done this way because the interpreter can't handle enums
    {
        var progress = GetProgressOf(target);
        if (progress == null)
            return false;
        return progress.Location == VoreLocation.Stomach;
    }

    public bool TargetIsInMyBalls(Person target)
    {
        var progress = GetProgressOf(target);
        if (progress == null)
            return false;
        return progress.Location == VoreLocation.Balls;
    }

    public bool TargetIsInMyWomb(Person target)
    {
        var progress = GetProgressOf(target);
        if (progress == null)
            return false;
        return progress.Location == VoreLocation.Womb;
    }

    public bool TargetIsInMyBowels(Person target)
    {
        var progress = GetProgressOf(target);
        if (progress == null)
            return false;
        return progress.Location == VoreLocation.Bowels;
    }

    public bool TargetOralVored(Person target)
    {
        var progress = GetProgressOf(target);
        if (progress == null)
            return false;
        return progress.OriginalType == VoreType.Oral;
    }

    public bool TargetUnbirthed(Person target)
    {
        var progress = GetProgressOf(target);
        if (progress == null)
            return false;
        return progress.OriginalType == VoreType.Unbirth;
    }

    public bool TargetCockVored(Person target)
    {
        var progress = GetProgressOf(target);
        if (progress == null)
            return false;
        return progress.OriginalType == VoreType.Cock;
    }

    public bool TargetAnalVored(Person target)
    {
        var progress = GetProgressOf(target);
        if (progress == null)
            return false;
        return progress.OriginalType == VoreType.Anal;
    }

    internal bool CouldVoreTarget(Person target, VoreType type, DigestionAlias digestion)
    {
        if (target.VoreImmune)
            return false;
        if (Self.VoreController.HasPrey(VoreLocation.Any) && Self.HasTrait(Quirks.LimitedCapacity))
            return false;
        if (target.PredatorTier > Self.PredatorTier)
        {
            if (
                digestion != DigestionAlias.CanEndo
                || State.World.Settings.PredatorTierAlsoRestrictsEndo
            )
                return false;
        }
        if (
            digestion != DigestionAlias.CanEndo
            && Self.GetRelationshipWith(target).KnowledgeAbout.KnowsDigestionImmune
        )
            return false;
        if (Self.Team != 0 && Self.Team == target.Team && digestion != DigestionAlias.CanEndo)
        {
            return false;
        }
        if (
            State.World.Settings.VoreFollowsOrientiation
            && Self.Romance.DesiresGender(target.GenderType) == false
        )
            return false;
        bool can = false;
        VoreLocation location = VoreLocation.Stomach;
        switch (type)
        {
            case VoreType.Oral:
                can = OralVoreCapable && State.World.Settings.OralVoreEnabled;
                location = VoreLocation.Stomach;
                break;
            case VoreType.Unbirth:
                can =
                    UnbirthCapable
                    && State.World.Settings.UnbirthEnabled
                    && Self.GenderType.HasVagina;
                location = VoreLocation.Womb;
                break;
            case VoreType.Cock:
                can =
                    CockVoreCapable
                    && State.World.Settings.CockVoreEnabled
                    && Self.GenderType.HasDick;
                location = VoreLocation.Balls;
                break;
            case VoreType.Anal:
                can = AnalVoreCapable && State.World.Settings.AnalVoreEnabled;
                if (State.World.Settings.AnalVoreGoesDirectlyToStomach)
                    location = VoreLocation.Stomach;
                else
                    location = VoreLocation.Bowels;
                break;
        }
        return HasRoomFor(target, location)
            && State.World.Settings.CheckDigestion(Self, digestion)
            && can;
    }

    internal bool TargetVoreImmune(Person target, DigestionAlias digestion)
    {
        if (target.VoreImmune)
            return true;
        if (
            State.World.Settings.VoreFollowsOrientiation
            && Self.Romance.DesiresGender(target.GenderType) == false
        )
            return true;
        if (target.PredatorTier > Self.PredatorTier)
        {
            if (
                digestion != DigestionAlias.CanEndo
                || State.World.Settings.PredatorTierAlsoRestrictsEndo
            )
                return true;
        }
        if (
            digestion != DigestionAlias.CanEndo
            && Self.GetRelationshipWith(target).KnowledgeAbout.KnowsDigestionImmune
        )
            return true;
        if (Self.Team != 0 && Self.Team == target.Team && digestion != DigestionAlias.CanEndo)
        {
            return true;
        }
        return false;
    }

    internal string GetReason(Person target, VoreType type, DigestionAlias digestion)
    {
        if (target.VoreImmune)
            return "Target is Vore Immune";
        if (CapableOfVore() == false)
            return "You're playing as someone incapable of vore";
        if (
            State.World.Settings.VoreFollowsOrientiation
            && Self.Romance.DesiresGender(target.GenderType) == false
        )
            return "You have it set that vore is based on attraction, and your character is not interested in the target's gender.";
        if (target.PredatorTier > Self.PredatorTier)
        {
            if (digestion != DigestionAlias.CanEndo)
                return "That character has a higher vore tier and it's set to digestion.";
            if (State.World.Settings.PredatorTierAlsoRestrictsEndo)
                return "That character has a higher vore tier and it's set to endo with the option preventing higher tier endo enabled.";
        }
        if (Self.Team != 0 && Self.Team == target.Team && digestion != DigestionAlias.CanEndo)
        {
            return "You're on the same team as them and trying to digest them.";
        }
        if (Self.VoreController.HasPrey(VoreLocation.Any) && Self.HasTrait(Quirks.LimitedCapacity))
        {
            return "You have the trait \"Limited Capacity\" and can only have one prey at a time.";
        }

        bool can = false;
        VoreLocation location = VoreLocation.Stomach;
        switch (type)
        {
            case VoreType.Oral:
                can = OralVoreCapable && State.World.Settings.OralVoreEnabled;
                location = VoreLocation.Stomach;
                break;
            case VoreType.Unbirth:
                can =
                    UnbirthCapable
                    && State.World.Settings.UnbirthEnabled
                    && Self.GenderType.HasVagina;
                location = VoreLocation.Womb;
                break;
            case VoreType.Cock:
                can =
                    CockVoreCapable
                    && State.World.Settings.CockVoreEnabled
                    && Self.GenderType.HasDick;
                location = VoreLocation.Balls;
                break;
            case VoreType.Anal:
                can = AnalVoreCapable && State.World.Settings.AnalVoreEnabled;
                if (State.World.Settings.AnalVoreGoesDirectlyToStomach)
                    location = VoreLocation.Stomach;
                else
                    location = VoreLocation.Bowels;
                break;
        } 
        if (HasRoomFor(target, location) == false)
            return "Not enough room in that location, or the prey is too big to fit.";

        if (can == false)
            return "Disabled by character / world setting, or character lacks the appropriate part";
        if (State.World.Settings.CheckDigestion(Self, digestion) == false)
            return "Can't use that digestion type on that target.";
        return "Couldn't find the reason";
    }

    /// <summary>
    /// Gets free space - Any returns the highest space of the 3, and excludes impossible types, specific types don't check for impossible types.
    /// </summary>
    /// <param name="location"></param>
    /// <returns></returns>
    internal float FreeSpace(VoreLocation location)
    {
        float space =
            State.World.Settings.PredCapacity
            * Self.Boosts.Capacity
            * (1 + (Self.Personality.Voracity * 3f));
        if (location == VoreLocation.Any)
        {
            float maxFree = 0;
            if (
                (OralVoreCapable && State.World.Settings.OralVoreEnabled)
                || AnalVoreCapable && State.World.Settings.AnalVoreEnabled
            )
            {
                maxFree = Math.Max(
                    maxFree,
                    space - PreySize(VoreLocation.Stomach) - PreySize(VoreLocation.Bowels)
                );
            }
            if (UnbirthCapable && State.World.Settings.UnbirthEnabled && Self.GenderType.HasVagina)
            {
                maxFree = Math.Max(maxFree, space - PreySize(VoreLocation.Womb));
            }
            if (CockVoreCapable && State.World.Settings.CockVoreEnabled && Self.GenderType.HasDick)
            {
                maxFree = Math.Max(maxFree, space - PreySize(VoreLocation.Balls));
            }
            return maxFree;
        }
        if (location == VoreLocation.Bowels || location == VoreLocation.Stomach)
            return space - PreySize(VoreLocation.Bowels) - PreySize(VoreLocation.Stomach);
        return space - PreySize(location);
    }
 
    internal bool HasRoomFor(Person target, VoreLocation location) =>
        CapableOfVore() ? (FreeSpace(location) >= target.VoreController.Bulk()) : false;

    internal bool Swallowing(VoreLocation location) =>
        GetRespectiveList(location).Any((s) => s.IsSwallowing());

    internal VoreProgress CurrentSwallow(VoreLocation location) =>
        GetRespectiveList(location).FirstOrDefault((s) => s.IsSwallowing());

    /// <summary>
    /// Only living, non-swallowed prey
    /// </summary>
    /// <param name="prey"></param>
    /// <param name="other"></param>
    /// <returns></returns>
    ///
    public bool AreSisterPrey(Person prey, Person other, bool includeSwallowing = false)
    {
        if (prey.Dead || other.Dead)
            return false;
        var location = GetProgressOf(prey).Location;
        return GetRespectiveList(location)
            .Where(s => s.Target == other && (includeSwallowing || s.IsSwallowing() == false))
            .Any();
    }

    internal void ClearData()
    {
        VoreTargets.Clear();
    }

    internal List<VoreProgress> GetSisterPrey(
        Person prey,
        bool onlyLiving = true,
        bool includeBeingSwallowed = false
    )
    {
        List<VoreProgress> ret = new List<VoreProgress>();
        var location = GetProgressOf(prey).Location;
        ret.AddRange(GetRespectiveList(location));
        if (onlyLiving)
            ret.RemoveAll(s => s.Target.Dead);
        if (includeBeingSwallowed == false)
            ret.RemoveAll(s => s.IsSwallowing());
        ret.RemoveAll(s => s.Target == prey);
        return ret;
    }

    /// <summary>
    /// Get self size + size of all prey and sub-prey
    /// </summary>
    /// <returns></returns>
    internal float Bulk() => GetModifiedSize(Self) + PreySize();

    public float BellySize()
    {
        float bulk = 0;
        foreach (var digestion in VoreTargets.Where(it => VoreBellyLocations.Contains(it.Location)))
        {
            bulk += GetModifiedSize(digestion.Target);
            bulk += digestion.Target.VoreController.PreySize();
        }
        return bulk;
    }

    public float BallsSize()
    {
        float bulk = 0;
        foreach (var digestion in VoreTargets.Where(it => it.Location == VoreLocation.Balls))
        {
            bulk += GetModifiedSize(digestion.Target);
            bulk += digestion.Target.VoreController.PreySize();
        }
        return bulk;
    }

    /// <summary>
    /// Includes all sub-prey
    /// </summary>
    /// <returns></returns>
    internal float PreySize(VoreLocation location = VoreLocation.Any, int depth = 0)
    {
        if (depth > 10)
        {
            UnityEngine.Debug.LogWarning("Depth exceeds 10, error likely");
            return 0;
        }
        float bulk = 0;
        foreach (var digestion in GetRespectiveList(location))
        {
            bulk += GetModifiedSize(digestion.Target);
            bulk += digestion.Target.VoreController.PreySize(depth: depth + 1);
        }
        return bulk;
    }

    /// <summary>
    /// Gets a person's current size based on their health
    /// </summary>
    /// <param name="person"></param>
    /// <returns></returns>
    float GetModifiedSize(Person person)
    {
        var bulk = person.PartList.Height / Self.PartList.Height;
        if (person.Health < 0)
            return bulk * (1 - Math.Abs((float)person.Health / Constants.EndHealth));
        else
            return bulk;
    }

    internal void GetPredInfo(ref StringBuilder sb)
    {
        ProcessType(ref sb, VoreLocation.Stomach, "swallowing", "stomach");
        ProcessType(ref sb, VoreLocation.Womb, "unbirthing", "womb");
        ProcessType(ref sb, VoreLocation.Balls, "cock voring", "balls");
        ProcessType(ref sb, VoreLocation.Bowels, "anal voring", "anus");
        return;

        void ProcessType(ref StringBuilder sbb, VoreLocation type, string action, string location)
        {
            if (CurrentSwallow(type) != null)
                sbb.AppendLine($"Is currently {action} {CurrentSwallow(type).Target.FirstName}.");
            else
            {
                if (GetRespectiveList(type).Count() > 0)
                    sbb.AppendLine($"Has {ListPrey(type)} in their {location}.");
                return;
            }

            if (GetRespectiveList(type).Count() > 1)
            {
                sbb.AppendLine($"Has {ListPrey(type)} in their {location}.");
            }
        }

        string ListPrey(VoreLocation location)
        {
            return string.Join(" , ", GetRespectiveList(location).Select(s => s.Target.FirstName));
        }
    }

    //HasLivingPrey
    public bool HasLivingBellyPrey() =>
        GetLiving(VoreLocation.Stomach) != null || GetLiving(VoreLocation.Womb) != null || GetLiving(VoreLocation.Bowels) != null;

    public bool HasLivingBallsPrey() => GetLiving(VoreLocation.Balls) != null;

    public bool HasLivingBowelsPrey() => GetLiving(VoreLocation.Bowels) != null;

    public bool HasLivingStomachPrey() => GetLiving(VoreLocation.Stomach) != null;

    public bool HasLivingWombPrey() => GetLiving(VoreLocation.Womb) != null;

    public bool HasLivingPrey() => GetLiving(VoreLocation.Any) != null;

    // HasPrey (includes swallowing)
    public bool HasAnyPrey() => HasPrey(VoreLocation.Any);

    public bool HasStomachPrey() => HasPrey(VoreLocation.Stomach);

    public bool HasWombPrey() => HasPrey(VoreLocation.Womb);

    public bool HasBowelsPrey() => HasPrey(VoreLocation.Bowels);

    public bool HasBallsPrey() => HasPrey(VoreLocation.Balls);

    public bool HasBellyPrey() =>
        HasPrey(VoreLocation.Womb) || HasPrey(VoreLocation.Stomach) || HasPrey(VoreLocation.Bowels);

    // HasPreySwallowed (does not include swallowing)
    public bool HasAnyPreySwallowed() => HasPrey(VoreLocation.Any, false);

    public bool HasAnyWillingPrey() => HasWillingPrey(VoreLocation.Any, false);

    public bool HasAnyUnwillingPrey() => HasUnwillingPrey(VoreLocation.Any, false);

    public bool HasStomachPreySwallowed() => HasPrey(VoreLocation.Stomach, false);

    public bool HasWombPreySwallowed() => HasPrey(VoreLocation.Womb, false);

    public bool HasBowelsPreySwallowed() => HasPrey(VoreLocation.Bowels, false);

    public bool HasBallsPreySwallowed() => HasPrey(VoreLocation.Balls, false);

    public bool HasBellyPreySwallowed() =>
        HasPrey(VoreLocation.Womb, false) || HasPrey(VoreLocation.Stomach, false) || HasPrey(VoreLocation.Bowels, false);

    // DigestingPrey
    public bool IsDigestingAnyPrey() => PartCurrentlyDigests(VoreLocation.Any);

    public bool IsDigestingStomachPrey() => PartCurrentlyDigests(VoreLocation.Stomach);

    public bool IsDigestingWombPrey() => PartCurrentlyDigests(VoreLocation.Womb);

    public bool IsDigestingBowelsPrey() => PartCurrentlyDigests(VoreLocation.Bowels);

    public bool IsDigestingBallsPrey() => PartCurrentlyDigests(VoreLocation.Balls);

    public bool HasLovedOne(Person target)
    {
        foreach (var progress in VoreTargets)
        {
            var prey = progress.Target;
            if (target.GetRelationshipWith(prey).RomanticLevel >= 0.7)
                return true;
            if (target.Romance.Dating == prey)
                return true;
        }
        return false;
    }

    public void DigestDamage(float damageMult, VoreLocation loc)
    {
        var Targets = GetRespectiveList(loc);
        foreach (var progress in Targets)
        {
            var prey = progress.Target;
            var damageMod = (
                Constants.DigestionDamage
                * State.World.Settings.DigestionSpeed
                * Self.Boosts.OutgoingDigestionSpeed
                * prey.Boosts.IncomingDigestionSpeed
            );
            if (TargetIsBeingDigested(prey) && !progress.IsSwallowing())
            {
                prey.Health -= (int)(damageMult * damageMod);
            }
        }
    }

    internal void AdvanceStages()
    {
        foreach (var digestion in VoreTargets.ToList())
        {
            digestion.AdvanceStage();
        }
        //if (Swallowing == false && Self.StreamingAction == InteractionType.Vore)
        //    Self.EndStreamingActions();
    }

    internal List<VoreProgress> GetAllSubPrey(int depth = 0)
    {
        depth++;
        if (depth > 150)
        {
            return new List<VoreProgress>();
        }
        List<VoreProgress> list = new List<VoreProgress>();
        foreach (var prey in VoreTargets)
        {
            list.AddRange(prey.Target.VoreController.GetAllSubPrey(depth));
        }
        list.AddRange(VoreTargets);
        return list;
    }
}
