﻿using OdinSerializer;

public enum VorePreference
{
    Either,
    Digestion,
    Endosoma
}

public class Personality
{
    [OdinSerialize]
    [Description("Affects odds of friendship and relationship action success.")]
    [Category("Personality")]
    public float Charisma { get; set; } = Rand.NextFloat(0, 1);

    [OdinSerialize]
    [Description("Affects forced action attack, defense, and vore rescue odds.")]
    [Category("Personality")]
    public float Strength { get; set; } = Rand.NextFloat(0, 1);

    [OdinSerialize]
    [Description(
        "Affects vore attack odds, greater effect than strength.  Also affects max capacity."
    )]
    [Category("Vore")]
    public float Voracity { get; set; } = Rand.NextFloat(0, 1);

    [OdinSerialize, ProperName("Sex Drive")]
    [Description(
        "Affects how their horniness changes by default.  At under 50% it will fall over time, at over 50% it will rise over time."
    )]
    [Category("Personality")]
    public float SexDrive { get; set; } = Rand.NextFloat(0, 1);

    [OdinSerialize, ProperName("Pred Willingness")]
    [Description("Affects how much they want to be pred. Controls how willing they are to eat someone and how often they pursue it.")]
    [Category("Vore")]
    public float PredWillingness { get; set; } = Rand.NextFloat(0, 1);

    [OdinSerialize, ProperName("Prey Willingness")]
    [Description(
        "Affects how much they want to be prey. Controls how hard they fight back, and people above the percentage set in game settings are always willing and will not fight back, and can be eaten in public without complaints."
    )]
    [Category("Vore")]
    public float PreyWillingness { get; set; } = Rand.NextFloat(0, 1);

    [OdinSerialize]
    [Description(
        "Affects romantic success odds, how much they go after romantic actions, and how comfortable they are with sex / masturbating in the open. "
    )]
    [Category("Personality")]
    public float Promiscuity { get; set; } = Rand.NextFloat(0, 1);

    [OdinSerialize]
    [Description(
        "Affects their pursuit of endosoma as both pred and prey, how often they rub bellies, and how much they're turned on by vore. Also causes them to perform more vore-related actions when horny."
    )]
    [Category("Vore")]
    public float Voraphilia { get; set; } = Rand.NextFloat(0, 1);

    [OdinSerialize, ProperName("Predator Loyalty")]
    [Description(
        "Affects how much friendship / relationships reduce their urge to eat digest someone.  I.e. at 1 they strongly affect it, at 0 there is no consideration. Also affects how likely they are to rescue friends"
    )]
    [Category("Vore")]
    public float PredLoyalty { get; set; } = Rand.NextFloat(0, 1);

    [OdinSerialize, ProperName("Prey Digestion Interest")]
    [Description(
        "Controls how much this wants to be digested specifically.  This works together with the prey willingness."
    )]
    [Category("Vore")]
    public float PreyDigestionInterest { get; set; } = Rand.NextFloat(0, 1);

    [OdinSerialize]
    [Description(
        "Affects how much they seek out social interaction.  Low values cause people to prefer to do things on their own."
    )]
    [Category("Personality")]
    public float Extroversion { get; set; } = Rand.NextFloat(0, 1);

    [OdinSerialize] // new slider
    [Description(
        "Affects their preferences for sex actions, and how often they force others to do things."
    )]
    [Category("Personality")]
    public float Dominance { get; set; } = Rand.NextFloat(0, 1);

    [OdinSerialize] // new slider
    [Description(
        "Affects how willing they are to help others in need and how receptive they are to non-romantic options."
    )]
    [Category("Personality")]
    public float Kindness { get; set; } = Rand.NextFloat(0, 1);

    [OdinSerialize, ProperName("Oral Vore Interest")]
    [Description(
        "Controls how much this character favors oral vore in comparison to the other vore types.  At 0 it will actually disable this vore type. Also affects what types they'll ask for."
    )]
    [Category("Vore")]
    public float OralVoreInterest { get; set; } = Rand.NextFloat(0, 1);

    [OdinSerialize, ProperName("Unbirth Interest")]
    [Description(
        "Controls how much this character favors unbirth in comparison to the other vore types. At 0 it will actually disable this vore type. Also affects what types they'll ask for."
    )]
    [Category("Vore")]
    public float UnbirthInterest { get; set; } = Rand.NextFloat(0, 1);

    [OdinSerialize, ProperName("Cock Vore Interest")]
    [Description(
        "Controls how much this character favors cock vore in comparison to the other vore types.  At 0 it will actually disable this vore type.Also affects what types they'll ask for."
    )]
    [Category("Vore")]
    public float CockVoreInterest { get; set; } = Rand.NextFloat(0, 1);

    [OdinSerialize, ProperName("Anal Vore Interest")]
    [Description(
        "Controls how much this character favors anal vore in comparison to the other vore types.  At 0 it will actually disable this vore type.Also affects what types they'll ask for."
    )]
    [Category("Vore")]
    public float AnalVoreInterest { get; set; } = Rand.NextFloat(0, 1);

    [OdinSerialize, ProperName("Cheats On Partner")]
    [Category("Romance")]
    [Description(
        "Affects whether the person will cheat on someone they're dating.  Also affects what cheating they will care about."
    )]
    public ThreePointScale CheatOnPartner { get; set; } =
        ThreePointScale.Never + (Rand.Next(4) == 0 ? Rand.Next(3) : 0);

    [OdinSerialize, ProperName("Cheating Acceptance")]
    [Category("Romance")]
    [Description(
        "Affects what cheating they will care about.  At none they'll be mad at any cheating, at minor they'll only care if they catch their partner having sex, and at the everything level they won't care at all"
    )]

    public CheatingAcceptance CheatAcceptance { get; set; } =
        CheatingAcceptance.None + (Rand.Next(4) == 0 ? Rand.Next(3) : 0);

    [OdinSerialize, ProperName("Preferred Clothing")]
    [Category("Personality")]
    [Description(
        "Affects how much clothing the character prefers to wear.   They generally won't bother to put clothes back on if they're comfortable with where they're at."
    )]

    public ClothingStatus PreferredClothing { get; set; } =
        ClothingStatus.Normal + (Rand.Next(4) == 0 ? Rand.Next(3) : 0);

    [OdinSerialize, ProperName("Vore Preference")]
    [Category("Vore")]
    [Description("Can use this to prevent a particular AI from using a vore type.")]

    public VorePreference VorePreference { get; set; } =
        VorePreference.Either + (Rand.Next(5) == 0 ? Rand.Next(3) : 0);

    [OdinSerialize, ProperName("Endosoma Dominator")]
    [Description(
        "If enabled, this character will treat endo the same way preds treat vore, i.e. they will eat people against their will -- but not digest them."
    )]
    [Category("Vore")]
    public bool EndoDominator { get; internal set; } = Rand.Next(5) == 0;
}

public class TemplatePersonality
{
    [OdinSerialize]
    [Category("Personality")]
    [Description("Affects odds of friendship and relationship action success.")]
    public float Charisma { get; set; } = Rand.NextFloat(0, 1);


    [OdinSerialize, ProperName("Randomized")]
    [Category("Personality")]
    public bool CharismaRandomized { get; set; } = true;

    [OdinSerialize]
    [Category("Personality")]
    [Description("Increases vore attack, vore defense, and vore rescue odds.")]
    public float Strength { get; set; } = Rand.NextFloat(0, 1);

    [OdinSerialize, ProperName("Randomized")]
    [Category("Personality")]
    public bool StrengthRandomized { get; set; } = true;

    [OdinSerialize]
    [Category("Vore")]
    [Description(
        "Affects vore attack odds, greater effect than strength.  Also affects max capacity."
    )]
    public float Voracity { get; set; } = Rand.NextFloat(0, 1);

    [OdinSerialize, ProperName("Randomized")]
    [Category("Vore")]
    public bool VoracityRandomized { get; set; } = true;

    [OdinSerialize, ProperName("Sex Drive")]
    [Category("Personality")]
    [Description(
        "Affects how their horniness changes by default.  At under 50% it will fall over time, at over 50% it will rise over time."
    )]
    public float SexDrive { get; set; } = Rand.NextFloat(0, 1);

    [OdinSerialize, ProperName("Randomized")]
    [Category("Personality")]
    public bool SexDriveRandomized { get; set; } = true;

    [OdinSerialize, ProperName("Pred Willingness")]
    [Category("Vore")]
    [Description("Affects how likely they are to want to eat others.")]
    public float PredWillingness { get; set; } = Rand.NextFloat(0, 1);

    [OdinSerialize, ProperName("Randomized")]
    [Category("Vore")]
    public bool PredWillingnessRandomized { get; set; } = true;

    [OdinSerialize, ProperName("Prey Willingness")]
    [Category("Vore")]
    [Description(
        "Affects how much the person wants to be prey. Controls how hard they fight back, and people above the percentage set in game settings are always willing and will not fight back, and can be eaten in public without complaints."
    )]
    public float PreyWillingness { get; set; } = Rand.NextFloat(0, 1);

    [OdinSerialize, ProperName("Randomized")]
    [Category("Vore")]
    public bool PreyWillingnessRandomized { get; set; } = true;

    [OdinSerialize]
    [Category("Personality")]
    [Description(
        "Affects romantic success odds, how much they go after romantic actions, and how comfortable they are with sex / masturbating in the open. "
    )]
    public float Promiscuity { get; set; } = Rand.NextFloat(0, 1);

    [OdinSerialize, ProperName("Randomized")]
    [Category("Personality")]
    public bool PromiscuityRandomized { get; set; } = true;

    [OdinSerialize]
    [Category("Vore")]
    [Description(
        "Affects their pursuit of endosoma as both pred and prey, how often they rub bellies, and how much they're turned on by vore."
    )]
    public float Voraphilia { get; set; } = Rand.NextFloat(0, 1);

    [OdinSerialize, ProperName("Randomized")]
    [Category("Vore")]
    public bool VoraphiliaRandomized { get; set; } = true;

    [OdinSerialize, ProperName("Predator Loyalty")]
    [Category("Vore")]
    [Description(
        "Affects how much friendship / relationships reduce their urge to eat digest someone.  I.e. at 1 they strongly affect it, at 0 there is no consideration. Also affects how likely they are to rescue friends"
    )]
    public float PredLoyalty { get; set; } = Rand.NextFloat(0, 1);

    [OdinSerialize, ProperName("Randomized")]
    [Category("Vore")]
    public bool PredLoyaltyRandomized { get; set; } = true;

    [OdinSerialize, ProperName("Prey Digestion Interest")]
    [Category("Vore")]
    [Description(
        "Controls how much this wants to be digested specifically.  This works together with the prey willingness."
    )]
    public float PreyDigestionInterest { get; set; } = Rand.NextFloat(0, 1);

    [OdinSerialize, ProperName("Randomized")]
    [Category("Vore")]
    public bool PreyDigestionInterestRandomized { get; set; } = true;

    [OdinSerialize]
    [Category("Personality")]
    [Description(
        "Affects how much they seek out social interaction.  Low values cause people to prefer to do things on their own."
    )]
    public float Extroversion { get; set; } = Rand.NextFloat(0, 1);

    [OdinSerialize, ProperName("Randomized")]
    [Category("Personality")]
    public bool ExtroversionRandomized { get; set; } = true;

    [OdinSerialize]
    [Category("Personality")]
    [Description(
        "Affects their preferences for sex actions, and how often they force others to do things."
    )]
    public float Dominance { get; set; } = Rand.NextFloat(0, 1);

    [OdinSerialize, ProperName("Randomized")]
    [Category("Personality")]
    public bool DominanceRandomized { get; set; } = true;

    [OdinSerialize]
    [Category("Personality")]
    [Description(
        "Affects how willing they are to help others in need and how receptive they are for non-romantic interactions."
    )]
    public float Kindness { get; set; } = Rand.NextFloat(0, 1);

    [OdinSerialize, ProperName("Randomized")]
    [Category("Personality")]
    public bool KindnessRandomized { get; set; } = true;

    [OdinSerialize, ProperName("Oral Vore Interest")]
    [Category("Vore")]
    [Description(
        "Controls how much this character favors oral vore in comparison to the other vore types.  At 0 it will actually disable this vore type. Also affects what types they'll ask for."
    )]
    public float OralVoreInterest { get; set; } = Rand.NextFloat(0, 1);

    [OdinSerialize, ProperName("Randomized")]
    [Category("Vore")]
    public bool OralVoreInterestRandomized { get; set; } = true;

    [OdinSerialize, ProperName("Unbirth Interest")]
    [Category("Vore")]
    [Description(
        "Controls how much this character favors unbirth in comparison to the other vore types. At 0 it will actually disable this vore type. Also affects what types they'll ask for."
    )]
    public float UnbirthInterest { get; set; } = Rand.NextFloat(0, 1);

    [OdinSerialize, ProperName("Randomized")]
    [Category("Vore")]
    public bool UnbirthInterestRandomized { get; set; } = true;

    [OdinSerialize, ProperName("Cock Vore Interest")]
    [Category("Vore")]
    [Description(
        "Controls how much this character favors cock vore in comparison to the other vore types.  At 0 it will actually disable this vore type.Also affects what types they'll ask for."
    )]
    public float CockVoreInterest { get; set; } = Rand.NextFloat(0, 1);

    [OdinSerialize, ProperName("Randomized")]
    [Category("Vore")]
    public bool CockVoreInterestRandomized { get; set; } = true;

    [OdinSerialize, ProperName("Anal Vore Interest")]
    [Category("Vore")]
    [Description(
        "Controls how much this character favors anal vore in comparison to the other vore types.  At 0 it will actually disable this vore type.Also affects what types they'll ask for."
    )]
    public float AnalVoreInterest { get; set; } = Rand.NextFloat(0, 1);

    [OdinSerialize, ProperName("Randomized")]
    [Category("Vore")]
    public bool AnalVoreInterestRandomized { get; set; } = true;

    [OdinSerialize, ProperName("Cheats On Partner")]
    [Category("Romance")]
    [Description(
        "Affects whether the person will cheat on someone they're dating.  Also affects what cheating they will care about."
    )]
    public ThreePointScale CheatOnPartner { get; set; } =
        ThreePointScale.Never + (Rand.Next(4) == 0 ? Rand.Next(3) : 0);

    [OdinSerialize, ProperName("Randomized")]
    [Category("Romance")]
    public bool CheatOnPartnerRandomized { get; set; } = true;

    [OdinSerialize, ProperName("Cheating Acceptance")]
    [Category("Romance")]
    [Description(
        "Affects what cheating they will care about.  At none they'll be mad at any cheating, at minor they'll only care if they catch their partner having sex, and at the everything level they won't care at all"
    )]
    public CheatingAcceptance CheatAcceptance { get; set; } =
        CheatingAcceptance.None + (Rand.Next(4) == 0 ? Rand.Next(3) : 0);

    [OdinSerialize, ProperName("Randomized")]
    [Category("Romance")]
    public bool CheatAcceptanceRandomized { get; set; } = true;

    [OdinSerialize, ProperName("Preferred Clothing")]
    [Category("Personality")]
    [Description(
        "Affects how much clothing the character prefers to wear.   They generally won't bother to put clothes back on if they're comfortable with where they're at."
    )]
    public ClothingStatus PreferredClothing { get; set; } =
        ClothingStatus.Normal + (Rand.Next(4) == 0 ? Rand.Next(3) : 0);

    [OdinSerialize, ProperName("Randomized")]
    [Category("Personality")]
    public bool PreferredClothingRandomized { get; set; } = true;

    [OdinSerialize, ProperName("Vore Preference")]
    [Category("Vore")]
    [Description("Can use this to prevent a particular AI from using a vore type.")]
    public VorePreference VorePreference { get; set; } = VorePreference.Either;

    [OdinSerialize, ProperName("Randomized")]
    [Category("Vore")]
    public bool VorePreferenceRandomized { get; set; } = true;

    [OdinSerialize, ProperName("Endosoma Dominator")]
    [Category("Vore")]
    [Description(
        "If enabled, this character will treat endo the same way preds treat vore, i.e. they will eat people against their will -- but not digest them."
    )]
    public bool EndoDominator { get; internal set; }

    [OdinSerialize, ProperName("Randomized")]
    [Category("Vore")]
    public bool EndoDominatorRandomized { get; set; } = true;
}
