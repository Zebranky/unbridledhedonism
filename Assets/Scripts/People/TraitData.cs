using System;
using System.Collections.Generic;
using static HelperFunctions;

class Boosts
{
    internal float RelationshipRepairRate = 1;
    internal float CleanlinessFallRate = 1;
    internal float IncomingDigestionSpeed = 1;
    internal float OutgoingDigestionSpeed = 1;
    internal float HungerRate = 1;
    internal float EnergyUseRate = 1;
    internal float PredatorSkill = 1;
    internal float PredatorDefenseSkill = 1;
    internal float EscapeSkill = 1;
    internal float Capacity = 1;
    internal float HealingRate = 1;
    internal float AttractsPreds = 1;
    internal float MaxMana = 0;
    internal float ManaRegen = 0;
}

class Trait
{
    internal string Description;
    internal int Group;
    internal Func<Person, Settings, bool> Conditional;

    public Trait(string description, int group, Func<Person, Settings, bool> cond = null)
    {
        Description = description;
        Group = group;
        if (cond != null)
            Conditional = cond;
        else
            Conditional = (p, s) => true;
    }
}

class TraitData : Trait
{
    internal Action<Boosts> Boost;

    public TraitData(
        string desc,
        int group,
        Action<Boosts> boost,
        Func<Person, Settings, bool> cond = null
    ) : base(desc, group, cond)
    {
        Boost = boost;
    }
}

static class TraitList
{
    static internal Trait GetTrait(Traits trait)
    {
        traits.TryGetValue(trait, out Trait retTrait);
        return retTrait;
    }

    static internal Trait GetTrait(Quirks trait)
    {
        quirks.TryGetValue(trait, out Trait retTrait);
        return retTrait;
    }

    static readonly Func<Person, Settings, bool> VoreCapable = (p, s) =>
        p.VoreController.CapableOfVore()
        && p.Personality.PredWillingness >= Rand.NextFloat(0.1, 0.7);
    static readonly Func<Person, Settings, bool> VoreAndSwitchCapable = (p, s) =>
        p.VoreController.CapableOfVore()
        && s.CheckDigestion(false, DigestionAlias.CanSwitch)
        && p.Personality.PredWillingness >= Rand.NextFloat(0.1, 0.7);
    static readonly Func<Person, Settings, bool> VoreAndDigestionCapable = (p, s) =>
        p.VoreController.CapableOfVore()
        && s.CheckDigestion(false, DigestionAlias.CanVore)
        && p.Personality.PredWillingness >= Rand.NextFloat(0.1, 0.7);
    static readonly Func<Person, Settings, bool> NotVoreImmune = (p, s) => p.VoreImmune == false;
    static readonly Func<Person, Settings, bool> HasBreasts = (p, s) => p.GenderType.HasBreasts;
    static readonly Func<Person, Settings, bool> CanCast = (p, s) => CanUseMagic(p);
    static readonly Func<Person, Settings, bool> MagicEnabled = (p, s) =>
        State.World.Settings.MagicType != MagicAvailability.NoMagic;

    //static readonly Func<Person, Settings, bool> AlwaysFalse = (p, s) => false;

    static Dictionary<Traits, Trait> traits = new Dictionary<Traits, Trait>()
    {
        [Traits.PredRespect] = new Trait(
            "Less likely to attempt to vore other predators.",
            1,
            VoreCapable
        ),
        [Traits.PredHunter] = new Trait(
            "More likely to attempt to vore other predators.",
            1,
            VoreCapable
        ),
        [Traits.RomanticPred] = new Trait(
            "Will only vore their romantic partners (dating or at a certain romantic level).",
            1,
            VoreCapable
        ),
        [Traits.AbductorPred] = new Trait(
            "Will hide in their room when digesting prey and wait until they finish absorbing to come out.",
            2,
            VoreAndDigestionCapable
        ),
        [Traits.BlackoutPred] = new Trait(
            "Will return to their room or use bed in current room when digesting prey and sleep until they finish absorbing.",
            2,
            VoreAndDigestionCapable
        ),
        [Traits.SirenPred] = new Trait(
            "Will attempt to lure people back to their room to eat them more often.",
            3,
            VoreCapable
        ),
        [Traits.NoVoreZone] = new Trait(
            "Will never attempt to lure others to their room for vore.",
            3,
            VoreCapable
        ),
        [Traits.Vengeful] = new Trait(
            "Will develop a <i>vendetta</i> against anyone that tries to harm them or tries to eat one of their friends.",
            4,
            VoreCapable
        ),
        [Traits.LovesPrivateSex] = new Trait(
            "Far more likely to accept sex when in a dorm room.",
            5
        ),
        [Traits.Intimidating] = new Trait(
            "Others are significantly less likely to try and free a prey from this predator.",
            6,
            VoreCapable
        ),
        [Traits.DoesNotForce] = new Trait(
            "Will only ask for vore, and will never use force (ignored if this character has vendetta against other person).",
            7,
            VoreCapable
        ),
        [Traits.LovesHugs] = new Trait(
            "Really likes giving hugs, and does so more often.",
            8
        ),
        [Traits.TentativeRomance] = new Trait(
            "Far less likely to try romance with people they don't know well, but is more receptive to advances.",
            9
        ),
        [Traits.Jerk] = new Trait(
            "Considerably more likely to do unfriendly actions.",
            10
        ),
        [Traits.VoraciousLover] = new Trait(
            "Has a higher chance of consuming their partner during sex.",
            11,
            VoreCapable
        ),
        [Traits.Gassy] = new Trait("Uses the burp interaction far more often when it can.", 12),
        [Traits.FullTour] = new Trait(
            "Likes to move orally swallowed prey on through to the bowels.",
            13,
            (p, s) =>
                p.VoreController.CapableOfVore()
                && s.AnalVoreGoesDirectlyToStomach
                && p.Personality.PredWillingness >= Rand.NextFloat(0.1, 0.7)
        ),
        [Traits.Traitor] = new Trait(
            "Has a higher chance of sneakily switching from endo to digestion. (Also lowers the chance they'll spare, to make it harder get them to stop this digestion).",
            14,
            VoreAndSwitchCapable
        ),
        [Traits.Unyielding] = new Trait(
            "Begging or trying to convince this character to spare is impossible.",
            15,
            VoreCapable
        ),
        [Traits.PersistentBeggar] = new Trait(
            "More likely to beg when prey, and doesn't give up in asking. (Disabled by default so it doesn't annoy players).",
            16,
            NotVoreImmune
        ),
        [Traits.PrefersAction] = new Trait(
            "If they're in someone, they want there to be action (digestion) (Increases want to be digested, sharply decreases want to be endoed).",
            17,
            NotVoreImmune
        ),
        [Traits.ShyPred] = new Trait("Predator will almost never hunt for prey.", 18, VoreCapable),
        [Traits.ConfusedAppetites] = new Trait(
            "More likely to initiate vore when horny.",
            19,
            VoreCapable
        ),
        [Traits.SelectivelyWilling] = new Trait(
            "Will only ever be willing to people they're close with.",
            20
        ),
        [Traits.NeverWilling] = new Trait("Never willing to be eaten, unless under mind control.", 20),
        [Traits.LoversEmbrace] = new Trait(
            "Will not consume their partner during sex.",
            11,
            VoreCapable
        ),
        [Traits.Unapproachable] = new Trait(
            "Others are less likely to approach this character when they aren't friends.",
            21
        ),
        [Traits.PrefersLivingPrey] = new Trait(
            "Strongly prefers looking for prey when hungry, and will only go to the cafeteria as a last resort.",
            22,
            VoreCapable
        ),
        [Traits.Jealous] = new Trait(
            "Has a chance of developing a <i>vendetta</i> against rival predators when witnessing vore.",
            23,
            VoreCapable
        ),
        [Traits.Uncaring] = new Trait(
            "Has no negative reaction towards a predator when one of their friends is eaten, and won't try to save them, either.",
            24,
            VoreCapable
        ),
        [Traits.Possessive] = new Trait(
            "Develops a <i>vendetta</i> against anyone that flirts/cheats with their partner. Happens regardless of cheating tolerance.",
            25,
            VoreCapable
        ),
        [Traits.OrientationFeeding] = new Trait(
            "Will only eat people that appeal to their sexual orientation.",
            26,
            VoreCapable
        ),
        [Traits.RarelyShowers] = new Trait("Character will very rarely shower, if ever.", 27),
        [Traits.GymRat] = new Trait("Character will work out at gyms more often.", 28),
        [Traits.Bookworm] = new Trait("Character will read more often in libraries.", 29),
        [Traits.SexAddict] = new Trait(
            "Will seek out sex and masturbate more often.",
            30
        ),
        [Traits.Talkative] = new Trait(
            "More inclined to communicate with others and is much more receptive to conversation.",
            31
        ),
        [Traits.BingeEater] = new Trait(
            "Likely to hunt again immediately after consuming prey.",
            32,
            VoreCapable
        ),
        [Traits.LovesKissing] = new Trait(
            "Really likes kissing, and does so more often.",
            33
        ),
        [Traits.DemandsWorship] = new Trait(
            "More likely to ask others to rub their belly, and will never turn down rubs.",
            34,
            VoreCapable
        ),
        [Traits.HornyWhenFull] = new Trait(
            "More likely to masturbate and ask for sex when full of prey.",
            35,
            VoreCapable
        ),
        [Traits.EasyShow] = new Trait(
            "More likely to strip for others, especially those they know.",
            36
        ),
        [Traits.Exhibitionist] = new Trait(
            "More willing to strip, have sex, or masturbate in public.",
            37
        ),
        [Traits.SexOnly] = new Trait("Character will not masturbate to relieve themselves.", 38),
        [Traits.BellyFixation] = new Trait(
            "More likely to rub bellies or ask for belly rubs.",
            39
        ),
        [Traits.Greedy] = new Trait(
            "Will never free endo prey on their own, and is much less willing to free when asked.",
            40,
            VoreCapable
        ),
        [Traits.StockholmSyndrome] = new Trait(
            "Being inside another character will slowly raise this character's opinion of them.",
            41
        ),
        [Traits.Opportunist] = new Trait(
            "More likely to hunt weakened or distracted prey (sleeping, masturbating, or having sex).",
            42,
            VoreCapable
        ),
        [Traits.Sadistic] = new Trait(
            "Prefers to target unwilling prey, will solicit sex from their victims loved ones, and will keep injured prey trapped inside.",
            43,
            VoreCapable
        ),
        [Traits.LustDevourer] = new Trait(
            "Will target prey who are very horny, currently masturbating, or having sex.",
            44,
            VoreCapable
        ),
        [Traits.Flirtatious] = new Trait(
            "More likely to flirt with or compliment others.",
            45
        ),
        [Traits.Widowmaker] = new Trait(
            "Prefers to target prey who are in romantic relationships.",
            46,
            VoreCapable
        ),
        [Traits.Bully] = new Trait(
            "Prefers to target prey who they have previously eaten.",
            47,
            VoreCapable
        ),
        [Traits.PreySlut] = new Trait(
            "Much more willing to be eaten when horny.",
            48,
            (p, s) =>
                p.Personality.PreyWillingness >= Rand.NextFloat(0.3, 0.7)
        ),
        [Traits.ForgetfulPred] = new Trait(
            "Likely to accidentally digest prey while sleeping.",
            49, 
            VoreCapable
        ),
        [Traits.HeavySleeper] = new Trait(
            "Will rest for a long time and is very difficult to wake up.",
            50
        ),
        [Traits.DownToFuck] = new Trait(
            "Will accept sex in their dorm room at any time, no questions asked.",
            5
        ),
        [Traits.Slutty] = new Trait(
            "Likes to have sex with or make out with new people.",
            51
        ),
        [Traits.Masochist] = new Trait(
            "Being pushed around or insulted makes them horny.",
            52
        ),


        // MAGIC TRAITS
        [Traits.GuineaPig] = new Trait(
            "Does not get upset when magic is cast on them.",
            60,
            MagicEnabled
        ),
        [Traits.RoomInvader] = new Trait(
            "Will use magic to get into other rooms to reach people.",
            61,
            CanCast
        ),
    };

    static Dictionary<Quirks, Trait> quirks = new Dictionary<Quirks, Trait>()
    {
        [Quirks.Forgiving] = new TraitData(
            "Negative relations recover faster than normal.",
            1,
            (s) => s.RelationshipRepairRate *= 2
        ),
        [Quirks.Unforgiving] = new TraitData(
            "Negative relations don't recover automatically.",
            1,
            (s) => s.RelationshipRepairRate *= 0
        ),
        [Quirks.FastDigestion] = new TraitData(
            "Digests and absorbs prey faster than usual.",
            2,
            (s) => s.OutgoingDigestionSpeed *= 1.5f,
            VoreAndDigestionCapable
        ),
        [Quirks.SlowDigestion] = new TraitData(
            "Digests and absorbs prey slower than usual.",
            2,
            (s) => s.OutgoingDigestionSpeed *= .666f,
            VoreAndDigestionCapable
        ),
        [Quirks.FilthMagnet] = new TraitData(
            "Becomes dirty faster than usual.",
            3,
            (s) => s.CleanlinessFallRate *= 1.65f
        ),
        [Quirks.DirtRepellant] = new TraitData(
            "Stays clean for longer than usual.",
            3,
            (s) => s.CleanlinessFallRate *= 0.5f
        ),
        [Quirks.Gluttonous] = new TraitData(
            "Becomes hungry faster than usual.",
            4,
            (s) => s.HungerRate *= 2
        ),
        [Quirks.WeakAppetite] = new TraitData(
            "Becomes hungry slower than usual.",
            4,
            (s) => s.HungerRate *= .6f
        ),
        [Quirks.EasilyTired] = new TraitData(
            "Uses up energy faster than usual.",
            5,
            (s) => s.EnergyUseRate *= 1.5f
        ),
        [Quirks.HighVitality] = new TraitData(
            "Uses up energy slower than usual.",
            5,
            (s) => s.EnergyUseRate *= .75f
        ),
        [Quirks.Claustrophobic] = new TraitData(
            "Gets overwhelmed and isn't very good at escaping.",
            6,
            (s) => s.EscapeSkill *= 0.6f,
            NotVoreImmune
        ),
        [Quirks.EscapeArtist] = new TraitData(
            "Especially good at escaping.",
            6,
            (s) => s.EscapeSkill *= 1.65f,
            NotVoreImmune
        ),
        [Quirks.SlipperyWhenWet] = new Trait("Harder to vore when horny.", 7, NotVoreImmune),
        [Quirks.SmartStruggle] = new Trait("Struggles take less energy.", 8, NotVoreImmune),
        [Quirks.Rescuer] = new Trait("Significant boost to rescuing odds.", 9),
        [Quirks.PredWorship] = new Trait(
            "Witnessing an act of vore will boost their relationship towards the predator, also increases the odds of belly rubs and shoving prey in.",
            10
        ),
        [Quirks.GutSlut] = new Trait(
            "Being eaten will raise their opinion of their predator.",
            11,
            NotVoreImmune
        ),
        [Quirks.LoveBites] = new Trait(
            "Vore success rate rises at high levels of horniness.",
            12,
            VoreCapable
        ),
        [Quirks.GreatHunger] = new Trait(
            "Vore success rate rises as hunger rises.",
            13,
            VoreCapable
        ),
        [Quirks.ViciousPredator] = new Trait(
            "Vore success rate increases against disliked targets.",
            14,
            VoreCapable
        ),
        [Quirks.BiggerFish] = new Trait(
            "Vore success rate increases against predators.",
            15,
            VoreCapable
        ),
        [Quirks.LowOnTheChain] = new Trait(
            "Vore success rate increases against non-predators.",
            15,
            VoreCapable
        ),
        [Quirks.PredInPrivate] = new Trait(
            "Vore success rate increases when prey is isolated.",
            16,
            VoreCapable
        ),
        [Quirks.SkilledEscape] = new Trait(
            "Escape rate increased when not tired, and decreased when exhausted.",
            17,
            NotVoreImmune
        ),
        [Quirks.Charmer] = new Trait(
            "Romantic relationships increase faster than normal. Character more likely to use the charm spell.",
            18
        ),
        [Quirks.HighCapacity] = new TraitData(
            "Can hold twice as many prey as their stats would otherwise allow.",
            19,
            (s) => s.Capacity *= 2,
            VoreCapable
        ),
        [Quirks.UnrealCapacity] = new TraitData(
            "Can hold ten times as many prey as their stats would otherwise allow.",
            19,
            (s) => s.Capacity *= 10,
            VoreCapable
        ),
        [Quirks.ElasticPred] = new TraitData(
            "Can hold a limitless amount prey of any size and ignores size penalties from eating larger prey.",
            19,
            (s) => s.Capacity = float.PositiveInfinity,
            VoreCapable
        ),
        [Quirks.LimitedCapacity] = new Trait(
            "Will only ever consume one prey at a time.",
            19,
            VoreCapable
        ),
        [Quirks.Insatiable] = new Trait(
            "Remains fairly horny even after an orgasm.",
            20
        ),
        [Quirks.ResilientBody] = new TraitData(
            "Takes much longer to become digested and absorbed.",
            21,
            (s) => s.IncomingDigestionSpeed *= .35f,
            NotVoreImmune
        ),
        [Quirks.EasyToDigest] = new TraitData(
            "Is digested and absorbed much faster than usual prey.",
            21,
            (s) => s.IncomingDigestionSpeed *= 2,
            NotVoreImmune
        ),
        [Quirks.FastHealer] = new TraitData(
            "Restores health faster than usual.",
            22,
            (s) => s.HealingRate *= 2,
            NotVoreImmune
        ),
        [Quirks.PreyPheromones] = new TraitData(
            "Predators are more likely to target this person.",
            23,
            (s) => s.AttractsPreds *= 1.25f,
            NotVoreImmune
        ),
        [Quirks.Untasty] = new TraitData(
            "Predators are less likely to target this person.",
            23,
            (s) => s.AttractsPreds *= 0.65f,
            NotVoreImmune
        ),
        [Quirks.FearsomeReputation] = new Trait(
            "Vore success chance increases slightly for every person they've digested (up to 20).",
            24,
            VoreCapable
        ),
        [Quirks.Prude] = new Trait(
            "Uncomfortable seeing others have sex, and dislikes those who do sexual acts in public.",
            25,
            VoreCapable
        ),
        [Quirks.AphrodisiacInsides] = new Trait(
            "Any prey inside them will be turned on more than usual (adds to any turn-on from prey voraphilia).",
            26,
            VoreCapable
        ),
        [Quirks.Olfactophilic] = new Trait(
            "Much more attracted to sweaty/dirty characters. Unlocks new sex actions.",
            28
        ),
        [Quirks.FootFetish] = new Trait(
            "Interactions may involve feet. Unlocks new sex actions.",
            29
        ),
        [Quirks.Motherly] = new Trait(
            "Others are more likely to rub this character's belly. Interactions may involve lactation or mommy content. Unlocks new sex actions.",
            30,
            HasBreasts
        ),
        [Quirks.FatteningMeal] = new Trait(
            "If this character is digested, their predator gains much more weight.",
            31
        ),
        [Quirks.SlowMetabolism] = new Trait("Loses weight slower than usual.", 32, VoreCapable),
        [Quirks.Gainer] = new Trait("Gains weight faster than usual.", 33, VoreCapable),
        [Quirks.LustfulDigestion] = new Trait(
            "Digests prey faster when horny, and deals increased digestion damage when having an orgasm.",
            34,
            VoreCapable
        ),
        [Quirks.Digestible] = new Trait(
            "Predators are much more interested in digesting this person.",
            35,
            NotVoreImmune
        ),
        [Quirks.PredPheromones] = new Trait(
            "Prey are more likely to offer selves to this person.",
            23,
            VoreCapable
        ),
        [Quirks.ArmpitFetish] = new Trait(
            "Interactions may involve armpits. Unlocks new sex actions.",
            36
        ),
        [Quirks.MilkyBoobs] = new Trait(
            "Interactions may involve lactation. Unlocks new sex actions.",
            30,
            HasBreasts
        ),

        // MAGIC
        [Quirks.Intelligent] = new TraitData(
            "Has increased spell success rate and a slightly higher maximum mana.",
            60,
            (s) => s.MaxMana += 1,
            CanCast
        ),
        [Quirks.WeakWilled] = new Trait(
            "More susceptible to magic effects.",
            61,
            MagicEnabled
        ),
        [Quirks.VastMind] = new TraitData(
            "Has higher maximum mana for spells casting.",
            62,
            (s) => s.MaxMana += 2,
            CanCast
        ),
        [Quirks.MagicalLust] = new Trait(
            "Magical success rate rises at high levels of horniness.",
            63,
            CanCast
        ),
        [Quirks.ManaRegenerator] = new TraitData(
            "Generates mana at a higher rate.",
            64,
            (s) => s.ManaRegen += 2,
            CanCast
        ),
        [Quirks.ManaVampire] = new Trait(
            "Drains mana when a target orgasms or when breastfeeding from them.",
            65,
            CanCast
        ),
        [Quirks.ManaDonor] = new Trait(
            "Grants the target mana when orgasming or when breastfeeding them.",
            66,
            MagicEnabled
        ),
        [Quirks.Transmuter] = new Trait(
            "Has increased success rate of growth/shrink spells, and uses them more often.",
            67,
            CanCast
        ),


        // CHEAT QUIRKS
        [Quirks.PerfectAttack] = new Trait(
            "Vore attacks made by this person always succeed unless the prey has the perfect defense trait. <i>Cheat Quirk</i>.",
            124,
            VoreCapable
        ),
        [Quirks.PerfectDefense] = new Trait(
            "Vore attacks against this person always fail unless the pred has the perfect attack trait. <i>Cheat Quirk</i>.",
            125,
            NotVoreImmune
        ),
        [Quirks.PerfectEscape] = new Trait(
            "Will always escape when they try, unless the pred has the perfect reflexes trait. <i>Cheat Quirk</i>.",
            126,
            NotVoreImmune
        ),
        [Quirks.PerfectReflexes] = new Trait(
            "Prey will always fail to escape this character when they try, unless the prey has the perfect escape trait. <i>Cheat Quirk</i>.",
            127,
            VoreCapable
        ),
        [Quirks.SexGuru] = new Trait(
            "Can use all sex actions, regardless of traits or dominance stat. <i>Cheat Quirk</i>.",
            128
        ),
        [Quirks.FreeEntry] = new Trait(
            "Can enter other character's dorm rooms. <i>Cheat Quirk</i>.",
            129
        ),
        [Quirks.PerfectSpells] = new Trait(
            "Spells cast by this character are always successful. <i>Cheat Quirk</i>.",
            130,
            CanCast
        ),
        [Quirks.LimitlessMagic] = new Trait(
            "Does not consume mana when casting spells. <i>Cheat Quirk</i>.",
            131,
            CanCast
        ),
    };
}
