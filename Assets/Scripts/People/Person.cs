﻿using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using OdinSerializer;
using System.Linq;
using Assets.Scripts.People;

public enum TaskStep : byte
{
    None,
    Moving,
    Acting
}

public enum ClothingStatus
{
    Normal,
    Underwear,
    Nude
}

public enum ThreePointScale
{
    Never,
    Rarely,
    Frequently,
}

public enum CheatingAcceptance
{
    None,
    Minor,
    Everything,
}

public class Person
{
    public Person(
        string firstName,
        string lastName,
        int gender,
        Orientation orientation,
        bool canVore,
        Vec2 position,
        Race race = null
    )
    {
        FirstName = firstName;
        Picture = firstName;
        LastName = lastName;
        Gender = gender;
        if (race == null)
            race = RaceManager.GetRace("Human");
        Race = race.Name;
        PartList = Create.Random(GenderType, race);
        Position = position;
        Needs = new Needs(this);
        Magic = new Magic(this);
        AI = new AI(this);
        Personality = new Personality();
        Events = new List<Record>();
        Romance = new Romance(this, orientation);
        Health = Constants.HealthMax;
        VoreController = new VoreController(this, canVore);
        MiscStats = new MiscStats();
        VoreTracking = new List<VoreTrackingRecord>();
        if (State.World?.TraitWeights != null)
        {
            Traits = State.World.TraitWeights.AssignTraits(this);
            Quirks = State.World.TraitWeights.AssignQuirks(this);
        }
        else
        {
            Traits = new List<Traits>();
            Quirks = new List<Quirks>();
        }
    }

    [OdinSerialize]
    public PartList PartList;

    [OdinSerialize, ProperName("First Name")]
    [Description("The first name for this person")]
    [Category("General")]
    public string FirstName;

    [OdinSerialize, ProperName("Last Name")]
    [Description("The last name for this person")]
    [Category("General")]
    public string LastName;

    [OdinSerialize, ProperName("Picture")]
    [Description("The filename prefix for custom images of this person")]
    [Category("General")]
    internal string Picture;

    [OdinSerialize, ProperName("Gender")]
    [GenderDescription]
    [Category("General")]
    [IntegerRange(0, 5)]
    public int Gender;

    [OdinSerialize, VariableEditorIgnores]
    public int ID;

    [OdinSerialize]

    public Personality Personality;

    [OdinSerialize]
    public Romance Romance;

    [OdinSerialize]
    public Needs Needs;

    [OdinSerialize]
    public Magic Magic;

    [OdinSerialize]
    public List<Relationship> Relationships = new List<Relationship>();

    [OdinSerialize]
    internal Vec2 Position;

    [OdinSerialize]
    public VoreController VoreController;

    [OdinSerialize]
    public List<VoreTrackingRecord> VoreTracking;

    [OdinSerialize]
    public ActiveSex ActiveSex;

    [OdinSerialize, ProperName("Clothing Level")]
    [Description("How clothed this character currently is")]
    [Category("General")]
    public ClothingStatus ClothingStatus;

    [OdinSerialize]
    [VariableEditorIgnores]
    public bool BeingEaten;

    [VariableEditorIgnores]
    public bool EatenDuringSex;

    [VariableEditorIgnores]
    internal bool BeingSwallowed => BeingEaten && (GetMyVoreProgress()?.IsSwallowing() ?? false);

    [OdinSerialize]
    internal AI AI;

    [OdinSerialize]
    readonly List<Record> Events;

    [OdinSerialize]
    [VariableEditorIgnores]
    public int LibrarySatisfaction;

    [OdinSerialize]
    [VariableEditorIgnores]
    public int GymSatisfaction;

    private Boosts _boosts;

    internal Boosts Boosts
    {
        get
        {
            if (_boosts == null)
                RecalculateBoosts();
            return _boosts;
        }
        set => _boosts = value;
    }

    [OdinSerialize]
    internal List<Traits> Traits;

    [OdinSerialize]
    internal List<Quirks> Quirks;

    [OdinSerialize, ProperName("Predator Tier")]
    [Description("Characters are incapable of digesting anyone above their tier.")]
    [Category("General")]
    [IntegerRange(0, 20)]
    internal int PredatorTier;

    [OdinSerialize, ProperName("Team")]
    [Description(
        "Characters will not be able to digest anyone on their team.  Characters with a team of 0 are considered to not be on a team."
    )]
    [Category("General")]
    [IntegerRange(0, 20)]
    internal int Team;

    [OdinSerialize]
    [VariableEditorIgnores]
    internal string Race;

    [OdinSerialize]
    [VariableEditorIgnores]
    internal int TurnsSinceOrgasm;

    [OdinSerialize]
    [VariableEditorIgnores]
    internal InteractionType StreamingAction;

    [OdinSerialize]
    internal Person StreamingTarget;

    [OdinSerialize]
    [VariableEditorIgnores]
    internal SelfActionType StreamingSelfAction;
    
    [OdinSerialize]
    [VariableEditorIgnores]
    internal InteractionType LastAction;

    [OdinSerialize]
    internal Person LastTarget;

    [OdinSerialize]
    [VariableEditorIgnores]
    internal SelfActionType LastSelfAction;

    [OdinSerialize]
    [VariableEditorIgnores]
    internal SexInteractionType LastSexAction;

    [OdinSerialize]
    [VariableEditorIgnores]
    internal InteractionType ThisAction;

    [OdinSerialize]
    internal Person ThisTarget;

    [OdinSerialize]
    [VariableEditorIgnores]
    internal SelfActionType ThisSelfAction;

    [OdinSerialize]
    [VariableEditorIgnores]
    internal SexInteractionType ThisSexAction;

    [OdinSerialize]
    [VariableEditorIgnores]
    internal int StreamedTurns;

    [OdinSerialize]
    public MiscStats MiscStats;

    [OdinSerialize]
    internal Vec2 MyRoom;

    [OdinSerialize]
    [VariableEditorIgnores]
    public int Health;

    [OdinSerialize]
    internal List<DisposalData> Disposals = new List<DisposalData>();

    [OdinSerialize, ProperName("Character's Label")]
    [Description(
        "How the character is labeled on the world map.  This value is ignored if you're using initials instead"
    )]
    [Category("General")]
    internal string Label;

    [VariableEditorIgnores]
    internal string Initials =>
        $"{FirstName.FirstOrDefault()}{LastName.FirstOrDefault()}".Replace('\0', ' '); //Replace string terminators...

    [VariableEditorIgnores]
    public bool Dead => Health <= 0;

    [OdinSerialize]
    [VariableEditorIgnores]
    internal ClothingStatus ClothesInTile;

    [OdinSerialize]
    [VariableEditorIgnores]
    internal bool Gone = false;

    [OdinSerialize]
    [VariableEditorIgnores]
    internal bool Stunned = false;

    [OdinSerialize, ProperName("Vore Immunity")]
    [Description("Makes the character completely unvorable, to both regular vore and endosoma")]
    [Category("General")]
    public bool VoreImmune;

    [OdinSerialize, ProperName("Digestion Immunity")]
    [Description("Makes the character unable to be digested.")]
    [Category("General")]
    public bool DigestionImmune;

    [OdinSerialize, ProperName("Always Reform")]
    [Description("Makes the character always reform, even if reformation is disabled in general.")]
    [Category("General")]
    public bool AlwaysReform;

    [OdinSerialize]
    internal Person LastSex;

    [OdinSerialize]
    [VariableEditorIgnores]
    internal int LastSexTurn;

    public GenderType GenderType =>
        State.World?.GenderList?.List[Gender]
        ?? State.GameManager?.StartScreen?.GenderList?.List[Gender]
        ?? State.BackupGenderList.List[Gender]
        ?? new GenderList().List[Gender];

    [VariableEditorIgnores]
    public bool CharacterFullyClothed => ClothingStatus == ClothingStatus.Normal;

    [VariableEditorIgnores]
    public bool CharacterInUnderwear => ClothingStatus == ClothingStatus.Underwear;

    [VariableEditorIgnores]
    public bool CharacterNude => ClothingStatus == ClothingStatus.Nude;

    [VariableEditorIgnores]
    public bool CharacterMasturbating => StreamingSelfAction == SelfActionType.Masturbate;

    [VariableEditorIgnores]
    public bool CharacterMakingOut => StreamingSelfAction == SelfActionType.Masturbate;

    [VariableEditorIgnores]
    public bool CharacterResting => StreamingSelfAction == SelfActionType.Rest;

    [VariableEditorIgnores]
    public bool CharacterControlled => this == State.World.ControlledPerson;

    [VariableEditorIgnores]
    public bool CharacterPrefersDigestion => Personality.VorePreference == VorePreference.Digestion;

    [VariableEditorIgnores]
    public bool CharacterPrefersEndo => Personality.VorePreference == VorePreference.Endosoma;

    internal string GetFullName()
    {
        if (string.IsNullOrWhiteSpace(LastName))
            return FirstName;
        return $"{FirstName} {LastName}";
    }

    internal string GetFirstNameWithLink()
    {
        return $"<link={ID}>{FirstName}</link>";
    }

    internal string GetFullNameWithLink()
    {
        if (string.IsNullOrWhiteSpace(LastName))
            return $"<link={ID}>{FirstName}</link>";
        return $"<link={ID}>{FirstName} {LastName}</link>";
    }

    /// <summary>
    /// Ends actions for self, and target
    /// </summary>
    internal void EndStreamingActions()
    {
        if (VoreController.CurrentSwallow(VoreLocation.Any) != null)
        {
            UnityEngine.Debug.Log("Prey freed due to interruption");
            VoreController.CurrentSwallow(VoreLocation.Any).FreePrey(true);
        }

        if (StreamingTarget != null && StreamingTarget.StreamingTarget == this)
        {
            StreamingTarget.StreamingAction = InteractionType.None;
            StreamingTarget.StreamingTarget = null;
        }
        StreamingAction = InteractionType.None;
        StreamingTarget = null;
        StreamingSelfAction = SelfActionType.None;
        if (ActiveSex != null)
        {
            LastSex = ActiveSex.Other;
            LastSexTurn = State.World.Turn;
            if (ActiveSex.Other.BeingEaten)
            {
                InteractionList.List[InteractionType.SexPartnerEaten].OnSucceed(
                    this,
                    ActiveSex.Other,
                    true
                );
            }
            ActiveSex.Other.ActiveSex = null;
        }

        ActiveSex = null;
    }

    public int CheatWillingness() // returns 2 if frequent cheater, 1 if rarely cheats, 0 if never cheats
    {
        if (this.Personality.CheatOnPartner == ThreePointScale.Frequently)
            return 2;
        else if (this.Personality.CheatOnPartner == ThreePointScale.Rarely)
            return 1;
        else
            return 0;
    }

    public Person FindBestFriend() // returns the Person with whom this character has the highest friendship, does not include someone they are dating
    {
        Relationship temp = Relationships
            .Where(r => r.Target != this.Romance.Dating)
            .OrderByDescending(r => r.FriendshipLevel)
            .FirstOrDefault();
        if (temp != null)
        {
            return temp.Target;
        }
        else
            return null;
    }

    public Person FindNemesis() // returns the Person with whom this character has the lowest friendship, does not include someone they are dating
    {
        Relationship temp = Relationships
            .Where(r => r.Target != this.Romance.Dating)
            .OrderBy(r => r.FriendshipLevel)
            .FirstOrDefault();
        if (temp != null)
        {
            return temp.Target;
        }
        else
            return null;
    }

    public Person FindCrush() // returns the Person with whom this character has the highest romantic interest in, does not include someone they are dating
    {
        Relationship temp = Relationships
            .Where(r => r.Target != this.Romance.Dating)
            .OrderByDescending(r => r.RomanticLevel)
            .FirstOrDefault();
        if (temp != null)
        {
            return temp.Target;
        }
        else
            return null;
    }

    public Person FindVendetta() // returns the most recent Person that this character has a vendetta with
    {
        Relationship temp = Relationships.Where(r => r.Vendetta).FirstOrDefault();
        if (temp != null)
        {
            return temp.Target;
        }
        else
            return null;
    }

    public Person FindMyPredator()
    {
        return State.World
            .GetPeople(true)
            .Where(s => s.VoreController.GetProgressOf(this) != null)
            .FirstOrDefault();
    }

    public bool PredHavingSex()
    {
        var pred = State.World
            .GetPeople(true)
            .Where(s => s.VoreController.GetProgressOf(this) != null)
            .FirstOrDefault();
        if (pred == null)
            return false;
        return pred.ActiveSex != null;
    }

    public bool PredMasturbating()
    {
        var pred = State.World
            .GetPeople(true)
            .Where(s => s.VoreController.GetProgressOf(this) != null)
            .FirstOrDefault();
        if (pred == null)
            return false;
        return pred.StreamingSelfAction == SelfActionType.Masturbate;
    }

    public VoreProgress GetMyVoreProgress()
    {
        foreach (var person in State.World.GetPeople(true))
        {
            var progress = person.VoreController.GetProgressOf(this);
            if (progress != null)
                return progress;
        }
        return null;
    }

    internal void AddEvent(Record record)
    {
        Events.Add(record);
        if (State.World.Settings.LogAllText)
        {
            State.TextLogger.AddText(record);
        }
    }

    internal void AddEvent(Record record, SexInteractionType type)
    {
        if (type < SexInteractionType.KissVore)
        {
            Events.Add(record);
            if (State.World.Settings.LogAllText)
            {
                State.TextLogger.AddText(record);
            }
        }
    }

    internal void AddTraitFromList(List<Traits> traitsList)
    {
        var randTrait = Rand.Next(traitsList.Count);
        Traits.Remove(traitsList[randTrait]);
        Traits.Add(traitsList[randTrait]);
    }
    internal void AddQuirkFromList(List<Quirks> quirksList)
    {
        var randQuirk = Rand.Next(quirksList.Count);
        Quirks.Remove(quirksList[randQuirk]);
        Quirks.Add(quirksList[randQuirk]);
    }

    internal void WitnessEvent(
        Record record,
        Person actor,
        Person target,
        InteractionType type,
        bool soundOnly,
        bool success,
        bool displayText = true
    )
    {
        //Eventually this should log to a seperate log
        if (displayText)
        {
            if (record.ToString() != "{SUPPRESS}")
            {
                bool denied = CheckForSuppressedVore();
                if (denied == false)
                    AddEvent(record);
            }
        }

        if (Dead)
            return;
        if (
            InteractionList.List[type].Class == ClassType.VoreUnwilling
            && (actor.FindMyPredator()?.BeingEaten ?? false) == false
        )
        {
            if (actor.FindMyPredator().Position.GetNumberOfMovesDistance(Position) < 7)
            {
                AI.DiscoveredVoreAttempt(actor);
            }

            if (
                soundOnly == false
                && (Personality.Voraphilia > .4f)
                && Romance.Orientation != Orientation.Asexual
            )
            {
                Needs.Horniness = Utility.PushTowardOne(
                    Needs.Horniness,
                    Personality.SexDrive * .08f * Personality.Voraphilia
                );
                if (actor == this || target == this)
                    SelfActionList.List[SelfActionType.TurnedOnByOwnVore].OnDo(this);
                else
                    SelfActionList.List[SelfActionType.TurnedOnByVore].OnDo(this);
            }
        }
        else if (
            InteractionList.List[type].Class == ClassType.Vore
            && actor.Position.GetNumberOfMovesDistance(target.Position) < 7
        )
        {
            //I think this is sound now, though slightly indirect.
            VoreProgress progress =
                actor?.VoreController.GetProgressOf(target)
                ?? target?.VoreController.GetProgressOf(actor)
                ?? target?.FindMyPredator()?.VoreController.GetProgressOf(target)
                ?? actor?.FindMyPredator()?.VoreController.GetProgressOf(actor);
            if (
                progress != null
                && progress.IsSwallowing()
                && progress.Willing == false
                && (progress.Target.FindMyPredator()?.BeingEaten ?? false) == false
            )
            {
                AI.DiscoveredVoreAttempt(progress.Target);
            }
            if (
                soundOnly == false
                && (Personality.Voraphilia > .4f)
                && Romance.Orientation != Orientation.Asexual
            )
            {
                Needs.Horniness = Utility.PushTowardOne(
                    Needs.Horniness,
                    Personality.SexDrive * .08f * Personality.Voraphilia
                );
                if (actor == this || target == this)
                    SelfActionList.List[SelfActionType.TurnedOnByOwnVore].OnDo(this);
                else
                    SelfActionList.List[SelfActionType.TurnedOnByVore].OnDo(this);
            }
        }
        bool CheckForSuppressedVore()
        {
            //This is only for actions that don't involve one of the participants, direct ones are handled in InteractionBase.cs
            if (InteractionList.List[type].Class != ClassType.Vore)
                return false;
            switch (type)
            {
                case InteractionType.PreyBeg:
                case InteractionType.PreyStruggle:
                case InteractionType.PreyViolentStruggle:
                case InteractionType.PreySootheOtherPrey:
                    if (success)
                        return true;
                    break;

                case InteractionType.PreyScream:
                case InteractionType.PreyTalkToAnotherPrey:
                case InteractionType.PreyKissOtherPrey:
                case InteractionType.PreyFondleOtherPrey:
                case InteractionType.PreyWillingYell:
                case InteractionType.PreyWillingSquirm:
                case InteractionType.PreyWillingMasturbate:
                case InteractionType.PreyWillingBellyRub:
                case InteractionType.PreyWillingWombRub:
                case InteractionType.PreyWillingBallsRub:
                case InteractionType.PreyWillingAnusRub:
                case InteractionType.PreyWillingNap:
                case InteractionType.PreyWait:
                case InteractionType.PreyRecover:
                case InteractionType.PreyMasturbate:
                case InteractionType.PreyMeet:
                    break;

                default:
                    return false;
            }
            return Config.SuppressVoreMessages > Rand.NextFloat(0, 1);
        }
    }

    internal void WitnessEvent(Record record, SexInteractionType type)
    {
        //Eventually this should log to a seperate log
        if (type < SexInteractionType.KissVore)
            AddEvent(record);
    }

    internal void WitnessEvent(
        Record record,
        Person actor,
        SelfActionType type,
        bool soundOnly = false
    )
    {
        AddEvent(record);
        if (Dead)
            return;
        if (
            soundOnly == false
            && Personality.Voraphilia > .7f
            && actor != this
            && type >= SelfActionType.ScatDisposalBathroom
            && type <= SelfActionType.UnbirthDisposalFloor
        )
        {
            SelfActionList.List[SelfActionType.TurnedOnByDisposal].OnDo(this);
        }

        if (
            type == SelfActionType.Scream
            && (actor.FindMyPredator()?.BeingEaten ?? false) == false
            && actor.Position.GetNumberOfMovesDistance(actor.Position) < 7
        )
        {
            AI.DiscoveredVoreAttempt(actor);
        }
    }

    internal void WitnessEvent(Record record, bool displayText = true)
    {
        if (displayText)
        {
            AddEvent(record);
        }
    }

    internal string ListEvents(GameManager.DisplayedTextType mainTextType)
    {
        StringBuilder sb = new StringBuilder();
        if (mainTextType == GameManager.DisplayedTextType.Both)
        {
            foreach (Record record in Events)
            {
                if (Config.HighlightPlayerText && record.AmActor(this))
                    sb.AppendLine($"<color=#ff881a>{record}</color>");
                else if (Config.HighlightPlayerText && record.AmTarget(this))
                    sb.AppendLine($"<color=#52AB00ff>{record}</color>");
                else
                {
                    sb.AppendLine(record.ToString());
                }
            }
        }
        else if (mainTextType == GameManager.DisplayedTextType.SelfOnly)
        {
            foreach (Record record in Events)
            {
                if (record.InvolvesMe(this))
                {
                    if (Config.HighlightPlayerText && record.AmActor(this))
                        sb.AppendLine($"<color=#ff881a>{record}</color>");
                    else if (Config.HighlightPlayerText && record.AmTarget(this))
                        sb.AppendLine($"<color=#7c3b00ff>{record}</color>");
                    else
                    {
                        sb.AppendLine(record.ToString());
                    }
                }
                else if (record.GlobalEvent())
                    sb.AppendLine(record.ToString());
            }
        }
        else if (mainTextType == GameManager.DisplayedTextType.ObservedOnly)
        {
            foreach (Record record in Events)
            {
                if (!record.InvolvesMe(this))
                    sb.AppendLine(record.ToString());
            }
        }

        string ret = sb.ToString();

        if (ret.Length > 14000)
            ret = ret.Substring(ret.Length - 14000);

        return ret;
    }

    internal void GetStreamingActions(ref StringBuilder sb)
    {
        if (StreamingAction != InteractionType.None)
        {
            string desc = InteractionList.List[StreamingAction].StreamingDescription;
            if (desc != "")
                sb.AppendLine($"{desc} {StreamingTarget.FirstName}");
            else
                sb.AppendLine($"Doing {StreamingAction} with {StreamingTarget.FirstName}");
        }
        else if (StreamingSelfAction != SelfActionType.None)
        {
            string desc = SelfActionList.List[StreamingSelfAction].StreamingDescription;
            if (desc != "")
                sb.AppendLine(desc);
            else
                sb.AppendLine($"Doing: {StreamingSelfAction}");
        }
    }

    public bool IsBusy()
    {
        if (
            StreamingSelfAction == SelfActionType.ResearchCommunication
            || StreamingSelfAction == SelfActionType.BrowseWeb
            || StreamingSelfAction == SelfActionType.Exercise
        )
            return false;
        return StreamingAction != InteractionType.None
            || StreamingSelfAction != SelfActionType.None;
    }

    //public bool IsBusy => ActiveSex != null || VoreController.CurrentSwallow(VoreLocation.Any) != null || StreamingSelfAction != SelfActionType.Masturbate || StreamingSelfAction != SelfActionType.Rest || StreamingSelfAction != SelfActionType.Shower;

    /// Returns true if this actor is performing a rest action.
    public bool IsResting() => StreamingSelfAction == SelfActionType.Rest;

    /// Returns true if this actor is awake.
    public bool IsConscious() => !IsResting();

    /// Get the size of this actor relative to other.
    public float SizeRelativeTo(Person other) => PartList.Height / other.PartList.Height;

    /// Get the power scale of this actor relative to other.  Respecting the global size factor setting.
    public float EffectiveSizeTo(Person other) =>
        (float)Math.Pow(PartList.Height / other.PartList.Height, State.World.Settings.SizeFactor);

    /// Return true if the relative size of this actor compared to other is within the given bounds.
    public bool IsRelativeSizeInBounds(Person other, float lower, float upper)
    {
        if (lower > upper)
        {
            UnityEngine.Debug.Log("Lower bound is higher than the upper bound!");
        }
        float relative_size = SizeRelativeTo(other);
        return lower < relative_size && relative_size <= upper;
    }

    /// The lowest size difference to consider an actor to be a specific size.
    public const float SIZE_LOWER_BOUND_SAME_SIZE = 1f / 2f;
    public const float SIZE_LOWER_BOUND_HALF_SIZE = 1f / 4f;
    public const float SIZE_LOWER_BOUND_SMALL = 1f / 8f;
    public const float SIZE_LOWER_BOUND_DOLL_SIZE = 1f / 20f;
    public const float SIZE_LOWER_BOUND_BITE_SIZE = 1f / 50f;
    public const float SIZE_LOWER_BOUND_BUG_SIZE = 1f / 100f;

    /// The highest size difference to consider an actor to be a specific size.
    /// Right now each category overlaps into categories surrounding them.
    public const float SIZE_UPPER_BOUND_SAME_SIZE = 1f / SIZE_LOWER_BOUND_SAME_SIZE;
    public const float SIZE_UPPER_BOUND_HALF_SIZE = 3f / 4f;
    public const float SIZE_UPPER_BOUND_SMALL = SIZE_LOWER_BOUND_SAME_SIZE;
    public const float SIZE_UPPER_BOUND_DOLL_SIZE = SIZE_LOWER_BOUND_HALF_SIZE;
    public const float SIZE_UPPER_BOUND_BITE_SIZE = SIZE_LOWER_BOUND_SMALL;
    public const float SIZE_UPPER_BOUND_BUG_SIZE = SIZE_LOWER_BOUND_DOLL_SIZE;
    public const float SIZE_UPPER_BOUND_MICROSCOPIC = SIZE_LOWER_BOUND_BITE_SIZE;

    /// The size to treat an actor as a micro in general.
    public const float SIZE_UPPER_BOUND_MICRO = SIZE_LOWER_BOUND_SMALL;

    /// Derived from SIZE_UPPER_BOUND_MICRO.
    public const float SIZE_LOWER_BOUND_SIMILAR = SIZE_UPPER_BOUND_MICRO;
    public const float SIZE_UPPER_BOUND_SIMILAR = 1f / SIZE_LOWER_BOUND_SIMILAR;

    /// Return true if an actor is in a size category relative to other.
    /// The names are supposed to be self-explanatory. Bounds constants should be tweaked if they feel off.
    public bool IsSameSizeFor(Person other) =>
        IsRelativeSizeInBounds(other, SIZE_LOWER_BOUND_SAME_SIZE, SIZE_UPPER_BOUND_SAME_SIZE);

    public bool IsHalfSizeFor(Person other) =>
        IsRelativeSizeInBounds(other, SIZE_LOWER_BOUND_HALF_SIZE, SIZE_UPPER_BOUND_HALF_SIZE);

    public bool IsSmallFor(Person other) =>
        IsRelativeSizeInBounds(other, SIZE_LOWER_BOUND_SMALL, SIZE_UPPER_BOUND_SMALL);

    public bool IsDollSizeFor(Person other) =>
        IsRelativeSizeInBounds(other, SIZE_LOWER_BOUND_DOLL_SIZE, SIZE_UPPER_BOUND_DOLL_SIZE);

    public bool IsBiteSizeFor(Person other) =>
        IsRelativeSizeInBounds(other, SIZE_LOWER_BOUND_BITE_SIZE, SIZE_UPPER_BOUND_BITE_SIZE);

    public bool IsBugSizeFor(Person other) =>
        IsRelativeSizeInBounds(other, SIZE_LOWER_BOUND_BUG_SIZE, SIZE_UPPER_BOUND_BUG_SIZE);

    public bool IsMicroscopicFor(Person other) =>
        IsRelativeSizeInBounds(other, 0f, SIZE_UPPER_BOUND_MICROSCOPIC);

    /// Return true if neither actor has a significant size difference. Other is not a macro or a micro.
    public bool IsSimilarSizeFor(Person other) =>
        IsRelativeSizeInBounds(other, SIZE_LOWER_BOUND_SIMILAR, SIZE_UPPER_BOUND_SIMILAR);

    /// True if this actor would be considered a micro to others size.
    /// This ignores the actual physical strength between characters.
    public bool IsMicroFor(Person other) => SizeRelativeTo(other) < SIZE_UPPER_BOUND_MICRO;

    public bool StomachIsDigesting() => VoreController.PartCurrentlyDigests(VoreLocation.Stomach);

    public bool ZoneContainsObject(ObjectType type) =>
        State.World.Zones[Position.x, Position.y]?.Objects?.Contains(type) ?? false;

    public bool ZoneContainsGym() =>
        State.World.Zones[Position.x, Position.y]?.Objects?.Contains(ObjectType.Gym) ?? false;

    public bool ZoneContainsLibrary() =>
        State.World.Zones[Position.x, Position.y]?.Objects?.Contains(ObjectType.Library) ?? false;

    public bool ZoneContainsShower() =>
        State.World.Zones[Position.x, Position.y]?.Objects?.Contains(ObjectType.Shower) ?? false;

    public bool ZoneContainsBathroom() =>
        State.World.Zones[Position.x, Position.y]?.Objects?.Contains(ObjectType.Bathroom) ?? false;

    public bool ZoneContainsNurseOffice() =>
        State.World.Zones[Position.x, Position.y]?.Objects?.Contains(ObjectType.NurseOffice)
        ?? false;

    public bool ZoneContainsBed() =>
        State.World.Zones[Position.x, Position.y]?.Objects?.Contains(ObjectType.Bed) ?? false;

    public bool ZoneContainsFood() =>
        State.World.Zones[Position.x, Position.y]?.Objects?.Contains(ObjectType.Food) ?? false;

    public bool ZoneContainsMyRoom() => this.Position == this.MyRoom;

    public bool OthersOnTile() => HelperFunctions.GetPeopleWithinXSquares(this, 0).Count > 0;

    public int OthersOnTileCount() => HelperFunctions.GetPeopleWithinXSquares(this, 0).Count;

    // privacy check that is accessible via dialogue reader
    public bool IsInPrivateArea(Person other) => HelperFunctions.InPrivateArea(this, other);

    public bool CanUseMagic() => HelperFunctions.CanUseMagic(this);

    //public void CreateRelation(Person target) => Relationships.Add(new Relationship(target, Romance.GetGenderDesire(target.Gender)));
    public bool HasRelationshipWith(Person target) => GetRelationshipWith(target).Met;

    public bool NeedsScatDisposal() => Disposals.Where(s => s.Location == VoreLocation.Bowels || s.Location == VoreLocation.Stomach).Any();
    public bool NeedsBallsDisposal() => Disposals.Where(s => s.Location == VoreLocation.Balls).Any();
    public bool NeedsWombDisposal() => Disposals.Where(s => s.Location == VoreLocation.Womb).Any();

    public Person SexPartner()
    {
        if (this.ActiveSex == null)
            return null;
        else
            return this.ActiveSex.Other;
    }

    public bool IsRejected(Person target) => this.GetRelationshipWith(target).Rejections.ContainsKey(ThisAction);

    public bool IsNewRomanticEvent() => this.GetRelationshipWith(ThisTarget).RomanceHistory.Contains(ThisAction) == false;

    /// <summary>
    /// returns an int corresponding to where the character is penetrating another
    /// </summary>
    /// <returns> 0 = no penetration, 1 = oral, 2 = vaginal, 3 = anal</returns>
    public int Penetrating()
    {
        if (this.ActiveSex == null)
            return 0; // no penetration

        Person partner = this.ActiveSex.Other;

        if (this.ThisSexAction == SexInteractionType.FaceFuck || partner.ThisSexAction == SexInteractionType.BlowJob)
            return 1; // oral penetration
        if (this.ThisSexAction == SexInteractionType.VaginalGiving || partner.ThisSexAction == SexInteractionType.VaginalReceiving)
            return 2; // vaginal penetration
        if (this.ThisSexAction == SexInteractionType.AnalGiving || partner.ThisSexAction == SexInteractionType.AnalReceiving)
            return 3; // anal penetration

        return 0; // no penetration
    }

    /// <summary>
    /// returns an int corresponding to where the character is being penetrated by another
    /// </summary>
    /// <returns> 0 = no penetration, 1 = oral, 2 = vaginal, 3 = anal</returns>
    public int BeingPenetrated()
    {
        if (this.ActiveSex == null)
            return 0; // no penetration

        return this.ActiveSex.Other.Penetrating();
    }

    public bool HasDigestingBellyPrey()
    {
        if (
            VoreController.HasPrey(VoreLocation.Stomach)
            && VoreController.PartCurrentlyDigests(VoreLocation.Stomach)
        )
            return true;
        if (
            VoreController.HasPrey(VoreLocation.Bowels)
            && VoreController.PartCurrentlyDigests(VoreLocation.Bowels)
        )
            return true;
        if (
            VoreController.HasPrey(VoreLocation.Womb)
            && VoreController.PartCurrentlyDigests(VoreLocation.Womb)
        )
            return true;
        return false;
    }

    internal bool HasTrait(Traits trait) => Traits.Contains(trait);

    internal bool HasTrait(Quirks trait) => Quirks.Contains(trait);

    /// <summary>
    /// This is a slower method intended to be used by the Conditional processor.   May be replaced later on.
    /// </summary>
    /// <param name="str"></param>
    /// <returns></returns>
    public bool HasTrait(string str)
    {
        if (Enum.TryParse(str, true, out Traits trait))
        {
            return HasTrait(trait);
        }
        if (Enum.TryParse(str, true, out Quirks quirk))
        {
            return HasTrait(quirk);
        }
        UnityEngine.Debug.Log("Searched for invalid trait/quirk: " + str);
        return false;
    }

    public Relationship GetRelationshipWith(Person target)
    {
        if (target == null)
        {
            UnityEngine.Debug.LogWarning("Tried to get relationship with an invalid target");
            return new Relationship(this, false); //Dummy data
        }
        foreach (Relationship relationship in Relationships)
        {
            if (relationship.Target == target)
                return relationship;
        }

        var rel = new Relationship(target, Romance.DesiresGender(target.GenderType));
        Relationships.Add(rel);
        return rel;
    }

    internal void RecalculateBoosts()
    {
        Boosts = new Boosts();
        foreach (var trait in Traits)
        {
            Trait Trait = TraitList.GetTrait(trait);
            if (Trait is TraitData data)
                data.Boost(Boosts);
        }
        foreach (var trait in Quirks) //Nothing uses this at the point this comment was added, but it's there for safety
        {
            Trait Trait = TraitList.GetTrait(trait);
            if (Trait is TraitData data)
                data.Boost(Boosts);
        }
    }

    public void Update()
    {
        if (BeingEaten && FindMyPredator() == null)
        {
            UnityEngine.Debug.LogWarning(
                "Person was still marked as being eaten, but couldn't find predator -- fixing."
            );
            BeingEaten = false;
        }

        Romance.Update();
        Needs.Update();
        Magic.Update();
        Events.RemoveAll(s => s.Expired());

        LastTarget = ThisTarget;
        LastAction = ThisAction;
        LastSelfAction = ThisSelfAction;
        LastSexAction = ThisSexAction;

        ThisTarget = null;
        ThisAction = InteractionType.None;
        ThisSelfAction = SelfActionType.None;
        ThisSexAction = SexInteractionType.None;

        AddEvent(
            new Record(new SimpleString(null, null, "--------------------------------------"))
        );
        if (
            (
                BeingEaten == false
                || (FindMyPredator().VoreController.TargetIsBeingDigested(this) == false)
            )
            && Dead == false
            && Needs.Hunger < 1
            && Needs.Energy < 1
        )
            Health += (int)(Constants.HealthPerTurnRegen * Boosts.HealingRate);
        if (Health > Constants.HealthMax)
            Health = Constants.HealthMax;
        foreach (Relationship relationship in Relationships.ToList())
        {
            if (relationship.Target.Gone)
                Relationships.Remove(relationship);
            if (relationship.FriendshipLevel < 0)
                relationship.FriendshipLevel *= 1 - (.003f * Boosts.RelationshipRepairRate);
            if (relationship.RomanticLevel < 0)
                relationship.RomanticLevel *= 1 - (.0005f * Boosts.RelationshipRepairRate);
            if (State.World.Settings.RelationshipDecay)
            {
                if (relationship.FriendshipLevel > 0)
                    relationship.FriendshipLevel *= .9996f;
                if (relationship.RomanticLevel > 0)
                    relationship.RomanticLevel *= .9998f;
            }
            relationship.KnowledgeAbout.ClearUseless();
            relationship.LastAskedAboutDating++;

            foreach (var key in relationship.Rejections.Keys.ToList())
            {
                if (relationship.Rejections[key] <= 1)
                    relationship.Rejections.Remove(key);
                else
                    relationship.Rejections[key] -= 1;
            }

        }
        if (ActiveSex != null)
        {
            ActiveSex.Turns += 1;
            ActiveSex.LastPositionChange += 1;
            if (BeingEaten || ActiveSex.Other.BeingEaten)
                EndStreamingActions();
        }

        if (LibrarySatisfaction > 0)
            LibrarySatisfaction -= 1;
        if (GymSatisfaction > 0)
            GymSatisfaction -= 1;

        if (State.World.Settings.FlexibleStats && State.World.Settings.FlexibleStatDecaySpeed > 0)
        {
            if (State.World.HasGym)
                Personality.Strength *= 1 - .00008f * State.World.Settings.FlexibleStatDecaySpeed;
            Personality.Voracity *= 1 - .00012f * State.World.Settings.FlexibleStatDecaySpeed;
            if (State.World.HasLibrary)
                Personality.Charisma *= 1 - .00003f * State.World.Settings.FlexibleStatDecaySpeed;
        }

        if (Health > 0)
            MiscStats.TurnsAlive++;

        AI.StripTurns--;
        TurnsSinceOrgasm++;
    }

    internal void QuickMeet(Person target, bool hostile)
    {
        if (GetRelationshipWith(target).Met == false)
        {
            if (Dead || target.Dead)
            {
                GetRelationshipWith(target).Met = true;
                return;
            }
            GetRelationshipWith(target).Met = true; //These two are done for safety, so that a call below won't cause a stack overflow if it's called from the wrong place.
            target.GetRelationshipWith(this).Met = true;
            if (hostile)
            {
                InteractionList.List[InteractionType.HostileMeet].OnSucceed(this, target);
            }
            else
            {
                InteractionList.List[InteractionType.Meet].OnSucceed(this, target);
            }
        }
    }

    internal void RaceChanged(string race, bool fullRandom)
    {
        var newRace = RaceManager.GetRace(race);
        if (newRace != null)
        {
            if (fullRandom)
            {
                PartList = Create.Random(GenderType, newRace);
            }
            else
            {
                PartList = Create.ChangeMinimalRandom(PartList, GenderType, newRace);
            }
            Race = race;
        }
    }

    /// <summary>
    /// This was intended to fix some of the issues from 7, but is a general purpose thing as well.
    /// </summary>
    internal void Purify()
    {
        Relationships = new List<Relationship>();
        VoreController.ClearData();
        ActiveSex = null;
        BeingEaten = false;
        AI = new AI(this);
        Romance.Dating = null;
        Events.Clear();
        Disposals.Clear();
        ThisAction = InteractionType.None;
        ThisSelfAction = SelfActionType.None;
        ThisSexAction = SexInteractionType.None;
        ThisTarget = null;
        LastAction = InteractionType.None;
        LastSelfAction = SelfActionType.None;
        LastSexAction = SexInteractionType.None;
        LastTarget = null;
        StreamingAction = InteractionType.None;
        StreamingSelfAction = SelfActionType.None;
        StreamingTarget = null;
        Health = 1000;
        LastSex = null;
        LastSexTurn = 0;
        Label = "";
        Gone = false;
    }

    public override string ToString()
    {
        return FirstName;
    }

    internal bool IsWillingPreyToTarget(Person target, bool digest = false)
    {
        if (Magic.IsCharmedBy(target))
            return true;

        if ((Personality.PreyWillingness * (digest ? Personality.PreyDigestionInterest : 1)) <= State.World.Settings.WillingThreshold)
            return false;

        if (HasTrait(global::Traits.SelectivelyWilling))
        {
            if (
                (
                    GetRelationshipWith(target).FriendshipLevel > .7
                    || GetRelationshipWith(target).RomanticLevel > .7
                ) == false
            )
                return false;
        }

        if (HasTrait(global::Traits.NeverWilling))
        {
            return false;
        }

        return true;
    }

    internal bool IsWillingPreyToPublic()
    {
        if (Personality.PreyWillingness <= State.World.Settings.WillingThreshold)
            return false;

        if (HasTrait(global::Traits.NeverWilling) || HasTrait(global::Traits.SelectivelyWilling))
        {
            return false;
        }

        return true;
    }
}
