﻿using OdinSerializer;
using System.Collections.Generic;
using System.Diagnostics;

[DebuggerDisplay("{Target.FirstName} {Target.LastName}")]
public class Relationship
{
    [OdinSerialize]
    public Person Target { get; set; }

    [OdinSerialize]
    [VariableEditorIgnores]
    [Category("Events")]
    public Dictionary<InteractionType, int> Rejections = new Dictionary<InteractionType, int>();

    [OdinSerialize]
    [VariableEditorIgnores]
    [Category("Events")]
    public List<InteractionType> RomanceHistory = new List<InteractionType>();

    [OdinSerialize]
    [Category("Events")]
    public Knowledge KnowledgeAbout { get; set; }

    [OdinSerialize]
    [VariableEditorIgnores]
    [Category("Events")]
    [Description("Whether they've met before.  Mostly a cosmetic thing at this point")]
    public bool Met { get; set; }

    [OdinSerialize]
    [VariableEditorIgnores]
    [Category("Events")]
    [Description("Whether they've had sex before.")]
    public bool HaveHadSex { get; set; }

    [OdinSerialize]
    [VariableEditorIgnores]
    [Category("Events")]
    [Description("Whether this character has eaten them.")]
    public bool HasEaten { get; set; }

    [OdinSerialize]
    [VariableEditorIgnores]
    [Category("Events")]
    [Description("Whether they have eaten this character.")]
    public bool HasBeenEatenBy { get; set; }

    [OdinSerialize]
    [VariableEditorIgnores]
    [Category("Events")]
    [Description("Whether this character has digested them.")]
    public bool HasDigested { get; set; }

    [OdinSerialize]
    [VariableEditorIgnores]
    [Category("Events")]
    [Description("Whether they have digested this character.")]
    public bool HasBeenDigestedBy { get; set; }

    [OdinSerialize]
    [VariableEditorIgnores]
    [Category("Events")]
    [Description(
        "Whether this character has a vendetta towards the other, which means they really want to eat and digest them once, usually applied by the vengeful trait."
    )]
    public bool Vendetta { get; set; }

    public Relationship(Person target, bool romanticAble, bool met = false)
    {
        Target = target;
        if (romanticAble)
            RomanticSpark = Rand.NextFloat(-0.3f, 0.4f);
        else
            RomanticSpark = 0;
        KnowledgeAbout = new Knowledge();
        Met = met;
    }

    [OdinSerialize]
    [ProperName("Friendship Level")]
    [Description("The level of friendship towards this person")]
    [Category("Relationship")]
    [FloatRange(-1, 1)]
    public float FriendshipLevel { get; set; }

    [OdinSerialize]
    [ProperName("Romantic Level")]
    [Description("The level of romantic interest towards this person")]
    [Category("Relationship")]
    [FloatRange(-1, 1)]
    public float RomanticLevel { get; set; }

    [OdinSerialize]
    [VariableEditorIgnores]
    [Category("Relationship")]
    public float HowWellKnown { get; set; }

    [OdinSerialize]
    [ProperName("Romantic Spark")]
    [Category("Relationship")]
    [Description(
        "How well this person 'hits it off' with the target, romantically.  Provides a small permanent bonus (or penalty) towards relationship values"
    )]
    [FloatRange(-1, 1)]
    public float RomanticSpark { get; set; }

    [OdinSerialize]
    [Category("Events")]
    public int LastAskedAboutDating { get; set; } = 30;

    [OdinSerialize]
    [Category("Relationship")]
    [Description(
        "How well this person trusts the other regarding endo vore, being unwillingly eaten, or attempting to digest them without their consent will make them less likely to accept future endo requests."
    )]
    [FloatRange(-1, 1)]
    public float Trust { get; set; } = 0;

    internal void VoreBetrayed()
    {
        Trust = Utility.PushTowardsNegativeOne(Trust, .6f * State.World.Settings.VoreAnger);
    }

    internal void VoreWilling()
    {
        Trust = Utility.PushTowardOne(Trust, .1f);
    }

    internal void VoreTaste()
    {
        Trust = Utility.PushTowardOne(Trust, .01f);
    }

    public float GetRomanticLevelToTarget()
    {
        return Utility.PushTowardOne(RomanticLevel, RomanticSpark);
    }
}
