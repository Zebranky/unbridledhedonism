﻿using Assets.Scripts.People;
using OdinSerializer;

public class Romance
{
    [OdinSerialize]
    private readonly Person Self;

    [OdinSerialize]
    [ProperName("Orientation")]
    [Description("The character's sexual orientation.")]
    [Category("Romance")]
    internal Orientation Orientation;

    public Romance(Person self, Orientation orientation)
    {
        Self = self;
        Orientation = orientation;
    }

    [OdinSerialize]
    [ProperName("Romantic Desperation")]
    [Description(
        "How receptive this person is to romantic advances.   After a long time out of a relationship, their standards gradually lower.  Gradually cools back down while in a relationship"
    )]
    [Category("Romance")]
    [FloatRange(0, 1)]
    public float RomanticDesperation { get; set; }

    [OdinSerialize]
    public Person Dating { get; set; }

    public void Update()
    {
        if (IsDating && Dating.Gone)
        {
            if (Dating.Romance.Dating == Self)
                Dating.Romance.Dating = null;
            Dating = null;
        }

        if (IsDating)
            RomanticDesperation *= .99f;
        else
            RomanticDesperation = Utility.PushTowardOne(RomanticDesperation, .0004f);
    }

    /// <summary>
    /// Gets the attraction towards this specific gender
    /// </summary>
    /// <param name="gender"></param>
    /// <returns></returns>
    public bool DesiresGender(GenderType genderType)
    {
        switch (Orientation)
        {
            case Orientation.All:
                return true;
            case Orientation.Asexual:
                return false;
            case Orientation.FemaleOnly:
                return (genderType.AttractedOrientations & Orientations.Female)
                    == Orientations.Female;
            case Orientation.Gynesexual:
                return (genderType.AttractedOrientations & Orientations.Gynesexual)
                    == Orientations.Gynesexual;
            case Orientation.MaleOnly:
                return (genderType.AttractedOrientations & Orientations.Male) == Orientations.Male;
            case Orientation.Androsexual:
                return (genderType.AttractedOrientations & Orientations.Androsexual)
                    == Orientations.Androsexual;
            case Orientation.ExclusiveBi:
                return (genderType.AttractedOrientations & Orientations.ExclusiveBi)
                    == Orientations.ExclusiveBi;
            case Orientation.Skoliosexual:
                return (genderType.AttractedOrientations & Orientations.Skoliosexual)
                    == Orientations.Skoliosexual;
        }
        return false;
    }

    [VariableEditorIgnores]
    public bool IsDating => Dating != null;

    /// <summary>
    /// Checks known orientation, and dating status to see if a romantic interaction is at all possible
    /// </summary>
    /// <param name="target"></param>
    /// <returns></returns>
    public bool CanSafelyRomance(Person target, bool IgnoreCharm = false)
    {
        if (!IgnoreCharm && Self.Magic.IsCharmedBy(target))
            return true;
        if (Self.Romance.DesiresGender(target.GenderType) == false)
            return false;
        if (
            target.Romance.DesiresGender(Self.GenderType) == false
            && Self.GetRelationshipWith(target).KnowledgeAbout.KnowsOrientation
        )
            return false;
        if (Dating == target)
            return true;
        if (IsDating == false || Self.Personality.CheatOnPartner != ThreePointScale.Never)
            return true;
        return false;
    }
}
