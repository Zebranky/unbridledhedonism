﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;
using TMPro;
using System.Linq;
using System;
using OdinSerializer;
using System.IO;

using Assets.Scripts.UI;
using OdinSerializer.Utilities;

public class StartScreen : MonoBehaviour
{
    public List<StartPersonUI> PeopleObjects;

    public GameObject PersonObjectPrefab;

    public Transform PersonFolder;

    public SavedCharacterScreen SavedCharacterScreen;

    public TMP_Dropdown Map;

    public Button TestWorld;

    public Button ChangeTemplateSetButton;

    public GameObject CustomizeAppearancePanel;

    public PersonTemplateUI SampleUnit;

    public Slider Height;
    public Slider Weight;
    public Slider BreastSize;
    public Slider DickSize;
    public Slider BallSize;
    public TMP_InputField HairColor;
    public TMP_InputField HairLength;
    public TMP_InputField HairStyle;
    public TMP_InputField EyeColor;
    public TMP_InputField ShoulderDescription;
    public TMP_InputField HipDescription;

    public BulkCharacterSaver BulkCharacterSaver;

    Settings Settings;

    public Slider PredOdds;

    internal GenderList GenderList;
    internal RaceWeights RaceWeights;
    internal TraitWeights TraitWeights;

    //public TextMeshProUGUI PersonalitySliderValues;
    public TextMeshProUGUI AppearanceSliderValues;

    public TextMeshProUGUI OverPop;

    StartPersonUI EditingPerson;

    World tempWorld;

    int MapCount;

    public TextMeshProUGUI TooltipText;

    int MaxPeople = 12;

    int MaxEntries = 250;

    public static List<string> PersonalityOptions = new List<string> { "Random", "True Random", "<i>Custom</i>", "Willing Prey", "Never Willing", "Novice Pred", "Expert Pred", "Promiscuous", "Magic Adept", "Magic Expert", "Dominant", "Submissive", "Kindhearted", "Jerk", "Sadistic", "Outcast" };

    internal void ChangeToolTip(string text)
    {
        TooltipText.text = text;
    }

    private void Start()
    {
        BuildMapList();
        MapChanged();

        var tempHeight = GetRange(typeof(PartList), nameof(PartList.Height));
        Height.minValue = tempHeight.x;
        Height.maxValue = tempHeight.y;

        var tempWeight = GetRange(typeof(PartList), nameof(PartList.Weight));
        Weight.minValue = tempWeight.x;
        Weight.maxValue = tempWeight.y;

        SampleUnit.SetUpOptions();

        var num = PlayerPrefs.GetInt("CurrentTemplate", 0);
        ChangeTemplateSetButton.GetComponentInChildren<TextMeshProUGUI>().text =
            $"Using Template Set #{num + 1}";
    }

    public void Open()
    {
        // Only load settings once.
        if (Settings == null)
            Settings = State.ReadFromFile<Settings>("Settings.dat");
        // Load defaults if settings file is missing.
        if (Settings == null)
            Settings = new Settings();
        Settings.CheckUpdateVersion();
        gameObject.SetActive(true);
        if (GenderList == null)
        {
            GenderList = State.ReadFromFile<GenderList>("Sexes.dat");

            if (GenderList == null)
                GenderList = new GenderList();
            else
                GenderList.CheckStatus();
            RaceWeights = State.RaceWeights;
            TraitWeights = State.TraitWeights;
        }
        else
        {
            MapUpdate();
        }
        State.World.Settings = Settings; //Done to sync for the trait calculations.
        State.World.TraitWeights = TraitWeights; //Done to sync for the trait calculations.
        RefreshSexesSample();
        TestWorld.gameObject.SetActive(false);
#if UNITY_EDITOR
        TestWorld.gameObject.SetActive(true);
#endif
    }

    void AddReflectedDescription<T>(GameObject obj, string variable)
    {
        obj = obj.transform.parent.gameObject;
        Type type = typeof(T);
        var field = type.GetField(variable, VariableEditor.Bindings);
        if (field != null)
        {
            foreach (Attribute attr in Attribute.GetCustomAttributes(field))
            {
                if (attr is DescriptionAttribute desc)
                {
                    obj.gameObject.AddComponent<StartScreenTooltip>();
                    obj.GetComponent<StartScreenTooltip>().Text = desc.Description;
                }
            }
        }
        var property = type.GetProperty(variable, VariableEditor.Bindings);
        if (property != null)
        {
            foreach (Attribute attr in Attribute.GetCustomAttributes(property))
            {
                if (attr is DescriptionAttribute desc)
                {
                    obj.gameObject.AddComponent<StartScreenTooltip>();
                    obj.GetComponent<StartScreenTooltip>().Text = desc.Description;
                }
            }
        }
    }

    internal Vector2 GetRange(Type type, string variable)
    {
        var property = type.GetProperty(variable, VariableEditor.Bindings);
        if (property != null)
        {
            foreach (Attribute attr in Attribute.GetCustomAttributes(property))
            {
                if (attr is FloatRangeAttribute desc)
                {
                    return new Vector2(desc.Min, desc.Max);
                }
            }
        }
        return new Vector2();
    }

    public void RefreshAppearanceSliderValues()
    {
        string height = $"{MiscUtilities.FancyHeight(Height.value)}";
        if (EditingPerson == null)
            return;
        var gender = GenderList.List[EditingPerson.Gender.value];
        string text = $"{height}\n{MiscUtilities.ConvertedWeight(Weight.value)}\n";
        if (gender.HasBreasts)
            text += $"{GetWord.BreastSize(BreastSize.value)}\n";
        else
            text += "\n";
        if (gender.HasDick)
            text += $"{GetWord.DickSize(DickSize.value)}\n{GetWord.BallSize(BallSize.value)}";
        AppearanceSliderValues.text = text;
    }

    internal void RemovePersonObject(StartPersonUI obj)
    {
        PeopleObjects.Remove(obj);
        Destroy(obj.gameObject);
        CheckPeopleCount();
    }

    public void OpenCharacterScreen()
    {
        SavedCharacterScreen.Open();
    }

    public void OpenTemplateScreen()
    {
        State.GameManager.TemplateEditorScreen.Open();
    }

    public void OpenTraitWeights()
    {
        State.GameManager.VariableEditor.Open(TraitWeights, "Trait & Quirk Weights");
        State.GameManager.VariableEditor.SetExtraButton(
            "Save as Defaults",
            () => State.WriteToFile(TraitWeights, "TraitWeights.dat")
        );
        State.GameManager.VariableEditor.SetSecondExtraButton(
            "Reset Settings to Original",
            () =>
            {
                var dialog = State.GameManager.CreateDialogBox();
                dialog.SetData(
                    () =>
                    {
                        State.GameManager.VariableEditor.Open(TraitWeights = new TraitWeights(), "Trait & Quirk Weights", false, true);
                    },
                    "Reset",
                    "Cancel",
                    "This will reset the listed options to the base game default settings, not your saved settings.  It basically changes them to how they'd be if you ran the game for the first time."
                );
            }
        );
    }

    /// <summary>Return all map files.</summary>
    internal static List<string> GetMapFiles()
    {
        var files = Directory.GetFiles(Application.streamingAssetsPath, "*.map").ToList();
        files.AddRange(Directory.GetFiles(State.MapDirectory, "*.map"));
        if (Directory.Exists(State.MapDirectoryOld))
            files.AddRange(Directory.GetFiles(State.MapDirectoryOld, "*.map"));
        return files;
    }

    void BuildMapList()
    {
        Map.ClearOptions();
        if (Directory.Exists(State.MapDirectory) == false)
            Directory.CreateDirectory(State.MapDirectory);

        List<string> options = new List<string>();

        foreach (string file in GetMapFiles())
            options.Add(Path.GetFileNameWithoutExtension(file));

        options = options.Distinct().ToList();

        MapCount = options.Count();

        Map.AddOptions(options);
        Map.value = 0;
        Map.RefreshShownValue();
    }

    internal void MapUpdate()
    {
        List<string> options = new List<string>();
        foreach (string file in GetMapFiles())
            options.Add(Path.GetFileNameWithoutExtension(file));
        options = options.Distinct().ToList();

        if (options.Count != MapCount)
        {
            BuildMapList();
            MapChanged();
        }
    }

    public void MapChanged()
    {
        try
        {
            string filename = Path.Combine(State.MapDirectory, $"{Map.captionText.text}.map");

            if (File.Exists(filename) == false)
            {
                filename = Path.Combine(
                    Application.streamingAssetsPath,
                    $"{Map.captionText.text}.map"
                );
            }
            else
            {
                if (
                    File.Exists(
                        Path.Combine(Application.streamingAssetsPath, $"{Map.captionText.text}.map")
                    )
                )
                    State.GameManager.CreateMessageBox(
                        "Map of this name exists in streaming assets folder and the map directory -- "
                            + "using the one from the map directory, as it's assuming you modified a stock map and that you'd want to use the modifed version.  If this isn't the case, delete the unwanted one or rename one of them."
                    );
            }

            if (File.Exists(filename) == false)
            {
                State.GameManager.CreateMessageBox(
                    "Map can't be found, this should only happen if it was deleted after this screen loaded"
                );
                return;
            }

            byte[] bytes = File.ReadAllBytes(filename);
            tempWorld = SerializationUtility.DeserializeValue<World>(bytes, DataFormat.Binary);
            if (string.Compare(tempWorld.SaveVersion, "7") < 0)
            {
                tempWorld.FixZones();
            }
            tempWorld.MapName = Path.GetFileName(filename);
            MaxPeople = tempWorld.DormRooms.Distinct().Count();
            CheckPeopleCount();
        }
        catch (Exception e)
        {
            Debug.LogException(e);
            State.GameManager.CreateMessageBox(
                "Failed to load the map properly, using the old default."
            );
            tempWorld = null;
            MaxPeople = 12;
        }
    }

    public void CreateWorld()
    {
        if (PeopleObjects.Count == 0)
        {
            State.GameManager.CreateMessageBox("Can't start without any people");
            return;
        }

        List<Person> people = new List<Person>();

        var randomizedList = PeopleObjects.ToList();
        randomizedList.RemoveAt(0);
        randomizedList.Shuffle();
        randomizedList = randomizedList.Take(MaxPeople - 1).ToList();
        var list = PeopleObjects.Intersect(randomizedList).ToList();
        list.Insert(0, PeopleObjects[0]);

        for (int i = 0; i < MaxPeople; i++)
        {
            if (i == list.Count)
                break;
            var person = list[i];
            person.FirstName.text = person.FirstName.text.Trim();
            person.LastName.text = person.LastName.text.Trim();
            //if (person.FirstName.text.Count(s => char.IsWhiteSpace(s)) > 1)
            //{
            //    State.GameManager.CreateMessageBox($"Too many spaces in a first name ({person.FirstName.text}), the selection system permits 2 first names (i.e. 'Mary Beth', trying to go past " +
            //        "that will cause issues when trying to select from multiple units in a single tile, use - to separate if you have more than that.");
            //    return;
            //}
            //if (person.LastName.text.Count(s => char.IsWhiteSpace(s)) > 0)
            //{
            //    State.GameManager.CreateMessageBox($"Too many spaces in a last name ({person.LastName.text}), the selection system permits only a single last name, trying to go past " +
            //        "that will cause issues when trying to select from multiple units in a single tile, use - to separate if you have more than that.");
            //    return;
            //}
            Person newPerson;
            if (person.Person != null)
            {
                newPerson = Utility.SerialClone(person.Person);
                newPerson.FirstName = person.FirstName.text;
                newPerson.LastName = person.LastName.text;
                newPerson.Gender = person.Gender.value;
                newPerson.Romance.Orientation = (Orientation)person.Orientation.value;
                newPerson.VoreController.GeneralVoreCapable = person.CanVore.isOn;
            }
            else
                newPerson = CreatePerson(person);
            newPerson.GymSatisfaction = Rand.Next(80, 160);
            newPerson.LibrarySatisfaction = Rand.Next(80, 160);
            people.Add(newPerson);
        }

        if (Settings.CheckDigestion(false, DigestionAlias.CanVore) == false)
        {
            foreach (Person person in people)
            {
                person.VoreController.StomachDigestsPrey = false;
                person.VoreController.WombAbsorbsPrey = false;
                person.VoreController.BallsAbsorbPrey = false;
                person.VoreController.BowelsDigestPrey = false;
            }
        }
        if (Settings.CheckDigestion(false, DigestionAlias.CanEndo) == false)
        {
            foreach (Person person in people)
            {
                person.VoreController.StomachDigestsPrey = true;
                person.VoreController.WombAbsorbsPrey = true;
                person.VoreController.BallsAbsorbPrey = true;
                person.VoreController.BowelsDigestPrey = true;
            }
        }

        if (people.Count == 0) //Second Check
        {
            State.GameManager.CreateMessageBox("Can't start without any people");
            return;
        }

        if (tempWorld != null)
            State.World = new World(
                people,
                Settings,
                GenderList,
                tempWorld,
                RaceWeights,
                TraitWeights
            );
        else
            State.World = new World(people, Settings, GenderList, false);
        State.GameManager?.TileManager.DrawWorld();
        State.GameManager.ClickedPerson = State.World.ControlledPerson;
        State.GameManager.DisplayInfo();
        State.GameManager.RefreshCameraSettings();
        State.GameManager.CenterCameraOnTile(
            State.World.ControlledPerson?.Position ?? new Vec2(5, 5)
        );
        State.World.UpdatePlayerUI();
        gameObject.SetActive(false);
    }

    internal static Person CreatePerson(StartPersonUI person)
    {
        if (person.Person != null)
        {
            if (person.Person.Personality == null)
                person.Person.Personality = new Personality();
            return Utility.SerialClone(person.Person);
        }
        Person newPerson = new Person(
            person.FirstName.text,
            person.LastName.text,
            person.Gender.value,
            (Orientation)person.Orientation.value,
            person.CanVore.isOn,
            new Vec2(0, 0),
            RaceManager.GetRace(person.Race.captionText.text)
        );
        newPerson.PartList = Utility.SerialClone(person.CustomAppearance);
       
        // Controlled Random (not True Random)
        if (person.Personality.captionText.text == "Random" || person.Personality.captionText.text == "Controlled Random")
        {
            newPerson.Personality = RaceRandomStats.GetRandomStatsFor(
                newPerson.Race,
                newPerson.GenderType.Feminine
            );
        }

        // Custom Personality
        else if (person.Personality.captionText.text == "<i>Custom</i>" || person.Personality.captionText.text == "Custom")
        {
            newPerson.Personality = Utility.SerialClone(person.CustomPersonality);
        }

        // Willing Prey
        else if (person.Personality.captionText.text == "Willing Prey")
        {
            newPerson.Personality.PreyWillingness = Rand.NextFloat(0.8, 1.0);
            newPerson.Personality.Strength = Rand.NextFloat(0, 0.2);
            newPerson.Personality.Dominance = Rand.NextFloat(0, 0.4);
            // 1/4 chance to also add the preypheromones quirk (removes it first if it exists to avoid doubles)
            if (Rand.Next(0, 3) == 0)
            {
                newPerson.Quirks.Remove(Quirks.PreyPheromones);
                newPerson.Quirks.Add(Quirks.PreyPheromones);
            }
        }

        // Never Willing
        else if (person.Personality.captionText.text == "Never Willing")
        {
            newPerson.Personality.PreyWillingness = Rand.NextFloat(0.0, 0.1);
            newPerson.Personality.PreyDigestionInterest = Rand.NextFloat(0.0, 0.05);
        }

        // Pred Novice
        else if (person.Personality.captionText.text == "Novice Pred" || person.Personality.captionText.text == "Vore Novice")
        {
            newPerson.Personality.PredWillingness = Rand.NextFloat(0.5, 0.7);
            newPerson.Personality.Voracity = Rand.NextFloat(0.4, 0.6);
            newPerson.Personality.Strength = Rand.NextFloat(0.3, 0.5);
        }

        // Pred Expert
        else if (person.Personality.captionText.text == "Expert Pred" || person.Personality.captionText.text == "Vore Expert")
        {
            newPerson.Personality.PredWillingness = Rand.NextFloat(0.7, 1.0);
            newPerson.Personality.Voracity = Rand.NextFloat(0.7, 1.0);
            newPerson.Personality.Strength = Rand.NextFloat(0.5, 1.0);
            newPerson.Personality.Dominance = Rand.NextFloat(0.6, 1.0);
            if (newPerson.IsWillingPreyToPublic())
                newPerson.Personality.PreyWillingness /= 2;
            // Pick a quirk to add
            List<Quirks> PredQuirks = new List<Quirks> { Quirks.FastDigestion, Quirks.FearsomeReputation, Quirks.GreatHunger, Quirks.HighCapacity, Quirks.ViciousPredator, Quirks.Gluttonous };
            newPerson.AddQuirkFromList(PredQuirks);
        }

        // Promiscuous
        else if (person.Personality.captionText.text == "Promiscuous")
        {
            newPerson.Personality.SexDrive = Rand.NextFloat(0.7, 1.0);
            newPerson.Personality.Promiscuity = Rand.NextFloat(0.7, 1.0);
            newPerson.Personality.CheatOnPartner = ThreePointScale.Frequently;
            newPerson.Personality.CheatAcceptance = CheatingAcceptance.Everything;
            // Pick a trait from this list to add
            List<Traits> PromiscTraits = new List<Traits> { Traits.EasyShow, Traits.Flirtatious, Traits.Exhibitionist, Traits.LovesPrivateSex, Traits.SexAddict, Traits.SexOnly, Traits.DownToFuck, Traits.Slutty};
            newPerson.AddTraitFromList(PromiscTraits);
        }

        // Magic Adept
        else if (person.Personality.captionText.text == "Magic Adept")
        {
            newPerson.Magic.CanCast = true;
            newPerson.Magic.Potency = Rand.NextFloat(0.4, 0.6);
            newPerson.Magic.MaxMana = Rand.Next(3, 4);
        }

        // Magic Expert
        else if (person.Personality.captionText.text == "Magic Expert")
        {
            newPerson.Magic.CanCast = true;
            newPerson.Magic.Potency = Rand.NextFloat(0.7, 1.0);
            newPerson.Magic.Proficiency = Rand.NextFloat(0.6, 0.8);
            newPerson.Magic.MaxMana = Rand.Next(4, 5);
            newPerson.Magic.ManaRegen = Rand.NextFloat(1.2, 1.8);
            // Pick a quirk from this list to add
            List<Quirks> MagicQuirks = new List<Quirks> { Quirks.VastMind, Quirks.Intelligent, Quirks.Transmuter, Quirks.MagicalLust };
            newPerson.AddQuirkFromList(MagicQuirks);
            // 1/3 chance to also add RoomInvader
            if (Rand.Next(0,2)==0)
            {
                newPerson.Traits.Remove(Traits.RoomInvader);
                newPerson.Traits.Add(Traits.RoomInvader);
            }
        }

        // Dominant
        else if (person.Personality.captionText.text == "Dominant")
        {
            newPerson.Personality.PredWillingness = Rand.NextFloat(0.4, 1.0);
            newPerson.Personality.PreyWillingness = Rand.NextFloat(0.0, 0.3);
            newPerson.Personality.Dominance = Rand.NextFloat(0.8, 1.0);
            // Pick a trait from this list to add
            List<Traits> DomTraits = new List<Traits> { Traits.Flirtatious, Traits.DemandsWorship, Traits.Jerk };
            if (newPerson.VoreController.CapableOfVore())
            {
                DomTraits.Add(Traits.Vengeful);
                DomTraits.Add(Traits.Greedy);
            }
            newPerson.AddTraitFromList(DomTraits);
        }

        // Submissive
        else if (person.Personality.captionText.text == "Submissive")
        {
            newPerson.Personality.PredWillingness = Rand.NextFloat(0.0, 0.6);
            newPerson.Personality.PreyWillingness = Rand.NextFloat(0.4, 1.0);
            newPerson.Personality.Dominance = Rand.NextFloat(0.0, 0.2);
            // Pick two traits from this list to add
            List<Traits> SubTraits = new List<Traits> { Traits.BellyFixation, Traits.EasyShow, Traits.GuineaPig, Traits.LovesKissing, Traits.TentativeRomance, Traits.LovesPrivateSex, Traits.Masochist };
            newPerson.AddTraitFromList(SubTraits);
        }

        // Jerk
        else if (person.Personality.captionText.text == "Jerk")
        {
            newPerson.Personality.PredWillingness = Rand.NextFloat(0.5, 1.0);
            newPerson.Personality.Kindness = Rand.NextFloat(0.0, 0.2);
            // Pick two traits from this list to add
            List<Traits> MeanTraits = new List<Traits> { Traits.Bully, Traits.Jerk, Traits.Unapproachable, Traits.Uncaring, Traits.Jealous, Traits.Vengeful, Traits.Unyielding, Traits.Greedy, Traits.Sadistic, Traits.Traitor, Traits.Widowmaker };
            newPerson.AddTraitFromList(MeanTraits);
            newPerson.AddTraitFromList(MeanTraits);
        }

        // Sadistic
        else if (person.Personality.captionText.text == "Sadistic")
        {
            newPerson.Personality.PredWillingness = Rand.NextFloat(0.7, 1.0);
            newPerson.Personality.Kindness = Rand.NextFloat(0.0, 0.1);
            newPerson.Personality.Dominance = Rand.NextFloat(0.8, 1.0);
            newPerson.Traits.Remove(Traits.Sadistic);
            newPerson.Traits.Add(Traits.Sadistic);
        }

        // Kindhearted
        else if (person.Personality.captionText.text == "Kindhearted")
        {
            newPerson.Personality.Kindness = Rand.NextFloat(0.8, 1.0);
            newPerson.Personality.Voracity = Rand.NextFloat(0.0, 0.6);
            newPerson.Personality.CheatOnPartner = ThreePointScale.Never;

            // remove any of these traits if it was randomly selected
            List<Traits> MeanTraits = new List<Traits> { Traits.Bully, Traits.Jerk, Traits.Unapproachable, Traits.Uncaring, Traits.Jealous, Traits.Vengeful, Traits.Unyielding, Traits.Greedy, Traits.Sadistic, Traits.Traitor, Traits.Widowmaker };
            foreach (Traits trait in MeanTraits)
                if (newPerson.HasTrait(trait))
                    newPerson.Traits.Remove(trait);

            // add one of these traits
            List<Traits> KindTraits = new List<Traits> { Traits.LovesHugs,  Traits.TentativeRomance};
            if (newPerson.VoreController.CapableOfVore())
            {
                KindTraits.Add(Traits.NoVoreZone);
                KindTraits.Add(Traits.RomanticPred);
            }
            newPerson.AddTraitFromList(KindTraits);
            // 1/3 chance to also add the rescuer quirk
            if (Rand.Next(0,2) == 0)
            {
                newPerson.Quirks.Remove(Quirks.Rescuer);
                newPerson.Quirks.Add(Quirks.Rescuer);
            }
        }

        // Outcast
        else if (person.Personality.captionText.text == "Outcast")
        {
            newPerson.Personality.Charisma = Rand.NextFloat(0.0, 0.4);
            newPerson.Personality.Extroversion = Rand.NextFloat(0.0, 0.4);
            // Pick a trait and a quirk from lists to add
            List<Traits> WeirdTraits = new List<Traits> { Traits.Bookworm, Traits.Gassy, Traits.Jerk, Traits.RarelyShowers };
            List<Quirks> WeirdQuirks = new List<Quirks> { Quirks.Olfactophilic, Quirks.FootFetish, Quirks.ArmpitFetish, Quirks.Gluttonous, Quirks.Untasty, Quirks.FilthMagnet };
            if (newPerson.VoreController.CapableOfVore())
            {
                WeirdTraits.Add(Traits.Vengeful);
                WeirdTraits.Add(Traits.Sadistic);
                WeirdTraits.Add(Traits.PrefersLivingPrey);
            }
            newPerson.AddTraitFromList(WeirdTraits);
            newPerson.AddQuirkFromList(WeirdQuirks);
        }


        return newPerson;
    }

    public void ChangeTemplateSet()
    {
        var num = PlayerPrefs.GetInt("CurrentTemplate", 0);
        num++;
        if (num > 2)
            num = 0;
        ChangeTemplateSetButton.GetComponentInChildren<TextMeshProUGUI>().text =
            $"Using Template Set #{num + 1}";
        PlayerPrefs.SetInt("CurrentTemplate", num);
        State.TemplateController.Container = null;
    }

    public void AddPerson()
    {
        if (PeopleObjects.Count >= MaxEntries)
            return;
        var obj = Instantiate(PersonObjectPrefab, PersonFolder).GetComponent<StartPersonUI>();
        PeopleObjects.Add(obj);
        RefreshSexes(obj);
        if (SampleUnit.Gender.value == 0)
            obj.Gender.value = GenderList.GetRandomGender();
        else
            obj.Gender.value = SampleUnit.Gender.value - 1;

        obj.FirstName.text = GenderList.List[obj.Gender.value].FeminineName
            ? GetRandomFemaleName()
            : GetRandomMaleName();
        RefreshSexes(obj);

        obj.SetUpRaces();
        obj.SetUpPersonality();

        obj.LastName.text = NameGenerator.GetRandomLastName();
        obj.Gender.RefreshShownValue();
        obj.CanVore.isOn = Rand.NextFloat(0, 1) < PredOdds.value;
        if (SampleUnit.Orientation.value == 0)
            obj.Orientation.value = GenderList.GetRandomOrientation(obj.Gender.value);
        else
            obj.Orientation.value = SampleUnit.Orientation.value - 1;
        obj.Orientation.RefreshShownValue();

        obj.Personality.options = SampleUnit.Personality.options;
        obj.Personality.value = SampleUnit.Personality.value;
        obj.Personality.RefreshShownValue();

        obj.CustomPersonality = new Personality();


        if (SampleUnit.Race.captionText.text == "Random")
        {
            var rand = RaceWeights.GetRandomRace().Name;
            for (int i = 0; i < obj.Race.options.Count; i++)
            {
                if (obj.Race.options[i].text == rand)
                    obj.Race.value = i;
            }
        }
        else
            obj.Race.value = SampleUnit.Race.value - 1;
        obj.Race.RefreshShownValue();

        if (SampleUnit.CustomPersonality.CharismaRandomized == false)
            obj.CustomPersonality.Charisma = SampleUnit.CustomPersonality.Charisma;
        if (SampleUnit.CustomPersonality.PredWillingnessRandomized == false)
            obj.CustomPersonality.PredWillingness = SampleUnit.CustomPersonality.PredWillingness;
        if (SampleUnit.CustomPersonality.PreyWillingnessRandomized == false)
            obj.CustomPersonality.PreyWillingness = SampleUnit.CustomPersonality.PreyWillingness;
        if (SampleUnit.CustomPersonality.PromiscuityRandomized == false)
            obj.CustomPersonality.Promiscuity = SampleUnit.CustomPersonality.Promiscuity;
        if (SampleUnit.CustomPersonality.SexDriveRandomized == false)
            obj.CustomPersonality.SexDrive = SampleUnit.CustomPersonality.SexDrive;
        if (SampleUnit.CustomPersonality.StrengthRandomized == false)
            obj.CustomPersonality.Strength = SampleUnit.CustomPersonality.Strength;
        if (SampleUnit.CustomPersonality.VoracityRandomized == false)
            obj.CustomPersonality.Voracity = SampleUnit.CustomPersonality.Voracity;
        if (SampleUnit.CustomPersonality.VoraphiliaRandomized == false)
            obj.CustomPersonality.Voraphilia = SampleUnit.CustomPersonality.Voraphilia;
        if (SampleUnit.CustomPersonality.PredLoyaltyRandomized == false)
            obj.CustomPersonality.PredLoyalty = SampleUnit.CustomPersonality.PredLoyalty;
        if (SampleUnit.CustomPersonality.PreyDigestionInterestRandomized == false)
            obj.CustomPersonality.PreyDigestionInterest = SampleUnit
                .CustomPersonality
                .PreyDigestionInterest;
        if (SampleUnit.CustomPersonality.ExtroversionRandomized == false)
            obj.CustomPersonality.Extroversion = SampleUnit.CustomPersonality.Extroversion;
        if (SampleUnit.CustomPersonality.DominanceRandomized == false)
            obj.CustomPersonality.Dominance = SampleUnit.CustomPersonality.Dominance;
        if (SampleUnit.CustomPersonality.KindnessRandomized == false)
            obj.CustomPersonality.Kindness = SampleUnit.CustomPersonality.Kindness;
        if (SampleUnit.CustomPersonality.EndoDominatorRandomized == false)
            obj.CustomPersonality.EndoDominator = SampleUnit.CustomPersonality.EndoDominator;
        if (SampleUnit.CustomPersonality.PreferredClothingRandomized == false)
            obj.CustomPersonality.PreferredClothing = SampleUnit
                .CustomPersonality
                .PreferredClothing;

        if (SampleUnit.CustomPersonality.CheatOnPartnerRandomized == false)
            obj.CustomPersonality.CheatOnPartner = SampleUnit.CustomPersonality.CheatOnPartner;
        if (SampleUnit.CustomPersonality.CheatAcceptanceRandomized == false)
            obj.CustomPersonality.CheatAcceptance = SampleUnit.CustomPersonality.CheatAcceptance;

        if (SampleUnit.CustomPersonality.VorePreferenceRandomized == false)
            obj.CustomPersonality.VorePreference = SampleUnit.CustomPersonality.VorePreference;

        if (SampleUnit.CustomPersonality.OralVoreInterestRandomized == false)
            obj.CustomPersonality.OralVoreInterest = SampleUnit.CustomPersonality.OralVoreInterest;
        if (SampleUnit.CustomPersonality.AnalVoreInterestRandomized == false)
            obj.CustomPersonality.AnalVoreInterest = SampleUnit.CustomPersonality.AnalVoreInterest;
        if (SampleUnit.CustomPersonality.CockVoreInterestRandomized == false)
            obj.CustomPersonality.CockVoreInterest = SampleUnit.CustomPersonality.CockVoreInterest;
        if (SampleUnit.CustomPersonality.UnbirthInterestRandomized == false)
            obj.CustomPersonality.UnbirthInterest = SampleUnit.CustomPersonality.UnbirthInterest;

        CheckPeopleCount();
    }

    public void AddPersonFromTemplates()
    {
        if (PeopleObjects.Count >= MaxEntries)
            return;

        var obj = GetPersonFromTemplates(null, null);
        if (obj == null)
            return;


        PeopleObjects.Add(obj);

        CheckPeopleCount();
    }

    internal StartPersonUI GetPersonFromTemplates(GenderList genderList, RaceWeights raceWeights)
    {
        var savedTemplate = State.TemplateController.Container.GetRandom();
        if (savedTemplate == null)
            return null;

        var obj = Instantiate(PersonObjectPrefab, PersonFolder).GetComponent<StartPersonUI>();

        RefreshSexes(obj);

        if (genderList == null)
            genderList = GenderList;
        if (raceWeights == null)
            raceWeights = RaceWeights;

        if (savedTemplate.Gender == 0)
            obj.Gender.value = genderList.GetRandomGender();
        else
            obj.Gender.value = savedTemplate.Gender - 1;

        obj.FirstName.text = genderList.List[obj.Gender.value].FeminineName
            ? GetRandomFemaleName()
            : GetRandomMaleName();
        RefreshSexes(obj);

        obj.SetUpRaces();
        obj.SetUpPersonality();

        obj.LastName.text = NameGenerator.GetRandomLastName();
        obj.Gender.RefreshShownValue();
        obj.CanVore.isOn = Rand.NextFloat(0, 1) < savedTemplate.VoreOdds;
        if (savedTemplate.Orientation == 0)
            obj.Orientation.value = genderList.GetRandomOrientation(obj.Gender.value);
        else
            obj.Orientation.value = savedTemplate.Orientation - 1;
        obj.Orientation.RefreshShownValue();
        obj.Personality.value = savedTemplate.Personality;
        obj.Personality.RefreshShownValue();
        obj.CustomPersonality = new Personality();

        if (savedTemplate.Race == "Random")
        {
            var rand = raceWeights.GetRandomRace().Name;
            for (int i = 0; i < obj.Race.options.Count; i++)
            {
                if (obj.Race.options[i].text == rand)
                    obj.Race.value = i;
            }
        }
        else
        {
            TryToSetRace(obj, savedTemplate.Race);
        }

        obj.Race.RefreshShownValue();

        if (savedTemplate.CustomPersonality.CharismaRandomized == false)
            obj.CustomPersonality.Charisma = savedTemplate.CustomPersonality.Charisma;
        if (savedTemplate.CustomPersonality.PredWillingnessRandomized == false)
            obj.CustomPersonality.PredWillingness = savedTemplate.CustomPersonality.PredWillingness;
        if (savedTemplate.CustomPersonality.PreyWillingnessRandomized == false)
            obj.CustomPersonality.PreyWillingness = savedTemplate.CustomPersonality.PreyWillingness;
        if (savedTemplate.CustomPersonality.PromiscuityRandomized == false)
            obj.CustomPersonality.Promiscuity = savedTemplate.CustomPersonality.Promiscuity;
        if (savedTemplate.CustomPersonality.SexDriveRandomized == false)
            obj.CustomPersonality.SexDrive = savedTemplate.CustomPersonality.SexDrive;
        if (savedTemplate.CustomPersonality.StrengthRandomized == false)
            obj.CustomPersonality.Strength = savedTemplate.CustomPersonality.Strength;
        if (savedTemplate.CustomPersonality.VoracityRandomized == false)
            obj.CustomPersonality.Voracity = savedTemplate.CustomPersonality.Voracity;
        if (savedTemplate.CustomPersonality.VoraphiliaRandomized == false)
            obj.CustomPersonality.Voraphilia = savedTemplate.CustomPersonality.Voraphilia;
        if (savedTemplate.CustomPersonality.PredLoyaltyRandomized == false)
            obj.CustomPersonality.PredLoyalty = savedTemplate.CustomPersonality.PredLoyalty;
        if (savedTemplate.CustomPersonality.PreyDigestionInterestRandomized == false)
            obj.CustomPersonality.PreyDigestionInterest = savedTemplate
                .CustomPersonality
                .PreyDigestionInterest;
        if (savedTemplate.CustomPersonality.ExtroversionRandomized == false)
            obj.CustomPersonality.Extroversion = savedTemplate.CustomPersonality.Extroversion;
        if (savedTemplate.CustomPersonality.DominanceRandomized == false)
            obj.CustomPersonality.Dominance = savedTemplate.CustomPersonality.Dominance;
        if (savedTemplate.CustomPersonality.KindnessRandomized == false)
            obj.CustomPersonality.Kindness = savedTemplate.CustomPersonality.Kindness;
        if (savedTemplate.CustomPersonality.EndoDominatorRandomized == false)
            obj.CustomPersonality.EndoDominator = savedTemplate.CustomPersonality.EndoDominator;
        if (savedTemplate.CustomPersonality.PreferredClothingRandomized == false)
            obj.CustomPersonality.PreferredClothing = savedTemplate
                .CustomPersonality
                .PreferredClothing;

        if (savedTemplate.CustomPersonality.CheatOnPartnerRandomized == false)
            obj.CustomPersonality.CheatOnPartner = savedTemplate.CustomPersonality.CheatOnPartner;
        if (savedTemplate.CustomPersonality.CheatAcceptanceRandomized == false)
            obj.CustomPersonality.CheatAcceptance = savedTemplate.CustomPersonality.CheatAcceptance;

        if (savedTemplate.CustomPersonality.VorePreferenceRandomized == false)
            obj.CustomPersonality.VorePreference = savedTemplate.CustomPersonality.VorePreference;

        if (savedTemplate.CustomPersonality.OralVoreInterestRandomized == false)
            obj.CustomPersonality.OralVoreInterest = savedTemplate
                .CustomPersonality
                .OralVoreInterest;
        if (savedTemplate.CustomPersonality.AnalVoreInterestRandomized == false)
            obj.CustomPersonality.AnalVoreInterest = savedTemplate
                .CustomPersonality
                .AnalVoreInterest;
        if (savedTemplate.CustomPersonality.CockVoreInterestRandomized == false)
            obj.CustomPersonality.CockVoreInterest = savedTemplate
                .CustomPersonality
                .CockVoreInterest;
        if (savedTemplate.CustomPersonality.UnbirthInterestRandomized == false)
            obj.CustomPersonality.UnbirthInterest = savedTemplate.CustomPersonality.UnbirthInterest;

        return obj;
    }

    internal void AddPerson(SavedPerson person)
    {
        if (PeopleObjects.Count > MaxEntries)
            return;
        var obj = Instantiate(PersonObjectPrefab, PersonFolder).GetComponent<StartPersonUI>();
        PeopleObjects.Add(obj);
        RefreshSexes(obj);
        obj.SetUpRaces();
        obj.SetUpPersonality();

        obj.FirstName.text = person.FirstName;
        obj.LastName.text = person.LastName;
        obj.Gender.value = person.Gender;
        obj.Orientation.value = person.Orientation;
        obj.Personality.value = person.Personality;
        obj.CanVore.isOn = person.CanVore;
        obj.CustomAppearance = Utility.SerialClone(person.CustomAppearance);
        obj.CustomPersonality = Utility.SerialClone(person.CustomPersonality);
        obj.Person = Utility.SerialClone(person.Person);
        obj.Tags = person.Tags;
        if (person.Race.IsNullOrWhitespace())
            obj.Race.captionText.text = "Human";
        else
            TryToSetRace(obj, person.Race);

        obj.Gender.RefreshShownValue();
        obj.Orientation.RefreshShownValue();
        obj.Personality.RefreshShownValue();

        if (obj.Person != null)
        {
            obj.RaceTab.gameObject.SetActive(false);
            obj.CustomizeAppearance.gameObject.SetActive(false);
            obj.CustomizePersonality.interactable = false;
            obj.Personality.interactable = false;
            obj.CharacterTab.gameObject.SetActive(true);
            obj.GenCharacter.gameObject.SetActive(false);
            obj.Race.gameObject.SetActive(false);
            obj.CustomPersonality = null;
            obj.CustomAppearance = null;
        }
        else if (obj.CustomPersonality != null)
        {
            if (
                obj.CustomPersonality.AnalVoreInterest == 0
                && obj.CustomPersonality.OralVoreInterest == 0
                && obj.CustomPersonality.UnbirthInterest == 0
                && obj.CustomPersonality.CockVoreInterest == 0
            )
            {
                obj.CustomPersonality.AnalVoreInterest = Rand.NextFloat(0, 1);
                obj.CustomPersonality.OralVoreInterest = Rand.NextFloat(0, 1);
                obj.CustomPersonality.UnbirthInterest = Rand.NextFloat(0, 1);
                obj.CustomPersonality.CockVoreInterest = Rand.NextFloat(0, 1);
            }
            if (obj.CustomPersonality.PreyDigestionInterest == 0)
            {
                obj.CustomPersonality.PreyDigestionInterest = Rand.NextFloat(0, 1);
            }
        }
        else
        {
            obj.RaceTab.gameObject.SetActive(true);
            obj.CustomizePersonality.gameObject.SetActive(true);
            obj.CustomizeAppearance.gameObject.SetActive(true);
            obj.Personality.gameObject.SetActive(true);
            obj.CharacterTab.gameObject.SetActive(false);
            obj.GenCharacter.gameObject.SetActive(true);
        }

        CheckPeopleCount();
    }

    void CheckPeopleCount()
    {
            OverPop.text =
                $"{PeopleObjects.Count} characters / {MaxPeople} slots in map\nExcess chars will be selected randomly";
    }

    string GetRandomMaleName()
    {
        for (int i = 0; i < 500; i++)
        {
            var name = NameGenerator.GetRandomMaleName();
            if (PeopleObjects.Where(s => s.FirstName.text == name).Any())
                continue;
            return name;
        }
        return "Boy";
    }

    string GetRandomFemaleName()
    {
        for (int i = 0; i < 500; i++)
        {
            var name = NameGenerator.GetRandomFemaleName();
            if (PeopleObjects.Where(s => s.FirstName.text == name).Any())
                continue;
            return name;
        }
        return "Girl";
    }

    public void OpenCustomPersonality(StartPersonUI startPersonUI)
    {
        State.GameManager.VariableEditor.Open(
            startPersonUI.CustomPersonality,
            startPersonUI.FirstName.text + " " + startPersonUI.LastName.text
        );
    }

    public void RandomizeAppearanceToFemale()
    {
        EditingPerson.CustomAppearance = Create.Random(
            State.World.GenderList.List[1],
            RaceManager.GetRace(EditingPerson.Race.captionText.text)
        );
        OpenCustomAppearance(EditingPerson);
    }

    public void RandomizeAppearanceToMale()
    {
        EditingPerson.CustomAppearance = Create.Random(
            State.World.GenderList.List[0],
            RaceManager.GetRace(EditingPerson.Race.captionText.text)
        );
        OpenCustomAppearance(EditingPerson);
    }

    public void RandomizeAppearanceToGiant()
    {
        var being = EditingPerson.CustomAppearance;
        being.Height = Rand.NextFloat(74, 80);
        being.Weight = being.Height * being.Height / 32.5f * Rand.NextFloat(.95, 1.15f);
        if (being.BreastSize > 0)
        {
            being.BreastSize = Rand.NextFloat(2, 6);
        }

        if (being.DickSize > 0)
        {
            being.DickSize = Rand.NextFloat(2, 5);
            being.BallSize = being.DickSize * Rand.NextFloat(.7f, 1.3f);
        }
        OpenCustomAppearance(EditingPerson);
    }

    public void RandomizeAppearanceToDwarf()
    {
        var being = EditingPerson.CustomAppearance;
        being.Height = Rand.NextFloat(48, 56);
        being.Weight = being.Height * being.Height / 32.5f * Rand.NextFloat(.85, 1.15f);
        OpenCustomAppearance(EditingPerson);
        if (being.BreastSize > 0)
        {
            being.BreastSize = Rand.NextFloat(0, 4);
        }

        if (being.DickSize > 0)
        {
            being.DickSize = Rand.NextFloat(0, 3);
            being.BallSize = being.DickSize * Rand.NextFloat(.7f, 1.3f);
        }
    }

    public void OpenCustomAppearance(StartPersonUI startPersonUI)
    {
        State.GameManager.VariableEditor.Open(startPersonUI.CustomAppearance, startPersonUI.FirstName.text + " " + startPersonUI.LastName.text);
    }

    public void OpenSettings()
    {
        State.GameManager.VariableEditor.Open(Settings, "Game Settings");
        State.GameManager.VariableEditor.SetExtraButton(
            "Save as Defaults",
            () => State.WriteToFile(Settings, "Settings.dat")
        );
        State.GameManager.VariableEditor.SetSecondExtraButton(
            "Reset Settings to Original",
            () =>
            {
                var dialog = State.GameManager.CreateDialogBox();
                dialog.SetData(
                    () =>
                    {
                        State.GameManager.VariableEditor.Open(new Settings(), "Game Settings", false, true);
                    },
                    "Reset",
                    "Cancel",
                    "This will reset the listed options to the base game default settings, not your saved settings.  It basically changes them to how they'd be if you ran the game for the first time. "
                );
            }
        );
    }

    public void OpenTestingWorld()
    {
        TestingWorld.CreateTest();
        gameObject.SetActive(false);
    }

    public void OpenVariableEditorOnAppearance()
    {
        CloseCustomAppearance();
        State.GameManager.VariableEditor.Open(EditingPerson.CustomAppearance, "Appearance");
    }

    public void CloseCustomAppearance()
    {
        CustomizeAppearancePanel.SetActive(false);
        EditingPerson.CustomAppearance.Height = Height.value;
        EditingPerson.CustomAppearance.Weight = Weight.value;
        EditingPerson.CustomAppearance.BreastSize = BreastSize.value;
        EditingPerson.CustomAppearance.DickSize = DickSize.value;
        EditingPerson.CustomAppearance.BallSize = BallSize.value;
        EditingPerson.CustomAppearance.HairColor = HairColor.text;
        EditingPerson.CustomAppearance.HairLength = HairLength.text;
        EditingPerson.CustomAppearance.HairStyle = HairStyle.text;
        EditingPerson.CustomAppearance.EyeColor = EyeColor.text;
        EditingPerson.CustomAppearance.ShoulderDescription = ShoulderDescription.text;
        EditingPerson.CustomAppearance.HipDescription = HipDescription.text;
    }

    internal void TryToSetRace(StartPersonUI start, string race)
    {
        var index = RaceManager.PickableRaces.IndexOf(race);
        if (index >= 0)
        {
            start.Race.value = index;
            start.Race.RefreshShownValue();
        }
        else if (RaceManager.GetRace(race)?.NamedCharacter ?? false)
        {
            start.SetNamedRace(race);
        }
        else
        {
            start.Race.value = 0;
            start.Race.RefreshShownValue();
        }
    }

    public void OpenBulkSaver()
    {
        BulkCharacterSaver.gameObject.SetActive(true);
    }

    internal void RefreshAllSexes()
    {
        foreach (var obj in PeopleObjects)
        {
            RefreshSexes(obj);
        }
        RefreshSexesSample();
    }

    void RefreshSexes(StartPersonUI obj)
    {
        int option = obj.Gender.value;
        obj.Gender.ClearOptions();
        var genders = GenderList;
        if (State.GameManager.StartScreen.gameObject.activeSelf == false)
        {
            genders = State.World.GenderList;
        }
        var options = new List<string>()
        {
            genders.List[0].Name,
            genders.List[1].Name,
            genders.List[2].Name,
            genders.List[3].Name,
            genders.List[4].Name,
            genders.List[5].Name,
        };
        obj.Gender.AddOptions(options);
        obj.Gender.value = option;
        obj.Gender.RefreshShownValue();
    }

    void RefreshSexesSample()
    {
        int option = SampleUnit.Gender.value;
        SampleUnit.Gender.ClearOptions();
        var options = new List<string>()
        {
            "Random",
            GenderList.List[0].Name,
            GenderList.List[1].Name,
            GenderList.List[2].Name,
            GenderList.List[3].Name,
            GenderList.List[4].Name,
            GenderList.List[5].Name,
        };
        SampleUnit.Gender.AddOptions(options);
        SampleUnit.Gender.value = option;
        SampleUnit.Gender.RefreshShownValue();
    }
}
