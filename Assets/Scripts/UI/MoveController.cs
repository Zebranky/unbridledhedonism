﻿using UnityEngine;
using UnityEngine.UI;

public class MoveController : MonoBehaviour
{
    public Button NorthButton;
    public Button WestButton;
    public Button EastButton;
    public Button SouthButton;

    internal void UpdateButtons()
    {
        if (State.World.RemainingSkippedTurns <= 0 && State.World.PlayerIsObserver() == false)
        {
            NorthButton.interactable = State.World.CanMove(State.World.ControlledPerson, 0, 1);
            WestButton.interactable = State.World.CanMove(State.World.ControlledPerson, -1, 0);
            EastButton.interactable = State.World.CanMove(State.World.ControlledPerson, 1, 0);
            SouthButton.interactable = State.World.CanMove(State.World.ControlledPerson, 0, -1);
        }
        else
        {
            NorthButton.interactable = false;
            WestButton.interactable = false;
            EastButton.interactable = false;
            SouthButton.interactable = false;
        }
    }

    public void MoveWest()
    {
        State.World.Move(State.World.ControlledPerson, -1, 0);
        State.World.ControlledPerson.EndStreamingActions();
        State.World.ControlledPerson.AI.ClearTasks();
        State.World.NextTurn();
    }

    public void MoveEast()
    {
        State.World.Move(State.World.ControlledPerson, 1, 0);
        State.World.ControlledPerson.EndStreamingActions();
        State.World.ControlledPerson.AI.ClearTasks();
        State.World.NextTurn();
    }

    public void MoveNorth()
    {
        State.World.Move(State.World.ControlledPerson, 0, 1);
        State.World.ControlledPerson.EndStreamingActions();
        State.World.ControlledPerson.AI.ClearTasks();
        State.World.NextTurn();
    }

    public void MoveSouth()
    {
        State.World.Move(State.World.ControlledPerson, 0, -1);
        State.World.ControlledPerson.EndStreamingActions();
        State.World.ControlledPerson.AI.ClearTasks();
        State.World.NextTurn();
    }

    public void SkipTurn()
    {
        State.World.ControlledPerson.AI.ClearTasks();
        State.World.NextTurn();
        State.GameManager.RepeatAction.SetLastWait();
    }
}
