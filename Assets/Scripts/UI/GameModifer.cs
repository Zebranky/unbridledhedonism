﻿using UnityEngine;
using TMPro;
using System.Collections.Generic;
using UnityEngine.UI;

public class GameModifer : MonoBehaviour
{
    public TMP_Dropdown SelectedPerson;
    public TMP_Dropdown TargetPerson;

    public Button RelationshipIn;
    public Button RelationshipOut;
    public Button EveryoneToTarget;
    public Button TargetToEveryone;
    public Button EveryoneToEveryone;

    public void Open()
    {
        gameObject.SetActive(true);
        SelectedPerson.ClearOptions();
        TargetPerson.ClearOptions();
        List<string> people = new List<string>();
        int playerIndex = 0;
        int count = 0;
        foreach (Person person in State.World.GetPeople(true))
        {
            people.Add($"{person.FirstName} {person.LastName}");
            if (person == State.World.ControlledPerson)
                playerIndex = count;
            count++;
        }
        SelectedPerson.AddOptions(people);
        SelectedPerson.value = playerIndex;
        SelectedPerson.RefreshShownValue();
        TargetPerson.AddOptions(people);
        TargetPerson.value = 0;
        TargetPerson.RefreshShownValue();
    }

    public void DropdownsChanged()
    {
        RelationshipIn.interactable = SelectedPerson.value != TargetPerson.value;
        RelationshipOut.interactable = SelectedPerson.value != TargetPerson.value;
    }

    public void OpenGameSettings()
    {
        State.GameManager.VariableEditor.Open(State.World.Settings, "Game Settings");
    }

    public class DummyRelation
    {
        [ProperName("Friendship Level")]
        [Description("The level of friendship towards this person")]
        [Category("General")]
        [FloatRange(-1, 1)]
        public float FriendshipLevel { get; set; }

        [ProperName("Romantic Level")]
        [Description("The level of romantic interest towards this person")]
        [Category("General")]
        [FloatRange(-1, 1)]
        public float RomanticLevel { get; set; }
    }

    public void EditAll()
    {
        if (State.World.GetAllPeople(true).Count <= SelectedPerson.value)
            return;
        State.GameManager.VariableEditor.OpenAndProcessPerson(
            State.World.GetPeople(true)[SelectedPerson.value]
        );
    }

    //public void EditPersonality()
    //{
    //    State.GameManager.VariableEditor.Open(State.World.GetPeople(true)[SelectedPerson.value].Personality, $"{SelectedPerson.captionText.text}'s Personality");
    //}

    //public void EditAppearance()
    //{
    //    State.GameManager.VariableEditor.Open(State.World.GetPeople(true)[SelectedPerson.value].PartList, $"{SelectedPerson.captionText.text}'s Appearance");
    //}

    //public void EditNeeds()
    //{
    //    State.GameManager.VariableEditor.Open(State.World.GetPeople(true)[SelectedPerson.value].Needs, $"{SelectedPerson.captionText.text}'s Needs");
    //}

    //public void EditRomanticData()
    //{
    //    State.GameManager.VariableEditor.Open(State.World.GetPeople(true)[SelectedPerson.value].Romance, $"{SelectedPerson.captionText.text}'s Romantic Info");
    //}

    public void SaveCharacterOut()
    {
        State.SavedPersonController.AddCharacterToSaved(
            State.World.GetPeople(true)[SelectedPerson.value]
        );
    }

    public void EditTraits()
    {
        State.GameManager.TraitEditorScreen.Open(State.World.GetPeople(true)[SelectedPerson.value]);
    }

    public void EditRelationshipOut()
    {
        State.GameManager.VariableEditor.Open(
            State.World.GetPeople(true)[SelectedPerson.value].GetRelationshipWith(
                State.World.GetPeople(true)[TargetPerson.value]
            ),
            $"{SelectedPerson.captionText.text}'s relationship towards {TargetPerson.captionText.text}"
        );
    }

    public void EditRelationshipIn()
    {
        State.GameManager.VariableEditor.Open(
            State.World.GetPeople(true)[TargetPerson.value].GetRelationshipWith(
                State.World.GetPeople(true)[SelectedPerson.value]
            ),
            $"{TargetPerson.captionText.text}'s relationship towards {SelectedPerson.captionText.text}"
        );
    }

    public void EditRelationToEveryone()
    {
        DummyRelation rel = new DummyRelation();
        State.GameManager.VariableEditor.Open(
            rel,
            $"{TargetPerson.captionText.text}'s relationship towards everyone"
        );
        State.GameManager.VariableEditor.OnClose = new System.Action(() =>
        {
            var self = State.World.GetPeople(true)[TargetPerson.value];
            foreach (var target in State.World.GetPeople(true))
            {
                if (target == self)
                    continue;
                self.GetRelationshipWith(target).FriendshipLevel = rel.FriendshipLevel;
                self.GetRelationshipWith(target).RomanticLevel = rel.RomanticLevel;
            }
        });
    }

    public void EditEveryoneToTarget()
    {
        DummyRelation rel = new DummyRelation();
        State.GameManager.VariableEditor.Open(
            rel,
            $"Everyone's relation towards {TargetPerson.captionText.text}"
        );
        State.GameManager.VariableEditor.OnClose = new System.Action(() =>
        {
            var target = State.World.GetPeople(true)[TargetPerson.value];
            foreach (var self in State.World.GetPeople(true))
            {
                if (target == self)
                    continue;
                self.GetRelationshipWith(target).FriendshipLevel = rel.FriendshipLevel;
                self.GetRelationshipWith(target).RomanticLevel = rel.RomanticLevel;
            }
        });
    }

    public void EditEveryoneToEveryone()
    {
        DummyRelation rel = new DummyRelation();
        State.GameManager.VariableEditor.Open(rel, $"Everyone's relationship towards everyone");
        State.GameManager.VariableEditor.OnClose = new System.Action(() =>
        {
            foreach (var self in State.World.GetPeople(true))
            {
                foreach (var target in State.World.GetPeople(true))
                {
                    if (target == self)
                        continue;
                    self.GetRelationshipWith(target).FriendshipLevel = rel.FriendshipLevel;
                    self.GetRelationshipWith(target).RomanticLevel = rel.RomanticLevel;
                }
            }
        });
    }

    public void CheckRaceChanged() { }

    public void CloseEditor()
    {
        gameObject.SetActive(false);

        foreach (Person person in State.World.GetPeople(true))
        {
            if (
                person.VoreController.StomachDigestsPrey
                && (State.World.Settings.CheckDigestion(person, DigestionAlias.CanVore) == false)
            )
                person.VoreController.StomachDigestsPrey = false;
            else if (
                person.VoreController.StomachDigestsPrey == false
                && (State.World.Settings.CheckDigestion(person, DigestionAlias.CanEndo) == false)
            )
                person.VoreController.StomachDigestsPrey = true;
            if (
                person.VoreController.WombAbsorbsPrey
                && (State.World.Settings.CheckDigestion(person, DigestionAlias.CanVore) == false)
            )
                person.VoreController.WombAbsorbsPrey = false;
            else if (
                person.VoreController.WombAbsorbsPrey == false
                && (State.World.Settings.CheckDigestion(person, DigestionAlias.CanEndo) == false)
            )
                person.VoreController.WombAbsorbsPrey = true;
            if (
                person.VoreController.BallsAbsorbPrey
                && (State.World.Settings.CheckDigestion(person, DigestionAlias.CanVore) == false)
            )
                person.VoreController.BallsAbsorbPrey = false;
            else if (
                person.VoreController.BallsAbsorbPrey == false
                && (State.World.Settings.CheckDigestion(person, DigestionAlias.CanEndo) == false)
            )
                person.VoreController.BallsAbsorbPrey = true;
            if (
                person.VoreController.BowelsDigestPrey
                && (State.World.Settings.CheckDigestion(person, DigestionAlias.CanVore) == false)
            )
                person.VoreController.BowelsDigestPrey = false;
            else if (
                person.VoreController.BowelsDigestPrey == false
                && (State.World.Settings.CheckDigestion(person, DigestionAlias.CanEndo) == false)
            )
                person.VoreController.BowelsDigestPrey = true;
        }
        State.GameManager.DisplayInfo();
    }
}
