﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class OptionsScreen : MonoBehaviour
{
    [Description(
        "Hides the various turned on messages (turned on by sex, vore, etc.)  They were designed to let you know that things are changing but can get a little spammy if there's a lot of people nearby"
    )]
    public Toggle HideTurnedOnMessages;

    [Description("Switches the game from displaying imperial units to metric units")]
    public Toggle UseMetric;

    [Description("Uses initials instead of single letters for people")]
    public Toggle UseInitials;

    [Description(
        "Makes identifiers use the gender based colors in the gender settings, can sometimes make letters hard to read depending on the colors"
    )]
    public Toggle ColoredNames;

    [Description(
        "Prey names will be colored based on their current location in the listing of players in a tile, if disabled they'll all be yellow"
    )]
    public Toggle ColoredPreyNames;

    [Description(
        "If enabled, the displayed image for a character will be the full image of their pred (if it exists), instead of the prey image"
    )]
    public Toggle AltBellyImage;

    [Description(
        "If enabled, character images will display a location backdrop behind them. Best results with Kisekae character portraits"
    )]
    public Toggle UseBackgrounds;

    [Description(
        "<u>Requires a restart!</u> If enabled, the displayed images for characters be cropped to the same size. Best results with Kisekae character portraits"
    )]
    public Toggle CropPortraits;

    [Description(
        "<u>Requires a restart!</u> If enabled, default character images will not be used. Suppresses character silhouettes and other defaults"
    )]
    public Toggle DisableDefaultImages;

    [Description(
        "If enabled, character action buttons will appear at the top of the sidebar and goals, statuses, etc will be moved down"
    )]
    public Toggle ActionsFirstInSidebar;

    [Description(
        "If enabled, text for character stats will be replaced with a graphical bar that fills/depletes"
    )]
    public Toggle DisplayStatbars;

    [Description(
        "If enabled, the \"Misc Stats\" screen will show detailed statistics for each vore type"
    )]
    public Toggle DetailedVoreStats;

    [Description(
        "If enabled, the \"Misc Stats\" screen will show detailed statistics for each aspect of weight gain"
    )]
    public Toggle DetailedWGStats;

    [Description(
        "Prevents loading of portrait icon previews on the setup screen. Increases performance when loading a large number of characters."
    )]
    public Toggle DisableSetupIcons;

    [Description(
        "If enabled, romantic actions will be hidden from the player sidebar if the player character does not like target character gender"
    )]
    public Toggle HideIncompatibleActions;

    [Description("Shows the ai characters's current goal in their info panel")]
    public Toggle DebugViewGoals;

    [Description("Shows the precise values for variables, instead of just the words")]
    public Toggle DebugPreciseValues;

    [Description(
        "Every single event that happens will show up in your chat log, instead of just the ones your character observes"
    )]
    public Toggle DebugViewAllEvents;

    [Description(
        "Turns off the pop-up that shows up when you hover over an interaction explaining what it does"
    )]
    public Toggle HidePopupTooltip;

    [Description(
        "With this on, the camera will center on your character at the beginning of every turn."
    )]
    public Toggle AutoCenterCamera;

    [Description(
        "Can only see tiles and view people on them that your character has vision to (or all tiles if observer).  If you're eaten, you can only see things on your current tile.  Note that if the see through walls game setting is enabled, it will also affect the vision for this."
    )]
    public Toggle PlayerVision;

    [Description(
        "With this on, actions you've performed will have their text in a different color."
    )]
    public Toggle HighlightPlayerText;

    [Description(
        "With this on, tiles on which vore is occuring will be highlighted (you can change the graphic the same as you can with tiles)."
    )]
    public Toggle HighlightVore;

    [Description(
        "With this on, tiles on which sex is occuring will be highlighted (you can change the graphic the same as you can with tiles)."
    )]
    public Toggle HighlightSex;

    [Description(
        "With this on, tiles on which disposal is occuring will be highlighted (you can change the graphic the same as you can with tiles)."
    )]
    public Toggle HighlightDisposal;

    [Description(
        "The percentage of vore prey messages that will be ignored (Useful if you have many prey, particularly endo).  Does not hide important messages, like struggles, state changes or prey eating other prey"
    )]
    public Slider SuppressVoreMessages;

    [Description(
        "The percentage of vore endo prey status messages that will be ignored.  These probably should have always been uncommon since they're just checking in on the prey and not representative of any actions, but now you can adjust the odds."
    )]
    public Slider SuppressEndoMessages;

    [Description("Controls whether Wasd controls nothing, camera movement, or player movement")]
    public TMP_Dropdown Wasd;

    [Description(
        "Controls whether the arrow keys control nothing, camera movement, or player movement"
    )]
    public TMP_Dropdown ArrowKeys;

    [Description(
        "Controls whether the numpad control nothing, camera movement, or player movement (Note that if numlock is off, the numpad acts like the arrow keys at the system level)"
    )]
    public TMP_Dropdown Numpad;

    //public Text TooltipText;

    private void Start()
    {
        WireUpTooltips.WireUp<OptionsScreen, HoveringTooltipDisplayer>(this);
    }

    public void Open()
    {
        GetValues();
        gameObject.SetActive(true);
    }

    public void CloseAndSave()
    {
        SetNewValues();
        gameObject.SetActive(false);
        if (State.GameManager.TitleScreen.gameObject.activeSelf == false)
            State.GameManager.DisplayInfo();
    }

    //public void AskClearAllSettings()
    //{
    //    var box = Instantiate(State.GameManager.DialogBoxPrefab).GetComponent<DialogBox>();
    //    box.SetData(ClearAllSettings, "Delete them all", "Cancel", "This clears all saved settings in options and content menus, are you sure you want to do this?");
    //}

    //void ClearAllSettings()
    //{
    //    PlayerPrefs.DeleteAll();
    //    GetValues();
    //}


    void GetValues()
    {
        SuppressVoreMessages.value = PlayerPrefs.GetFloat("SuppressVoreMessages", 0);
        SuppressEndoMessages.value = PlayerPrefs.GetFloat("SuppressEndoMessages", 0.75f);
        HideTurnedOnMessages.isOn = PlayerPrefs.GetInt("HideTurnedOnMessages", 1) == 1;
        DebugViewGoals.isOn = PlayerPrefs.GetInt("DebugViewGoals", 0) == 1;
        DebugPreciseValues.isOn = PlayerPrefs.GetInt("DebugPreciseValues", 0) == 1;
        DebugViewAllEvents.isOn = PlayerPrefs.GetInt("DebugViewAllEvents", 0) == 1;
        HidePopupTooltip.isOn = PlayerPrefs.GetInt("HidePopupTooltip", 0) == 1;
        AutoCenterCamera.isOn = PlayerPrefs.GetInt("AutoCenterCamera", 0) == 1;
        HighlightPlayerText.isOn = PlayerPrefs.GetInt("HighlightPlayerText", 0) == 1;
        HighlightVore.isOn = PlayerPrefs.GetInt("HighlightVore", 0) == 1;
        HighlightSex.isOn = PlayerPrefs.GetInt("HighlightSex", 0) == 1;
        HighlightDisposal.isOn = PlayerPrefs.GetInt("HighlightDisposal", 0) == 1;
        PlayerVision.isOn = PlayerPrefs.GetInt("PlayerVision", 0) == 1;
        UseMetric.isOn = PlayerPrefs.GetInt("UseMetric", 0) == 1;
        UseInitials.isOn = PlayerPrefs.GetInt("UseInitials", 0) == 1;
        ColoredNames.isOn = PlayerPrefs.GetInt("ColoredNames", 0) == 1;
        ColoredPreyNames.isOn = PlayerPrefs.GetInt("ColoredPreyNames", 0) == 1;
        AltBellyImage.isOn = PlayerPrefs.GetInt("AltBellyImage", 0) == 1;
        UseBackgrounds.isOn = PlayerPrefs.GetInt("UseBackgrounds", 0) == 1;
        CropPortraits.isOn = PlayerPrefs.GetInt("CropPortraits", 0) == 1;
        DisableDefaultImages.isOn = PlayerPrefs.GetInt("DisableDefaultImages", 0) == 1;
        ActionsFirstInSidebar.isOn = PlayerPrefs.GetInt("ActionsFirstInSidebar", 0) == 1;
        DisplayStatbars.isOn = PlayerPrefs.GetInt("DisplayStatbars", 0) == 1;
        DetailedVoreStats.isOn = PlayerPrefs.GetInt("DetailedVoreStats", 0) == 1;
        DetailedWGStats.isOn = PlayerPrefs.GetInt("DetailedWGStats", 0) == 1;
        DisableSetupIcons.isOn = PlayerPrefs.GetInt("DisableSetupIcons", 0) == 1;
        HideIncompatibleActions.isOn = PlayerPrefs.GetInt("HideIncompatibleActions", 0) == 1;
        Wasd.value = PlayerPrefs.GetInt("Wasd", 1);
        ArrowKeys.value = PlayerPrefs.GetInt("ArrowKeys", 1);
        Numpad.value = PlayerPrefs.GetInt("Numpad", 2);

        LoadFromStored();
    }

    //public void RefreshText()
    //{
    //    StrategicDelaySlider.GetComponentInChildren<Text>().text = $"{StrategicDelaySlider.name}: {Math.Round(StrategicDelaySlider.value, 3)} sec";
    //    TacticalPlayerMovementDelaySlider.GetComponentInChildren<Text>().text = $"{TacticalPlayerMovementDelaySlider.name}: {Math.Round(TacticalPlayerMovementDelaySlider.value, 3)} sec";
    //    TacticalAIMovementDelaySlider.GetComponentInChildren<Text>().text = $"{TacticalAIMovementDelaySlider.name}: {Math.Round(TacticalAIMovementDelaySlider.value, 3)} sec";
    //    TacticalAttackDelaySlider.GetComponentInChildren<Text>().text = $"{TacticalAttackDelaySlider.name}: {Math.Round(TacticalAttackDelaySlider.value, 3)} sec";
    //    TacticalVoreDelaySlider.GetComponentInChildren<Text>().text = $"{TacticalVoreDelaySlider.name}: {Math.Round(TacticalVoreDelaySlider.value, 3)} sec";
    //}

    void SetNewValues()
    {
        //PlayerPrefs.SetFloat("StrategicDelaySlider", StrategicDelaySlider.value);
        PlayerPrefs.SetFloat("SuppressVoreMessages", SuppressVoreMessages.value);
        PlayerPrefs.SetFloat("SuppressEndoMessages", SuppressEndoMessages.value);
        PlayerPrefs.SetInt("HideTurnedOnMessages", HideTurnedOnMessages.isOn ? 1 : 0);
        PlayerPrefs.SetInt("DebugViewGoals", DebugViewGoals.isOn ? 1 : 0);
        PlayerPrefs.SetInt("DebugPreciseValues", DebugPreciseValues.isOn ? 1 : 0);
        PlayerPrefs.SetInt("DebugViewAllEvents", DebugViewAllEvents.isOn ? 1 : 0);
        PlayerPrefs.SetInt("HidePopupTooltip", HidePopupTooltip.isOn ? 1 : 0);
        PlayerPrefs.SetInt("AutoCenterCamera", AutoCenterCamera.isOn ? 1 : 0);
        PlayerPrefs.SetInt("HighlightPlayerText", HighlightPlayerText.isOn ? 1 : 0);
        PlayerPrefs.SetInt("HighlightVore", HighlightVore.isOn ? 1 : 0);
        PlayerPrefs.SetInt("HighlightSex", HighlightSex.isOn ? 1 : 0);
        PlayerPrefs.SetInt("HighlightDisposal", HighlightDisposal.isOn ? 1 : 0);
        PlayerPrefs.SetInt("PlayerVision", PlayerVision.isOn ? 1 : 0);
        PlayerPrefs.SetInt("UseMetric", UseMetric.isOn ? 1 : 0);
        PlayerPrefs.SetInt("UseInitials", UseInitials.isOn ? 1 : 0);
        PlayerPrefs.SetInt("ColoredNames", ColoredNames.isOn ? 1 : 0);
        PlayerPrefs.SetInt("ColoredPreyNames", ColoredPreyNames.isOn ? 1 : 0);
        PlayerPrefs.SetInt("AltBellyImage", AltBellyImage.isOn ? 1 : 0);
        PlayerPrefs.SetInt("UseBackgrounds", UseBackgrounds.isOn ? 1 : 0);
        PlayerPrefs.SetInt("CropPortraits", CropPortraits.isOn ? 1 : 0);
        PlayerPrefs.SetInt("DisableDefaultImages", DisableDefaultImages.isOn ? 1 : 0);
        PlayerPrefs.SetInt("ActionsFirstInSidebar", ActionsFirstInSidebar.isOn ? 1 : 0);
        PlayerPrefs.SetInt("DisplayStatbars", DisplayStatbars.isOn ? 1 : 0);
        PlayerPrefs.SetInt("DetailedWGStats", DetailedWGStats.isOn ? 1 : 0);
        PlayerPrefs.SetInt("DetailedVoreStats", DetailedVoreStats.isOn ? 1 : 0);
        PlayerPrefs.SetInt("DisableSetupIcons", DisableSetupIcons.isOn ? 1 : 0);
        PlayerPrefs.SetInt("HideIncompatibleActions", HideIncompatibleActions.isOn ? 1 : 0);
        PlayerPrefs.SetInt("Wasd", Wasd.value);
        PlayerPrefs.SetInt("ArrowKeys", ArrowKeys.value);
        PlayerPrefs.SetInt("Numpad", Numpad.value);
        LoadFromStored();
        PlayerPrefs.Save();
    }

    public void LoadFromStored()
    {
        //Config.StrategicAIMoveDelay = PlayerPrefs.GetFloat("StrategicDelaySlider", .25f);
        Config.SuppressVoreMessages = PlayerPrefs.GetFloat("SuppressVoreMessages", 0);
        Config.SuppressEndoMessages = PlayerPrefs.GetFloat("SuppressEndoMessages", 0);
        Config.HideTurnedOnMessages = PlayerPrefs.GetInt("HideTurnedOnMessages", 1) == 1;
        Config.DebugViewGoals = PlayerPrefs.GetInt("DebugViewGoals", 0) == 1;
        Config.DebugViewPreciseValues = PlayerPrefs.GetInt("DebugPreciseValues", 0) == 1;
        Config.DebugViewAllEvents = PlayerPrefs.GetInt("DebugViewAllEvents", 0) == 1;
        Config.HidePopupTooltip = PlayerPrefs.GetInt("HidePopupTooltip", 0) == 1;
        Config.AutoCenterCamera = PlayerPrefs.GetInt("AutoCenterCamera", 0) == 1;
        Config.HighlightPlayerText = PlayerPrefs.GetInt("HighlightPlayerText", 0) == 1;
        Config.HighlightVore = PlayerPrefs.GetInt("HighlightVore", 0) == 1;
        Config.HighlightSex = PlayerPrefs.GetInt("HighlightSex", 0) == 1;
        Config.HighlightDisposal = PlayerPrefs.GetInt("HighlightDisposal", 0) == 1;
        Config.PlayerVision = PlayerPrefs.GetInt("PlayerVision", 0) == 1;
        Config.UseMetric = PlayerPrefs.GetInt("UseMetric", 0) == 1;
        Config.UseInitials = PlayerPrefs.GetInt("UseInitials", 1) == 1;
        Config.ColoredNames = PlayerPrefs.GetInt("ColoredNames", 1) == 1;
        Config.ColoredPreyNames = PlayerPrefs.GetInt("ColoredPreyNames", 0) == 1;
        Config.AltBellyImage = PlayerPrefs.GetInt("AltBellyImage", 0) == 1;
        Config.UseBackgrounds = PlayerPrefs.GetInt("UseBackgrounds", 0) == 1;
        Config.CropPortraits = PlayerPrefs.GetInt("CropPortraits", 0) == 1;
        Config.DisableDefaultImages = PlayerPrefs.GetInt("DisableDefaultImages", 0) == 1;
        Config.ActionsFirstInSidebar = PlayerPrefs.GetInt("ActionsFirstInSidebar", 0) == 1;
        Config.DisplayStatbars = PlayerPrefs.GetInt("DisplayStatbars", 0) == 1;
        Config.DetailedWGStats = PlayerPrefs.GetInt("DisplayDetailedStatistics", 0) == 1;
        Config.DetailedVoreStats = PlayerPrefs.GetInt("DisplayDetailedStatistics", 0) == 1;
        Config.DisableSetupIcons = PlayerPrefs.GetInt("DisableSetupIcons", 0) == 1;
        Config.HideIncompatibleActions = PlayerPrefs.GetInt("HideIncompatibleActions", 0) == 1;
        Config.Wasd = (KeySetType)PlayerPrefs.GetInt("Wasd", 1);
        Config.ArrowKeys = (KeySetType)PlayerPrefs.GetInt("ArrowKeys", 1);
        Config.Numpad = (KeySetType)PlayerPrefs.GetInt("Numpad", 1);
    }

    //internal void ChangeToolTip(int value)
    //{
    //    TooltipText.text = DefaultTooltips.Tooltip(value);
    //}
}
