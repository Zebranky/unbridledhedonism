﻿using UnityEngine;
using TMPro;
using System;
using UnityEngine.UI;

public class SetKeys : MonoBehaviour
{
    public Button Submit;
    public Button Submit2;
    public Button Cancel;
    public Button AIControl;
    public Button Wait;
    public Button CenterCameraOnPlayer;
    public Button RepeatAction;

    Button SelectedButton;

    void Start()
    {
        Submit.onClick.AddListener(() => SelectedButton = Submit);
        Submit2.onClick.AddListener(() => SelectedButton = Submit2);
        Cancel.onClick.AddListener(() => SelectedButton = Cancel);
        AIControl.onClick.AddListener(() => SelectedButton = AIControl);
        Wait.onClick.AddListener(() => SelectedButton = Wait);
        CenterCameraOnPlayer.onClick.AddListener(() => SelectedButton = CenterCameraOnPlayer);
        RepeatAction.onClick.AddListener(() => SelectedButton = RepeatAction);
        UpdateLabels();
    }

    void UpdateLabels()
    {
        Submit.GetComponentInChildren<TextMeshProUGUI>().text = State.KeyManager.SubmitKey;
        Submit2.GetComponentInChildren<TextMeshProUGUI>().text = State.KeyManager.Submit2Key;
        Cancel.GetComponentInChildren<TextMeshProUGUI>().text = State.KeyManager.CancelKey;
        AIControl.GetComponentInChildren<TextMeshProUGUI>().text = State.KeyManager.AIControlKey;
        Wait.GetComponentInChildren<TextMeshProUGUI>().text = State.KeyManager.WaitKey;
        CenterCameraOnPlayer.GetComponentInChildren<TextMeshProUGUI>().text = State
            .KeyManager
            .CenterCameraOnPlayerKey;
        RepeatAction.GetComponentInChildren<TextMeshProUGUI>().text = State
            .KeyManager
            .RepeatActionKey;
    }

    public void ResetToDefault()
    {
        State.KeyManager.ChangeSubmitKey(KeyCode.Return);
        State.KeyManager.ChangeSubmit2Key(KeyCode.KeypadEnter);
        State.KeyManager.ChangeCancelKey(KeyCode.Space);
        State.KeyManager.ChangeAIControlKey(KeyCode.Slash);
        State.KeyManager.ChangeWaitKey(KeyCode.Keypad5);
        State.KeyManager.ChangeCenterCameraOnPlayerKey(KeyCode.Period);
        State.KeyManager.ChangeRepeatActionKey(KeyCode.R);
        SelectedButton = null;
        UpdateLabels();
    }

    public void CloseScreen()
    {
        gameObject.SetActive(false);
        SelectedButton = null;
    }

    void Update()
    {
        if (gameObject.activeSelf && Input.anyKeyDown && SelectedButton != null)
        {
            foreach (KeyCode key in (KeyCode[])Enum.GetValues(typeof(KeyCode)))
            {
                if (key.ToString().Contains("Mouse"))
                    continue;
                if (Input.GetKeyDown(key))
                {
                    if (SelectedButton == Submit)
                        State.KeyManager.ChangeSubmitKey(key);
                    if (SelectedButton == Submit2)
                        State.KeyManager.ChangeSubmit2Key(key);
                    if (SelectedButton == Cancel)
                        State.KeyManager.ChangeCancelKey(key);
                    if (SelectedButton == AIControl)
                        State.KeyManager.ChangeAIControlKey(key);
                    if (SelectedButton == Wait)
                        State.KeyManager.ChangeWaitKey(key);
                    if (SelectedButton == CenterCameraOnPlayer)
                        State.KeyManager.ChangeCenterCameraOnPlayerKey(key);
                    if (SelectedButton == RepeatAction)
                        State.KeyManager.ChangeRepeatActionKey(key);
                    SelectedButton = null;
                    UpdateLabels();
                    break;
                }
            }
        }
    }
}
