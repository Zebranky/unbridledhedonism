﻿using UnityEngine;
using TMPro;

public class CombinedInputfield : MonoBehaviour
{
    public TextMeshProUGUI Text;
    public TMP_InputField Inputfield;
}
