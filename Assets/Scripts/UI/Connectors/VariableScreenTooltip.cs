﻿using UnityEngine;
using UnityEngine.EventSystems;
using Assets.Scripts.UI.Connectors;

public class VariableScreenTooltip
    : MonoBehaviour,
        IPointerEnterHandler,
        IPointerExitHandler,
        ITooltip
{
    public string Text { get; set; }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (State.GameManager.VariableEditor.gameObject.activeSelf)
            State.GameManager.VariableEditor.ChangeToolTip(Text);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if (State.GameManager.VariableEditor.gameObject.activeSelf)
            State.GameManager.VariableEditor.ChangeToolTip("");
    }
}
