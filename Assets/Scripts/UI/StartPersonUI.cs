﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class StartPersonUI : MonoBehaviour
{
    public TMP_InputField FirstName;
    public TMP_InputField LastName;
    public TMP_Dropdown Gender;
    public TMP_Dropdown Orientation;
    public TMP_Dropdown Personality;
    public TMP_Dropdown Race;
    public Toggle CanVore;
    public Button CustomizePersonality;
    public Button CustomizeAppearance;
    public Button RemoveButton;
    public Button SaveButton;
    public Button SaveButton2;
    public RectTransform RaceTab;
    public RectTransform CharacterTab;

    public Button GenCharacter;

    public RectTransform Picture;

    public Button EditCharacter;
    public Button EditTraits;

    internal Personality CustomPersonality;
    internal PartList CustomAppearance;
    internal Person Person;

    internal string Tags;

    internal IconController IconController;

    public void PersonalityChanged()
    {
        CustomizePersonality.interactable = Personality.captionText.text == "<i>Custom</i>" || Personality.captionText.text == "Custom";
    }

    internal void SetNamedRace(string name)
    {
        Race.captionText.text = name;
        Race.interactable = false;
        Race.ClearOptions();
        Race.AddOptions(new List<string>() { name });
        Gender.interactable = false;
        FirstName.interactable = false;
        LastName.interactable = false;
        CustomizeAppearance.interactable = false;
        SaveButton.interactable = false;
    }

    void GenerateCharacter()
    {
        Person = StartScreen.CreatePerson(this);
        RaceTab.gameObject.SetActive(false);
        CustomizePersonality.interactable = false;
        Personality.interactable = false;
        CustomizeAppearance.gameObject.SetActive(false);
        CharacterTab.gameObject.SetActive(true);
        GenCharacter.gameObject.SetActive(false);
        Race.gameObject.SetActive(false);
        CustomPersonality = null;
        CustomAppearance = null;
        SetUpIcon();
    }

    internal void SetUpRaces()
    {
        Race.AddOptions(RaceManager.PickableRaces);
    }

    internal void SetUpPersonality()
    {
        Personality.ClearOptions();
        Personality.AddOptions(StartScreen.PersonalityOptions);
    }

    internal void SetUpIcon()
    {
        if (Config.DisableSetupIcons)
            return;

        IconController = new IconController();

        if (IconController.GetIcon(Person) != null)
            Picture.gameObject.GetComponent<Image>().sprite = IconController.GetIcon(Person);
    }

    internal void RaceChanged()
    {
        if (
            CustomAppearance.Race != Race.captionText.text
            && State.World.GenderList?.List?.Count() > 0
        )
            CustomAppearance = Create.Random(
                State.World.GenderList.List[
                    Math.Min(Gender.value, State.World.GenderList.List.Count() - 1)
                ],
                RaceManager.GetRace(Race.captionText.text)
            );
    }

    private void Start()
    {
        RemoveButton.onClick.AddListener(
            () => State.GameManager.StartScreen.RemovePersonObject(this)
        );
        if (CustomPersonality == null)
            CustomPersonality = new Personality();
        if (CustomAppearance == null)
        {
            CustomAppearance = Create.Random(
                State.World.GenderList.List[Gender.value],
                RaceManager.GetRace(Race.captionText.text)
            );
        }
        Race.onValueChanged.AddListener((s) => RaceChanged());

        CustomizePersonality.onClick.AddListener(
            () => State.GameManager.StartScreen.OpenCustomPersonality(this)
        );
        CustomizeAppearance.onClick.AddListener(
            () => State.GameManager.StartScreen.OpenCustomAppearance(this)
        );
        EditCharacter.onClick.AddListener(() =>
        {
            Person.FirstName = FirstName.text;
            Person.LastName = LastName.text;
            Person.Gender = Gender.value;
            Person.Romance.Orientation = (Orientation)Orientation.value;
            Person.VoreController.GeneralVoreCapable = CanVore.isOn;

            State.GameManager.VariableEditor.OpenAndProcessPerson(Person);
        });
        SaveButton.onClick.AddListener(
            () => State.GameManager.StartScreen.SavedCharacterScreen.SaveCharacter(this)
        );
        SaveButton2.onClick.AddListener(
            () => State.GameManager.StartScreen.SavedCharacterScreen.SaveCharacter(this)
        );
        GenCharacter.onClick.AddListener(() => GenerateCharacter());
        EditTraits.onClick.AddListener(() => State.GameManager.TraitEditorScreen.Open(Person));

        if (Person != null)
            SetUpIcon(); 
        else
            CharacterTab.gameObject.SetActive(false);
    }
}
