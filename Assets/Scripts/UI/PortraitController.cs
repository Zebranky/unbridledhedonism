using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

enum PortraitFlag
{
    Dead,
    Alive, // New, implied when dead isn't set.
    Eaten,
    Full,
    Swallowing,
    BeingSwallowed,
    Both, // Implies Belly/Balls.
    Belly, // New.
    Balls,
    Stomach,
    Bowels,
    Womb,
    Unwilling,
    Weakened,
    Healthy,
    Nude,
    Underwear,
    Undressed,
    Disposal,
    NonDisposal, // Implied when Disposal is not set.
    Horny,
    Absorbing,
    Multiple,
    Hungry,
    CV,
    UB,
    AV,
    OV,
    Digesting, // If this person is eaten and being digested.
    Safe, // If this person is eaten and not being digested.

    // weight gain related
    Gain, // Implies Gain +10% or more
    Weight, // weight gain
    Breast, // breast size
    Dick, // dick/balls size

    // size diff related
    Shrink, // affected by shrink spell
    Tiny, // affected by shrink and < 2ft tall
    Grow, // affected by grow spell
    Huge, // affected by grow and > 12ft tall

    Fem, // use with "default", for chars w/ boobs
    Masc, // use with "default", for chars w/o boobs

    Icon, // used with the IconController, is in this list so it doesnt throw an error as an invalid flag
}

class PortraitController
{
    /// <summary>PicturesAny[name][flags] = filepath</summary>
    Dictionary<string, Dictionary<HashSet<PortraitFlag>, string>> PicturesAny;

    /// <summary>Pictures[name][flags] = sprite</summary>
    Dictionary<string, Dictionary<HashSet<PortraitFlag>, Sprite>> PicturesLoaded;

    public PortraitController()
    {
        PicturesAny = new Dictionary<string, Dictionary<HashSet<PortraitFlag>, string>>();
        PicturesLoaded = new Dictionary<string, Dictionary<HashSet<PortraitFlag>, Sprite>>();

        foreach (string filepath in GetPictureFiles())
        {
            string filename = Path.GetFileNameWithoutExtension(filepath).ToLower();
            var name = filename.Split('-')[0];
            var flags = filename.Split('-').ElementAtOrDefault(1);
            if (!PicturesAny.ContainsKey(name))
                PicturesAny[name] = new Dictionary<HashSet<PortraitFlag>, string>();
            PicturesAny[name][ParseStringToFlags(flags, filepath)] = filepath;
        }
    }

    /// <summary>Return all valid picture files.</summary>
    internal static List<string> GetPictureFiles()
    {
        var files = Directory
            .GetFiles(
                Path.Combine(Application.streamingAssetsPath, "Pictures"),
                "*.*",
                SearchOption.AllDirectories
            )
            .ToList();
        files.AddRange(Directory.GetFiles(State.PicDirectory, "*.*", SearchOption.AllDirectories));
        return files
            .Where(
                s =>
                    s.EndsWith(".png", StringComparison.InvariantCultureIgnoreCase)
                    || s.EndsWith(".jpg", StringComparison.InvariantCultureIgnoreCase)
                    || s.EndsWith(".jpeg", StringComparison.InvariantCultureIgnoreCase)
            )
            .ToList();
    }

    /// <summary>Returns the plain hash set of flags from a contiguous string of flag names without more processing.</summary>
    internal static HashSet<PortraitFlag> ParseStringToFlagsRaw(string str, string filepath)
    {
        if (String.IsNullOrWhiteSpace(str))
            return new HashSet<PortraitFlag>();
        str = str.ToLower();
        foreach (
            var flagName in Enum.GetNames(typeof(PortraitFlag)).OrderByDescending(x => x.Length)
        )
        {
            if (!str.StartsWith(flagName.ToLower()))
                continue;
            var flags = ParseStringToFlags(str.Substring(flagName.Length));
            flags.Add((PortraitFlag)Enum.Parse(typeof(PortraitFlag), flagName));
            return flags;
        }

        if (UnityEngine.Object.FindObjectOfType<MessageBox>() == null)
            State.GameManager.CreateMessageBox(
                $"Did not understand condition \"{str}\" in portrait image at:\n\n<size=80%>{filepath}</size>\n\nPlease rename or remove this image."
            );
        Debug.LogError($"Can not parse next flag from \"{str}\" in image at {filepath}");
        return new HashSet<PortraitFlag>();
    }

    /// <summary>Returns a hash set of flags from a contiguous string of flag names. Takes the filepath too in order to spit it back out if an error occurs.</summary>
    internal static HashSet<PortraitFlag> ParseStringToFlags(string str, string filepath = "filename not provided")
    {
        var flags = ParseStringToFlagsRaw(str, filepath);
        if (flags.Count == 0)
            return flags; // Default image.
        // Keep both/belly/balls flags consistent.
        if (flags.Contains(PortraitFlag.Both))
        {
            flags.Add(PortraitFlag.Belly);
            flags.Add(PortraitFlag.Balls);
        }
        if (flags.Contains(PortraitFlag.Belly) && flags.Contains(PortraitFlag.Balls))
            flags.Add(PortraitFlag.Both);
        return flags;
    }

    /// <summary>
    /// Loads an image from the filepath info stored in PicturesUnloaded, and stores it to Pictures
    /// </summary>
    internal Sprite AttemptLoad(string name, HashSet<PortraitFlag> flags)
    {
        // Check in PicturesLoaded first
        if (PicturesLoaded.ContainsKey(name))
            if (PicturesLoaded[name].ContainsKey(flags))
                return PicturesLoaded[name][flags];

        // if PicturesAny has an entry for this image, load the image and then remove the entry
        if (PicturesAny.ContainsKey(name))
            if (PicturesAny[name].ContainsKey(flags))
            {
                if (!PicturesLoaded.ContainsKey(name))
                    PicturesLoaded[name] = new Dictionary<HashSet<PortraitFlag>, Sprite>();
                PicturesLoaded[name][flags] = LoadPNG(PicturesAny[name][flags]);
                return PicturesLoaded[name][flags];
            }

        // else...
        return null;
    }

    /// <summary>
    /// Return the 'best' picture for `name` with `flags`.
    /// The best picture is the one with the most conditions where all conditions are fulfilled.
    /// </summary>
    Sprite GetBestMatch(string name, Person person, HashSet<PortraitFlag> flags)
    {
        name = name.ToLower();
        Sprite img = null;
        if (!PicturesAny.ContainsKey(name))
            name = "default";
        if (!PicturesAny.ContainsKey(name))
            return null;

        var bestSets = PicturesAny[name].Keys
            .Where(s => s.IsSubsetOf(flags))
            .OrderByDescending(s => s.Count);

        // if gone, no photo
        if (person.Health <= Constants.EndHealth)
            return null;

        // if dead, display a dead photo if it exists
        if (person.Health <= 0)
        {
            bestSets = PicturesAny[name].Keys
                .Where(s => s.IsSubsetOf(flags) && flags.Contains(PortraitFlag.Dead))
                .OrderByDescending(s => s.Count);
            foreach (var best in bestSets)
            {
                img = AttemptLoad(name, best);
                if (img != null)
                    return img;
            }
        }

        // if eaten, display an eaten photo if it exists
        if (person.BeingEaten)
        {
            // if eaten and full, show that over regular eaten photos
            if (person.VoreController.HasPrey(VoreLocation.Any))
            {
                bestSets = PicturesAny[name].Keys
                    .Where(
                        s =>
                            s.IsSubsetOf(flags)
                            && flags.Contains(PortraitFlag.Eaten)
                            && flags.Contains(PortraitFlag.Full)
                    )
                    .OrderByDescending(s => s.Count);
                foreach (var best in bestSets)
                {
                    img = AttemptLoad(name, best);
                    if (img != null)
                        return img;
                }
            }
            else
            {
                bestSets = PicturesAny[name].Keys
                    .Where(s => s.IsSubsetOf(flags) && flags.Contains(PortraitFlag.Eaten))
                    .OrderByDescending(s => s.Count);
                foreach (var best in bestSets)
                {
                    img = AttemptLoad(name, best);
                    if (img != null)
                        return img;
                }
            }
        }

        // if disposing, display a disposal photo if it exists
        if (
            person.StreamingSelfAction == SelfActionType.CockDisposalBathroom
            || person.StreamingSelfAction == SelfActionType.CockDisposalFloor
            || person.StreamingSelfAction == SelfActionType.UnbirthDisposalBathroom
            || person.StreamingSelfAction == SelfActionType.UnbirthDisposalFloor
            || person.StreamingSelfAction == SelfActionType.ScatDisposalBathroom
            || person.StreamingSelfAction == SelfActionType.ScatDisposalFloor
        )
        {
            bestSets = PicturesAny[name].Keys
                .Where(s => s.IsSubsetOf(flags) && flags.Contains(PortraitFlag.Disposal))
                .OrderByDescending(s => s.Count);
            foreach (var best in bestSets)
            {
                img = AttemptLoad(name, best);
                if (img != null)
                    return img;
            }
        }

        // if full, display a full photo if it exists
        if (person.VoreController.HasPrey(VoreLocation.Any))
        {
            bestSets = PicturesAny[name].Keys
                .Where(s => s.IsSubsetOf(flags) && flags.Contains(PortraitFlag.Full))
                .OrderByDescending(s => s.Count);
            foreach (var best in bestSets)
            {
                img = AttemptLoad(name, best);
                if (img != null)
                    return img;
            }
        }

        // finally, check any other case
        bestSets = PicturesAny[name].Keys
            .Where(s => s.IsSubsetOf(flags))
            .OrderByDescending(s => s.Count);
        foreach (var best in bestSets)
        {
            img = AttemptLoad(name, best);
            if (img != null)
                return img;
        }

        Debug.LogError("No picture found.  Missing a picture with no conditions.");
        return null;
    }

    internal Sprite GetPicture(Person person)
    {
        string str = person.Picture;
        if (string.IsNullOrWhiteSpace(str))
            return null;
        if (
            PicturesAny.ContainsKey(str.ToLower()) == false
            && PicturesAny.ContainsKey("default") == false
        )
            return null;
        var flags = new HashSet<PortraitFlag>();

        // DEFAULT PORTRAIT GENDER CHECKS
        if (PicturesAny.ContainsKey(str.ToLower()) == false && PicturesAny.ContainsKey("default"))
        {
            if (person.GenderType.HasBreasts)
                flags.Add(PortraitFlag.Fem);
            else
                flags.Add(PortraitFlag.Masc);
        }

        // DEAD CHECKS
        if (person.Health <= 0)
        {
            flags.Add(PortraitFlag.Dead);
            var vorelocation = person.VoreTracking.LastOrDefault().Location.ToString().ToLower();
            if (vorelocation == "balls")
                flags.Add(PortraitFlag.CV);
            if (vorelocation == "womb")
                flags.Add(PortraitFlag.UB);
            if (vorelocation == "stomach")
                flags.Add(PortraitFlag.OV);
            if (vorelocation == "bowels")
                flags.Add(PortraitFlag.AV);
        }
        else
        {
            flags.Add(PortraitFlag.Alive);
        }

        // PREY CHECKS
        if (person.BeingEaten && person.Health > Constants.EndHealth)
        {
            if (Config.AltBellyImage)
            {
                var parent = GetPicture(person.FindMyPredator());
                if (parent != null)
                    return parent;
            }

            flags.Add(PortraitFlag.Eaten);

            if (person.FindMyPredator().VoreController.GetProgressOf(person).IsSwallowing())
                flags.Add(PortraitFlag.BeingSwallowed);

            if (person.FindMyPredator().VoreController.GetProgressOf(person).Willing == false)
                flags.Add(PortraitFlag.Unwilling);

            if (person.FindMyPredator().VoreController.TargetIsBeingDigested(person))
                flags.Add(PortraitFlag.Digesting);
            else
                flags.Add(PortraitFlag.Safe);

            var vorelocation = person.VoreTracking.LastOrDefault().Location.ToString().ToLower();
            if (vorelocation == "balls")
                flags.Add(PortraitFlag.CV);
            if (vorelocation == "womb")
                flags.Add(PortraitFlag.UB);
            if (vorelocation == "stomach")
                flags.Add(PortraitFlag.OV);
            if (vorelocation == "bowels")
                flags.Add(PortraitFlag.AV);
        }

        // DISPOSAL CHECKS
        if (
            person.StreamingSelfAction == SelfActionType.CockDisposalBathroom
            || person.StreamingSelfAction == SelfActionType.CockDisposalFloor
        )
        {
            flags.Add(PortraitFlag.Disposal);
            flags.Add(PortraitFlag.CV);
        }
        if (
            person.StreamingSelfAction == SelfActionType.UnbirthDisposalBathroom
            || person.StreamingSelfAction == SelfActionType.UnbirthDisposalFloor
        )
        {
            flags.Add(PortraitFlag.Disposal);
            flags.Add(PortraitFlag.UB);
        }
        if (
            person.StreamingSelfAction == SelfActionType.ScatDisposalBathroom
            || person.StreamingSelfAction == SelfActionType.ScatDisposalFloor
        )
        {
            flags.Add(PortraitFlag.Disposal);
        }
        if (!flags.Contains(PortraitFlag.Disposal))
            flags.Add(PortraitFlag.NonDisposal);

        // PREDATOR CHECKS

        if (person.VoreController.HasPrey(VoreLocation.Any))
        {
            flags.Add(PortraitFlag.Full);

            if (person.VoreController.Swallowing(VoreLocation.Any))
                flags.Add(PortraitFlag.Swallowing);

            if (person.VoreController.HasBellyPrey())
            {
                flags.Add(PortraitFlag.Belly);
                if (person.VoreController.BellySize() > 1.5)
                    flags.Add(PortraitFlag.Multiple);
            }

            if (person.VoreController.HasPrey(VoreLocation.Balls))
            {
                flags.Add(PortraitFlag.Balls);
                if (person.VoreController.BallsSize() > 1.5)
                    flags.Add(PortraitFlag.Multiple);
            }

            if (flags.Contains(PortraitFlag.Belly) && flags.Contains(PortraitFlag.Balls))
                flags.Add(PortraitFlag.Both);
            if (person.VoreController.HasPrey(VoreLocation.Stomach))
                flags.Add(PortraitFlag.Stomach);
            if (person.VoreController.HasPrey(VoreLocation.Bowels))
                flags.Add(PortraitFlag.Bowels);
            if (person.VoreController.HasPrey(VoreLocation.Womb))
                flags.Add(PortraitFlag.Womb);

            if (
                person.VoreController.HasPrey(VoreLocation.Any)
                && person.VoreController.BellySize() < 0.7
                && person.VoreController.BallsSize() < 0.7
            )
            {
                flags.Add(PortraitFlag.Absorbing);
            }
        }

        // WEIGHT GAIN CHECKS
        var wgThreshold = 1.1f; // at +10% growth, display WG images
        if (
            person.MiscStats.CurrentWeightGain >= wgThreshold
            || person.MiscStats.CurrentBreastGain >= wgThreshold
            || person.MiscStats.CurrentDickGain >= wgThreshold
        )
        {
            flags.Add(PortraitFlag.Gain);
            if (person.MiscStats.CurrentWeightGain >= wgThreshold)
                flags.Add(PortraitFlag.Weight);
            if (person.MiscStats.CurrentBreastGain >= wgThreshold)
                flags.Add(PortraitFlag.Breast);
            if (person.MiscStats.CurrentDickGain >= wgThreshold)
                flags.Add(PortraitFlag.Dick);
        }

        // SIZE DIFF CHECKS
        if (person.Magic.Resize_Level > 0)
        {
            flags.Add(PortraitFlag.Grow);
            if (person.PartList.Height > 144) // if greater than 12 ft tall and grown, get "Huge"
                flags.Add(PortraitFlag.Huge);
        }
        if (person.Magic.Resize_Level < 0)
        {
            flags.Add(PortraitFlag.Shrink);
            if (person.PartList.Height < 24) // if less than 2 ft tall and shrunk, get "Tiny"
                flags.Add(PortraitFlag.Tiny);
        }

        // GENERAL CHECKS (NOT VORE RELATED)
        if (person.Health < Constants.HealthMax / 1.5)
            flags.Add(PortraitFlag.Weakened);
        if (person.Health == Constants.HealthMax)
            flags.Add(PortraitFlag.Healthy);
        if (
            person.Needs.Horniness > .8f
            || person.StreamingSelfAction == SelfActionType.Masturbate
            || person.ActiveSex != null
        )
            flags.Add(PortraitFlag.Horny);
        if (person.Needs.Hunger > .6f || person.Magic.Duration_Hunger > 0)
            flags.Add(PortraitFlag.Hungry);
        switch (person.ClothingStatus)
        {
            case ClothingStatus.Nude:
                flags.Add(PortraitFlag.Nude);
                flags.Add(PortraitFlag.Undressed);
                break;
            case ClothingStatus.Underwear:
                flags.Add(PortraitFlag.Underwear);
                flags.Add(PortraitFlag.Undressed);
                break;
            case ClothingStatus.Normal:
                break;
            default:
                break;
        }
        return GetBestMatch(str, person, flags);
    }

    static Sprite LoadPNG(string filePath)
    {
        Texture2D tex = null;
        byte[] fileData;

        if (File.Exists(filePath))
        {
            fileData = File.ReadAllBytes(filePath);
            tex = new Texture2D(2, 2, TextureFormat.BGRA32, false);
            tex.LoadImage(fileData); //..this will auto-resize the texture dimensions.
        }
        if (tex == null)
            return null;

        Rect rect = new Rect(new Vector2(0, 0), new Vector2(tex.width, tex.height));
        Vector2 pivot = new Vector2(0.5f, 0.5f);
        int higherDimension = Math.Max(tex.width, tex.height);

        if (Config.CropPortraits)
        {
            var texRatio = ((float)tex.height / (float)tex.width);
            var targetRatio = 1.6;

            if (texRatio > targetRatio) // adjust an image that is too tall
            {
                var heightAdj = (int)(tex.width * targetRatio);
                rect.Set(0, (tex.height - heightAdj), tex.width, heightAdj);
                higherDimension = Math.Max(tex.width, heightAdj);
            }
        }

        Sprite sprite = Sprite.Create(tex, rect, pivot, higherDimension);
        return sprite;
    }
}
