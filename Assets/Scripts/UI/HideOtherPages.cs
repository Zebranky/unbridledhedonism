using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HideOtherPages : MonoBehaviour
{
    public Transform Pages;
    public Transform Sidebar;

    public void Hide(string selfName)
    {
        foreach (Transform child in Pages)
        {
            if (child.name == selfName)
            {
                child.gameObject.SetActive(true);
            }
            else
            {
                child.gameObject.SetActive(false);
            }
        }

        foreach (Transform child in Sidebar)
        {
            if (child.name == selfName)
            {
                child.gameObject.GetComponentInChildren<Button>().interactable = false;
            }
            else
            {
                child.gameObject.GetComponentInChildren<Button>().interactable = true;
            }
        }
    }
}
