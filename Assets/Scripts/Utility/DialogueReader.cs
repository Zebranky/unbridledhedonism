﻿using System.Text.RegularExpressions;
using Assets.Scripts.TextGeneration;
using OdinSerializer.Utilities;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using UnityEngine;

namespace DialogueReader
{
    class InteractionString
    {
        internal string Type;
        internal string Priority;
        internal string FallThrough;
        internal string ActorRace;
        internal string TargetRace;
        internal string Message;
        internal string Conditional;
        internal string Success;
        internal string FromFile; // File path which this interaction belongs to.
        internal int LineNumber; // File line number starting at one.

        public InteractionString(
            string type,
            string priority,
            string fallThrough,
            string actorRace,
            string targetRace,
            string message,
            string conditional,
            string success,
            string fromFile,
            int lineNumber
        )
        {
            Type = type ?? "";
            Priority = priority ?? "";
            FallThrough = fallThrough ?? "";
            ActorRace = actorRace ?? "";
            TargetRace = targetRace ?? "";
            Message = message ?? "";
            Conditional = conditional ?? "";
            Success = success ?? "";
            FromFile = fromFile ?? "";
            LineNumber = lineNumber;
        }

        public InteractionString(InteractionString parent)
        {
            Type = parent.Type;
            Priority = parent.Priority;
            FallThrough = parent.FallThrough;
            ActorRace = parent.ActorRace;
            TargetRace = parent.TargetRace;
            Message = parent.Message;
            Conditional = parent.Conditional;
            Success = parent.Success;
            FromFile = parent.FromFile;
            LineNumber = parent.LineNumber;
        }

        /// <summary>
        /// Parse and return the priority of this interaction.
        /// If the priority is blank then it is generated from the total number of conditions
        /// checked using bitwise and.
        /// </summary>
        public int GetPriority()
        {
            if (!Priority.IsNullOrWhitespace())
                if (int.TryParse(Priority, out int priority))
                    return priority; // Priority was set manually.
                else
                    Debug.LogWarning(
                        $"{FromFile}:{LineNumber} Priority: \"{Priority}\" is not a valid integer."
                    );

            if (Conditional.IsNullOrWhitespace())
                return 0; // No conditions, so this is a default interaction.

            // Count "&&" and add 1.  So that "a && b && c" is 3.
            return Regex.Matches(Conditional, "&&").Count + 1;
        }

        /// <summary>
        /// Parse and return the FallThrough of this interaction.
        /// </summary>
        public float GetFallThrough()
        {
            if (FallThrough.IsNullOrWhitespace())
            {
                if (Priority.IsNullOrWhitespace() && GetPriority() != 0)
                {
                    // Blank FallThrough and Priority is automatic and not zero.
                    return 0.5f;
                }
                return 0; // Priority was manually set, preserve old hacks with a default of zero.
            }
            if (float.TryParse(FallThrough, out float value))
                return value; // FallThrough was set manually.
            Debug.LogWarning(
                $"{FromFile}:{LineNumber} FallThrough: \"{FallThrough}\" is not valid."
            );
            return 0;
        }

        /// <summary>
        /// Parse and return the SuccessRequirement of this interaction.
        /// </summary>
        public SuccessRequirement GetSuccessRequirement()
        {
            if (Success.IsNullOrWhitespace())
                return SuccessRequirement.Either;
            if (Success == "Yes")
                return SuccessRequirement.Success;
            if (Success == "No")
                return SuccessRequirement.Fail;
            Debug.LogWarning($"{FromFile}:{LineNumber} Success: \"{Success}\" is not valid.");
            return SuccessRequirement.Either;
        }

        /// <summary>
        /// Return true if the priority of this interaction is non-zero and automatically assigned.
        /// </summary>
        public bool IsAutoPrioritySet() => (Priority.IsNullOrWhitespace() && GetPriority() != 0);
    }

    class DialogueReader
    {
        internal void Read(MessageData data)
        {
            CultureInfo baseCult = CultureInfo.CurrentCulture;
            CultureInfo newCult = CultureInfo.GetCultureInfo("en-US");
            Thread.CurrentThread.CurrentCulture = newCult;
            StringBuilder summaryLog = new StringBuilder();
            var textSex = new Dictionary<SexInteractionType, List<InteractionString>>();
            var textInteraction = new Dictionary<InteractionType, List<InteractionString>>();
            var textSelfAction = new Dictionary<SelfActionType, List<InteractionString>>();
            var textVore = new Dictionary<VoreMessageType, List<InteractionString>>();
            var textBody = new Dictionary<BodyPartDescriptionType, List<InteractionString>>();
            var files = Directory
                .GetFiles(
                    Path.Combine(Application.streamingAssetsPath, "Texts"),
                    "*.*",
                    SearchOption.AllDirectories
                )
                .ToList();
            files.AddRange(
                Directory.GetFiles(State.TextDirectory, "*.*", SearchOption.AllDirectories)
            );
            files = files.Where(s => s.EndsWith(".csv") || s.EndsWith(".tsv")).ToList();
            foreach (var file in files)
            {
                string[] lines;
                try
                {
                    lines = File.ReadAllLines(file);
                }
                catch (Exception e)
                {
                    Debug.LogException(e);
                    State.GameManager.CreateMessageBox(
                        $"Couldn't read a file from the Texts directory ({Path.GetFileName(file)}), it's likely you have it open in another program.  You should close UH and open it again to avoid issues."
                    );
                    continue;
                }

                foreach (var i in Enumerable.Range(0, lines.Count()))
                {
                    var line = lines[i];
                    var lineNumber = i + 1;
                    var sections = line.Split('\t');
                    if (sections.Length == 0)
                        continue;
                    if (sections[0] == "")
                        continue;
                    if (sections[0].StartsWith("//"))
                        continue;
                    if (sections.Length < 4)
                    {
                        Debug.LogWarning(
                            $"Unusual section count ({sections.Length}) at {file}:{lineNumber}:\n\"{line}\""
                        );
                        continue;
                    }
                    var interaction = new InteractionString(
                        type: sections[0],
                        priority: sections[1],
                        fallThrough: sections[2],
                        actorRace: sections[3],
                        targetRace: sections.ElementAtOrDefault(4),
                        message: sections.ElementAtOrDefault(5),
                        conditional: sections.ElementAtOrDefault(6),
                        success: sections.ElementAtOrDefault(7),
                        fromFile: file,
                        lineNumber: lineNumber
                    );
                    if (interaction.Message.Count() < 4)
                        continue;

                    string[] ActorRaces = sections[3].Split('&');
                    string[] TargetRaces = sections[4].Split('&');
                    if (ActorRaces.Count() == 0)
                        ActorRaces.Append("");
                    if (TargetRaces.Count() == 0)
                        TargetRaces.Append("");
                    foreach (var actor in ActorRaces)
                    {
                        foreach (var target in TargetRaces)
                        {
                            var tempObj = new InteractionString(interaction)
                            {
                                TargetRace = target,
                                ActorRace = actor
                            };
                            AddSet(
                                summaryLog,
                                textSex,
                                textInteraction,
                                textSelfAction,
                                textVore,
                                textBody,
                                file,
                                sections,
                                tempObj
                            );
                        }
                    }
                }
            }

            /// Converts a dictionary of InteractionString's into GenericEventString objects.
            /// Outputs directly into MessageData.
            void ConvertInteractions<InteractionT, InteractionEnumT>(
                Dictionary<InteractionEnumT, List<InteractionString>> textInput,
                out Dictionary<InteractionEnumT, List<GenericEventString<InteractionT>>> textOutput
            )
            {
                textOutput =
                    new Dictionary<InteractionEnumT, List<GenericEventString<InteractionT>>>();
                foreach (var interactionGroup in textInput)
                {
                    var convertedList = new List<GenericEventString<InteractionT>>();
                    foreach (var interaction in interactionGroup.Value)
                        convertedList.Add(
                            new GenericEventString<InteractionT>(
                                CleanText(interaction.Message),
                                interaction.ActorRace,
                                interaction.TargetRace,
                                interaction.FromFile, // passed through to provide context for debug messages
                                interaction.GetPriority(), // auto priority calculation for the line
                                interaction.GetFallThrough(), // auto fallthrough calculation for the line
                                CleanTextFormula(interaction.Conditional),
                                interaction.GetSuccessRequirement(),
                                autoPriority: interaction.Priority.IsNullOrWhitespace() // fallthrough for every line that does not have manually set priority
                            )
                        );
                    textOutput[interactionGroup.Key] = convertedList;
                }
            }
            ConvertInteractions(textInteraction, out data.InteractionTexts);
            ConvertInteractions(textSex, out data.SexTexts);
            ConvertInteractions(textSelfAction, out data.SelfTexts);
            ConvertInteractions(textVore, out data.VoreTexts);
            ConvertInteractions(textBody, out data.BodyPartTexts);

            foreach (
                SexInteractionType type in (SexInteractionType[])
                    Enum.GetValues(typeof(SexInteractionType))
            )
            {
                if (
                    type >= SexInteractionType.KissVore
                    && type <= SexInteractionType.SexAskToBeAnalVoredEndo
                )
                    continue;
                if (textSex.ContainsKey(type) == false)
                    summaryLog.AppendLine($"No entries for {type}");
            }
            foreach (
                SelfActionType type in (SelfActionType[])Enum.GetValues(typeof(SelfActionType))
            )
            {
                if (type == SelfActionType.None)
                    continue;
                if (
                    type >= SelfActionType.ScatDisposalBathroom
                    && type <= SelfActionType.UnbirthDisposalFloor
                )
                    continue;
                if (textSelfAction.ContainsKey(type) == false)
                    summaryLog.AppendLine($"No entries for {type}");
            }
            foreach (
                InteractionType type in (InteractionType[])Enum.GetValues(typeof(InteractionType))
            )
            {
                if (type == InteractionType.None)
                    continue;
                if (type >= InteractionType.OralVore && type <= InteractionType.AnalVore)
                    continue;
                if (
                    type >= InteractionType.KissVore
                    && type <= InteractionType.SexAskToAnalVoreDigest
                )
                    continue;
                if (textInteraction.ContainsKey(type) == false)
                    summaryLog.AppendLine($"No entries for {type}");
            }
            foreach (
                VoreMessageType type in (VoreMessageType[])Enum.GetValues(typeof(VoreMessageType))
            )
            {
                if (textVore.ContainsKey(type) == false)
                    summaryLog.AppendLine($"No entries for {type}");
            }
            foreach (
                BodyPartDescriptionType type in (BodyPartDescriptionType[])
                    Enum.GetValues(typeof(BodyPartDescriptionType))
            )
            {
                if (textBody.ContainsKey(type) == false)
                {
                    if (
                        type == BodyPartDescriptionType.ShoulderDescription
                        || type == BodyPartDescriptionType.LegDescription
                        || type == BodyPartDescriptionType.FeetDescription
                    )
                        summaryLog.AppendLine(
                            $"No entries for {type} - Though these were deliberately left out for humans as not adding much, but you can add them if you wish."
                        );
                    else
                        summaryLog.AppendLine($"No entries for {type}");
                }
            }

            foreach (
                SexInteractionType type in (SexInteractionType[])
                    Enum.GetValues(typeof(SexInteractionType))
            )
            {
                if (textSex.ContainsKey(type))
                    summaryLog.AppendLine(
                        $"{type} - {textSex[type].Count} entries, {textSex[type].Where(s => s.ActorRace.IsNullOrWhitespace() && s.TargetRace.IsNullOrWhitespace()).Count()} for humans"
                    );
            }
            foreach (
                SelfActionType type in (SelfActionType[])Enum.GetValues(typeof(SelfActionType))
            )
            {
                if (textSelfAction.ContainsKey(type))
                    summaryLog.AppendLine(
                        $"{type} - {textSelfAction[type].Count} entries, {textSelfAction[type].Where(s => s.ActorRace.IsNullOrWhitespace() && s.TargetRace.IsNullOrWhitespace()).Count()} for humans"
                    );
            }
            foreach (
                InteractionType type in (InteractionType[])Enum.GetValues(typeof(InteractionType))
            )
            {
                if (textInteraction.ContainsKey(type))
                    summaryLog.AppendLine(
                        $"{type} - {textInteraction[type].Count} entries, {textInteraction[type].Where(s => s.ActorRace.IsNullOrWhitespace() && s.TargetRace.IsNullOrWhitespace()).Count()} for humans"
                    );
            }
            foreach (
                VoreMessageType type in (VoreMessageType[])Enum.GetValues(typeof(VoreMessageType))
            )
            {
                if (textVore.ContainsKey(type))
                    summaryLog.AppendLine(
                        $"{type} - {textVore[type].Count} entries, {textVore[type].Where(s => s.ActorRace.IsNullOrWhitespace() && s.TargetRace.IsNullOrWhitespace()).Count()} for humans"
                    );
            }
            foreach (
                BodyPartDescriptionType type in (BodyPartDescriptionType[])
                    Enum.GetValues(typeof(BodyPartDescriptionType))
            )
            {
                if (textBody.ContainsKey(type))
                    summaryLog.AppendLine(
                        $"{type} - {textBody[type].Count} entries, {textBody[type].Where(s => s.ActorRace.IsNullOrWhitespace() && s.TargetRace.IsNullOrWhitespace()).Count()} for humans"
                    );
            }

            try
            {
                File.Delete("summary.txt");

                File.WriteAllText("summary.txt", summaryLog.ToString());
            }
            catch (Exception e)
            {
                Debug.LogException(e);
            }

            Thread.CurrentThread.CurrentCulture = baseCult;

            void AddToDict<T>(
                Dictionary<T, List<InteractionString>> thisDict,
                InteractionString msg,
                T type
            )
            {
                if (thisDict.ContainsKey(type))
                {
                    thisDict[type].Add(msg);
                }
                else
                {
                    thisDict[type] = new List<InteractionString> { msg };
                }
            }

            string CleanText(string text)
            {
                //These are done without brackets to also include the ! versions without extra replaces, since they should never normally appear in a sentence anyway
                return text.FixCapitalization("[ActorName]")
                    .FixCapitalization("[TargetName]")
                    .FixCapitalization("ActorHeIs")
                    .FixCapitalization("ActorHeHas")
                    .FixCapitalization("ActorHis")
                    .FixCapitalization("ActorHisCapitalized")
                    .FixCapitalization("ActorHim")
                    .FixCapitalization("ActorHimself")
                    .FixCapitalization("ActorHe")
                    .FixCapitalization("ActorBastard")
                    .FixCapitalization("TargetHeIs")
                    .FixCapitalization("TargetHeHas")
                    .FixCapitalization("TargetHis")
                    .FixCapitalization("TargetHim")
                    .FixCapitalization("TargetHimself")
                    .FixCapitalization("TargetHe")
                    .FixCapitalization("TargetBastard")
                    .FixCapitalization("[SIfActorSingular]")
                    .FixCapitalization("[ESIfActorSingular]")
                    .FixCapitalization("[IESIfActorSingular]")
                    .FixCapitalization("[ActorBoobSize]")
                    .FixCapitalization("[TargetBoobSize]")
                    .FixCapitalization("[ActorBoobsSize]")
                    .FixCapitalization("[TargetBoobsSize]")
                    .FixCapitalization("[ActorDickSize]")
                    .FixCapitalization("[TargetDickSize]")
                    .FixCapitalization("[ActorBallSize]")
                    .FixCapitalization("[TargetBallSize]")
                    .FixCapitalization("[ActorBallsSize]")
                    .FixCapitalization("[TargetBallsSize]")
                    .FixCapitalization("[PredatorOfTarget]")
                    .FixCapitalization("[PredatorOfActor]")
                    .FixCapitalization("[Breasts]")
                    .FixCapitalization("[Pussy]")
                    .FixCapitalization("[Pussies]")
                    .FixCapitalization("[Dick]")
                    .FixCapitalization("[Balls]")
                    .FixCapitalization("[Clit]")
                    .FixCapitalization("[Clits]")
                    .FixCapitalization("[Anus]")
                    .FixCapitalization("[TargetEyeColor]")
                    .FixCapitalization("[TargetHairColor]")
                    .FixCapitalization("[TargetHairLength]")
                    .FixCapitalization("[TargetHairStyle]")
                    .FixCapitalization("[TargetLocationInPred]")
                    .FixCapitalization("[ActorLocationInPred]")
                    .FixCapitalization("[ShoulderAdjective]")
                    .FixCapitalization("[WaistAdjective]")
                    .FixCapitalization("[ActorCustom-")
                    .FixCapitalization("[TargetCustom-");
            }

            string CleanTextFormula(string text)
            {
                if (string.IsNullOrWhiteSpace(text))
                    return "true";
                string ret = text
                // CHARACTER BODY STATES
                .Replace(
                        "[ActorFullyClothed]",
                        "s.Actor.CharacterFullyClothed",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[ActorInUnderwear]",
                        "s.Actor.CharacterInUnderwear",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[ActorNude]",
                        "s.Actor.CharacterNude",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[TargetFullyClothed]",
                        "s.Target.CharacterFullyClothed",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[TargetInUnderwear]",
                        "s.Target.CharacterInUnderwear",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[TargetNude]",
                        "s.Target.CharacterNude",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[ActorHasBoobs]",
                        "s.Actor.GenderType.HasBreasts",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[TargetHasBoobs]",
                        "s.Target.GenderType.HasBreasts",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[ActorHasDick]",
                        "s.Actor.GenderType.HasDick",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[TargetHasDick]",
                        "s.Target.GenderType.HasDick",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[ActorHasVagina]",
                        "s.Actor.GenderType.HasVagina",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[TargetHasVagina]",
                        "s.Target.GenderType.HasVagina",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[ActorBellyPreySize]",
                        "s.Actor.VoreController.BellySize()",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[ActorBellySize]",
                        "s.Actor.VoreController.BellySize()",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[TargetBellyPreySize]",
                        "s.Target.VoreController.BellySize()",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[TargetBellySize]",
                        "s.Target.VoreController.BellySize()",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[ActorBallsPreySize]",
                        "s.Actor.VoreController.BallsSize()",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[TargetBallsPreySize]",
                        "s.Target.VoreController.BallsSize()",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    // WORLD SETTINGS
                    .Replace(
                        "[VoreKnowledge]",
                        "State.World.Settings.VoreKnowledge",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[DisposalEnabled]",
                        "State.World.Settings.DisposalEnabled",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[DisposalCockEnabled]",
                        "State.World.Settings.DisposalCockEnabled",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[DisposalBonesEnabled]",
                        "State.World.Settings.DisposalBonesEnabled",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[DisposalUnbirthEnabled]",
                        "State.World.Settings.DisposalUnbirthEnabled",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[BodyWeightGain]",
                        "State.World.Settings.WeightGainBody > 0",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[BoobWeightGain]",
                        "State.World.Settings.WeightGainBoob > 0",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[UnbirthLactation]",
                        "State.World.Settings.UnbirthLactation",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[MagicExists]",
                        "State.World.Settings.MagicType != MagicAvailability.NoMagic",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    // LOCATIONS
                    .Replace(
                        "[TileHasGym]",
                        "s.Actor.ZoneContainsGym()",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[TileHasLibrary]",
                        "s.Actor.ZoneContainsLibrary()",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[TileHasShower]",
                        "s.Actor.ZoneContainsShower()",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[TileHasBathroom]",
                        "s.Actor.ZoneContainsBathroom()",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[TileHasNurseOffice]",
                        "s.Actor.ZoneContainsNurseOffice()",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[TileHasBed]",
                        "s.Actor.ZoneContainsBed()",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[TileHasFood]",
                        "s.Actor.ZoneContainsFood()",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[TileIsActorsRoom]",
                        "s.Actor.ZoneContainsMyRoom()",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[TileIsTargetsRoom]",
                        "s.Target.ZoneContainsMyRoom()",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[OthersOnTile]",
                        "s.Actor.OthersOnTileCount()",
                        StringComparison.InvariantCultureIgnoreCase
                    )

                    // ACTION RELATED
                    .Replace(
                        "[ActorRejected]",
                        "s.Actor.IsRejected(s.Target)",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[TargetRejected]",
                        "s.Target.IsRejected(s.Actor)",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[ActorHavingSex]",
                        "s.Actor.ActiveSex != null",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[PredOfActorHavingSex]",
                        "s.Actor.PredHavingSex()",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[TargetHavingSex]",
                        "s.Target.ActiveSex != null",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[PredOfTargetHavingSex]",
                        "s.Target.PredHavingSex()",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[ActorBeingPenetrated]",
                        "s.Actor.BeingPenetrated()",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[ActorPenetrating]",
                        "s.Actor.Penetrating()",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[TargetBeingPenetrated]",
                        "s.Target.BeingPenetrated()",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[TargetPenetrating]",
                        "s.Target.Penetrating()",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[ActorControlled]",
                        "s.Actor.CharacterControlled",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[TargetControlled]",
                        "s.Target.CharacterControlled",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[ActorMasturbating]",
                        "s.Actor.CharacterMasturbating",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[PredOfActorMasturbating]",
                        "s.Actor.PredMasturbating()",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[TargetMasturbating]",
                        "s.Target.CharacterMasturbating",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[PredOfTargetMasturbating]",
                        "s.Target.PredMasturbating()",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[ActorMakingOut]",
                        "s.Actor.CharacterMakingOut",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[TargetMakingOut]",
                        "s.Target.CharacterMakingOut",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[ActorResting]",
                        "s.Actor.CharacterResting",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[TargetResting]",
                        "s.Target.CharacterResting",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[ActorCanCast]",
                        "s.Actor.CanUseMagic()",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[TargetCanCast]",
                        "s.Target.CanUseMagic()",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    // Hacky conditional used to check if a self cast spell succeeded
                    .Replace(
                        "[SelfCastSuccess]",
                        "s.Actor.Magic.SelfCastSuccess",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    // RELATIONSHIP RELATED
                    .Replace(
                        "[RomanceFirstTime]",
                        "s.Actor.IsNewRomanticEvent()",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[ActorLikesTargetGender]",
                        "s.Actor.Romance.DesiresGender(s.Target.GenderType)",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[TargetLikesActorGender]",
                        "s.Target.Romance.DesiresGender(s.Actor.GenderType)",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[TargetFriendshipTowardActor]",
                        "s.Target.GetRelationshipWith(s.Actor).FriendshipLevel",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[ActorFriendshipTowardTarget]",
                        "s.Actor.GetRelationshipWith(s.Target).FriendshipLevel",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[TargetRomanticTowardActor]",
                        "s.Target.GetRelationshipWith(s.Actor).RomanticLevel",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[ActorRomanticTowardTarget]",
                        "s.Actor.GetRelationshipWith(s.Target).RomanticLevel",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[ActorIsDatingTarget]",
                        "s.Actor.Romance.Dating == s.Target",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[ActorCheatsFrequently]",
                        "s.Actor.CheatWillingness() == 2",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[ActorCheatsRarely]",
                        "s.Actor.CheatWillingness() == 1",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[ActorCheatsNever]",
                        "s.Actor.CheatWillingness() == 0",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[TargetCheatsFrequently]",
                        "s.Target.CheatWillingness() == 2",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[TargetCheatsRarely]",
                        "s.Target.CheatWillingness() == 1",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[TargetCheatsNever]",
                        "s.Target.CheatWillingness() == 0",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[ActorIsDating]",
                        "s.Actor.Romance.Dating != null",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[TargetIsDating]",
                        "s.Target.Romance.Dating != null",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[ActorHasNemesis]",
                        "s.Actor.GetRelationshipWith(s.Actor.FindNemesis()).FriendshipLevel >= 0.5",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[TargetHasNemesis]",
                        "s.Target.GetRelationshipWith(s.Target.FindNemesis()).FriendshipLevel >= 0.5",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[ActorNemesisIsTarget]",
                        "s.Actor.FindNemesis() == s.Target",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[TargetNemesisIsActor]",
                        "s.Target.FindNemesis() == s.Actor",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[ActorHasBestie]",
                        "s.Actor.GetRelationshipWith(s.Actor.FindBestFriend()).FriendshipLevel <= -0.3",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[TargetHasBestie]",
                        "s.Target.GetRelationshipWith(s.Target.FindBestFriend()).FriendshipLevel <= -0.3",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[ActorBestieIsTarget]",
                        "s.Actor.FindBestFriend() == s.Target",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[TargetBestieIsActor]",
                        "s.Target.FindBestFriend() == s.Actor",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[ActorHasCrush]",
                        "s.Actor.GetRelationshipWith(s.Actor.FindCrush()).RomanticLevel >= 0.3",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[TargetHasCrush]",
                        "s.Target.GetRelationshipWith(s.Target.FindCrush()).RomanticLevel >= 0.3",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[ActorCrushIsTarget]",
                        "s.Actor.FindCrush() == s.Target",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[TargetCrushIsActor]",
                        "s.Target.FindCrush() == s.Actor",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[ActorHasVendetta]",
                        "s.Actor.FindVendetta() != null",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[TargetHasVendetta]",
                        "s.Target.FindVendetta() != null",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[ActorVendettaIsTarget]",
                        "s.Actor.FindVendetta() == s.Target",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[TargetVendettaIsActor]",
                        "s.Target.FindVendetta() == s.Actor",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[ActorTargetHaveHadSex]",
                        "s.Actor.GetRelationshipWith(s.Target).HaveHadSex",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[ActorHasEatenTarget]",
                        "s.Actor.GetRelationshipWith(s.Target).HasEaten",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[TargetHasEatenActor]",
                        "s.Target.GetRelationshipWith(s.Actor).HasEaten",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[ActorHasDigestedTarget]",
                        "s.Actor.GetRelationshipWith(s.Target).HasDigested",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[TargetHasDigestedActor]",
                        "s.Target.GetRelationshipWith(s.Actor).HasDigested",
                        StringComparison.InvariantCultureIgnoreCase
                    )

                    // VORE RELATED
                    .Replace(
                        "[ActorCanVore]",
                        "s.Actor.VoreController.CapableOfVore()",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[TargetCanVore]",
                        "s.Target.VoreController.CapableOfVore()",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[ActorWillingPrey]",
                        "s.Actor.FindMyPredator().VoreController.GetProgressOf(s.Actor).Willing",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[TargetWillingPrey]",
                        "s.Target.FindMyPredator().VoreController.GetProgressOf(s.Target).Willing",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[ActorGenerallyWilling]",
                        "s.Actor.Personality.PreyWillingness > State.World.Settings.WillingThreshold",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[TargetGenerallyWilling]",
                        "s.Target.Personality.PreyWillingness > State.World.Settings.WillingThreshold",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[TargetIsActorPrey]",
                        "s.Target.FindMyPredator() == s.Actor",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[ActorIsTargetPrey]",
                        "s.Actor.FindMyPredator() == s.Target",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[ActorPrefersDigestion]",
                        "s.Actor.CharacterPrefersDigestion",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[TargetPrefersDigestion]",
                        "s.Target.CharacterPrefersDigestion",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[ActorPrefersEndo]",
                        "s.Actor.CharacterPrefersEndo",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[TargetPrefersEndo]",
                        "s.Target.CharacterPrefersEndo",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[ActorDigestsPrey]",
                        "s.Actor.VoreController.TargetIsBeingDigested(s.Target)",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[TargetDigestsPrey]",
                        "s.Target.VoreController.TargetIsBeingDigested(s.Actor)",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[ActorHasDigestingAnyPrey]",
                        "s.Actor.VoreController.IsDigestingAnyPrey()",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[TargetHasDigestingAnyPrey]",
                        "s.Target.VoreController.IsDigestingAnyPrey()",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[ActorHasDigestingStomachPrey]",
                        "s.Actor.VoreController.IsDigestingStomachPrey()",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[TargetHasDigestingStomachPrey]",
                        "s.Target.VoreController.IsDigestingStomachPrey()",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[ActorHasDigestingWombPrey]",
                        "s.Actor.VoreController.IsDigestingWombPrey()",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[TargetHasDigestingWombPrey]",
                        "s.Target.VoreController.IsDigestingWombPrey()",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[ActorHasDigestingBowelsPrey]",
                        "s.Actor.VoreController.IsDigestingBowelsPrey()",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[TargetHasDigestingBowelsPrey]",
                        "s.Target.VoreController.IsDigestingBowelsPrey()",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[ActorHasDigestingBallsPrey]",
                        "s.Actor.VoreController.IsDigestingBallsPrey()",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[TargetHasDigestingBallsPrey]",
                        "s.Target.VoreController.IsDigestingBallsPrey()",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[ActorIsBeingDigested]",
                        "s.Actor.FindMyPredator().VoreController.TargetIsBeingDigested(s.Actor)",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[TargetIsBeingDigested]",
                        "s.Target.FindMyPredator().VoreController.TargetIsBeingDigested(s.Target)",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[TargetIsInActorStomach]",
                        "s.Actor.VoreController.TargetIsInMyStomach(s.Target)",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[TargetIsInActorBalls]",
                        "s.Actor.VoreController.TargetIsInMyBalls(s.Target)",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[TargetIsInActorBowels]",
                        "s.Actor.VoreController.TargetIsInMyBowels(s.Target)",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[TargetIsInActorWomb]",
                        "s.Actor.VoreController.TargetIsInMyWomb(s.Target)",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[ActorIsInTargetStomach]",
                        "s.Target.VoreController.TargetIsInMyStomach(s.Actor)",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[ActorIsInTargetBalls]",
                        "s.Target.VoreController.TargetIsInMyBalls(s.Actor)",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[ActorIsInTargetBowels]",
                        "s.Target.VoreController.TargetIsInMyBowels(s.Actor)",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[ActorIsInTargetWomb]",
                        "s.Target.VoreController.TargetIsInMyWomb(s.Actor)",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[TargetOralVored]",
                        "s.Target.FindMyPredator().VoreController.TargetOralVored(s.Target)",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[TargetUnbirthed]",
                        "s.Target.FindMyPredator().VoreController.TargetUnbirthed(s.Target)",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[TargetCockVored]",
                        "s.Target.FindMyPredator().VoreController.TargetCockVored(s.Target)",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[TargetAnalVored]",
                        "s.Target.FindMyPredator().VoreController.TargetAnalVored(s.Target)",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[ActorOralVored]",
                        "s.Actor.FindMyPredator().VoreController.TargetOralVored(s.Actor)",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[ActorUnbirthed]",
                        "s.Actor.FindMyPredator().VoreController.TargetUnbirthed(s.Actor)",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[ActorCockVored]",
                        "s.Actor.FindMyPredator().VoreController.TargetCockVored(s.Actor)",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[ActorAnalVored]",
                        "s.Actor.FindMyPredator().VoreController.TargetAnalVored(s.Actor)",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[ActorOfferedSelf]",
                        "s.Actor.FindMyPredator().VoreController.GetProgressOf(s.Actor).PreyInitiated",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[TargetOfferedSelf]",
                        "s.Target.FindMyPredator().VoreController.GetProgressOf(s.Target).PreyInitiated",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[ActorIsBeingEaten]",
                        "s.Actor.BeingEaten",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[TargetIsBeingEaten]",
                        "s.Target.BeingEaten",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[ActorHasWillingPrey]",
                        "s.Actor.VoreController.HasAnyWillingPrey()",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[TargetHasWillingPrey]",
                        "s.Target.VoreController.HasAnyWillingPrey()",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[ActorHasUnwillingPrey]",
                        "s.Actor.VoreController.HasAnyUnwillingPrey()",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[TargetHasUnwillingPrey]",
                        "s.Target.VoreController.HasAnyUnwillingPrey()",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[ActorHasDigestingBellyPrey]",
                        "s.Actor.HasDigestingBellyPrey()",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[TargetHasDigestingBellyPrey]",
                        "s.Target.HasDigestingBellyPrey()",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[ActorHasLivingPrey]",
                        "s.Actor.VoreController.HasLivingPrey()",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[TargetHasLivingPrey]",
                        "s.Target.VoreController.HasLivingPrey()",
                        StringComparison.InvariantCultureIgnoreCase
                        )
                    .Replace(
                        "[ActorHasLivingBellyPrey]",
                        "s.Actor.VoreController.HasLivingBellyPrey()",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[TargetHasLivingBellyPrey]",
                        "s.Target.VoreController.HasLivingBellyPrey()",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[ActorHasLivingBallsPrey]",
                        "s.Actor.VoreController.HasLivingBallsPrey()",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[TargetHasLivingBallsPrey]",
                        "s.Target.VoreController.HasLivingBallsPrey()",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[ActorHasAnyPrey]",
                        "s.Actor.VoreController.HasAnyPrey()",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[TargetHasAnyPrey]",
                        "s.Target.VoreController.HasAnyPrey()",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[ActorHasStomachPrey]",
                        "s.Actor.VoreController.HasStomachPrey()",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[TargetHasStomachPrey]",
                        "s.Target.VoreController.HasStomachPrey()",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[ActorHasWombPrey]",
                        "s.Actor.VoreController.HasWombPrey()",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[TargetHasWombPrey]",
                        "s.Target.VoreController.HasWombPrey()",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[ActorHasBowelsPrey]",
                        "s.Actor.VoreController.HasBowelsPrey()",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[TargetHasBowelsPrey]",
                        "s.Target.VoreController.HasBowelsPrey()",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[ActorHasBallsPrey]",
                        "s.Actor.VoreController.HasBallsPrey()",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[TargetHasBallsPrey]",
                        "s.Target.VoreController.HasBallsPrey()",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[ActorHasBellyPrey]",
                        "s.Actor.VoreController.HasBellyPrey()",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[TargetHasBellyPrey]",
                        "s.Target.VoreController.HasBellyPrey()",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[ActorTargetSisterPrey]",
                        "s.Actor.FindMyPredator().VoreController.AreSisterPrey(s.Actor, s.Target, false)",
                        StringComparison.InvariantCultureIgnoreCase
                    )

                    .Replace(
                        "[InPrivateArea]",
                        "s.Actor.IsInPrivateArea(s.Target)",
                        StringComparison.InvariantCultureIgnoreCase
                    )

                    // Health ranges.
                    .Replace(
                        "[TargetAlive]",
                        "s.Target.Health > .0f",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[TargetDead]",
                        "s.Target.Health <= .0f",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[TargetHealthExcellent]",
                        "s.Target.Health >= Constants.HealthMax * .8f",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[TargetHealthInjured]",
                        "(Constants.HealthMax * .8f >= s.Target.Health && s.Target.Health >= Constants.HealthMax * .6f)",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[TargetHealthOkay]", // Excellent || Injured
                        "s.Target.Health >= Constants.HealthMax * .6f",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[TargetHealthWounded]",
                        "(Constants.HealthMax * .6f >= s.Target.Health && s.Target.Health >= Constants.HealthMax * .2f)",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[TargetHealthCritical]",
                        "(Constants.HealthMax * .2f >= s.Target.Health && s.Target.Health > .0f)",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[TargetDecayLow]",
                        "(.0f >= s.Target.Health && s.Target.Health >= Constants.EndHealth * .5f)",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[TargetDecayHigh]",
                        "Constants.EndHealth * .5f >= s.Target.Health",
                        StringComparison.InvariantCultureIgnoreCase
                    )

                    // DISPOSAL CHECKS
                    .Replace(
                        "[ActorNeedsScatDisposal]",
                        "s.Actor.NeedsScatDisposal()",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[TargetNeedsScatDisposal]",
                        "s.Target.NeedsScatDisposal()",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[ActorNeedsBallsDisposal]",
                        "s.Actor.NeedsBallsDisposal()",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[TargetNeedsBallsDisposal]",
                        "s.Target.NeedsBallsDisposal()",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[ActorNeedsWombDisposal]",
                        "s.Actor.NeedsWombDisposal()",
                        StringComparison.InvariantCultureIgnoreCase
                    )
                    .Replace(
                        "[TargetNeedsWombDisposal]",
                        "s.Target.NeedsWombDisposal()",
                        StringComparison.InvariantCultureIgnoreCase
                    )


                    // HELPER METHODS
                    .Replace("&&", " AND ", StringComparison.InvariantCultureIgnoreCase)
                    .Replace("||", " OR ", StringComparison.InvariantCultureIgnoreCase)
                    .Replace("!=", " <> ", StringComparison.InvariantCultureIgnoreCase)
                    .Replace("==", " = ", StringComparison.InvariantCultureIgnoreCase)
                    .Replace("Rand.", "", StringComparison.InvariantCultureIgnoreCase)
                    .Replace("State.", "", StringComparison.InvariantCultureIgnoreCase)
                    .Replace("Constants.", "", StringComparison.InvariantCultureIgnoreCase)
                    .Replace("  ", " ", StringComparison.InvariantCultureIgnoreCase);

                if (ret.Contains("["))
                    summaryLog.AppendLine(ret + " has an invalid substitution");
                return ret;
            }

            void AddSet(
                StringBuilder SummaryLog,
                Dictionary<SexInteractionType, List<InteractionString>> TextSex,
                Dictionary<InteractionType, List<InteractionString>> TextInteraction,
                Dictionary<SelfActionType, List<InteractionString>> TextSelfAction,
                Dictionary<VoreMessageType, List<InteractionString>> TextVore,
                Dictionary<BodyPartDescriptionType, List<InteractionString>> TextBody,
                string file,
                string[] sections,
                InteractionString interaction
            )
            {
                if (Enum.TryParse(sections[0], out InteractionType interType))
                {
                    AddToDict(TextInteraction, interaction, interType);
                }
                else if (Enum.TryParse(sections[0], out SexInteractionType sexType))
                {
                    AddToDict(TextSex, interaction, sexType);
                }
                else if (Enum.TryParse(sections[0], out SelfActionType selfType))
                {
                    AddToDict(TextSelfAction, interaction, selfType);
                }
                else if (Enum.TryParse(sections[0], out VoreMessageType voreType))
                {
                    AddToDict(TextVore, interaction, voreType);
                }
                else if (Enum.TryParse(sections[0], out BodyPartDescriptionType bodyType))
                {
                    AddToDict(TextBody, interaction, bodyType);
                }
                else
                {
                    if (sections[0].Trim() != "Type")
                        SummaryLog.AppendLine(
                            $"{sections[0]} listed in {Path.GetFileName(file)} but no such interaction exists."
                        );
                }
            }
        }
    }

    internal static class Utility
    {
        public static string Replace(
            this string str,
            string oldValue,
            string @newValue,
            StringComparison comparisonType
        )
        {
            // Check inputs.
            if (str == null)
            {
                // Same as original .NET C# string.Replace behavior.
                throw new ArgumentNullException(nameof(str));
            }
            if (str.Length == 0)
            {
                // Same as original .NET C# string.Replace behavior.
                return str;
            }
            if (oldValue == null)
            {
                // Same as original .NET C# string.Replace behavior.
                throw new ArgumentNullException(nameof(oldValue));
            }
            if (oldValue.Length == 0)
            {
                // Same as original .NET C# string.Replace behavior.
                throw new ArgumentException("String cannot be of zero length.");
            }

            //if (oldValue.Equals(newValue, comparisonType))
            //{
            //This condition has no sense
            //It will prevent method from replacesing: "Example", "ExAmPlE", "EXAMPLE" to "example"
            //return str;
            //}



            // Prepare string builder for storing the processed string.
            // Note: StringBuilder has a better performance than String by 30-40%.
            StringBuilder resultStringBuilder = new StringBuilder(str.Length);

            // Analyze the replacement: replace or remove.
            bool isReplacementNullOrEmpty = string.IsNullOrEmpty(@newValue);

            // Replace all values.
            const int valueNotFound = -1;
            int foundAt;
            int startSearchFromIndex = 0;
            while (
                (foundAt = str.IndexOf(oldValue, startSearchFromIndex, comparisonType))
                != valueNotFound
            )
            {
                // Append all characters until the found replacement.
                int @charsUntilReplacment = foundAt - startSearchFromIndex;
                bool isNothingToAppend = @charsUntilReplacment == 0;
                if (!isNothingToAppend)
                {
                    resultStringBuilder.Append(str, startSearchFromIndex, @charsUntilReplacment);
                }

                // Process the replacement.
                if (!isReplacementNullOrEmpty)
                {
                    resultStringBuilder.Append(@newValue);
                }

                // Prepare start index for the next search.
                // This needed to prevent infinite loop, otherwise method always start search
                // from the start of the string. For example: if an oldValue == "EXAMPLE", newValue == "example"
                // and comparisonType == "any ignore case" will conquer to replacing:
                // "EXAMPLE" to "example" to "example" to "example" … infinite loop.
                startSearchFromIndex = foundAt + oldValue.Length;
                if (startSearchFromIndex == str.Length)
                {
                    // It is end of the input string: no more space for the next search.
                    // The input string ends with a value that has already been replaced.
                    // Therefore, the string builder with the result is complete and no further action is required.
                    return resultStringBuilder.ToString();
                }
            }

            // Append the last part to the result.
            int @charsUntilStringEnd = str.Length - startSearchFromIndex;
            resultStringBuilder.Append(str, startSearchFromIndex, @charsUntilStringEnd);

            return resultStringBuilder.ToString();
        }

        public static string FixCapitalization(this string str, string value)
        {
            StringComparison comparisonType = StringComparison.InvariantCultureIgnoreCase;
            // Check inputs.
            if (str == null)
            {
                // Same as original .NET C# string.Replace behavior.
                throw new ArgumentNullException(nameof(str));
            }
            if (str.Length == 0)
            {
                // Same as original .NET C# string.Replace behavior.
                return str;
            }
            if (value == null)
            {
                // Same as original .NET C# string.Replace behavior.
                throw new ArgumentNullException(nameof(value));
            }
            if (value.Length == 0)
            {
                // Same as original .NET C# string.Replace behavior.
                throw new ArgumentException("String cannot be of zero length.");
            }

            // Prepare string builder for storing the processed string.
            // Note: StringBuilder has a better performance than String by 30-40%.
            StringBuilder resultStringBuilder = new StringBuilder(str.Length);

            // Analyze the replacement: replace or remove.
            bool isReplacementNullOrEmpty = string.IsNullOrEmpty(value);

            // Replace all values.
            const int valueNotFound = -1;
            int foundAt;
            int startSearchFromIndex = 0;
            while (
                (foundAt = str.IndexOf(value, startSearchFromIndex, comparisonType))
                != valueNotFound
            )
            {
                // Append all characters until the found replacement.
                int @charsUntilReplacment = foundAt - startSearchFromIndex;
                bool isNothingToAppend = @charsUntilReplacment == 0;
                if (!isNothingToAppend)
                {
                    resultStringBuilder.Append(str, startSearchFromIndex, @charsUntilReplacment);
                }

                // Process the replacement.
                if (!isReplacementNullOrEmpty)
                {
                    if (str.Substring(foundAt, value.Length) != value)
                        Debug.LogWarning($"Capitalized '{value}' in '{str}'");
                    resultStringBuilder.Append(value);
                }

                // Prepare start index for the next search.
                // This needed to prevent infinite loop, otherwise method always start search
                // from the start of the string. For example: if an oldValue == "EXAMPLE", newValue == "example"
                // and comparisonType == "any ignore case" will conquer to replacing:
                // "EXAMPLE" to "example" to "example" to "example" … infinite loop.
                startSearchFromIndex = foundAt + value.Length;
                if (startSearchFromIndex == str.Length)
                {
                    // It is end of the input string: no more space for the next search.
                    // The input string ends with a value that has already been replaced.
                    // Therefore, the string builder with the result is complete and no further action is required.
                    return resultStringBuilder.ToString();
                }
            }

            // Append the last part to the result.
            int @charsUntilStringEnd = str.Length - startSearchFromIndex;
            resultStringBuilder.Append(str, startSearchFromIndex, @charsUntilStringEnd);

            return resultStringBuilder.ToString();
        }
    }
}
