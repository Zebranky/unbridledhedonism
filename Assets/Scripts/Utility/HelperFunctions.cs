﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using static UnityEngine.GraphicsBuffer;

static class HelperFunctions
{
    public static bool CanUseMagic(Person a)
    {
        if (State.World.Settings.MagicType == MagicAvailability.NoMagic)
            return false;
        else if (State.World.Settings.MagicType == MagicAvailability.Everyone)
            return true;
        else if (a.Magic.CanCast)
            return true;
        else
            return false;
    }

    public static void CastDecreaseFriendship(Person disliker, Person disliked, float friendship)
    {
        if (
            disliker.HasTrait(Traits.GuineaPig) == false
            || (
                disliker.BeingEaten
                && disliker.FindMyPredator().VoreController.AreSisterPrey(disliker, disliked)
                    == false
            )
        )
        {
            Relationship rel = disliker.GetRelationshipWith(disliked);
            rel.FriendshipLevel = Utility.PushTowardsNegativeOne(rel.FriendshipLevel, friendship);
            if (Rand.NextFloat(-1, 0) > rel.FriendshipLevel && disliker.HasTrait(Traits.Vengeful))
                rel.Vendetta = true;
        }
    }

    public static void DecreaseFriendship(Person disliker, Person disliked, float friendship)
    {
        Relationship rel = disliker.GetRelationshipWith(disliked);
        rel.FriendshipLevel = Utility.PushTowardsNegativeOne(rel.FriendshipLevel, friendship);
    }

    public static void DecreaseRomantic(Person disliker, Person disliked, float romantic)
    {
        Relationship rel = disliker.GetRelationshipWith(disliked);
        rel.RomanticLevel = Utility.PushTowardsNegativeOne(rel.RomanticLevel, romantic);
    }

    public static void DecreaseFriendshipAndRomantic(Person disliker, Person disliked, float change)
    {
        Relationship rel = disliker.GetRelationshipWith(disliked);
        rel.FriendshipLevel = Utility.PushTowardsNegativeOne(rel.FriendshipLevel, change);
        rel.RomanticLevel = Utility.PushTowardsNegativeOne(rel.RomanticLevel, change);
    }

    public static void IncreaseFriendship(Person liker, Person liked, float friendship)
    {
        if (friendship < .3f) //Don't scale huge changes
        {
            friendship *= State.World.Settings.FriendshipMultiplier;
        }
        Relationship rel = liker.GetRelationshipWith(liked);
        rel.FriendshipLevel = Utility.PushTowardOne(rel.FriendshipLevel, friendship);
    }

    public static void IncreaseFriendshipSymetric(
        Person actor,
        Person target,
        float friendship,
        float howWellKnown
    )
    {
        if (friendship < .3f) //Don't scale huge changes
        {
            friendship *= State.World.Settings.FriendshipMultiplier;
            howWellKnown *= State.World.Settings.FriendshipMultiplier;
        }
        Relationship away = actor.GetRelationshipWith(target);
        Relationship towards = target.GetRelationshipWith(actor);
        away.FriendshipLevel = Utility.PushTowardOne(away.FriendshipLevel, friendship);
        away.HowWellKnown = Utility.PushTowardOne(away.HowWellKnown, howWellKnown);
        towards.FriendshipLevel = Utility.PushTowardOne(towards.FriendshipLevel, friendship);
        towards.HowWellKnown = Utility.PushTowardOne(towards.HowWellKnown, howWellKnown);
    }

    public static void IncreaseRomantic(Person liker, Person liked, float romance)
    {
        romance *= State.World.Settings.RomanticMultiplier;

        Relationship rel = liker.GetRelationshipWith(liked);
        rel.RomanticLevel = Utility.PushTowardOne(rel.RomanticLevel, romance);
        if (liked.HasTrait(Quirks.Charmer))
            rel.RomanticLevel = Utility.PushTowardOne(rel.RomanticLevel, romance);
    }

    public static void IncreaseRomanticSymetric(
        Person actor,
        Person target,
        float romance,
        float howWellKnown
    )
    {
        romance *= State.World.Settings.RomanticMultiplier;

        howWellKnown *= State.World.Settings.RomanticMultiplier;
        Relationship away = actor.GetRelationshipWith(target);
        Relationship towards = target.GetRelationshipWith(actor);
        away.RomanticLevel = Utility.PushTowardOne(away.RomanticLevel, romance);
        if (target.HasTrait(Quirks.Charmer))
            away.RomanticLevel = Utility.PushTowardOne(away.RomanticLevel, romance);
        away.HowWellKnown = Utility.PushTowardOne(away.HowWellKnown, howWellKnown);
        towards.RomanticLevel = Utility.PushTowardOne(towards.RomanticLevel, romance);
        if (actor.HasTrait(Quirks.Charmer))
            towards.RomanticLevel = Utility.PushTowardOne(towards.RomanticLevel, romance);
        towards.HowWellKnown = Utility.PushTowardOne(towards.HowWellKnown, howWellKnown);
    }

    internal static float HealthyWeight(Person actor)
    {
        return actor.PartList.Height * actor.PartList.Height / 32.5f;
    }

    public static List<Person> GetPeopleWithinXSquares(Person source, int tiles)
    {
        List<Person> people = new List<Person>();
        foreach (var person in State.World.GetPeople(false))
        {
            if (person == source || person.Dead)
                continue;
            if (source.Position.GetNumberOfMovesDistance(person.Position) <= tiles)
            {
                people.Add(person);
            }
        }
        return people;
    }

    //public static void YellForHelp(Person person)
    //{
    //    var squares = Sound.GetAffectedSquares(person.Position, 3);
    //    foreach (var other in State.World.GetPeople(false))
    //    {
    //        if (person == other)
    //            return;
    //        if (person.Position == other.Position)
    //            return;
    //        if (squares.Contains(other.Position))
    //        {
    //            other.AI.DiscoveredVoreAttempt(person);
    //        }
    //    }
    //}

    public static bool InPrivateRoom(Person person)
    {
        return State.World.Zones[person.Position.x, person.Position.y].AllowedPeople?.Count() > 0;
    }

    public static bool InPrivateArea(Person source, Person target)
    {
        // check for each person in list of all people...
        foreach (var person in State.World.GetPeople(false))
        {
            // skip check if person is self, target, or dead
            if (person == source || person == target || person.Dead)
                continue;
            // if person can see source, return false
            if (LOS.Check(person.Position, source.Position))
                return false;
        }
        return true;
    }

    internal static string GetRandomStringFrom(params string[] messages)
    {
        return messages[Rand.Next(messages.Length)];
    }

    internal static string GetRandomStringFrom(int seed, params string[] messages)
    {
        Random rand = new Random(seed);
        return messages[rand.Next(messages.Length)];
    }

    /// <summary>
    /// Checks for possession, and whether it should be possible or not
    /// </summary>
    /// <returns></returns>
    internal static bool CanDigestPrey(Person pred, Person prey)
    {
        if (pred.Team != 0 && pred.Team == prey.Team)
            return false;
        if (prey.DigestionImmune)
            return false;
        return prey.FindMyPredator() == pred
            && pred.VoreController.TargetIsBeingDigested(prey) == false
            && State.World.Settings.CheckDigestion(pred, DigestionAlias.CanVore)
            && pred.PredatorTier >= prey.PredatorTier;
    }

    public static void GetRelationships(ref StringBuilder sb, Person source, Person target)
    {
        var relIn = target.GetRelationshipWith(source);
        var relOut = source.GetRelationshipWith(target);
        if (source.Romance.CanSafelyRomance(target))
        {
            if (Config.DebugViewPreciseValues)
            {
                if (Math.Abs(relIn.FriendshipLevel - relOut.FriendshipLevel) < .05f)
                    sb.AppendLine(
                        $"Friendship level: {GetWord.Friendship(relIn.FriendshipLevel)} ({Math.Round(relIn.FriendshipLevel, 3)})"
                    );
                else
                {
                    sb.AppendLine(
                        $"Friendship toward me: {GetWord.Friendship(relIn.FriendshipLevel)} ({Math.Round(relIn.FriendshipLevel, 3)})"
                    );
                    sb.AppendLine(
                        $"Friendship toward them: {GetWord.Friendship(relOut.FriendshipLevel)} ({Math.Round(relOut.FriendshipLevel, 3)})"
                    );
                }

                if (Math.Abs(relIn.RomanticLevel - relOut.RomanticLevel) < .05f)
                    sb.AppendLine(
                        $"Romantic level: {GetWord.Romantic(relIn.RomanticLevel)} ({Math.Round(relIn.RomanticLevel, 3)})"
                    );
                else
                {
                    sb.AppendLine(
                        $"Romantic toward me: {GetWord.Romantic(relIn.RomanticLevel)} ({Math.Round(relIn.RomanticLevel, 3)})"
                    );
                    sb.AppendLine(
                        $"Romantic toward them: {GetWord.Romantic(relOut.RomanticLevel)} ({Math.Round(relOut.RomanticLevel, 3)})"
                    );
                }
            }
            else
            {
                if (Math.Abs(relIn.FriendshipLevel - relOut.FriendshipLevel) < .05f)
                    sb.AppendLine($"Friendship level: {GetWord.Friendship(relIn.FriendshipLevel)}");
                else
                {
                    sb.AppendLine(
                        $"Friendship toward me: {GetWord.Friendship(relIn.FriendshipLevel)}"
                    );
                    sb.AppendLine(
                        $"Friendship toward them: {GetWord.Friendship(relOut.FriendshipLevel)}"
                    );
                }

                if (Math.Abs(relIn.RomanticLevel - relOut.RomanticLevel) < .05f)
                    sb.AppendLine($"Romantic level: {GetWord.Romantic(relIn.RomanticLevel)}");
                else
                {
                    sb.AppendLine($"Romantic toward me: {GetWord.Romantic(relIn.RomanticLevel)}");
                    sb.AppendLine(
                        $"Romantic toward them: {GetWord.Romantic(relOut.RomanticLevel)}"
                    );
                }
            }
        }
        else
        {
            if (Config.DebugViewPreciseValues)
            {
                if (Math.Abs(relIn.FriendshipLevel - relOut.FriendshipLevel) < .05f)
                    sb.AppendLine(
                        $"Friendship level: {GetWord.Friendship(relIn.FriendshipLevel)} ({Math.Round(relIn.FriendshipLevel, 3)})"
                    );
                else
                {
                    sb.AppendLine(
                        $"Friendship toward me: {GetWord.Friendship(relIn.FriendshipLevel)} ({Math.Round(relIn.FriendshipLevel, 3)})"
                    );
                    sb.AppendLine(
                        $"Friendship toward them: {GetWord.Friendship(relOut.FriendshipLevel)} ({Math.Round(relOut.FriendshipLevel, 3)})"
                    );
                }
            }
            else
            {
                if (Math.Abs(relIn.FriendshipLevel - relOut.FriendshipLevel) < .05f)
                    sb.AppendLine($"Friendship level: {GetWord.Friendship(relIn.FriendshipLevel)}");
                else
                {
                    sb.AppendLine(
                        $"Friendship toward me: {GetWord.Friendship(relIn.FriendshipLevel)}"
                    );
                    sb.AppendLine(
                        $"Friendship toward them: {GetWord.Friendship(relOut.FriendshipLevel)}"
                    );
                }
            }
        }
    }

    public static void GetRelationshipsPeople(ref StringBuilder sb, Person source, Person target)
    {
        if (source.Romance.CanSafelyRomance(target))
        {
            if (target.Romance.Dating != null)
                sb.AppendLine(
                    $"Dating: {target.Romance.Dating.FirstName} {target.Romance.Dating.LastName}"
                );
            else
                sb.AppendLine($"Dating: Single");

            if (target.FindBestFriend() != null)
            {
                if (target.GetRelationshipWith(target.FindBestFriend()).FriendshipLevel >= 0.5)
                    sb.AppendLine(
                        $"Bestie: {target.FindBestFriend().FirstName} {target.FindBestFriend().LastName}"
                    );
            }

            if (target.FindNemesis() != null)
            {
                if (target.GetRelationshipWith(target.FindNemesis()).FriendshipLevel <= -0.3)
                    sb.AppendLine(
                        $"Nemesis: {target.FindNemesis().FirstName} {target.FindNemesis().LastName}"
                    );
            }

            if (target.FindNemesis() != target.FindVendetta() && target.FindVendetta() != null)
                sb.AppendLine(
                    $"Currently angry at: {target.FindVendetta().FirstName} {target.FindVendetta().LastName}"
                );
        }
        else
        {
            if (target.Romance.Dating != null)
                sb.AppendLine(
                    $"Dating: {target.Romance.Dating.FirstName} {target.Romance.Dating.LastName}"
                );

            if (target.FindBestFriend() != null)
            {
                if (target.GetRelationshipWith(target.FindBestFriend()).FriendshipLevel >= 0.5)
                    sb.AppendLine(
                        $"Bestie: {target.FindBestFriend().FirstName} {target.FindBestFriend().LastName}"
                    );
            }

            if (target.FindNemesis() != null)
            {
                if (target.GetRelationshipWith(target.FindNemesis()).FriendshipLevel <= -0.3)
                    sb.AppendLine(
                        $"Nemesis: {target.FindNemesis().FirstName} {target.FindNemesis().LastName}"
                    );
            }

            if (target.FindNemesis() != target.FindVendetta() && target.FindVendetta() != null)
                sb.AppendLine(
                    $"Currently angry at: {target.FindVendetta().FirstName} {target.FindVendetta().LastName}"
                );
        }
    }

    public static void RomanticRejection(Person actor, Person target, float impact = 1)
    {
        Relationship actorToTarget = actor.GetRelationshipWith(target);
        if (
            target.Romance.IsDating
            && target.Romance.Dating != actor
            && target.Personality.CheatOnPartner != ThreePointScale.Frequently
        )
        {
            if (target.Personality.CheatOnPartner == ThreePointScale.Never)
            {
                //Already in a relationship and not willing to cheat
                actorToTarget.RomanticLevel -= .2f * impact;
                actorToTarget.KnowledgeAbout.IsDating = true;
                actorToTarget.KnowledgeAbout.DatingTarget = target.Romance.Dating;
            }
            else
            {
                //A slightly above average pushback for rarely cheat
                actorToTarget.RomanticLevel = Utility.PushTowardsNegativeOne(
                    actorToTarget.RomanticLevel,
                    .04f * impact
                );
            }

            //Log.Add($"{actor.FirstName} let {target.FirstName} know that they are already in a romantic relationship with someone else");
        }
        else if (target.Romance.DesiresGender(actor.GenderType) == false)
        {
            //Romantically Incompatible, but should depend on personality if they tell this person that that's the case or just a general rejection
            actorToTarget.RomanticLevel = Utility.PushTowardsNegativeOne(
                actorToTarget.RomanticLevel,
                .05f * impact
            );
        }
        else
        {
            if ((target.Romance.Dating == actor) == false)
                actorToTarget.RomanticLevel = Utility.PushTowardsNegativeOne(
                    actorToTarget.RomanticLevel,
                    .01f * impact
                );
        }
    }
}
