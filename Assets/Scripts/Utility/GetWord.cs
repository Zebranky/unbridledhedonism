﻿using System.Collections.Generic;
using System.Linq;

static class GetWord
{
    const string ColorCriticalHorny = "#ff81a6";
    const string ColorHighHorny = "#ff81a6";
    const string ColorCriticalNeed = "#ff5c33";
    const string ColorHighNeed = "#ffc233";

    internal static string StatBars(float value)
    {
        if (value >= 1.0f)
            return "◼◼◼◼◼◼◼◼◼◼";
        else if (value >= .9f)
            return "◼◼◼◼◼◼◼◼◼◻";
        else if (value >= .8f)
            return "◼◼◼◼◼◼◼◼◻◻";
        else if (value >= .7f)
            return "◼◼◼◼◼◼◼◻◻◻";
        else if (value >= .6f)
            return "◼◼◼◼◼◼◻◻◻◻";
        else if (value >= .5f)
            return "◼◼◼◼◼◻◻◻◻◻";
        else if (value >= .4f)
            return "◼◼◼◼◻◻◻◻◻◻";
        else if (value >= .3f)
            return "◼◼◼◻◻◻◻◻◻◻";
        else if (value >= .2f)
            return "◼◼◻◻◻◻◻◻◻◻";
        else if (value >= .1f)
            return "◼◻◻◻◻◻◻◻◻◻";
        else
            return "◻◻◻◻◻◻◻◻◻◻";
    }

    internal static string HornyBar(float value)
    {
        if (value >= .9f)
            return $"<color={ColorCriticalHorny}>◼◼◼◼◼◼◼◼◼◼</color>";
        else if (value >= .82f)
            return $"<color={ColorCriticalHorny}>◼◼◼◼◼◼◼◼◼◻</color>";
        else if (value >= .74f)
            return $"<color={ColorCriticalHorny}>◼◼◼◼◼◼◼◼◻◻</color>";
        else if (value >= .66f)
            return $"<color={ColorHighHorny}>◼◼◼◼◼◼◼◻◻◻</color>";
        else if (value >= .58f)
            return $"<color={ColorHighHorny}>◼◼◼◼◼◼◻◻◻◻</color>";
        else if (value >= .5f)
            return $"<color={ColorHighHorny}>◼◼◼◼◼◻◻◻◻◻</color>";
        else if (value >= .42f)
            return "◼◼◼◼◻◻◻◻◻◻";
        else if (value >= .32f)
            return "◼◼◼◻◻◻◻◻◻◻";
        else if (value >= .21f)
            return "◼◼◻◻◻◻◻◻◻◻";
        else if (value >= .1f)
            return "◼◻◻◻◻◻◻◻◻◻";
        else
            return "◻◻◻◻◻◻◻◻◻◻";
    }

    internal static string HealthBars(float value)
    {
        if (value >= .9f)
            return "◼◼◼◼◼◼◼◼◼◼";
        else if (value >= .8f)
            return "◼◼◼◼◼◼◼◼◼◻";
        else if (value >= .7f)
            return "◼◼◼◼◼◼◼◼◻◻";
        else if (value >= .6f)
            return "◼◼◼◼◼◼◼◻◻◻";
        else if (value >= .5f)
            return "◼◼◼◼◼◼◻◻◻◻";
        else if (value >= .4f)
            return $"<color={ColorHighNeed}>◼◼◼◼◼◻◻◻◻◻</color>";
        else if (value >= .3f)
            return $"<color={ColorHighNeed}>◼◼◼◼◻◻◻◻◻◻</color>";
        else if (value >= .2f)
            return $"<color={ColorHighNeed}>◼◼◼◻◻◻◻◻◻◻</color>";
        else if (value >= .1f)
            return $"<color={ColorCriticalNeed}>◼◼◻◻◻◻◻◻◻◻</color>";
        else if (value > .0f)
            return $"<color={ColorCriticalNeed}>◼◻◻◻◻◻◻◻◻◻</color>";
        else
            return $"<color={ColorCriticalNeed}>◻◻◻◻◻◻◻◻◻◻</color>";
    }

    internal static string Hunger(float value)
    {
        if (value >= 1f)
            return $"<color={ColorCriticalNeed}>starving</color>";
        else if (value >= .8f)
            return $"<color={ColorCriticalNeed}>very hungry</color>";
        else if (value >= .5f)
            return $"<color={ColorHighNeed}>hungry</color>";
        else if (value >= .3f)
            return "peckish";
        else if (value >= .2f)
            return "neutral";
        else if (value >= .1f)
            return "full";
        else
            return "stuffed";
    }

    internal static string Extroversion(float value)
    {
        if (value >= .9f)
            return "very outgoing";
        else if (value >= .65f)
            return "extrovert";
        else if (value >= .35f)
            return "ambivert";
        else if (value >= .10f)
            return "introvert";
        else
            return "extremely shy";
    }

    internal static string Dominance(float value)
    {
        if (value >= .9f)
            return "very dominant";
        else if (value >= .65f)
            return "fairly dominant";
        else if (value >= .35f)
            return "switch";
        else if (value >= .10f)
            return "fairly submissive";
        else
            return "very submissive";
    }

    internal static string Kindness(float value)
    {
        if (value >= .9f)
            return "purely selfless";
        else if (value >= .65f)
            return "kindhearted";
        else if (value >= .35f)
            return "average";
        else if (value >= .10f)
            return "selfish";
        else
            return "highly selfish";
    }

    internal static string DigestionWillingness(float value)
    {
        if (value >= .9f)
            return "digest me";
        else if (value >= .75f)
            return "high";
        else if (value >= .55f)
            return "sometimes";
        else if (value >= .10f)
            return "rarely";
        else
            return "absolutely not";
    }

    internal static string Tiredness(float value)
    {
        if (value >= 1f)
            return $"<color={ColorCriticalNeed}>exhausted</color>";
        else if (value >= .8f)
            return $"<color={ColorCriticalNeed}>sleep deprived</color>";
        else if (value >= .6f)
            return $"<color={ColorHighNeed}>tired</color>";
        else if (value >= .5f)
            return $"<color={ColorHighNeed}>weary</color>";
        else if (value >= .3f)
            return "rested";
        else
            return "well rested";
    }

    internal static string Cleanliness(float value)
    {
        if (value >= 1f)
            return $"<color={ColorCriticalNeed}>disgusting</color>";
        else if (value >= .8f)
            return $"<color={ColorCriticalNeed}>very sweaty</color>";
        else if (value >= .6f)
            return $"<color={ColorHighNeed}>sweaty</color>";
        else if (value >= .5f)
            return $"<color={ColorHighNeed}>adequate</color>";
        else if (value >= .3f)
            return "acceptable";
        else if (value >= .1f)
            return "fresh";
        else
            return "squeaky clean";
    }

    internal static string Horniness(float value)
    {
        if (value >= .9f)
            return $"<color={ColorCriticalHorny}>near climax</color>";
        else if (value >= .74f)
            return $"<color={ColorCriticalHorny}>panting</color>";
        else if (value >= .5f)
            return $"<color={ColorHighHorny}>aroused</color>";
        else if (value >= .3f)
            return "warmed up";
        else if (value >= .1f)
            return "minimal";
        else
            return "none";
    }

    internal static string Healthiness(int value)
    {
        if (value >= .8f * Constants.HealthMax)
            return "excellent";
        else if (value >= .5f * Constants.HealthMax)
            return "injured";
        else if (value >= .2f * Constants.HealthMax)
            return $"<color={ColorHighNeed}>badly wounded</color>";
        else if (value > 0)
            return $"<color={ColorCriticalNeed}>critical shape</color>";
        else
            return $"<color={ColorCriticalNeed}>dead</color>";
    }

    internal static string Charisma(float value)
    {
        if (value >= .8f)
            return "charming";
        else if (value >= .5f)
            return "smooth";
        else if (value >= .2f)
            return "rough";
        else
            return "poor";
    }

    internal static string SexDrive(float value)
    {
        if (value >= .7f)
            return "lustful";
        else if (value >= .5f)
            return "warm";
        else if (value >= .3f)
            return "neutral";
        else
            return "cold";
    }

    internal static string Promiscuity(float value)
    {
        if (value >= .9f)
            return "sex addict";
        else if (value >= .7f)
            return "pleasure driven";
        else if (value >= .4f)
            return "tease";
        else
            return "prude";
    }

    internal static string Strength(float value)
    {
        if (value >= .8f)
            return "peak shape";
        else if (value >= .6f)
            return "strong";
        else if (value >= .4f)
            return "average";
        else if (value >= .2f)
            return "weak";
        else
            return "pathetic";
    }

    internal static string Voracity(float value)
    {
        if (value >= .9f)
            return "apex predator";
        else if (value >= .7f)
            return "veteran";
        else if (value >= .4f)
            return "experienced";
        else if (value >= .2f)
            return "adequate";
        else
            return "inexperienced";
    }

    internal static string PredWillingness(float value)
    {
        if (value >= .8f)
            return "people are food";
        else if (value >= .6f)
            return "frequent pred";
        else if (value >= .4f)
            return "occasional pred";
        else
            return "reluctant pred";
    }

    internal static string PredLoyalty(float value)
    {
        if (value >= .8f)
            return "won't eat friends";
        else if (value >= .6f)
            return "values friends";
        else if (value >= .4f)
            return "thinks twice";
        else
            return "everyone is food";
    }

    internal static string PreyWillingness(float value)
    {
        if (value >= .7f)
            return "fully willing";
        else if (value >= .5f)
            return "partially willing";
        else if (value >= .3f)
            return "hesitant";
        else
            return "fight to the end";
    }

    internal static string Voraphilia(float value)
    {
        if (value >= .8f)
            return "loves vore";
        else if (value >= .6f)
            return "likes vore";
        else if (value >= .4f)
            return "curious";
        else if (value >= .2f)
            return "ambivalent";
        else
            return "yucky";
    }

    internal static string Friendship(float value)
    {
        if (value >= .8f)
            return "close friends";
        else if (value >= .6f)
            return "good buddies";
        else if (value >= .3f)
            return "friendly";
        else if (value >= .1f)
            return "warm";
        else if (value >= -.1f)
            return "neutral";
        else if (value >= -.3f)
            return "cold";
        else if (value >= -.6f)
            return $"<color={ColorCriticalNeed}>disliked</color>";
        else if (value >= -.8f)
            return $"<color={ColorCriticalNeed}>intense dislike</color>";
        else
            return $"<color={ColorCriticalNeed}>mortal enemies</color>";
    }

    internal static string Romantic(float value)
    {
        if (value >= .8f)
            return $"<color={ColorCriticalHorny}>lovers</color>";
        else if (value >= .4f)
            return $"<color={ColorHighHorny}>like them</color>";
        else if (value >= .1f)
            return "curious";
        else if (value >= -.1f)
            return "neutral";
        else if (value >= -.4f)
            return "disinterested";
        else if (value >= -.7f)
            return $"<color={ColorCriticalNeed}>disgusted</color>";
        else
            return $"<color={ColorCriticalNeed}>repulsive</color>";
    }

    internal static string DickSize(float size)
    {
        if (size >= 4.5f)
            return "titanic";
        else if (size >= 4f)
            return "massive";
        else if (size >= 3.5f)
            return "giant";
        else if (size >= 3f)
            return "huge";
        else if (size >= 2.5f)
            return "large";
        else if (size >= 2f)
            return "impressive";
        else if (size >= 1.2f)
            return "average";
        else if (size >= .8f)
            return "modest";
        else if (size >= .5f)
            return "small";
        else if (size >= .25f)
            return "tiny";
        else if (size >= 0)
            return "miniscule";
        return "";
    }

    internal static string BallSize(float size)
    {
        if (size >= 4.5f)
            return "titanic";
        else if (size >= 4f)
            return "massive";
        else if (size >= 3.5f)
            return "giant";
        else if (size >= 3f)
            return "huge";
        else if (size >= 2.5f)
            return "large";
        else if (size >= 2f)
            return "impressive";
        else if (size >= 1.2f)
            return "average";
        else if (size >= .8f)
            return "modest";
        else if (size >= .5f)
            return "small";
        else if (size >= .25f)
            return "tiny";
        else if (size >= 0)
            return "miniscule";
        return "";
    }

    internal static string BreastSize(float size)
    {
        if (size >= 4.5f)
            return "titanic";
        else if (size >= 4f)
            return "massive";
        else if (size >= 3.5f)
            return "giant";
        else if (size >= 3f)
            return "huge";
        else if (size >= 2.5f)
            return "large";
        else if (size >= 2f)
            return "impressive";
        else if (size >= 1.2f)
            return "average";
        else if (size >= .8f)
            return "modest";
        else if (size >= .5f)
            return "small";
        else if (size >= .25f)
            return "tiny";
        else if (size >= 0)
            return "flat";
        return "";
    }

    internal static string RelationshipWord(Person actor, Person target)
    {
        var rel = target.GetRelationshipWith(actor);
        float friendship = rel.FriendshipLevel;

        if (rel.Met == false)
            return "not yet met";
        else if (target.Magic.IsCharmedBy(actor))
            return "charmed by you";
        else if (rel.Vendetta)
            return "wants you dead";
        else if (actor.Romance.Dating == target)
            return "dating";
        else if (rel.RomanticLevel >= .8f)
            return "lover";
        else if (friendship >= .8f)
            return "close friend";
        else if (friendship >= .6f)
            return "good buddy";
        else if (friendship >= .3f)
            return "friendly";
        else if (friendship >= .1f)
            return "warm";
        else if (friendship >= -.1f)
            return "neutral";
        else if (friendship >= -.3f)
            return "cold";
        else if (friendship >= -.6f)
            return "disliked";
        else if (friendship >= -.8f)
            return "intense dislike";
        else
            return "mortal enemy";
    }

    internal static string PreyStatusDescription(Person actor)
    {
        float health = actor.Health;
        var progress = actor.FindMyPredator().VoreController.GetProgressOf(actor);
        string str = "";

        // general description of character's health
        if (health <= 0)
            str += "dead";
        else if (actor.FindMyPredator().VoreController.TargetIsBeingDigested(actor) == false)
            str += "safe";
        else if (health < .2f * Constants.HealthMax)
            str += "critical";
        else if (health < .5f * Constants.HealthMax)
            str += "badly wounded";
        else if (health < .8f * Constants.HealthMax)
            str += "injured";
        else
            str += "healthy";

        // add descriptor for willingness
        str += ", ";
        if (progress.Willing)
            str += "willing prey";
        else
            str += "unwilling";

        // add descriptor for location
        str += " (";
        if (progress.IsSwallowing())
            str += "going to ";
        else
            str += "in ";

        str += $"{progress.Location.ToString().ToLower()})";

        return str;
    }



    internal static string SizeWord(Person actor, Person target) {
        // generate some words that describe a character's size

        // initialize a list of strings
        List<string> words = new List<string> { null };

        // if there are two characters involved in this interaction
        if (actor != null && target != null && actor != target)
        {
            // run checks for if actor is smaller than target
            if (actor.SizeRelativeTo(target) < 0.8 && actor.SizeRelativeTo(target) >= 0.5)
            {
                words.Add("smaller");
                words.Add("comparatively small");
            }
            else if (actor.SizeRelativeTo(target) < 0.5 && actor.SizeRelativeTo(target) >= 0.2)
            {
                words.Add("much smaller");
                words.Add("comparatively tiny");
            }
            else if (actor.SizeRelativeTo(target) < 0.2 && actor.SizeRelativeTo(target) >= 0.08)
            {
                words.Add("far smaller");
                words.Add("comparatively miniscule");
            }
            else if (actor.SizeRelativeTo(target) < 0.08)
            {
                words.Add("bite-sized");
                words.Add("comparatively microscopic");
                words.Add("nearly insignificant");
            }

            // run checks for if target is smaller than actor
            else if (actor.SizeRelativeTo(target) > 1.2 && actor.SizeRelativeTo(target) <= 1.6)
            {
                words.Add("taller");
                words.Add("comparatively tall");
            }
            else if (actor.SizeRelativeTo(target) > 1.6 && actor.SizeRelativeTo(target) <= 2.0)
            {
                words.Add("much taller");
                words.Add("much larger");
                words.Add("comparatively huge");
            }
            else if (actor.SizeRelativeTo(target) > 2.0 && actor.SizeRelativeTo(target) <= 3.0)
            {
                words.Add("far larger");
                words.Add("comparatively giant");
            }
            else if (actor.SizeRelativeTo(target) > 3.0)
            {
                words.Add("far, far larger");
                words.Add("comparatively massive");
            }
        }

        // if char changed by size magic, also include these words
        if (actor.Magic.Resize_Level != 0)
        {
            if (actor.Magic.Resize_Level < 0)
            {
                words.Add("shrunken");
            }
            else if (actor.Magic.Resize_Level > 0)
            {
                words.Add("enlarged");
            }
        }

        // height based checks
        {
            // around average human height
            // additionally, only make a note of this if there is nothing else selected yet
            if (actor.PartList.Height > 64 && actor.PartList.Height <= 72 && words.Count == 1)
            {
                words.Add("average sized");
            }

            // taller
            // only use these words if no words were yet selected, or if actor is larger than the target
            if (target == null || words == null || actor.PartList.Height > target.PartList.Height) 
            {
                if (actor.PartList.Height > 72 && actor.PartList.Height <= 80)
                {
                    words.Add("tall");
                }
                else if (actor.PartList.Height > 80 && actor.PartList.Height <= 92)
                {
                    words.Add("very tall");
                    words.Add("large");
                }
                else if (actor.PartList.Height > 92 && actor.PartList.Height <= 120)
                {
                    words.Add("huge");
                    words.Add("near giant");
                    words.Add("very large");
                }
                else if (actor.PartList.Height > 120 && actor.PartList.Height <= 180)
                {
                    words.Add("massive");
                    words.Add("giant");
                }
                else if (actor.PartList.Height > 180 && actor.PartList.Height <= 300)
                {
                    words.Add("gigantic");
                }
                else if (actor.PartList.Height > 300)
                {
                    words.Add("titanic");
                    words.Add("earth-shatteringly huge");
                }
            }

            // smaller
            // only use these words if no words were yet selected, or if actor is smaller than the target
            if (target == null || words == null || actor.PartList.Height < target.PartList.Height)
            {
                if (actor.PartList.Height > 56 && actor.PartList.Height <= 64)
                {
                    words.Add("short");
                }
                else if (actor.PartList.Height > 44 && actor.PartList.Height <= 56)
                {
                    words.Add("very short");
                    words.Add("small");
                }
                else if (actor.PartList.Height > 30 && actor.PartList.Height <= 44)
                {
                    words.Add("tiny");
                    words.Add("very small");
                }
                else if (actor.PartList.Height > 16 && actor.PartList.Height <= 30)
                {
                    words.Add("extremely small");
                    words.Add("miniature");
                }
                else if (actor.PartList.Height > 8 && actor.PartList.Height <= 16)
                {
                    words.Add("miniscule");
                }
                else if (actor.PartList.Height <= 8)
                {
                    words.Add("microscopic");
                    words.Add("bite-sized");
                }
            }
        }

        if (words.Count > 1)
            words.Remove(null);

        // finally, return a randomly selected word from the list
        return words[Rand.Next(words.Count)];
    }

    internal static string FatnessWord(Person actor)
    {
        // generate some words that describe a character's fatness

        // initialize a list of strings
        List<string> words = new List<string> { null };

        var bmi = (703 * actor.PartList.Weight) / (actor.PartList.Height * actor.PartList.Height);

        // "healthy" bmi range
        if (bmi > 18.5 && bmi <= 25)
        {
            words.Add("healthy");
        }

        // overweight range
        else if (bmi > 25 && bmi <= 32)
        {
            words.Add("chubby");
            words.Add("curvy");
            words.Add("soft");
        }
        else if (bmi > 32 && bmi <= 38)
        {
            words.Add("fat");
            words.Add("lush");
            words.Add("plush");
            words.Add("very chubby");
        }
        else if (bmi > 38 && bmi <= 50)
        {
            words.Add("very fat");
            words.Add("extra large");
            words.Add("indulgent");
        }
        else if (bmi > 50)
        {
            words.Add("extremely fat");
            words.Add("massively fat");
            words.Add("titanic");
        }

        // underweight range
        else if (bmi > 16 && bmi <= 18.5)
        {
            words.Add("skinny");
            words.Add("slim");
            words.Add("lean");
        }
        else if (bmi > 15 && bmi <= 16)
        {
            words.Add("very skinny");
            words.Add("very slim");
            words.Add("very lean");
            words.Add("underweight");
        }
        else if (bmi < 15)
        {
            words.Add("extremely skinny");
        }

        if (words.Count > 1)
            words.Remove(null);

        // finally, return a randomly selected word from the list
        return words[Rand.Next(words.Count)];
    }

}
