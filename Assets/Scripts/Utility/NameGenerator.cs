﻿using System.Collections.Generic;
using System.IO;
using System.Text;

static class NameGenerator
{
    static internal string GetRandomFemaleName() => FemaleNames[Rand.Next(FemaleNames.Length)];

    static internal string GetRandomMaleName() => MaleNames[Rand.Next(MaleNames.Length)];

    static internal string GetRandomLastName() => LastNames[Rand.Next(LastNames.Length)];

    static NameGenerator()
    {
        Encoding encoding = Encoding.GetEncoding("iso-8859-1");

        if (
            File.Exists(Path.Combine(UnityEngine.Application.streamingAssetsPath, "male names.txt"))
        )
        {
            var logFile = File.ReadAllLines(
                Path.Combine(UnityEngine.Application.streamingAssetsPath, "male names.txt"),
                encoding
            );
            MaleNames = new List<string>(logFile).ToArray();
        }

        if (
            File.Exists(
                Path.Combine(UnityEngine.Application.streamingAssetsPath, "female names.txt")
            )
        )
        {
            var logFile = File.ReadAllLines(
                Path.Combine(UnityEngine.Application.streamingAssetsPath, "female names.txt"),
                encoding
            );
            FemaleNames = new List<string>(logFile).ToArray();
        }

        if (
            File.Exists(Path.Combine(UnityEngine.Application.streamingAssetsPath, "last names.txt"))
        )
        {
            var logFile = File.ReadAllLines(
                Path.Combine(UnityEngine.Application.streamingAssetsPath, "last names.txt"),
                encoding
            );
            LastNames = new List<string>(logFile).ToArray();
        }
    }

    static string[] FemaleNames = new string[]
    {
        "Mary",
        "Patricia",
        "Elizabeth",
        "Jennifer",
        "Linda",
        "Barbara",
        "Margaret",
        "Susan",
        "Dorothy",
        "Jessica",
        "Sarah",
        "Betty",
        "Nancy",
        "Karen",
        "Lisa",
        "Helen",
        "Sandra",
        "Ashley",
        "Donna",
        "Kimberly",
        "Carol",
        "Michelle",
        "Emily",
        "Amanda",
        "Laura",
        "Melissa",
        "Deborah",
        "Stephanie",
        "Rebecca",
        "Ruth",
        "Anna",
        "Sharon",
        "Kathleen",
        "Cynthia",
        "Shirley",
        "Amy",
        "Angela",
        "Virginia",
        "Catherine",
        "Brenda",
        "Katherine",
        "Pamela",
        "Nicole",
        "Christine",
        "Janet",
        "Samantha",
        "Debra",
        "Carolyn",
        "Rachel",
        "Frances",
        "Heather",
        "Maria",
        "Diane",
        "Joyce",
        "Julie",
        "Evelyn",
        "Emma",
        "Martha",
        "Joan",
        "Alice",
        "Kelly",
        "Christina",
    };

    static string[] MaleNames = new string[]
    {
        "James",
        "John",
        "Robert",
        "Michael",
        "William",
        "David",
        "Richard",
        "Joseph",
        "Charles",
        "Thomas",
        "Christopher",
        "Daniel",
        "Matthew",
        "Donald",
        "Anthony",
        "Paul",
        "Mark",
        "George",
        "Steven",
        "Kenneth",
        "Andrew",
        "Edward",
        "Joshua",
        "Brian",
        "Kevin",
        "Ronald",
        "Timothy",
        "Jason",
        "Jeffrey",
        "Gary",
        "Ryan",
        "Nicholas",
        "Eric",
        "Jacob",
        "Stephen",
        "Frank",
        "Jonathan",
        "Larry",
        "Scott",
        "Justin",
        "Raymond",
        "Brandon",
        "Gregory",
        "Samuel",
        "Benjamin",
        "Patrick",
        "Jack",
    };

    static string[] LastNames = new string[]
    {
        "Klemm",
        "Vantrease",
        "Brindley",
        "Alejos",
        "Myres",
        "Fasano",
        "Fang",
        "Smelser",
        "Bratten",
        "Prins",
        "Fukushima",
        "Roma",
        "Cahall",
        "Gillard",
        "Facemire",
        "Monsen",
        "Murrell",
        "Eisen",
        "Overly",
        "Fleming",
        "Fairall",
        "Neptune",
        "Mcwain",
        "Depaz",
        "Laverdiere",
        "Pellegren",
        "Ciesielski",
        "Rients",
        "Moseley",
        "Depalma",
        "Lederman",
        "Bouffard",
        "Smith",
        "Johnson",
        "Williams",
        "Jones",
        "Coleman",
        "Wright",
        "Cook",
        "Long",
        "Adams",
        "Baker",
        "Thompson",
        "Ward",
        "Simmons",
        "Gray",
        "Robinson",
        "Collins",
        "Hayes",
    };
}
