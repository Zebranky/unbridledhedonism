﻿using OdinSerializer;
using OdinSerializer.Utilities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

public static class State
{
    public const string Version = "9F RH 9C";
    static int saveErrors = 0;
    internal static GameManager GameManager;
    public static World World;

    public static string SaveDirectory;
    public static string MapDirectoryOld;
    public static string MapDirectory;
    public static string PicDirectory;
    public static string CharDirectory;
    public static string RaceDirectory;
    public static string TextDirectory;
    public static string BackgroundDirectory;

    public static string StorageDirectory;

    internal static KeyManager KeyManager;

    internal static TextLogger TextLogger;

    internal static GenderList BackupGenderList;
    internal static RaceWeights RaceWeights;
    internal static TraitWeights TraitWeights;

    internal static SavedPersonController SavedPersonController = new SavedPersonController();
    internal static TemplateController TemplateController = new TemplateController();

    internal static List<string> InitializeErrors = new List<string>();

    static State()
    {
        SaveDirectory = Path.Combine(Application.persistentDataPath, "Saves");
        MapDirectoryOld = Path.Combine(Application.persistentDataPath, "Maps");
        MapDirectory = Path.Combine(Application.persistentDataPath, "Content", "Raw", "Maps");
        PicDirectory = Path.Combine(Application.persistentDataPath, "Content", "Raw", "Pictures");
        CharDirectory = Path.Combine(Application.persistentDataPath, "Content", "Raw", "Chars");
        RaceDirectory = Path.Combine(Application.persistentDataPath, "Content", "Raw", "Races");
        TextDirectory = Path.Combine(Application.persistentDataPath, "Content", "Raw", "Texts");
        BackgroundDirectory = Path.Combine(Application.persistentDataPath, "Content", "Raw", "Backgrounds");
        StorageDirectory = Application.persistentDataPath;
        Directory.CreateDirectory(SaveDirectory);
        Directory.CreateDirectory(MapDirectory);
        Directory.CreateDirectory(PicDirectory);
        Directory.CreateDirectory(CharDirectory);
        Directory.CreateDirectory(RaceDirectory);
        Directory.CreateDirectory(TextDirectory);
        Directory.CreateDirectory(BackgroundDirectory);
        BackupGenderList = ReadFromFile<GenderList>("Sexes.dat") ?? new GenderList();
        RaceWeights = ReadFromFile<RaceWeights>("RaceWeights.dat") ?? new RaceWeights();
        TraitWeights = ReadFromFile<TraitWeights>("TraitWeights.dat");
        if (TraitWeights == null)
            TraitWeights = new TraitWeights();
        else
            TraitWeights.CatchUp();
        KeyManager = new KeyManager();
        TextLogger = new TextLogger();
        World = new World();
        GameManager?.TileManager?.DrawWorld();
    }

    public static void Save(string filename)
    {
        try
        {
            World.SaveVersion = Version;
            byte[] bytes = SerializationUtility.SerializeValue(World, DataFormat.Binary);
            File.WriteAllBytes(filename, bytes);
        }
        catch (Exception e)
        {
            Debug.LogException(e);
            saveErrors++;
            if (saveErrors < 3)
            {
                GameManager.CreateMessageBox(
                    $"Unable to save properly, {filename} didn't work (will only warn 3 times in a single session)"
                );
            }
            else if (saveErrors == 3)
            {
                GameManager.CreateMessageBox(
                    $"Unable to save properly, {filename} didn't work (will no longer warn you this session)"
                );
            }
        }
    }

    /// <summary>
    /// Writes Object to a file.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="obj"></param>
    /// <param name="filename">Filename only needs the last bit</param>
    public static void WriteToFile<T>(T obj, string filename)
    {
        filename = Path.Combine(StorageDirectory, filename);
        try
        {
            byte[] bytes = SerializationUtility.SerializeValue(obj, DataFormat.Binary);
            File.WriteAllBytes(filename, bytes);
        }
        catch (Exception e)
        {
            Debug.LogException(e);
            saveErrors++;
            if (saveErrors < 3)
            {
                GameManager.CreateMessageBox(
                    $"Unable to save properly, {filename} didn't work (will only warn 3 times in a single session)"
                );
            }
            else if (saveErrors == 3)
            {
                GameManager.CreateMessageBox(
                    $"Unable to save properly, {filename} didn't work (will no longer warn you this session)"
                );
            }
        }
    }

    public static T ReadFromFile<T>(string filename)
    {
        filename = Path.Combine(StorageDirectory, filename);
        if (!File.Exists(filename))
        {
            return default;
        }

        try
        {
            byte[] bytes = File.ReadAllBytes(filename);
            return SerializationUtility.DeserializeValue<T>(bytes, DataFormat.Binary);
        }
        catch (Exception e)
        {
            Debug.LogException(e);
            return default;
        }
    }

    public static World PreviewSave(string filename)
    {
        if (!File.Exists(filename))
        {
            return null;
        }
        World tempWorld;
        try
        {
            byte[] bytes = File.ReadAllBytes(filename);
            tempWorld = SerializationUtility.DeserializeValue<World>(bytes, DataFormat.Binary);
            return tempWorld;
        }
        catch (Exception e)
        {
            Debug.LogException(e);
            return null;
        }
    }

    public static void Load(string filename, bool loadingMap = false)
    {
        TextLogger.WriteOut();
        if (!File.Exists(filename))
        {
            GameManager.CreateMessageBox("Couldn't find the saved file");
            return;
        }
        try
        {
            byte[] bytes = File.ReadAllBytes(filename);
            World = SerializationUtility.DeserializeValue<World>(bytes, DataFormat.Binary);

            if (string.Compare(World.SaveVersion, "5") < 0)
            {
                GameManager.CreateMessageBox(
                    "Can't load that save as it's from before version 5 (there were many changes to the way things were saved, I opted to do a bunch at once so that it's less likely to happen in the future.)"
                );
                return;
            }

            if (string.Compare(World.SaveVersion, "5B") < 0)
            {
                foreach (var person in World.GetPeople(true))
                {
                    person.MyRoom = World.RandomOwnedSquare(person);
                }
            }

            if (string.Compare(World.SaveVersion, "6") < 0)
            {
                World.Settings.MaxSightRange = 15;
                foreach (var person in World.GetPeople(true))
                {
                    if (person.Race.IsNullOrWhitespace())
                        person.Race = "Human";
                }
                World.Settings.EndoPreferredTurns = 80;
            }

            if (string.Compare(World.SaveVersion, "6E") < 0)
            {
                World.RaceWeights = RaceWeights;
                World.Settings.MaxPopulation = 100;
            }

            if (string.Compare(World.SaveVersion, "7") < 0)
            {
                World.TraitWeights = TraitWeights;
                World.Settings.HungerRate = 1;
                World.Settings.OralVoreDifficulty = 1;
                World.Settings.AnalVoreDifficulty = 1;
                World.Settings.CockVoreDifficulty = 1;
                World.Settings.UnbirthDifficulty = 1;
                World.Settings.PublicDisposalRate = 0.2f;
                World.FixZones();
            }

            if (string.Compare(World.SaveVersion, "8A") < 0)
            {
                foreach (var person in World.GetAllPeople(true))
                {
                    if (
                        person.BeingEaten
                        && (person.VoreTracking == null || person.VoreTracking.Any() == false)
                    )
                    {
                        var progress = person.GetMyVoreProgress();
                        if (progress != null)
                        {
                            person.VoreTracking = new List<VoreTrackingRecord>();
                            person.VoreTracking.Add(
                                new VoreTrackingRecord(progress.Actor, progress.Target)
                            );
                        }
                    }
                }
            }

            if (string.Compare(World.SaveVersion, "9A") < 0) //Might have been better to make a general case for this, but just doing the simple clean up for this
            {
                foreach (var person in World.GetAllPeople(true))
                {
                    if (person.Race == "Lamia")
                    {
                        person.PartList.Tags.Add(
                            "Pattern Color",
                            HelperFunctions.GetRandomStringFrom(
                                "blue",
                                "yellow",
                                "green",
                                "orange",
                                "cyan",
                                "red"
                            )
                        );
                    }
                }
            }

            if (string.Compare(World.SaveVersion, Version) != 0)
            {
                foreach (var person in World.GetAllPeople(true))
                {
                    if (person.Personality == null)
                        person.Personality = new Personality();
                    PersonUpdater.Update(World.SaveVersion, person, false);
                }
                World.GenderList?.CheckStatus();
                World.TraitWeights.CatchUp();
                World.Settings.CheckUpdateVersion();
                if (World.ControlledPerson != null)
                    PersonUpdater.Update(World.SaveVersion, World.ControlledPerson, false);
            }

            if (World.Settings == null)
                World.Settings = new Settings();

            if (World.TraitWeights == null)
            {
                World.TraitWeights = TraitWeights;
            }

            GameManager.ClickedPerson = World.ControlledPerson;

            if (World.GetPeople(true).Any() == false && loadingMap == false)
            {
                GameManager.CreateMessageBox(
                    "That save has no people, but would be suitable as loading as a map.  "
                );
                return;
            }

            if (World.TemplateContainer == null)
                World.TemplateContainer = new TemplateController.TemplateContainer();

            World.UpdatePlayerUI();

            ExtraSafetyChecks();

            World.GenderList.CheckStatus();

            PathFinder.Initialized = false;

            GameManager.TileManager.DrawWorld();
            GameManager.CenterCameraOnTile(World.ControlledPerson.Position);
        }
        catch (Exception ex)
        {
            Debug.LogException(ex);
            GameManager.CreateMessageBox("Encountered an error when trying to load the save");
            return;
        }
        GameManager?.RefreshCameraSettings();
    }

    private static void ExtraSafetyChecks()
    {
        if (World.Settings.FriendshipMultiplier == 0)
        {
            World.Settings.FriendshipMultiplier = 1;
            World.Settings.RomanticMultiplier = 1;
            World.Settings.VoreKnowledge = true;
        }

        if (World.GetPeople(true).FirstOrDefault()?.Label == null)
        {
            World.RelabelPeople();
        }

        if (World.Digestions == null)
            World.Digestions = new List<DigestionRecordItem>();

        foreach (var person in World.GetAllPeople(true))
        {
            if (person.ID == 0)
            {
                person.ID = World.NextID;
                World.NextID++;
            }
        }
    }
}
