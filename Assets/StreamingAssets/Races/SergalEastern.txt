Parent Race: Sergal
Selectable: True
Named Character: False

Hair Color, dark green, green, dark gray, black, dark blue, dark brown, brown, blue, indigo, gray-green, gray-purple, gray-blue, gray-brown, dark green, green, dark gray, black, dark blue, dark brown, brown, blue, indigo, gray-green, gray-purple, gray-blue, gray-brown, yellow, gold
//Eastern sergals have darker cooler colors. Some colors are rarer than others
Height, 77, 80
//Eastern sergals tend to be stronger. More muscles = more weight
Weight, 1, 1.5