Parent Race: Fur
Selectable: True
Named Character: False

Hair Color, brown, black, golden, ginger, gray, cream, white
//Some colors are used more than once to more them more likely to be picked
Eye Color, brown, hazel, yellow, gold, orange, dark gray
Shoulder Description, wide, broad, narrow
Hip Description, wide, broad, narrow
Hair Length, Feminine, short, chin level, shoulder length, mid-back length
Hair Length, Masculine, short, medium, long
Hair Style, Feminine, scruffy, wild, neatly groomed, subtle
Hair Style, Masculine, scruffy, wild, neatly groomed, subtle
//Heights in inches because that's what they are internally
Height, Feminine, 54, 74
Height, Masculine, 58, 77
//Generates between 85% and 115% of the 'normal' weight for their height
Weight, .85, 1.15
Breast Size, Feminine, .6, 5
//Masculine breast size is for sexes with breasts but not feminine
Breast Size, Masculine, .2, 3

Dick Size, Masculine, 0, 5
Dick Size, Feminine, 0, 4
//Ball sizes are tied to the dick size, defaults to 85-115%
Ball Size, Masculine, .85, 1.15
Ball Size, Feminine, .85, 1.15

Custom, Coat Color, brown, black, golden, ginger, gray, cream, white